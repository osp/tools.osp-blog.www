from pathlib import Path
import re

pathlist = Path("/home/colm/git/OSP/osp.tools.osp-blog/content").glob('**/*.md')
for path in pathlist:
    path_in_str = str(path)
    data = open(path_in_str, 'r+')
    c = data.read()
    t0 = re.findall(r'Tags: .*\nTags: .*\n', c)
    if len(t0) == 0 :
        continue
    else:
        #print(t0)
        t0 = t0[0]
        t0 = t0.split()
        #print(t0)
        filtered = [ v for v in t0 if not v.startswith('Tags:') ]
        #print(filtered)
        filtered = ' '.join(filtered)
        #print(filtered)
        tagstr = 'Tags: ' + filtered + '\n'
        print(tagstr)
        t1 = re.sub(r'Tags: .*\nTags: .*\n', tagstr, c)
        #print(t1)
        f = open(path, 'w')
        f.write(t1)
        f.close()
