Title: Awkward Gestures
Date: 2008-11-02 20:40
Author: Femke
Tags: Texts, Culture of work, Print Party, Printing + Publishing
Slug: awkward-gestures
Status: published

Out now: **The Mag.net reader 3: Processual Publishing. Actual
Gestures**, edited by Alessandro Ludovico and Nat Muller.

![]({filename}/images/uploads/screenshot-magnet_reader_3.png "screenshot-magnet_reader_3"){: .float }<small>From the introduction:</small>  
"*a radical change is to be detected between the lines: publishing on
paper is not about rigorously selling and distributing content to a
specific target readership. It is more a 'gesture' that creates a space
of intimacy between the publisher/editor and the reader.This space of
intimacy is definitely a 'physical' one*"

Download the publication in PDF or order a paper copy here:
<http://www.moreismore.net/en/magnet-reader-3-processual-publishing-actual>

<div class="clear">

</div>

OSP is included in the chapter *Hacktivist Publishing* with *Awkward
Gestures, designing with Free Software*:

"*While a familiar gesture is one that fits perfectly well in a
generally accepted model, an awkward gesture is a movement that is not
completely synchronic. It’s not a counter-movement, nor a break from the
norm; it doesn’t exist outside of the pattern, nor completely in it.
Just as a moiré effect reveals the presence of a grid, awkward behaviour
can lead to a state of increased awareness; a form of productive
insecurity*"

Download the text here:
<http://ospublish.constantvzw.org/blog/wp-content/uploads/awkward_gestures.pdf>
