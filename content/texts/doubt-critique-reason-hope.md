Title: doubt, critique, reason, hope
Date: 2008-10-07 16:11
Author: Femke
Tags: Texts, History, Tools, Type
Slug: doubt-critique-reason-hope
Status: published

For everyone (like me) who keeps re-reading the 1992 edition of Robin
Kinross' *Modern Typography: an essay in critical history*... his
revised edition</a> (published in 2004) ends in an interestingly
different way\*:

"*The phrase ‘democratization of typography’ has become common,
referring to the wide availability of the tools of production for type
and typographic design. One may take this with some skepticism: after
all, for the majority, the generation and production of these tools is
still largely in the hands of a few corporations — though the open
source software movement may provide an alternative.*  
<!--more-->*If democracy implies a spreading of power to the people,
this is the wrong description of what is going on here: it is more a
simple spreading of typography among the masses. The astonishing
development in this period has not been the contribution of any designer
or writer, but rather the spread of the means of making sophisticated
typography to anyone with a computer. The domain of typography has been
opened up, as never before, and there is a much wider interest in the
activity now than there was even twenty years ago. (...) The great
negative of the modern — irrevocable and disastrous damage to the
natural world — gathers terrifying pace. Enlightenment thus proceeds,
amid much babble and confusion. The watchwords remain: doubt, critique,
reason, hope.*”

(Robin Kinross in: [Modern Typography: an essay in critical
history](http://www.hyphenpress.co.uk/books/978-0-907259-18-3). Hyphen
Press, 2004)

<small>Thank you Dave Crossland for the reminder :-)</small>

\* <small>1992 ending: "*The attempt of this essay has been to point to
the effort of reason that has extended over centuries and which, in
typography, has shown itself in a concern for fundamental issues: the
means by which the processes of production can be controlled; the ways
in which the needs and desires of readers and users can be incorporated
into the shaping of products; the description and ordering of the
activity and its materials. Reflection and discussion are the chief
means by which this process has been effected: thus the 'black art' has
been lifted from its dark, magical origins, into what is sometimes
(though still too rarely) a forum of articulate practice and
constructive criticism. There is some connection between this critical
rationality and an approach to the production of artifacts and their
eventual form. This connection will be left undefined here: it is open
for discussion and exploration.*"</small>
