Title: Play!
Date: 2007-10-17 09:22
Author: Femke
Tags: Texts, Reading list, Retrospective Reading
Slug: retrospective-reading-play
Status: published

"It may well turn out that one of the most important effects of open
source’s success will be to teach us that play is the most economically
efficient mode of creative work." <small>(Eric S. Raymond, postscript
(2000) to *The Cathedral and the Bazaar*)</small>

<!--more-->  
***The Cathedral and the Bazaar*** is probably one of the most quotable
texts about Open Source you could find. Raymond confidently explains why
a distributed, decentralized and seemingly chaotic model of software
development (The Bazaar) actually 'works'. This essay, which was
originally published in 1997, coined aphorisms such as '*Given enough
eyeballs, all bugs are shallow*' and '*Every good work of software
starts by scratching a developer's personal itch*'. He does not paint a
very exciting image for non-hacker-use but we'll get back to this later.

<http://www.catb.org/~esr/>

Full quote:

> Rather, I want to suggest what may be a wider lesson about software,
> (and probably about every kind of creative or professional work).
> Human beings generally take pleasure in a task when it falls in a sort
> of optimal-challenge zone; not so easy as to be boring, not too hard
> to achieve. A happy programmer is one who is neither underutilized nor
> weighed down with ill-formulated goals and stressful process friction.
> Enjoyment predicts efficiency.
>
> Relating to your own work process with fear and loathing (even in the
> displaced, ironic way suggested by hanging up Dilbert cartoons) should
> therefore be regarded in itself as a sign that the process has failed.
> Joy, humor, and playfulness are indeed assets; it was not mainly for
> the alliteration that I wrote of "happy hordes" above, and it is no
> mere joke that the Linux mascot is a cuddly, neotenous penguin.
>
> It may well turn out that one of the most important effects of open
> source’s success will be to teach us that play is the most
> economically efficient mode of creative work.
