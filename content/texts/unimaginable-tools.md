Title: Unimaginable tools
Date: 2009-01-23 12:06
Author: Femke
Tags: Texts, Retrospective Reading, Thoughts + ideas, Tools
Slug: unimaginable-tools
Status: published

[![ol5196998m-m1]({filename}/images/uploads/ol5196998m-m1.jpg "ol5196998m-m1"){: .float }](http://openlibrary.org/b/OL5196998M)"*Only
rarely, if indeed ever, are a tool and an altogether original job it is
to do, invented together. Tools as symbols, however, invite their
imaginative displacements into other than their original contexts. In
their new frames of reference, that is, as new symbols in an already
established imaginative calculus, they may themselves be transformed,
and may even transform the originally prescriptive calculus. These
transformations may, in turn, create entirely new problems which then
engender the invention of hitherto literally unimaginable tools*"

<div class="clear">

</div>

<small>[Joseph Weizenbaum. Computer power and human reason: from
judgment to calculation. MIT,
1976](http://openlibrary.org/b/OL5196998M)</small>
