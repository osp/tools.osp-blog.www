Title: William Morris: Art and Its Producers
Date: 2007-10-27 19:25
Author: Femke
Tags: Texts, Reading list, Retrospective Reading
Slug: william-morris-art-and-its-producers
Status: published

![]({filename}/images/uploads/morris.gif "William Morris, Poet, Artist, Socialist. Springfield Republican, 11 Oct. 1896: 13"){: .float}While
looking for designers writing about their relation to tools, I
discovered the excellent [William Morris
Archives](http://www.marxists.org/archive/morris/works/index.htm), part
of the [Marxist writers' Internet
Archive](http://www.marxists.org/archive/index.htm). To Morris, to own
his means of production, was the only way a designer/workman could find
back pleasure in work, and this in turn he considered a prerequisite for
the production of (applied) art and beauty. It is hard to imagine
someone as keen on handicraft as William Morris in the age of computing,
but after reading his text [Art and its
producers](http://www.marxists.org/archive/morris/works/1888/producer.htm),
I wonder how he would have felt about using Free Software?

> 'I do not believe we should aim at abolishing all machinery; I would
> do some things with machinery which are now done by hand, and other
> things by hand which are now done by machinery; in short, we would be
> the masters of our machines and not their slaves, as we are now. It is
> not this or that... machine which we want to get rid of, but the great
> intangible machine of commercial tyranny which oppresses the lives of
> all of us'

<small>(William Morris: Art and Its Producers, 1881)</small>
