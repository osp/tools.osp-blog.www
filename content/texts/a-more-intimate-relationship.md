Title: A more intimate relationship
Date: 2008-02-20 19:42
Author: Femke
Tags: Texts, Reading list
Slug: a-more-intimate-relationship
Status: published

David Reinfurt's essay **[Making do and getting
by](http://www.adobe.com/designcenter/thinktank/makingdo)** departs from
the work of Muriel Cooper and Anthony Froshaug, and relates their
critically engaged practice to contemporary projects such as Juerg
Lehni's [Scriptographer](http://www.scriptographer.com). In this way, he
convincingly shows how designers can and should reclaim a more intimate
relationship with the digital production of their work. Although he
remains quite vague about what this could really mean; sticks to a
plugin architecture for Adobe software and uses MacOSX as example of an
Open Source Project... his text contains many inspiring examples and
interesting points of departure.

> "Individually, we can commit to using software critically, engage the
> mechanics of production, ask questions, use old software, share, write
> new software and refuse to passively consume the latest innovations.
> Like Muriel, Anthony, Will and Hektor have suggested, this might
> actually lead us back — closer to our work."

<http://www.adobe.com/designcenter/thinktank/makingdo/>

<small>Yes, indeed: published on the website of Adobe's *Design Center
Think Tank* ;-)</small>
