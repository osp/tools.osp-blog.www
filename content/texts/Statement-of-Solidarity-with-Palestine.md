Title: 'Artists and cultural workers in Belgium: Statement of Solidarity with Palestine'
Date: 2023-10-21
Author: OSP
Tags: Palestine, Solidarity
Slug: statement-of-solidarity-with-palestine
Status: published

OSP responds to the [Call for action for artistic and cultural organisations and institutions](https://docs.google.com/forms/d/e/1FAIpQLScPCxLZE0oSMFKWlQ31AehO26cbrAixlLC-4w0HQ5mIxKISOQ/viewform?fbclid=PAAaYZ7qx5vzlemrVyhQwgzyiKcTKabmsy0i4mHrbRXmVFlL-o6vJV0K7urxc_aem_ASNQzSpMLrAG-3oxaXm1E0WT0wJL1RHJZmUQr3VmG9s6hdFSMw8E3HkUGi6hGDlz-6I)

As cultural workers committed to opposing all forms of racism and colonial violence, 
we strongly and unambiguously support the Palestinian population in their struggle for freedom.

![Open-source designs 'Stop Genocide' by Atelier Brenda displaying Palestinian flag and the text Stop the genocide](http://blog.osp.kitchen/images/uploads/palestine/Stop_Genocide_Free_Palestine-09.png)

The call asks for symbolic and tangible actions that we are and will be undertaking in solidarity with the Palestinian people in this time of genocide.

As designers we have no audience per say but a network of collaborators and platforms accumulated over years of common projects.
We believe that those platforms and communities can be political tools.
We contacted our collaborators and relayed this call to such platforms with an intention of mutual responsabilisation as an action of support.

We invite the groups for which we build design to use their platforms according to this call.
We hope that it can help opening up spaces for: spreading information, dissusions and symbolic or material actions within the Brussels cultural sector.

We invite other design studio to do the same, and think about their position as builders of identities and visibility in time where unambiguous political positionning can impact the public opinion and the position and action of states.

OSP endorses the BDS movement as formulated by [BACBI](https://www.bacbi.be/cult/Home-bacbi)

In addition to the important links shared in in the call, we add:

* [Open-source designs 'Stop Genocide' by Atelier Brenda](https://www.dropbox.com/sh/pupxap5sisr8i25/AAD40OQ1FNLPXJ9JMmty1n5ya?dl=0)
* [Banners workshop at Shik Shak Shok solidarity edition](https://www.facebook.com/events/846685030412813/)

This declaration was inspired by [one from Varia](https://varia.zone/en/solidarity-with-palestine.html) and others.