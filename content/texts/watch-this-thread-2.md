Title: Watch this thread: Free Font Manifesto
Date: 2006-11-25 02:14
Author: Femke
Tags: Texts, Libre Fonts, Watch this thread
Slug: watch-this-thread-2
Status: published

Ellen Lupton's question: **[Why would a typeface designer want to give a
font
away?](http://www.blogger.com/comment.g?blogID=34853894&postID=115909195561855648)**
sparked off a series of comments worth reading. The discussion shows how
much typographers struggle with 'open source' as an idea. It makes you
wonder why Lupton decided to ask 'to give away a font' and not 'to share
source' - the latter probably fits the typographic spirit better. A few
samples:

> "(...) this movement you start up has the potential to make look bad
> and selfish the designers who wish not to participate, possibly
> because they are independents and just can't afford to give away
> months, sometimes years of hard labour. I don't want to sound
> pedantic, but I think your initiative could use a fair amount of
> discretion, because this possibly has already done harm to the type
> community without you even realising it."

> "To suggest that the world would be better if Latin "graphic
> designers" had more free fonts to choose from not only makes it seem
> like misers are in control here, but it also makes it clear that good
> thinkers do not exist out there."

> "The whole P2P community is breeding a generation of lazy idiots who
> think they can get anything for free. May it be music or typefaces.
> Why buy good type, when you can get it for free."

but also:

> "To say that an "Open Source" font initiative would put designers out
> of business, or dilute the value of legitimate 'commercial' fonts is
> simply trying to lock the barn after the horses have long since left.
> Where have they been over the past twenty years?"

Read full thread
[here](http://www.blogger.com/comment.g?blogID=34853894&postID=115909195561855648)
