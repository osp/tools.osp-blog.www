Title: Technobohemians or the new Cybertariat?
Date: 2007-04-16 12:46
Author: Femke
Tags: Texts, Culture of work, Reading list
Slug: technobohemians-or-the-new-cybertariat
Status: published

![gill.jpg]({filename}/images/uploads/gill.jpg){: .float}
[The Institute of Network
Cultures](http://www.networkcultures.org/networknotebooks/) published an
insightful study on the culture of new media work by Sociologist
[Rosalind
Gill](http://www.lse.ac.uk/collections/genderInstitute/whosWho/profiles/gill.htm).
***Technobohemians or the new Cybertariat? New media work in Amsterdam a
decade after the web***, is based on 40 'semi-structured' interviews
with practitioners (designers, developers, artists, information
architects, ...). Her study reveals the often precarious situations web
workers find themselves in, but most of all her analysis and critique of
their (our?) egalitarian self-image, makes it a Must-Read.

> "It is striking to see that two of the three people who commented on
> this issue (racism - FS) were themselves immigrants to The
> Netherlands; it simply did not seem to be visible or worthy of comment
> to others of our participants - a tendency which tells us a great deal
> about the normalization and power of whiteness."

[Download the
publication](http://www.networkcultures.org/_uploads/17.pdf)  
Lecture Rosalind Gill
<http://www.decadeofwebdesign.org/videos/day_2/encoded/1mbs/22_gill230205.mp4>
