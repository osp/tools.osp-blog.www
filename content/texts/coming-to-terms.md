Title: Coming to terms
Date: 2010-08-05 10:08
Author: Femke
Tags: Texts, Terminology
Slug: coming-to-terms
Status: published

"*If everything is both neutral and imbued with values at the same time,
how can we separate instrumentality from ideology? This is essentially
what I take the distinction between Free and Open to be about. Free is
an ideological standpoint, the idea that users of software should have
the right to look under the hood, to know exactly what their software is
doing and to make changes to it, should they so choose. Free is about
freedom, which is an admirable thing. Free takes on the idea that
freedom can be built into code and its licensing schemes. Open, on the
other hand, speaks to the instrumental. It speaks to the idea that
companies don't want to put the word Free on their products, for fear
that people will fail to make the distinction between freedom and
monetary freeness, as they do.*"

ginger coons coming to terms with Open, Free and Libre. Apparently
[something we all go
through](http://ospublish.constantvzw.org/tag/terminology), but hers is
eloquent and frank like we know her:
<http://www.adaptstudio.ca/blog/2010/08/coming-to-grips-with-my-own-opinions-about-open-libre-and-free.html>
