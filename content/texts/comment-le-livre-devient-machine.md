Title: Comment le livre devient machine
Date: 2011-09-06 12:28
Author: Pierre
Tags: Texts, book
Slug: comment-le-livre-devient-machine
Status: published

[![]({filename}/images/uploads/bookapp.png "bookapp"){: .alignnone .size-medium .wp-image-6716 }]({filename}/images/uploads/bookapp.png)

L'entrepreneur et amateur de texte justifié Kaplan nous livre [une
articulation des futurs du
livre](http://www.slideshare.net/frederickaplan/le-devenir-machinique-du-livre)
efficace comme son tunnel Powerpoint, voie A et b. [Le blog
Lafeuille](http://lafeuille.blog.lemonde.fr/2011/09/05/le-livre-machinique)
reformule et déploie (2 formats donc). Les torsions qui appuient sur le
savoir (à l'esprit dirait peut-être
[Stiegler](http://arsindustrialis.org/)) quand le livre est capturé dans
une application sur une machine fermée restent à creuser, ce qu'ils ne
font encore que peu, sans doute pour des raisons différentes.
