Title: Maternal Politics
Date: 2006-09-10 22:26
Author: Femke
Tags: Texts, Constant Verlag
Slug: constant-verlag-maternal-politics
Status: published

[![maternal1.jpg]({filename}/images/uploads/maternal1.jpg){: #image125}]({filename}/images/uploads/maternal1.jpg "maternal1.jpg"){: #image125}  
**Maternal Politics**, Irina Aristarkhova  
Text available on line:  
PDF lay-out:
[maternal\_politicsb.pdf]({filename}/images/uploads/maternal_politicsb.pdf){: #image125}  
License:  
Date of publishing: 03-06-2006 (Digitales)
