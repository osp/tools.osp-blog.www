Title: Asynchronous live blogging
Date: 2008-05-10 07:38
Author: Ivan
Tags: Texts, Tools, LGM 2008
Slug: asynchronous-live-blogging
Status: published

[![]({filename}/images/uploads/babl-a4poster.png "babl-a4poster"){: .alignnone .size-medium .wp-image-489 }]({filename}/images/uploads/babl-a4poster.png)

These past few days I've been navigating a sea of acronyms, neologisms
and tiny iconic metaphors here at LGM. The thing that I can't get out of
my head is the tool that [pippin](http://pippin.gimp.org) of GIMP used
for his talk. At first it looked like a PowerPoint clone, but then he
started correcting it on-the-fly by tweaking C code. I got the
impression that it was a custom tool that he was using to showcase the
library he was talking about. \`got the impression' because, to be
honest my mind went blank ten minutes or so into it. Anyway... it was
about GEGL, and GEGL is \`\`the next generation core of GIMP.'' I don't
know if I regretted not getting into the talk at a technical level. It
was great to see so much code fly across the screen with such
proficiency, and it was a great privilege to see one of the GIMP
developers at work. The
[fish](http://www.gegl.org/babl/graphics/babl-48x48.png) that he
occassionally sent swimming across the screen was also nice.
