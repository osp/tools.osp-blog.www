#!/usr/bin/env perl
use strict;

my $pubName = "copy";
my $people = 5;
mkdir "temporary_directory_for_work", 0755 or warn "cannot start 19 steps: $!";

my $person;
foreach $person (1..$people) {
    chdir "temporary_directory_for_work";
    my $copyNumber = sprintf "%03d", $person;
    system("pdftops -paper match ../$pubName$copyNumber.pdf $copyNumber.ps");
    system("psbook -s16 $copyNumber.ps >| $copyNumber" . "a.ps");
    system("psnup -2 -PA5 $copyNumber" . "a.ps >| $copyNumber" . "b.ps");
    system("ps2pdf $copyNumber" . "b.ps ../booklet$copyNumber" . ".pdf");
    chdir "..";
    system("lpr -o page-set=even -#1 booklet$copyNumber" . ".pdf");
}

unlink glob "temporary_directory_for_work/* temporary_directory_for_work/.*";
rmdir "temporary_directory_for_work";
