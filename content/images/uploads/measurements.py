#the script will start inserting boxes at page
start = 1
#the script will stop inserting boxes at page
end = 22

#the definition of text boxes follows the format:
#[x-position, y-position, width, height]
#x and y positions refer to top-left corner of boxes.
#units are based on the default set for the document.

#text boxes needed in even pages. edit as needed.
even = [[0, 0, 100, 100], [100, 100, 100, 100]]

#text boxes needed in odd pages. edit as needed.
odd = [[0, 100, 100, 100], [100, 0, 100, 100]]

#will the text boxes be linked? (yes or no)
link = 'yes'
#link = 'no'
