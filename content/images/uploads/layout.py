from scribus import *
import random

theSizes = [11, 16]
#theSizes =[17]
charactersInText = 1302
theFrames = ['Text3', 'Text4', 'Text5', 'Text6', 'Text7', 'Text8', 'Text9', 'Text10', 'Text11']
people = 5
someMath = 10.0 / people

def aCoverProposal(ejemplar):
    coverChars = getTextLength('coverFrame')
    someMath2 = float(ejemplar + 2) / coverChars
    print someMath2
    for position in range(coverChars):
        selectText(position, 1, 'coverFrame')
        pointSizeCover = 32 + (position * someMath2)
        print pointSizeCover
        setFontSize(pointSizeCover, 'coverFrame')

def alter(frameName, charsThatChange, pointSize):
    frameLength = getTextLength(frameName)
    positionsHere = []
    for choose in range(charsThatChange):
        positionsHere.append(random.randint(0, frameLength-1))
    for position in positionsHere:
        selectText(position, 1, frameName)
        setFontSize(pointSize, frameName)

def pdF(ejemplar):
    pdFobject = PDFfile()
    booklet = "%03d" % (ejemplar + 1)
    pdFobject.file = "copy" + booklet + ".pdf"
    pdFobject.save()

def revert(frameName):
    frameLength = getTextLength(frameName)
    selectText(0, 0, frameName)
    selectText(0, frameLength, frameName)
    setFontSize(11, frameName)

############theGist
ticket = 1
for aCopy in range(people):
    aCoverProposal(aCopy)
    for aFrame in theFrames:
        aSize = 11 + (aCopy * someMath)
        alter(aFrame, aCopy + 1, aSize)
    setText(str(ticket), 'ticketFrame')
    pdF(aCopy)
    for aFrame in theFrames:
        revert(aFrame)
    ticket = ticket + 1
############theGist
