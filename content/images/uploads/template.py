#!/usr/bin/env python
# -*- coding: utf-8 -*-
#In order that this script run, a document must already be open

""" Create text frames on all pages """

import sys
from measurements import *

try:
	from scribus import *
except ImportError:
	print "This script only runs from within Scribus."
	sys.exit(1)

if haveDoc():
	#according to measurements.py, per page there will be:
	NumberEvenFrames = len(even)
	NumberOddFrames = len(odd)

	#the frames from even pages will be held by the EvenFrames dict
	#likewise the frames from odd pages
	EvenFrames = {}
	OddFrames = {}
	for i in range(0, NumberEvenFrames):
		EvenFrames[i] = {}
	for i in range(0, NumberOddFrames):
		OddFrames[i] = {}

	#the following loop creates the text frames
	for j in range(start, end+1):
		if j % 2 == 0:
			try:
				gotoPage(j)
				for i in range(0, NumberEvenFrames):
					EvenFrames[i][j] =\
					createText(even[i][0],\
					even[i][1], even[i][2], even[i][3])
					lockObject(EvenFrames[i][j])
			except  IndexError :
				break
		else:
			try:
				gotoPage(j)
				for i in range(0, NumberOddFrames):
					OddFrames[i][j] =\
					createText(odd[i][0],\
					odd[i][1], odd[i][2], odd[i][3])
					lockObject(OddFrames[i][j])
			except  IndexError :
				break
	if link == 'yes':

		#the following loop links the text frames, if needed
		for j in range(start, end-1):
			if j % 2 == 0:
				for i in range(0, NumberEvenFrames):
					linkTextFrames(EvenFrames[i][j],\
					EvenFrames[i][j+2])
			else:
				for i in range(0, NumberOddFrames):
					linkTextFrames(OddFrames[i][j],\
					OddFrames[i][j+2])
else:
        messageBox(TITLE, "First open a scribus document", ICON_WARNING) 