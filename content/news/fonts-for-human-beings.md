Title: Fonts for human beings
Date: 2006-03-01 21:34
Author: nicolas
Tags: News, Type, Libre Fonts
Slug: fonts-for-human-beings
Status: published

![Ubuntu-title]({filename}/images/uploads/2006/03/ubuntu-font.png)

The "user-friendly" distribution Ubuntu is distributing its font under
the LGPL. I have not heard the same from other distributions. It seems
that Redhat or Novell do not want to share their corporate identity with
the rest of us. Ubuntu's founder Mark Shuttleworth, who sold his company
Thawte(a company that issues security certificates) to become the first
African citizen in space, supports free software. He understands the
free in *free software* as in *freedom* but also as in *free beer* since
the distribution is supposed to be always *free of charge*.

> The team behind Ubuntu makes the following public commitment to its
> users:
>
> \*Ubuntu will always be free of charge, and there is no extra fee for
> the "enterprise edition", we make our very best work available to
> everyone on the same Free terms.  
> \*Ubuntu comes with full commercial support from hundreds of companies
> around the world. Ubuntu is released regularly and predictably; a new
> release is made every six months. Each release is supported with free
> security updates and fixes for at least 18 months.  
> \*Ubuntu will include the very best in translations and accessibility
> infrastructure that the Free Software community has to offer, to make
> Ubuntu usable by as many people as possible. We collaborate as widely
> as possible on bug fixing and code sharing.  
> \*Ubuntu is entirely committed to the principles of free software
> development; we encourage people to use free and open source software,
> improve it and pass it on.

Although the [community
marketing](http://www.ubuntu.com/include/circle-510.png) of Ubuntu is
sometimes getting on my nerves, a panorama of the tools for open
publishing would be incomplete without
[Ubuntu-title.ttf](http://debuntu.free.fr/share/ubuntu-title.ttf).
