Title: OSP-summit
Date: 2010-10-10 17:31
Author: OSP
Tags: News, future, Thoughts + ideas
Slug: osp-summit
Status: published

[![Heure-en-famenne](http://ospublish.constantvzw.org/image/plog-content/thumbs/snapshots/heure-en-famenne/large/1571-p1070837.jpg)](http://ospublish.constantvzw.org/image/index.php?level=album&id=46)

Last Thursday and Friday OSP gathered in
[Heure-en-Famenne](http://www.openstreetmap.org/?lat=50.2729&lon=5.3826&zoom=14&layers=M)
to discuss future plans and current projects. After almost 5 years of
work we wanted to take a bit of time to look back and forward. We made
several resolutions and decisions. Some will be already visible in ~~the
coming weeks~~ now, others only in the years to come.
