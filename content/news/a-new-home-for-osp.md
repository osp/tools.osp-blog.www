Title: A new home for OSP
Date: 2015-10-06 17:41
Author: Eric
Tags: News
Slug: a-new-home-for-osp
Status: published

[![osp-crop-wtc]({filename}/images/uploads/osp-crop-wtc.jpg){: : .alignnone .size-medium .wp-image-7426 }]({filename}/images/uploads/osp-crop-wtc.jpg)

In what might be a some-what surprising move (even to ourselves) OSP has
taken up offices in the World Trade Center.

Do not hesitate to pass by! Send us [a mail](){: : .alignnone .size-medium .wp-image-7426 }, and we will
meet you in the lobby! Or write us a letter at:

<address>
Avenue Roi Albert II / Koning Albert II laan 30, Box n°8  
World Trade Center - Tower 1  
1000 Brussels</adress>

</p>
