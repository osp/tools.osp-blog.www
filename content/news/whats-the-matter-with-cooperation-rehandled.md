Title: What’s the Matter with Cooperation @ BUDA
Date: 2016-05-27 10:36
Author: Eric
Tags: News
Slug: whats-the-matter-with-cooperation
Status: published

[![Print Party What’s the Matter with Cooperation
BUDA]({filename}/images/uploads/IMG_1046.jpeg)]({filename}/images/uploads/IMG_1046.jpeg)

*What can we learn about our current society by looking at the
increasing production of art works that are made in cooperation with
their audience? And in reverse, how does the general raise of
cooperative activism in the society influence the arts?*

The art centre BUDA, located in the West Flemish city of Kortrijk,
organised a three day festival around these questions: ‘What’s the
matter with cooperation’. Reclaiming the idea of participation, people
inside and outside the Arts do believe that one can take part in shaping
the society. From the ‘emancipated spectator’ to the ‘empowered
citizen’, [BUDA was filled during 3
days](http://www.budakortrijk.be/files/events/WMWC_OK_lowres.pdf) with
performances, lectures and co-learning situations that all implicitly
and explicitly dealt with questions of
cooperation.

[![IMG\_20160428\_130300-s]({filename}/images/uploads/IMG_20160428_130300-s.png)]({filename}/images/uploads/IMG_20160428_130300-s.png)

*Plotted instructions for the participants*

OSP joined the festival to create in cooperation a publication, using
the
[Ethertoff](http://osp.kitchen/tools/ethertoff/ "OSP (Open Source Publishing) → you’re traveling in ethertoff")
platform. We concentrated on the theoretical section of the festival: a
series of lectures taking place in a makeshift forum. BUDA had
constructed this forum space in its entrance hall especially for the
event. OSP had setup a working station here. We projected the ethertoff
install right next to where the lectures slides were projected, to lure
attention to the tool and show there was activity there. As usual with
Ethertoff, we designed live while the content was still being
developed.

[![ethertoff-buda-screenshot]({filename}/images/uploads/ethertoff-buda-screenshot.png)]({filename}/images/uploads/ethertoff-buda-screenshot.png)

*Screenshot of the ethertoff platform while editing was in progress*

Ethertoff was first used in a similar context, to document the summer
school Relearn. A marked difference this time was that a number of
lecturers already provided a text beforehand, that we had already pasted
into Ethertoff. In this way, the various editors that participated did
not need to concentrate on just paraphrasing the lectures, but could
spend their time annotating the lectures. Every morning started with a
short workshop on the platform: how the etherpad collaborative works,
the markdown syntax. But also to create a common understanding of how to
treat the text, a common editing protocol.

The participants—most of them active in the milieu of the performing
arts—where remarkably enthousiastic to participate in this editing
process. We think this might have to do with the nature of performing
arts, in which makers are always looking for new ways to deal with the
ephemeral processes on which they work. To collaboratively create traces
of a shared event makes sense more so then for the general public.

At the closing of the festival we organised a print party, where the
participants eagerly took the (still laser-hot) papers out of Sarahs
hands as she exited the elevator with the final chapters: we had
underestimated the printing time. Everybody could learn how to use the
ring binding system, and then collate and bind their own copy. We had
created a stack of paper for each lecture, with the idea that one could
pick and choose—it turned out everybody took everything.

[![Print Party What’s the Matter with Cooperation
BUDA]({filename}/images/uploads/IMG_0972.jpeg)]({filename}/images/uploads/IMG_0972.jpeg)

[![Print Party What’s the Matter with Cooperation
BUDA]({filename}/images/uploads/IMG_0998.jpeg)]({filename}/images/uploads/IMG_0998.jpeg)

[![Publication What’s the Matter with Cooperation
BUDA]({filename}/images/uploads/IMG_1038.jpeg)]({filename}/images/uploads/IMG_1038.jpeg)

You can [download the
PDF]({filename}/images/uploads/wtmwc.pdf).
There’s also a [version without the coloured
backgrounds]({filename}/images/uploads/wtmwc.white_.pdf),
so you can print on coloured paper yourself.

A note on tooling: we installed Etherpad & -toff on a Raspberry Pi. It
was a tiny bit slow, but very stable. We plugged it in to BUDA’s
existing network infrastructure. This means the Ethertoff platform was
not accessible from the general internet—only from inside BUDA. It’s
great to have this self-contained unit, and not to be able to access the
tools when you leave the space. Both for the notetakers and for us. It
helps to combat a problem with the workflow: because it is a very free
collaboration model, participants can always think: someone will do it
later. Clear time constraints really help then. It also answers a
question we always have after the event: who takes care of the website?
We made it clear from the beginning that the tool would away at the end
of the event with the unplugging of the Raspberry. What we did do, is
create a HTML archive, next to the PDF—just like with the [Relearn 2013
website](http://relearn.be/2013/ "Relearn → applications").

[![Publication What’s the Matter with Cooperation
BUDA]({filename}/images/uploads/IMG_1025.jpeg)]({filename}/images/uploads/IMG_1025.jpeg)
