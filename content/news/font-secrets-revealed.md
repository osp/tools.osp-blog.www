Title: Font Secrets Revealed
Date: 2010-03-31 18:08
Author: OSP
Tags: News, Libre Fonts, Tools, typography
Slug: font-secrets-revealed
Status: published

<p>
<center>
[![]({filename}/images/uploads/secrets.png "secrets"){: .alignright .size-full .wp-image-4368 }]({filename}/images/uploads/secrets.png)  
  
  
You're welcome for a series of talks by [Dave Crossland</a<br>
**Thursday, April 1 at 10:00, 13:00 and 17:00**  
Address: Ecole de Recherche Graphique (ERG), Rue du Page 87, 1050
Bruxelles  
](http://understandinglimited.com/)[2010-04-01\_dave\_crossland\_poster\_2]({filename}/images/uploads/2010-04-01_dave_crossland_poster_2.pdf)
</center>
</p>

