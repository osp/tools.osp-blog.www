Title: Keep Your Fingers Crossed
Date: 2009-09-15 20:29
Author: OSP
Tags: News, LGM 2010
Slug: keep-your-fingers-crossed
Status: published

[![P1060690]({filename}/images/uploads/P1060690-75x75.jpg "P1060690"){: .float width="75" height="75"}]({filename}/images/uploads/P1060690.JPG)←
This letter proves that we just handed in a dossier at the Ministry of
Culture, Youth, Sport and Brussels (Vlaamse Gemeenschap). With Constant
colleague [Peter Westenberg](http://videomagazijn.org/), OSP wrote an
application to ask for support for the Libre Graphics Meeting 2010.
There is only a *very* small chance that it will make it (LGM is sort of
hors catégorie) but wouldn't it be nice if the committee 'Architectuur
en Vormgeving' also thinks that LGM is relevant?

Full dossier (in Dutch, 25 MB):
<http://ospublish.constantvzw.org/documents/LGM2010/LGM2010VG.pdf>
