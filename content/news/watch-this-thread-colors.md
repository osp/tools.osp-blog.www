Title: Watch this thread: The color of ideas
Date: 2007-01-30 03:21
Author: Femke
Tags: News, Tools, Printing + Publishing, Watch this thread
Slug: watch-this-thread-colors
Status: published

[![pantone.jpg]({filename}/images/uploads/pantone.jpg){: #image178}]({filename}/images/uploads/pantone.jpg "pantone.jpg"){: #image178}  
A widely used proprietary color-system such as Pantone, obviously raises
questions for Open Source graphic tools. Gregory Pittman writes:

> \* Obviously, no one, including Pantone, can copyright a color, and
> especially in these days where the RGB/CMYK color systems are freely
> usable -- ie, you can't put a claim on RGB color "ef9824".  
> \* They **can** copyright the names and the connection with their
> inks.  
> \* They want to control the ability of anyone to connect some other
> color system to Pantone names or inks. My guess is about all they can
> really do is attempt to keep you from using their color/ink name, as
> in "this matches Pantone Keepsake Lilac or Pantone 15-2705" (their
> current color of the day). Understandably they don't want someone else
> feeding off the system they have created.  
> \* *What it really begs for is someone to establish another system
> (open of course) with its own names which might in some way link up to
> Pantone and other inks, with the attached disclaimer that no promise
> is made that this product exactly matches any proprietary color or
> ink.*

[Read full thread
here](http://nashi.altmuehlnet.de/pipermail/scribus/2007-January/022187.html)
