Title: "instable", mais actuellement utilisée
Date: 2006-06-02 23:37
Author: Femke
Tags: News, Scribus
Slug: le-tigre-proudly-powered-by-scribus
Status: published

[![scribus
article](http://ospublish.constantvzw.org/blog/wp-content/_scribus.jpg "scribus article"){: }](http://ospublish.constantvzw.org/blog/wp-content/scribus.jpg)  
The French weekly *Le Tigre* announces:

> This week, we have an article about free softwares in "Le Tigre":  
> <http://www.le-tigre.net/No11.html>  
> There is a "encadré" about Scribus. You can see it there:  
> [http://rdereel.free.fr/letigre/011/scribus.jpg  
> ](http://rdereel.free.fr/letigre/011/scribus.jpg)
