Title: Who's Afraid of Adobe? - Not me, says the Mozilla foundation.
Date: 2006-12-09 19:43
Author: nicolas
Tags: News, Standards + Formats
Slug: whos-afraid-of-adobe-not-me-says-the-mozilla-foundation
Status: published

As we [already mentioned](http://ospublish.constantvzw.org/?page_id=2)
in this blog, Adobe owns many of the (proprietary) tools used by
designers nowadays: Indesign, Photoshop, Illustrator and after having
acquired Macromedia, it also owns Dreamweaver, Flash, Director, etc.
Monopoly rhymes with monoculture.

![End of life for the SVG
player]({filename}/images/uploads/endoflife.png){: #image160}

However, Adobe has not always been the enemy of free formats. A recent
example being the [viewer for
SVG](http://www.adobe.com/svg/viewer/install/) they released when Flash
was still a product from a competitor.
[Logically](http://hritcu.wordpress.com/2005/04/20/will-adobe-still-support-svg/)
the support for the SVG viewer is now discontinued, Flash having become
an asset of the company. [End of
life](http://www.adobe.com/svg/eol.html).

<!--more-->

A complementary explanation could be that the SVG format is also read
natively within a major open-source browser, firefox. Who needs a
[plugin
anymore](http://www.mail-archive.com/svg-developers@yahoogroups.com/msg10022.html)?  
If we applaud the development of the SVG support within firefox, we
regret that the softwares that have been developed specifically for the
[functionnalities](http://www.mail-archive.com/svg-developers@yahoogroups.com/msg10011.html)
added to the Adobe's SVG plugin may end with the software itself or will
need to be rewritten. As the source code for the SVG player has not been
published under a free license, we end up in a paradox: the programmers
that wrote code for the functionalities specific to the Adobe's player
chose SVG because it was free and open, but the player was itself a
black box. And now, part of this code will be locked in this box because
the same functionalities are not present in the firefox implementation.
The Adobe viewer will be removed from the download area of adobe.com and
the license doesn't allow for redistribution.

Why wouldn't Adobe donate the code of its SVG player to firefox?
Wouldn't it be better than to simply drop it? An alliance between
proprietary software giant and one of the open source biggest
achievement may seem odd. But it is already happening for another
product and it makes the news. Adobe announced it would donate the code
of its javascript engine, under the name
[Tamarin](http://www.mozilla.org/projects/tamarin/faq.html), to the
Mozilla foundation that is behind firefox. This reflects the renewed
interest of the company for the open source software. What Cnet.com
calls a "[quiet effort to become more involved with desktop
Linux](http://news.com.com/Adobe+dipping+toes+into+desktop+Linux+waters/2100-7344_3-5435397.html)".

Contrarily to what has been said here and there, Adobe is not giving the
code for the flash player to firefox but an important component that
will help the open source browser to interpret more efficiently the
javascript code and therefore boost the Ajax development.

From [JD on
EP](http://weblogs.macromedia.com/jd/archives/2006/11/tamarin_comment.cfm),
a series of interesting comments:

> Frank Hecker (Mozilla staffer) offers a great orientation to the Adobe
> engine and the collaboration, particularly oriented to those in the
> Mozilla community. "Note that Tamarin is not an open source version of
> the Flash player; it is simply the virtual machine embedded within
> Flash Player 9, and does not include all the other components that
> make up Flash (including the bits that display graphics and play music
> and video). Adobe will continue to develop and distribute the Flash
> player on its own as a product separate from Firefox itself... The
> current SpiderMonkey JavaScript engine (used in Firefox, etc.) will
> not be replaced, as it does more than just provide a virtual machine;
> rather the Tamarin code will be integrated into SpiderMonkey. On
> compilers, the current SpiderMonkey engine can convert JavaScript to
> byte code, but does not have the ability to convert byte code to
> native machine instructions; this is a major feature that Tamarin
> provides... Not only do we gain an important new piece of technology
> that's critical to our products, we and Adobe both gain the benefit of
> being able to more closely work together on ECMAScript language
> technology and avoid unnecessary duplication of efforts; this in turn
> will allow both the Mozilla project and Adobe to put more resources
> into other areas important for future innovations." \[via Mike
> Potter\]

To summarize:

> So this has nothing to do with putting Flash into Firefox. Firefox
> users will still require the Flash plugin to run SWFs. But
> contributing a high-performance virtual machine for a type-checked,
> object-oriented language is still a big deal!

> "AJAX in Flash, with a Web 2.0 hype engine. May god have mercy on us
> all."

To end this post about Adobe and open source software, it is still
worthy to recap some info about the alternatives to produce flash movies
with open source tools:

Open source flash on linux:  
<http://osflash.org/linux?s=linux>

Flash from PHP with Ming:  
<http://www16.brinkster.com/gazb/ming/index.html>

And last but not least, to throw an eye on this article based on an
interview with Paul Betlem, senior director of engineering for Adobe,
who explains 'Why Flash 9 for Linux is taking so long':  
<http://applications.linux.com/article.pl?sid=06/11/21/2138216&from=rss>

But why you would use open source tools to lock your software in a
proprietary format will be the subject of another post.

Thanks to [Peter
Westenberg](http://www.constantvzw.com/cn_core/guests/g2.php?&var_in=314)
to have sent me precious informations.
