Title: Out of stock
Date: 2010-03-16 12:26
Author: ludi
Tags: News, NotCourierSans, Type, Fontforge, Handmade, Libre Fonts, Not-Courier sans, Scribus
Slug: out-of-stock
Status: published

![]({filename}/images/uploads/NotCourier-poster_out1.png "NotCourier-poster_out"){: .aligncenter .size-full .wp-image-4307 }

NotCourierSans posters have been sent to
`Anderlues, Berlin, Bordeaux, Bruxelles, Buvrinnes, Cambrai, Chicago, Garden Grove, Gent, Helsinki, Ixelles, JKL, Köln, Merley Wimborne Dorset, Montesson, Munich, Portland, Romrod, Roscoff, Rotterdam, Saint Etienne, Saint-Maurice, Santa Barbara, Sèvres, Singapore, Southampton, Venegazzù, V.N. Gaia, Winton Bournemouth Dorset, Zürich`

![]({filename}/images/uploads/posters1.png "posters1"){: .aligncenter .size-full .wp-image-4307 }
