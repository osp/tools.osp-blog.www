Title: Liberation fonts
Date: 2007-05-14 15:12
Author: nicolas
Tags: News, Libre Fonts
Slug: liberation-fonts
Status: published

A news found on [Linuxhelp](http://linuxhelp.blogspot.com/):

> Visit any random website and chances are the website expects your
> machine to have a set of fonts which have become the de-facto standard
> on the Internet. The fonts being Arial, Times New Roman, Courier New
> and so on. While it may not be illegal to install these fonts on a
> Linux machine, they are propritery and are owned by Microsoft. And
> Microsoft does not licence third parties to redistribute these fonts -
> a reason why you don't find these commonly used popular fonts
> installed in Linux by default.
>
> This is going to change once and for all.

<!--more-->

> Red Hat in association with Ascender Corp has developed a set of fonts
> which are the metric equivalent of the most popular Microsoft fonts,
> and they have released it under the GPL+exception license.

Read [the full
article](http://linuxhelp.blogspot.com/2007/05/metric-equivalent-of-microsoft-fonts.html)

A few more details from the Redhat press release:

> To address this issue and to take a key step toward liberating
> desktops, Red Hat contracted with Ascender Corp., one of the leading
> commercial developers of fonts, to develop a set of fonts that are
> metrically equivalent to the key Microsoft fonts. Under the terms of
> that development agreement, Ascender retains rights in the fonts and
> can provide them under a traditional proprietary license to those who
> require such a license, e.g. printers that have fonts embedded in
> their firmware, but Red Hat receives a license that permits us to
> sublicense the fonts at no cost under the GPL+font exception. The
> fonts are being developed in two stages. The first release is a set of
> fully usable fonts, but they will lack the fully hinting capability
> (hinting adjusts font pixelization so that the fonts render with high
> quality at large and small sizes) provided by TrueType/FreeType
> technology. That release is now ready. The second release will provide
> full hinting of the fonts, and that release will be available by the
> end of the calendar year.

Read [the details of the press
release](http://www.press.redhat.com/2007/05/09/liberation-fonts/).

And last but not least, [download the first version of the fonts from
here](https://www.redhat.com/promo/fonts/)
