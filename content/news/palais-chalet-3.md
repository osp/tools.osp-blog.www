Title: Palais Chalet 3
Date: 2009-03-11 09:39
Author: Harrisson
Tags: News
Slug: palais-chalet-3
Status: published

![poster\_pc]({filename}/images/uploads/poster_pc.png "poster_pc"){: .alignleft .size-full .wp-image-2105 }

le 13 mars Palais Chalet

- griffes de velours -

avec

en concert :  
[Nora Düster](www.noraduester.net/) de Zurich  
[The Dreams](http://www.myspace.com/tropicold) de Strasbourg

un live de :  
Maison Concett de Milano

et aux platines : Athome, Atka, Caniche Noir, [Diamant
T](http://fr.wikipedia.org/wiki/Fichier:Steinlein-chatnoir.jpg).  
cocktails au Sailor’s Trap Bar

début : 20h30 toujours aussi scherp !  
Compilothèque, Quai des péniches 50, BXL
