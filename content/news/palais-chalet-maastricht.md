Title: Palais Chalet Maastricht
Date: 2009-05-18 20:47
Author: Harrisson
Tags: News, Music
Slug: palais-chalet-maastricht
Status: published

Our camarades are back. This time in The Netherlands.

[![taupeweb]({filename}/images/uploads/taupeweb.png "taupeweb"){: .alignleft .size-full .wp-image-2642 }](http://www.pneu.org/spip.php?rubrique8)

<!--more-->  
29 mai 2009  
Palais Chalet

Live:  
[Pierre Normal](http://www.myspace.com/pierrenormalpneu)  
[Bruno Cœurvert](http://amis.minimal.be/coeurvert/)

Dj's  
[Hugo Sanchez](http://www.myspace.com/ugosanchez)  
and the Palais Chalet Crew

B32 artspice,  
Bourgognestraat 32,  
6221 bz  
Maastricht

And don't miss [Atka](http://pneu.org/spip.php?article48) 's superheroes
in a collective exhibition  
from may 8 to 31  
C'est notre muse a tous.
