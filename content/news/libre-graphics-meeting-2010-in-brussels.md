Title: Libre Graphics Meeting 2010 in Brussels
Date: 2009-06-11 10:12
Author: OSP
Tags: News, Conference, Free Software Community, LGM 2010
Slug: libre-graphics-meeting-2010-in-brussels
Status: published

\[en français, voir ci-dessous\]

Next spring, the fifth Libre Graphics Meeting (LGM) will take place in
Brussels, Belgium. In May 2010, users and developers of Free, Libre and
Open Source creative software gather in the European capital for the
collective sharing of creativity, innovation and ideas.

<!--more-->**About the Libre Graphics Meeting**  
The Libre Graphics Meeting exists to unite and accelerate the efforts
behind Free, Libre and Open Source creative software. Since 2006, this
annual meeting has been the premiere conference for developers, users
and supporters of projects such as [GIMP](http://www.gimp.org/%20),
[Inkscape](http://www.inkscape.org/),
[Blender](http://www.blender.org/), [Krita](http://koffice.org/krita/),
[Scribus](http://www.scribus.net/),
[Hugin](http://hugin.sourceforge.net), the [Open Clipart
Library](http://www.openclipart.org), and the [Open Font
Library](http://www.openfontlibrary.org) gather to work on
interoperability, shared standards, and new ideas. Work at prior LGMs
has pushed the state of the art in important areas such as color
management, cross-application sharing of assets, and common formats.

Face-to-face meetings and opportunities for collaboration are important
to developers and users alike; in the form of tutorials, talks,
workshops, and birds-of-a-feather (BOF) the event offers many formal and
informal opportunities to interact.

The conference is free for everyone to attend.

LGM 2010 is hosted by [Constant](http://www.constantvzw.org) and the
[Libre Graphics Community](http://create.freedesktop.org/).

For more information, visit
[www.libregraphicsmeeting.org](http://www.libregraphicsmeeting.org)

-----

### Libre Graphics Meeting 2010 à Bruxelles

Les prochaines rencontres Libre Graphics (LGM), cinquièmes du genre, se
tiendront au mois de mai 2010 à Bruxelles. Ce prochain printemps sera
l'occasion pour les utilisateurs et développeurs de logiciels Libres et
Open-source destinés à la création graphique de se réunir dans la
capitale européenne pour un brassage d'idées, mais aussi un grand moment
de partage de créativité et d'innovations.

**A propos du Libre Graphics Meeting**  
Ces rencontres ont été créées pour coordonner, unifier et accélérer les
efforts consentis pour faire évoluer les logiciels Libres et Open-source
destinés à la création graphique. Ces rencontres annuelles se sont
imposées depuis leur première en 2006 comme le point focal des
conférences pour développeurs, utilisateurs et enthousiastes de projets
tels que The GIMP, Inkscape, Blender, Krita, Scribus, Hugin, the Open
Clipart Library, et the Open Font Library. Et ceci avec une vision
particulièrement orientée vers l'interopérabilité, les innovations et
l'application de standards communs. Les travaux résultant des premières
LGMs ont considérablement fait progresser des secteurs essentiels comme
la gestion de la couleur, le partage inter-applications des acquis et
les formats communs.

Les tête-à-tête et occasions de collaboration sont riches et essentiels
pour les utilisateurs et développeurs : les LGMs permettent d'offrir une
multitude d'occasions formalisées ou non d'interagir, sous la forme
d'ateliers, exposés, cours et démos et, enfin, de discussions ouvertes.

Ces rencontres sont ouvertes à toutes et à tous, gratuitement.

LGM 2010 est organisée par la communauté du Libre Graphics et Constant
v.z.w.

Pour plus d'informations, visitez
[www.libregraphicsmeeting.org](http://www.libregraphicsmeeting.org)
