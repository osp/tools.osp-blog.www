Title: PZI final show
Date: 2011-06-28 12:34
Author: Stephanie
Tags: News
Slug: pzi-final-show
Status: published

[![]({filename}/images/uploads/recto1.png "recto1"){: .alignnone .size-medium .wp-image-6591 }]({filename}/images/uploads/recto1.png)

This Friday, opening of the final show of the [Piet Zwart Institute
Master Media and Communication: Networked
Media](http://pzwart.wdka.nl/networked-media/2011/06/22/graduation-show-2011-catching-flies-in-the-alternet/).
There, you can get this year's catalogue we were proud to design as
alumnis! Flyers and catalogues made only with free software (more about
the process very soon!).

[![]({filename}/images/uploads/recto2.png "recto2"){: .alignnone .size-medium .wp-image-6591 }]({filename}/images/uploads/recto2.png)

Friday 1st of July  
19.00 to 23.00  
Roodkapje, 119-133 Meent  
3011 JH Rotterdam, Netherlands
