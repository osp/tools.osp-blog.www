Title: Biscoitos da Sorte
Date: 2010-03-03 00:20
Author: OSP
Tags: News, Print Party, Recipes
Slug: biscoitos-da-sorte
Status: published

[![]({filename}/images/uploads/RIMG_824911-e1267568673379.jpg "RIMG_82491"){: .alignright .size-medium .wp-image-4204 }](http://hacklaviva.net/2010/02/um-ano-depois-continuamos-a-sorrir)

Ricardo Lafuente and Ana Carvalho imported/exported a [Print
Party](http://hacklaviva.net/2010/02/um-ano-depois-continuamos-a-sorrir/)
to Porto. With live Fortune Cookies ... how brave! ([Recipe for cookies
+
messages](http://hacklaviva.net/2010/02/biscoitos-da-sorte-deixa-que-o-terminal-te-ponha-a-cozinhar/))
