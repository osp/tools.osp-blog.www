Title: Open kitchen clipart
Date: 2009-07-27 03:35
Author: Yi
Tags: News, Drawing, Inkscape, SVG
Slug: open-kitchen-clipart
Status: published

[![clipart\_complet]({filename}/images/uploads/clipart_complet2.png "clipart_complet"){: .alignright .size-full .wp-image-3260 }]({filename}/images/uploads/clipart_complet.png)

After the release of our [Puerto Livre de Cuisine
Kookboek](http://ospublish.constantvzw.org/news/cookbook-launch), all
the drawings and illustrations now are available as clipart on [Open
Clip Art
Library](http://openclipart.org/media/view/media/clip_art?offset=20).

Thanks to [nitrofurano](http://nitrofurano.linuxkafe.com/): download all
files in one go with `wget`!  
[Download
script]({filename}/images/uploads/kitchenclipart.sh)
and run from the commandline with: `$ bash kitchenclipart.sh`

Let's cook together :-)
