Title: November 7: PubliActie
Date: 2007-10-10 10:07
Author: Femke
Tags: News, Print Party
Slug: 7-november-publiactie
Status: published

[PubliActie](http://www.ooooo.be/publiactie/) @ [Boekenbeurs
Antwerpen](http://www.boekenbeurs.be/)

![publiactie\_thumbnail.jpg]({filename}/images/uploads/publiactie_thumbnail.jpg){: .float}Inspired
by the legacy of [Cornelius
Kiliaan,](http://nl.wikipedia.org/wiki/Cornelius_Kiliaan) inventor of
the first Dutch dictionary, Marthe van Dessel and Wendy van Wynsberghe
look for neologisms and definitions in search of a word. Using Free
Software and Free Licenses, the result will be an open source lexicon
taking into account our daily feelings, experiences and encounters for
and with digital technologies.

<http://www.ooooo.be/publiactie/>
