Title: Only a few days left to propose your LGM-talk
Date: 2011-03-28 09:56
Author: OSP
Tags: News, LGM 2011
Slug: only-a-few-days-left-to-propose-your-lgm-talk
Status: published

~~\* April 1 \*~~ **April 20** is the final deadline for submitting your
talk for the Libre Graphics Meeting in Montreal!

Are you a developer, designer or a mix of both and involved in Free,
Libre and Open Source Software for creative use? Please feel welcome to
propose a talk here:
<http://create.freedesktop.org/wiki/Conference_2011/Submit_Talk> ((For
those talks that are accepted, the LGM community [works hard to gather
enough funds](http://pledgie.com/campaigns/14610) so that your flight
can be reimbursed if necessary. Reimbursement happens only after the
conference, depending on the funds available and you will need to pay
for your own lodging)).

We would like to hear about your work, and meet you in Montreal for a
BOF ((Birds of a Feather: people group together based on a shared
interest and carry out discussions without any pre-planned agenda
http://en.wikipedia.org/wiki/Birds\_of\_a\_Feather\_%28computing)), a
workshop or other forms of exchange that can make our tools even better.
