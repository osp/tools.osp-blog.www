Title: Fund Libre Graphics Magazine 1.1!
Date: 2010-11-01 11:12
Author: OSP
Tags: News, Printing + Publishing
Slug: fund-libre-graphics-magazine-1-1
Status: published

We're very excited about the first issue of Libre Graphics Magazine
coming up soon. A place for discussion on the use and development of
Libre Graphics seems more relevant than ever. With ginger coons, Ana
Carvalho and Ricardo Lafuente on the editorial team, support from the
[Mardigrafe printing house](http://www.mardigrafe.com/) and [Studio
XX](http://www.studioxx.org) in Montreal, we look forward to a spicy mix
of well-known authors and new discoveries, served up in a carefully
designed and illustrated publication.

Visit <http://libregraphicsmag.com> for more information and contribute
to the print run of Libre Graphics Magazine issue 1.1:
<http://www.pledgie.com/campaigns/13812>!
