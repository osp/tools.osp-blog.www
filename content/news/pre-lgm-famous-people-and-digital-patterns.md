Title: pre-LGM: famous people and digital patterns
Date: 2011-04-22 10:20
Author: OSP
Tags: News, LGM 2011, Tools, Workshops + teaching
Slug: pre-lgm-famous-people-and-digital-patterns
Status: published

[![]({filename}/images/uploads/pants1-87x100.jpg "pants"){: .float width="87" height="100"}]({filename}/images/uploads/pants.jpg)We
are busy preparing [pre-LGM
activities](http://www.libregraphicsmeeting.org/2011/pre-lgm) (6-9 May)
and happy to report that it looks like an exciting weekend already. As a
way to warm up for the [Libre Graphics
Meeting](http://www.libregraphicsmeeting.org),
[StudioXX](http://www.studioxx.org/) will host a Digital Pattern Making
workshop, a celebrity draw-off and an exhibition. [ginger
coons](http://adaptstudio.ca/) is gathering a 'jury' of designers,
illustrators and developers for a public discussion about the works in
the exhibition and our relation to tools. For the exhibition you are
still welcome to submit files - see the call here:
<http://libregraphicsmag.com/blog/2011/04/tools-call-for-submissions>

Workshops are free to attend but places are limited so please register
here: <http://www.libregraphicsmeeting.org/2011/registration> and
mention the workshop(s) of your choice in the comments box.

You can find latest info on the
[LGM-website](http://www.libregraphicsmeeting.org/2011/pre-lgm) but to
give an idea of what we are cooking up:

<!--more-->

### Saturday May 7

Morning: **Digital Pattern Making Workshop** (workshop with Susan
Spencer)

Participants with Inkscape, Python & pySVG loaded on their laptops will
learn to transfer a 19th century pattern into python code, and generate
this pattern using their own measurements. To complete the workshop and
illustrate the practical and creative nature of the software, each
student will be provided scissors, needle, thread, muslin, and sewing
machine to cut out and sew their garment with simple sewing techniques.
No previous sewing level is required! Both sexes are encouraged to
attend. Each student will leave the workshop with a simply constructed
version of their garment, and a printed copy of their customized
pattern.

Afternoon: **Debate** and **Exhibition opening**

Location: Studio XX: 4001, rue Berri, suite 201 – between Duluth and
Roy, métro Sherbrooke (550 m) or métro Mont-Royal (700 m).

### Sunday May 8

Morning: **Famous People** (illustration workshop with Brad Phillips,
ginger coons, …)  
<http://openclipart.org/packages-famous-people>

Who are these Famous People? What are four versions of Che Guevarra, one
single woman (and a statue) and the author of Fahrenheit 451 doing in
the same collection? The category “Famous People” at the OpenClipart
library is both monomaniac and all over the place, begging to be
populated with inspiring rolemodels, shining stars, super-heroines and
other top cats.  
At this workshop amazing artists working with Gimp, Krita and Inkscape
will get together to update the Famous People category from the
OpenClipArt Library. Illustrators, fans or both please bring an image of
your favourite celebrity and find out about how to create clipart with
VIP status.  
The OpenClipart Library is a wonderful collection of clipart dedicated
to the public domain. It means you can copy, modify, distribute and
perform all of it (also for commercial purposes) without asking
permission.
