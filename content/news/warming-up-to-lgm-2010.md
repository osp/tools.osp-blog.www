Title: Warming up to LGM 2010
Date: 2010-05-23 15:10
Author: OSP
Tags: News, LGM 2010
Slug: warming-up-to-lgm-2010
Status: published

![]({filename}/images/uploads/20100523_009.jpg "20100523_009"){: .alignright .size-medium .wp-image-4513 }  
![]({filename}/images/uploads/20100523_005.jpg "20100523_005"){: .alignright .size-medium .wp-image-4513 }

This sunny Sunday afternoon, Ricardo, ginger, ale, Ana and Femke gather
in the Constant office backyard to get ready for LGM arriving in town.
