Title: Warm gray's liberation is officially delayed
Date: 2009-04-03 17:37
Author: OSP
Tags: News, Colors, Licenses, Watch this thread
Slug: warm-grays-liberation-is-officially-delayed
Status: published

![warmgrayfool]({filename}/images/uploads/warmgrayfool.png "warmgrayfool"){: .alignnone .size-full .wp-image-2373 }  
<!--more-->  
Even if our earlier post was based on a [real and outrageous e-mail
exchange](http://lists.freedesktop.org/archives/create/2009-January/001544.html)
between Gimp developer David Neary and the marketing department of
Pantone (trying to find a solution so that FLOSS softwares could use the
copyrighted colorsystem)... and even if the [Color Universal Design
(CUD) organization](http://jfly.iam.u-tokyo.ac.jp/color/) does exist
(and is interesting!)... the liberation of twenty new warm grays was
only one of the many dreams that OSP had on this sunny Belgian 1st of
April (and we hoped the same for those 1866 other jailed colors). A
well-known colorblind OSP co-author of the hoax confirms: "Adding shades
of gray is not a solution. Instead, give the vision defect preview in
Scribus a try".
