Title: One nouveau flyer voor Constant vzw/asbl
Date: 2010-09-17 03:06
Author: OSP
Tags: News, Works
Slug: one-nouveau-flyer-voor-constant-vzwasbl
Status: published

New at [OSP Works](http://ospublish.constantvzw.org):  
flyer for [Constant](http://constantvzw.org) which OSP is a subdivision
of.  
4 different flyers.  
Play on languages, loops…  
Made with Scribus.  
more
[here](http://ospublish.constantvzw.org/works/index.php?/projects/constant-flyer/)

[![]({filename}/images/uploads/DSC_4506_small.jpg "DSC_4506_small"){: .alignnone .size-medium .wp-image-4996 }]({filename}/images/uploads/DSC_4506_small.jpg)  
[![]({filename}/images/uploads/DSC_4514_small.jpg "DSC_4514_small"){: .alignnone .size-medium .wp-image-4996 }]({filename}/images/uploads/DSC_4514_small.jpg)
