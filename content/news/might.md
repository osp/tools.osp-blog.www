Title: Might
Date: 2011-12-12 21:25
Author: OSP
Tags: News
Slug: might
Status: published

Our friends [Nadine](http://nadine.be/) and
[Überknackig](http://uberknackig.com) have opened a nice place, the
[Might shop](http://uberknackig.com/might-shop "the Might Shop"), just a
few hundred meters of our Variable house using the [Crickx
font](http://ospublish.constantvzw.org/foundry/crickx) at its full
potential!  
[![]({filename}/images/uploads/2011-12-12-202054.jpg "Might Shop"){: .alignnone .size-medium .wp-image-6938 }]({filename}/images/uploads/2011-12-12-202054.jpg)
