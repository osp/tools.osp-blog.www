Title: Stop competing, Start collaborating
Date: 2010-11-03 01:16
Author: Femke
Tags: News, Free Software Community, Thoughts + ideas, Watch this thread
Slug: watch-this-thread-stop-competing-start-collaborating
Status: published

Excellent post by [Troy Sobotka](http://troy-sobotka.blogspot.com/)
explaining why designers should collaborate with Free Software projects
and stop responding to contests:

> "We need a round table interaction where literature, art, design,
> philosophy, psychology, and sociology can be engaged. We need a cross
> pollination of creative fields and studies. We need to have developer
> minds and creative minds becoming passionate and excited about
> alternate perceptions of the future"

> "The very notion of Libre / Free software holds cooperation and
> community with such high regard you would think that we would be
> visionary leaders regarding the means and methods we use to
> collaborate. We are not. We seem to suffer from a collision of unity
> with diversity. How can we more greatly create a world of legitimate
> discussion regarding art, design, aesthetic, music, and other such
> diverse fields when we are so stuck on how much more consistent a damn
> panel looks with tripe 22 pixel icons of a given flavour?"

<http://troy-sobotka.blogspot.com/2010/10/spec-work-and-contests-part-two.html>

:-)
