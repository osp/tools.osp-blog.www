Title: Polish Print Party
Date: 2008-06-08 22:37
Author: OSP
Tags: News, Print Party
Slug: polish-print-party
Status: published

![Jiaozi]({filename}/images/uploads/jiaozi11.jpg){: .float}June
27, ~~19:00~~ 20:00 @ Compilothèque\*  
**Polish Print Party**  
With: launch of NotCourier-sans, OSP FAQ, live lay-out, Jiaozi/Pierogi
and more.

<div class="clear">

</div>

<small>\*50 Quai des Péniches 1000 Brussels</small>
