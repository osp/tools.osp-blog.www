Title: Free Magenta!
Date: 2008-12-15 12:35
Author: nicolas
Tags: News, Colors, Licenses, Patents
Slug: free-magenta
Status: published

[![]({filename}/images/uploads/20081120mobilemagenta2.jpg "20081120mobilemagenta2"){: .alignnone .size-full .wp-image-1589}]({filename}/images/uploads/20081120mobilemagenta2.jpg)

[Qu'attend-on pour libérer le
magenta](http://www.rue89.com/mon-oeil/2008/11/26/quattend-on-pour-liberer-le-magenta)
? A very good article by Pascal Riché on the website of rue89 on how the
trademark law makes it possible for companies to **own** a color for a
specific commercial field.

> Autrement dit, si vous voulez repeindre votre maison en magenta
> ([appelé ainsi à cause de la bataille du même
> nom](http://fr.wikipedia.org/wiki/Magenta_(couleur))), pas de souci;
> en revanche, si vous utilisez cette couleur pour un usage qui se
> rapproche de près ou de loin à la téléphonie, Deutsche Telekom peut
> mettre ses avocats à vos trousses.

Spotted via [Calcyum](http://www.calcyum.org/le-magenta-copyrighte/),
thanks.
