Title: Laidout
Date: 2015-03-05 20:44
Author: Colm
Tags: News
Slug: laidout
Status: published

<div id="magicdomid2">

[![laidout-grafik-tools]({filename}/images/uploads/laidout-grafik-tools.png){: .alignnone .size-medium .wp-image-7331 }]({filename}/images/uploads/laidout-grafik-tools.png)<span
class="author-a-vsz80zcz89zqz83zanz79zgq9z87zz72zz86z image image_http://www.ludi.be/grafik/laidout-grafik-tools.png url">  
</span>

</div>

<div id="magicdomid4">

<span class="author-a-vsz80zcz89zqz83zanz79zgq9z87zz72zz86z">Tom Lechner</span>
===============================================================================

</div>

<div id="magicdomid5">

<span class="author-a-z82zz77zxgz66zz89z0kz84zsuz78zz83zz76zw7">Our
description of</span><span
class="author-a-j3z88zz68zz84zmrz65zojpz85zz66zz88zd7"> the</span><span
class="author-a-z82zz77zxgz66zz89z0kz84zsuz78zz83zz76zw7"> first
encounter we had with Tom would probably be similar to</span><span
class="author-a-j3z88zz68zz84zmrz65zojpz85zz66zz88zd7">the one
of</span><span
class="author-a-z82zz77zxgz66zz89z0kz84zsuz78zz83zz76zw7">any of the two
hundred people, sitting in the audience of the Libre Graphics Meeting in
Brussels, back in May 2010.</span>

</div>

 

<div>

<span class="author-a-z83z329z65z9nz122z4djz75zz122z8q4">The Libre
Graphics Meeting is an annual meeting for users and developers of Free,
Libre and Open Source graphic design software.</span><span
class="author-a-z82zz77zxgz66zz89z0kz84zsuz78zz83zz76zw7">[Looking back
at the archive](http://youtu.be/zLKjdUGu02A), ther</span><span
class="author-a-z66zuz69zsz80zcz81zz67zkz86zz122zra3kz89z">e</span><span
class="author-a-z82zz77zxgz66zz89z0kz84zsuz78zz83zz76zw7"> is a proper
cheer when Tom progressively peeled, like an orange, the image he was
stretching over a dodecahedron, demoing the way Laidout could be used to
prepare the file to</span><span
class="author-a-z82zz77zxgz66zz89z0kz84zsuz78zz83zz76zw7 i">*print*</span><span
class="author-a-z82zz77zxgz66zz89z0kz84zsuz78zz83zz76zw7"> such a
shape.</span>

</div>

 

<div id="magicdomid6">

<span class="author-a-z82zz77zxgz66zz89z0kz84zsuz78zz83zz76zw7">Tom has
as many fa</span><span
class="author-a-j3z88zz68zz84zmrz65zojpz85zz66zz88zd7">cets</span><span
class="author-a-z82zz77zxgz66zz89z0kz84zsuz78zz83zz76zw7"> as he has
obsessions for different mathematical geometrical schemes, after two and
a half years at Caltech where he spent a lot more time making artwork
than finishing homework, he</span><span
class="author-a-z66zuz69zsz80zcz81zz67zkz86zz122zra3kz89z">"</span><span
class="author-a-z82zz77zxgz66zz89z0kz84zsuz78zz83zz76zw7">moved to
Portland to attend</span><span
class="author-a-j3z88zz68zz84zmrz65zojpz85zz66zz88zd7">an</span><span
class="author-a-z82zz77zxgz66zz89z0kz84zsuz78zz83zz76zw7">art school and
then continued on to make artwork and open source computer art tools,
narrowly avoiding starving to death in abject misery under the weight of
student loans."</span>

</div>

<div id="magicdomid7">

</div>

 

<div id="magicdomid8">

[<span
class="author-a-z82zz77zxgz66zz89z0kz84zsuz78zz83zz76zw7">Laidout</span>](http://laidout.org/)<span
class="author-a-z82zz77zxgz66zz89z0kz84zsuz78zz83zz76zw7"> started as an
imposition tool used by Tom Lechner to print his comics. Over time more
intricate functionalities were added such as the afor</span><span
class="author-a-z83z329z65z9nz122z4djz75zz122z8q4">e</span><span
class="author-a-z82zz77zxgz66zz89z0kz84zsuz78zz83zz76zw7">mentioned
possib</span><span
class="author-a-z83z329z65z9nz122z4djz75zz122z8q4">i</span><span
class="author-a-z82zz77zxgz66zz89z0kz84zsuz78zz83zz76zw7">lity to work
image imposition onto 3</span><span
class="author-a-j3z88zz68zz84zmrz65zojpz85zz66zz88zd7">D</span><span
class="author-a-z82zz77zxgz66zz89z0kz84zsuz78zz83zz76zw7"> objects, or
the addition of drawing tools. As Tom developed his own
Graphical</span><span
class="author-a-j3z88zz68zz84zmrz65zojpz85zz66zz88zd7">User</span><span
class="author-a-z82zz77zxgz66zz89z0kz84zsuz78zz83zz76zw7">Interface
Toolkit for Laidout</span><span
class="author-a-j3z88zz68zz84zmrz65zojpz85zz66zz88zd7">,</span><span
class="author-a-z82zz77zxgz66zz89z0kz84zsuz78zz83zz76zw7"> the software
comes with a personal</span><span
class="author-a-j3z88zz68zz84zmrz65zojpz85zz66zz88zd7"> "</span><span
class="author-a-z82zz77zxgz66zz89z0kz84zsuz78zz83zz76zw7">voice</span><span
class="author-a-j3z88zz68zz84zmrz65zojpz85zz66zz88zd7">"</span><span
class="author-a-z82zz77zxgz66zz89z0kz84zsuz78zz83zz76zw7">.</span>

</div>

 

<div>

<span class="author-a-z82zz77zxgz66zz89z0kz84zsuz78zz83zz76zw7">It feels
like a very intimate tool full of hidden menus and shortcuts. With every
new version its exciting to see the added features and, how they all
come with their own approach and interface. Laidout appears to be like
any other tool at first glance, but when you take a closer look, or see
one of the screencasts on Youtube, you understand that Laidout has its
own approach</span> <span
class="author-a-j3z88zz68zz84zmrz65zojpz85zz66zz88zd7">—</span><span
class="author-a-z82zz77zxgz66zz89z0kz84zsuz78zz83zz76zw7">which is
really refreshing in a landscape where so many tools</span><span
class="author-a-j3z88zz68zz84zmrz65zojpz85zz66zz88zd7">are</span><span
class="author-a-z82zz77zxgz66zz89z0kz84zsuz78zz83zz76zw7"> alike.</span>

</div>

 

<span class="author-a-mgygz86zz89zyz122zdz68zz79zz81zp3z122zz84z">Engraving Fill Tool</span>
============================================================================================

<span class="author-a-mgygz86zz89zyz122zdz68zz79zz81zp3z122zz84z">The
most recent version of Laidout came with an engraving tool, a utility
that turns an image into line drawing. For the latest program of [La
Balsamine](http://balsamine.be), a Brussels theater for which we've been
designing the identity for some years now, the co-directors invited us
to work with engraving and engraving patterns.</span>

<span class="author-a-mgygz86zz89zyz122zdz68zz79zz81zp3z122zz84z">We
decided to experiment</span><span
class="author-a-z83z329z65z9nz122z4djz75zz122z8q4">on</span><span
class="author-a-mgygz86zz89zyz122zdz68zz79zz81zp3z122zz84z">digital
patterns and lines, digging</span><span
class="author-a-z82zz77zxgz66zz89z0kz84zsuz78zz83zz76zw7"> to
find</span><span
class="author-a-mgygz86zz89zyz122zdz68zz79zz81zp3z122zz84z"> different
ways to (re)draw images.</span><span
class="author-a-z82zz77zxgz66zz89z0kz84zsuz78zz83zz76zw7">Last year, at
the Leipzig edition of the Libre Graphics Meeting</span><span
class="author-a-mgygz86zz89zyz122zdz68zz79zz81zp3z122zz84z">,</span><span
class="author-a-z82zz77zxgz66zz89z0kz84zsuz78zz83zz76zw7"> we were
treated to</span><span
class="author-a-mgygz86zz89zyz122zdz68zz79zz81zp3z122zz84z"> yet another
wonderful presentation</span><span
class="author-a-z82zz77zxgz66zz89z0kz84zsuz78zz83zz76zw7">
of</span><span
class="author-a-mgygz86zz89zyz122zdz68zz79zz81zp3z122zz84z"> Laidout's
new tool ([called mesh
conceptions](https://www.youtube.com/watch?v=1uCRBAcd2SY)</span><span
class="author-a-mgygz86zz89zyz122zdz68zz79zz81zp3z122zz84z url">[)](https://www.youtube.com/watch?v=1uCRBAcd2SY,%29)</span><span
class="author-a-z82zz77zxgz66zz89z0kz84zsuz78zz83zz76zw7">.</span>

<span
class="author-a-z82zz77zxgz66zz89z0kz84zsuz78zz83zz76zw7">W</span><span
class="author-a-mgygz86zz89zyz122zdz68zz79zz81zp3z122zz84z">e</span><span
class="author-a-z82zz77zxgz66zz89z0kz84zsuz78zz83zz76zw7">fa</span><span
class="author-a-z66zuz69zsz80zcz81zz67zkz86zz122zra3kz89z">n</span><span
class="author-a-z82zz77zxgz66zz89z0kz84zsuz78zz83zz76zw7">tasized
over</span><span
class="author-a-mgygz86zz89zyz122zdz68zz79zz81zp3z122zz84z"> a similar
tool</span><span
class="author-a-j3z88zz68zz84zmrz65zojpz85zz66zz88zd7">which</span><span
class="author-a-mgygz86zz89zyz122zdz68zz79zz81zp3z122zz84z">
could</span><span
class="author-a-j3z88zz68zz84zmrz65zojpz85zz66zz88zd7">"</span><span
class="author-a-mgygz86zz89zyz122zdz68zz79zz81zp3z122zz84z">engrave</span><span
class="author-a-j3z88zz68zz84zmrz65zojpz85zz66zz88zd7">"</span><span
class="author-a-mgygz86zz89zyz122zdz68zz79zz81zp3z122zz84z"> images with
the same fluidity and magic. A different approach exist</span><span
class="author-a-z66zuz69zsz80zcz81zz67zkz86zz122zra3kz89z">s</span><span
class="author-a-mgygz86zz89zyz122zdz68zz79zz81zp3z122zz84z"> in Inkscape
(</span><span
class="author-a-z82zz77zxgz66zz89z0kz84zsuz78zz83zz76zw7">arguably</span><span
class="author-a-mgygz86zz89zyz122zdz68zz79zz81zp3z122zz84z"> the
main</span><span
class="author-a-z82zz77zxgz66zz89z0kz84zsuz78zz83zz76zw7"> vector editor
in the</span><span
class="author-a-mgygz86zz89zyz122zdz68zz79zz81zp3z122zz84z">
libre</span><span
class="author-a-z82zz77zxgz66zz89z0kz84zsuz78zz83zz76zw7"> graphics
world</span><span
class="author-a-mgygz86zz89zyz122zdz68zz79zz81zp3z122zz84z">), but which
is more manual and</span><span
class="author-a-z82zz77zxgz66zz89z0kz84zsuz78zz83zz76zw7">much</span><span
class="author-a-mgygz86zz89zyz122zdz68zz79zz81zp3z122zz84z"> less
granular for what we want</span><span
class="author-a-z82zz77zxgz66zz89z0kz84zsuz78zz83zz76zw7">ed</span><span
class="author-a-mgygz86zz89zyz122zdz68zz79zz81zp3z122zz84z"> to
achieve.</span>

<span class="author-a-z82zz77zxgz66zz89z0kz84zsuz78zz83zz76zw7">Laidout
had, by then, most of the building blocks in place for what we wanted to
create. This season</span><span
class="author-a-j3z88zz68zz84zmrz65zojpz85zz66zz88zd7">'</span><span
class="author-a-z82zz77zxgz66zz89z0kz84zsuz78zz83zz76zw7">s program
for</span><span
class="author-a-z66zuz69zsz80zcz81zz67zkz86zz122zra3kz89z">l</span><span
class="author-a-z82zz77zxgz66zz89z0kz84zsuz78zz83zz76zw7">a Balsa was
the perfect opportunity to co</span><span
class="author-a-j3z88zz68zz84zmrz65zojpz85zz66zz88zd7">m</span><span
class="author-a-z82zz77zxgz66zz89z0kz84zsuz78zz83zz76zw7">mission Tom
for this next step in the tool; applying the fill tool over an existing
image, a content aware overlay, of sorts.</span>

<div id="magicdomid12">

</div>

<div id="magicdomid13">

[![out-1\_762-1327]({filename}/images/uploads/out-1_762-1327.gif){: .alignnone .size-medium .wp-image-7331 }]({filename}/images/uploads/out-1_762-1327.gif)

</div>

<div id="magicdomid14">

<span
class="author-a-vsz80zcz89zqz83zanz79zgq9z87zz72zz86z image image_http://www.ludi.be/grafik/out-1_762-1327.gif url">  
</span>

</div>

 

<div>

<span
class="author-a-z82zz77zxgz66zz89z0kz84zsuz78zz83zz76zw7">It</span><span
class="author-a-z83z329z65z9nz122z4djz75zz122z8q4"> i</span><span
class="author-a-z82zz77zxgz66zz89z0kz84zsuz78zz83zz76zw7">s this
relationship that we thought could be interesting to portray here; this
ecology</span><span class="author-a-z83z329z65z9nz122z4djz75zz122z8q4">
of exchange</span><span
class="author-a-z82zz77zxgz66zz89z0kz84zsuz78zz83zz76zw7"> between
designer and developer</span><span
class="author-a-j3z88zz68zz84zmrz65zojpz85zz66zz88zd7">which</span><span
class="author-a-z82zz77zxgz66zz89z0kz84zsuz78zz83zz76zw7"> can only
really be afforded</span><span
class="author-a-z83z329z65z9nz122z4djz75zz122z8q4">in a working model
where neither is afraid to open up their sources</span><span
class="author-a-z82zz77zxgz66zz89z0kz84zsuz78zz83zz76zw7">. This
relation is at the center of our practice, we couldn't really see any
other way of working now.</span>

</div>

 

<div id="magicdomid26">

<span class="author-a-vsz80zcz89zqz83zanz79zgq9z87zz72zz86z">This
article was originally written as a response to a submission request to
[Grafik Magazine](https://www.grafik.net/) in the
[Screenshot](https://www.grafik.net/category/screenshot/) section. You
can view it in it's [intended context
here](https://www.grafik.net/category/screenshot/diy-software). the text
and embedded images are published under the [Free Art License
1.3](http://artlibre.org/licence/lal/en/) and
[CC](https://creativecommons.org/licenses/by-sa/2.0/)</span>[<span
class="author-a-rz77z37z89zz75zebcecz77zqz89zz70zz82z">-B</span><span
class="author-a-vsz80zcz89zqz83zanz79zgq9z87zz72zz86z">Y</span><span
class="author-a-rz77z37z89zz75zebcecz77zqz89zz70zz82z">-SA</span>](https://creativecommons.org/licenses/by-sa/2.0/)<span
class="author-a-rz77z37z89zz75zebcecz77zqz89zz70zz82z">.</span>

</div>

 
