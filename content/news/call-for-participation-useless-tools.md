Title: Call for participation: Useless Tools
Date: 2009-02-12 23:08
Author: Femke
Tags: News, Tools, Contribute!, History, Thoughts + ideas, Tools, Usability links
Slug: call-for-participation-useless-tools
Status: published

(Call forwarded from Isabelle Massu)

<div style="font-family:mono;font-size:11px;">

+ + + + + + + + + + + +  
![6612411]({filename}/images/uploads/6612411.jpg "6612411"){: }  
Museums narrate the history of man’s evolution through the display of
tools (silex, knife, jar, etc.). In contrast, we are looking for useless
tools. This call will result in a vitrine of objects titled 'Object
Inutile' to be displayed at the Musée de Préhistoire in France (late
Spring 2009) and the Thompson Gallery at San José State University,
U.S.A. (Fall 2009). Along with the objects will be an audio-guide that
describes and explains each object in its owners’ words.
</p>
This project is one display within a larger exhibition titled 'Early Man
On a Modern Road' by Dore Bowen and Isabelle Massu. The exhibition is
coordinated with the anniversary of Charles Darwin’s publication of the
Origin of Species. You can consult our website for more information on
the full exhibition at <http://www.aux2mondes.org/earlyman>

CALL FOR PARTICIPATION  
Email us a picture of your useless tool and a description based upon
these questions. Send to isa@aux2mondes.org. We will contact you if your
object is selected.

1.  What is your useless tool called?
2.  When was it made?
3.  What is it supposed to do?
4.  Why do you think it is useless?
5.  If you like, add a personal story about the object.

</div>

<!--more-->

<div style="font-family:mono;font-size:11px;">

THE CONCEPT OF USELESS TOOLS
</p>
1\) We are not necessarily looking for tools that don’t work or that time
has passed by; we are looking for tools that you find to be useless.
Tools suggest that a certain activity has value. When you define a tool
as useless you are defining its activity as useless. (A clock, for
instance, is for coordinating activities with other human beings. To
find a clock unuseful is to find timeliness, precision, and sociability
unuseful.) \[For a theoretical discussion concerning the phenomenology
of useless tools in Fluxus art see the essay "[The Function of
Dysfunction](http://switch.sjsu.edu/mambo/switch23/the_function_of_disfunction_3.html)"

2\) In addition to value, useless tools are related to consumerism. Over
the past half-century machines have been produced to fulfill all sorts
of unnecessary functions, or to fulfill necessary functions but without
precision. The egg topper scissors is a good example (see image above).
This tool was produced to facilitate the breaking of the top of an egg.
Most people break the top of an egg with a knife and still do. Why the
production of such useless tools? Since most consumers have all the
tools necessary to live, capitalism produces weird and sometimes
wonderful tools that are, essentially, useless.

3\) And finally, tools (and their related values) are associated with
class. Presumably, the working class man knows about cars, the middle
class woman about specialty cookware, and the upper class businessman
about fine watches. In France a signature middle class tool is the pince
a sucre, a device to pick up sugar cubes without using the hands. It is
essentially unnecessary as a tool (it’s easier to pick up sugar cubes
with the hands) but it marks a certain disdain for manual labor and the
body, and thus is a symbol of middle class cleanliness and propriety. To
find this tool useless means that you find its class values useless.
Such a choice acts as commentary and critique on class.

4\) How does the idea of useless tools relate to the larger theme of the
exhibition—Darwin’s theory of evolution? Related to the idea of unuseful
tools is the notion of evolutionary maladaptation. When a behavior is no
longer adaptive it is called 'maladaptive'. Wikipedia notes that
maladaptation can "signify an adaptation that, whilst reasonable at the
time, has become less and less suitable and more of a problem or
hindrance in its own right." Useless tools can help us to understand
maladaptation as they testify to traits or behaviors that we no longer
considered valuable. In 1859 Darwin wrote in the Origin of Species, "We
see beautiful adaptations everywhere and in every part of the organic
world." In its failure to perform as expected, the useless tool makes
awkward maladaptations visible as well.

+ + + + + + + + + + + +

</div>
