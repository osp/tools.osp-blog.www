Title: Balsa 2012-2013
Date: 2012-06-07 14:13
Author: ludi
Tags: News, Works
Slug: balsa-2012-2013
Status: published

![]({filename}/images/uploads/P1070077-s.jpg "P1070077-s"){: .aligncenter .size-medium .wp-image-7146 }  
![]({filename}/images/uploads/P1160525-s.jpg "P1160525-s"){: .aligncenter .size-medium .wp-image-7146 }

La Balsamine program 2012-2013. Freshly printed. Jacket folding and
stamping session this week. Season poster coming.
