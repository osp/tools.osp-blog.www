Title: Scribus Re-loaded
Date: 2010-11-22 00:19
Author: OSP
Tags: News, Tools, Scribus, Scribus Re-loaded
Slug: scribus-re-loaded
Status: published

Fresh news from Paris:

[![]({filename}/images/uploads/Scribus-OIF.png "Scribus-OIF"){: .alignnone .size-medium .wp-image-5319 }]({filename}/images/uploads/Scribus-OIF.png)

Since a few months the [Organisation Internationale de la
Francophonie](http://www.francophonie.org/) supports the Scribus team.
The goal is to make the program ready for new developers and users from
Asia and Africa and this is \*very\* good news for the rest of us too!

The project goes under the code name "Scribus-OIF" and involves intense
work on two related but very different axis: a complete rewrite of the
text-engine, and a new take on the properties panel.  
<!--more-->  
It all started with the need to support complex text-layout. In Arabic
script for example, different glyphs are used for the same character,
depending on their position in the word. Ligatures are another example
of a complex text-layout. The team also decided that it was important to
disentangle the text-engine code itself and to make it easier for
newcomers to contribute to the project. We'll talk about all this more
soon but it does not stop there. Work is under-way to provide
sophisticated paragraph based lay-outs and last but not least, the undo
function for text will finally be added :-D

The second part of the work concerns the properties palette. You could
see this central part of the interface as a representation of the
Scribus datamodel, a good place to make developers and designers start
thinking together. To invite contributions by people with different
skills, the code for the properties palette will be detached from the
main code-base and modularized to a high degree.

While impatiently waiting for all these developments to take an effect,
expect a series of posts about Scribus Re-loaded in the upcoming months.
We will detail the text-engine, discuss Scribus Persona, look closer at
the properties palette and QT-designer, test-drive the frame-rendering
feature and more.

Go Scribus Go!
