Title: Pour la premiere fois dans ta ville
Date: 2009-06-07 16:03
Author: Harrisson
Tags: News, Works, Music, Party, Type
Slug: pour-la-premiere-fois-dans-ta-ville
Status: published

![tournee\_juin\_pti2]({filename}/images/uploads/tournee_juin_pti2.jpg "tournee_juin_pti2"){: .alignright .size-full .wp-image-2966 }

[Pierre Normal](http://www.myspace.com/pierrenormalpneu)
([pneu](http://www.pneu.org/spip.php?rubrique8)) and [The
Dreams](http://www.myspace.com/tropicold) en tournee.

16 June - Strasbourg, Le Troc Café, Rue du Faubourg de Saverne  
17 June - Zürich, Kalki, Kalkbreitenstrasse (Palais Chalet)  
18 June - Lausanne, Espace Autogéré  
19 June - Stuttgart, Club für Flüssigkeiten & Schwingungen  
20 June - Genève, L’Ecurie, (Palais Chalet)  
21 June - Luzern, tbc  
22 June - Genève, Duplex, 波(なみ) La Vague, rue des amis 7, 22h,
(Palais Chalet)

Handmade xerox poster done by OSP (lettering) and
[ATKA](http://www.pneu.org/spip.php?article48) (polpetto). Don't miss it
if you're near!
