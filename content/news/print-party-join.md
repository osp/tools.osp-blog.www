Title: Print Party! Join!
Date: 2006-06-23 13:59
Author: Harrisson
Tags: News, Print Party
Slug: print-party-join
Status: published

**Rendez-vous le 7 juillet a [Quarantaine](http://www.quarantaine.biz)
pour une bonne Print Party!** Ce sera a 16:00, et ca durera jusque
peut-etre 20:00. Ca sera en français avec accent, (et bequilles en
anglais et en néerlandais), mené par la Constant Printing + Cooking
Team.

![ppfly.png]({filename}/images/uploads/2006/06/ppfly.png){: #image100 width="68" height="96"}

*Ou l'on apprendra comment mettre en page, imposer et imprimer un
booklet avec des logiciels libres!*

On pensait que c'était de la Science-Fiction, mais une fois encore les
experts se sont **trompés**: il est possible de faire du graphisme avec
du Open Source. C'est en fait assez proche de la cuisine!

*Si vous savez faire un gateau, vous saurez faire un livre*.

Pour accompagner cette performance, Kate Rich de Bristol fera des
cocktails avec du [Cube Cola](http://sparror.cubecinema.com/cube/cola/).

Et [+Nurse+](http://www.constant.irisnet.be/~constant/champagne/) nous
jouera un mix inspiré sur platines ensoleillées.

Quarantaine, c'est 43 rue Lesbroussart, 1050 IXL, Bruxelles, Belgique
[www.quarantaine.biz.](http://www.quarantaine.biz)  
On ne saurait trop vous conseiller de visiter!
