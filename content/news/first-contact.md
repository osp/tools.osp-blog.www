Title: First Contact
Date: 2011-05-23 10:50
Author: OSP
Tags: News, books, Hardware, ocr, typography
Slug: first-contact
Status: published

**Tomorrow/morgen/demain: First Contact!**

[![]({filename}/images/uploads/500px-ScanbotA1.jpg "500px-ScanbotA1"){: .alignleft .size-medium .wp-image-6475 }](https://hackerspace.be/ScanBot)

\[EN\] Tuesday 24 May @ 19:00 the [D.I.Y.
bookscanner](https://hackerspace.be/ScanBot) built by/in Hackerspace
Brussels will be operated for the first time through a speculative
software that Pierre Marchand developed with contributions from John
Haltiwanger. The experiment traverses text, image and data and plays
with the various textures in-between. The code is under development and
written in preparation of Constant Verbindingen/Jonctions 2011. Bring
Your Books!  
++++++++++++++++++++++++++++++  
\[NL\] Dinsdag 24 Mei @ 19:00 zal de [D.I.Y.
bookscanner](%20%3Ca%20href=) gebouwd door/in Hackerspace Brussel voor
het eerst worden aangestuurd door een speculatieve software die Pierre
Marchand ontwikkelt met bijdragen van John Haltiwanger. Het experiment
doorkruist tekst, beeld en data en speelt met de verschillende texturen
ertussenin. De code is geschreven in voorbereiding op Constant
Verbindingen/Jonctions 2011. Breng je boeken mee!  
++++++++++++++++++++++++++++++  
\[FR\] Mardi 24 mai @ 19:00 le [D.I.Y. bookscanner](%20%3Ca%20href=)
construit par/dans le Hackerspace Brussels sera pour la première fois
piloté par un logiciel spéculative conçu par Pierre Marchand avec des
contributions de John Haltiwanger. L’expérimente traverse le texte,
l'image et les données et joue avec leurs textures différentes. Le code
est développé et rédigé en préparation de Constant
Verbindingen/Jonctions 2011. Apportez Vos Livres!  
++++++++++++++++++++++++++++++  
Prinses Elisabethlaan 46, 1030 Schaarbeek (BE) / Av. Princesse Elisabeth
46, 1030 Schaerbeek (BE)  
++++++++++++++++++++++++++++++  
<https://hackerspace.be/ScanBot>  
<http://oep-h.com>  
<http://drippingdigital.com>
