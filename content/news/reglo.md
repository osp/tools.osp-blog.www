Title: Reglo
Date: 2011-11-18 17:09
Author: OSP
Tags: News, Type
Slug: reglo
Status: published

[![]({filename}/images/uploads/reglo.png "reglo"){: .alignnone .size-medium .wp-image-6922 }]({filename}/images/uploads/reglo.png)

We used Reglo Bold for the new identity of Radio Panik and we are happy
to release it today on the [OSP
foundry](http://ospublish.constantvzw.org/foundry/reglo/) under open
source license
