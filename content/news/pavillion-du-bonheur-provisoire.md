Title: Pavillion du Bonheur Provisoire
Date: 2008-04-16 00:11
Author: OSP
Tags: News, Design Samples, Libre Fonts
Slug: pavillion-du-bonheur-provisoire
Status: published

Opening tomorrow!

[![]({filename}/images/uploads/img_9094.jpg "img_9094"){: .alignnone .size-full .wp-image-435 width="400"}]({filename}/images/uploads/img_9094.jpg)

Signage for [The Pavilion of Provisionary
Happiness](http://blog.atomium.be/page/Pavillon-temporaire-du-Bonheur---Paviljoen-van-het-Tijdelijke-Geluk---Pavilion-of-Temporary-Happiness.aspx)
by OSP. The font will be released under an open license
<blink>**soon**</blink>. More images:
<http://ospublish.constantvzw.org/image/?level=album&id=7>
