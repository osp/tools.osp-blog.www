﻿Title: The means of production
Date: 2015-10-22 15:06
Author: Eric
Tags: News

*A lecture by Gijs de Heij and Eric Schrijver from Open Source
Publishing, presented October 27^th^ 2014 at the KABK in The Hague
during the Project Week ‘The Tribunal for Uncertain Objects’. The
project week was structured around the research of Susan Schüpli. Other
lectures that day were given by Jorinde Seijdel, Rosa Menkman and Jonas
Staal.*

We are Open Source Publishing, a caravan based in Brussels who only use
Free and Open Source software to produce graphic design.

We started as an experiment enticed by the vibrant culture in the world
of Free and Open Source software. We started to use this software as an
alternative to the homogenous software landscape habitually used in
graphic design.

Now we are not only using them but even producing them.

We prefer to call ourselves a kitchen.

[![OSP
cooking]({filename}/images/uploads/osp-cooking.jpeg){: .alignnone .size-medium .wp-image-7460 }]({filename}/images/uploads/osp-cooking.jpeg)

We do print parties in which we produce printed matter and we cook at
the same time. A recipe can not be copyrighted—it can only be kept
secret (like the recipe for Coca Cola) or shared (like your
grand-fathers apple pie). Not only can we use some else’s recipe, we can
also adapt it to our own version, that we in turn can pass on.

[![OSP
cooking]({filename}/images/uploads/osp-cooking-bw.jpg){: .alignnone .size-medium .wp-image-7460 }]({filename}/images/uploads/osp-cooking-bw.jpg)

We are in Brussels. We are 9—women and men from various backgrounds.

[![Own the means of
production]({filename}/images/uploads/txt-own-the-means-of-production.png){: .alignnone .size-medium .wp-image-7460 }]({filename}/images/uploads/txt-own-the-means-of-production.png)

Who owns the means of production? This is a central question in Marxist
theory, and we don’t pretend to be Marxist theorists but still this
question is very relevant to us.

All the technological means we normally use as graphic designers to
create our work, our production, are produced and often patented by
private companies. From the printing presses to the inks (Pantone
colours—a standard owned by a private company!) and of course the
computer software and hardware.

As a designer at the KABK, you are taught to be a conscious designer.
You know that a designer is not just someone who can make pretty
pictures to sell objects, but that design can tackle political
questions. Or that you can do both at the same time, as Roosje & Niels’
example of Toscani’s Benetton campaign illustrates.

So when we buy our computer and our computer software, we willingly give
our money to these multi-billion dollar corporations. Why is that?

When you install mainstream software, like that made by Apple or Adobe,
did you ever read one of these boxes where you click ‘I agree’? If you
did you might have noticed that companies are very careful in their
wording. They make it clear that you do not *own* the software, but
rather, that *license* the software, which means that you buy the right
to use the software in a certain way. The right to use the software is
governed by a strict set of constraints. Among other constraints, these
‘End User License Agreements’ always forbid you to open up with the
software, to tinker with the internals, to ‘reverse engineer’ it, to
change it.

Adobe has traditionally been doing a good job at entertaining relations
with educational institutions, offering them good deals on the software,
making sure that new generations of designers depend on these tools. The
software monoculture that has been created because of Adobe’s position
as a monopolist is not a subject of discussion in design education.

So we have a situation where all designers across the world use the same
tools made by the same small set of people, and they are not allowed to
modify and change these tools as a traditional craftsman might have
done.

We choose to use another type of tools: tools that are not privately
owned but owned by the community. Free and Open Source Software is
sometimes made by commercial companies, sometimes by amateurs, sometimes
by professionals in their spare time, but in each case, those people who
make the tools decide to license the software in such a way that it
becomes part of the commons. These tools are ‘free’ in the sense of
gratis, but more importantly we are allowed to open them up, access
their source-code, we can *see* and *understand* how they work. What is
maybe even more important: their license allows us to change the
software, and *redistribute* the changed versions.

[![slide-from-madrid-presentation-tool-universe]({filename}/images/uploads/slide-from-madrid-presentation-tool-universe.png){: .alignnone .size-medium .wp-image-7460 }]({filename}/images/uploads/slide-from-madrid-presentation-tool-universe.png)

We get by without Adobe and MacOS.

Here you see a part of our tool universe.

Now, many of these tools might be seen as counterparts to existing
proprietary software: Linux is as an operating system, like OS X;
Inkscape is a vector drawing program, like Adobe Illustrator;
LibreOffice is an Office Suite, like Microsoft Office; Scribus is a
desktop publishing program, like Adobe Indesign & QuarkExpress;
Fontforge is a font editor, like Fontlab and Gimp is a pixel-based image
editor, like Adobe Photoshop.

The similarity between the Open Source tools and their more well known
commercial counterparts varies. For instance, LibreOffice is a direct
clone of Microsoft Office. In comparison, Inkscape feels quite different
from Illustrator. Illustrator was built around the PostScript format,
squarely based in print publishing, whereas Inkscape is built around the
SVG format, which was conceived for the web and comes with its own
approach to vector graphics.

We also use tools that have no real equivalent in the world of
mainstream graphic design tools. Most of the time, these tools originate
from worlds other than graphic design and therefore bring with them
different approaches to visualisation.

Graphviz is a tool to produce network graphs developed by AT&T (the
telephone company). ConTeXt is a document formatting tool descended from
Latex & TeX—these are tools created by scientists and still mainly used
for scientific publications. Image-magick an image editor that works by
sending it series of instructions through the command line.

[![balsa\_poster\_11-12]({filename}/images/uploads/balsa_poster_11-12.jpg){: .alignnone .size-medium .wp-image-7460 }]({filename}/images/uploads/balsa_poster_11-12.jpg)

Here we see a poster, made with Inkscape, where the visual language is
inspired by the kind of graphs that Graphviz generates. Parts of the
poster were first generated with Graphviz.

[![logo-balsa]({filename}/images/uploads/logo-balsa.jpg){: .alignnone .size-medium .wp-image-7460 }]({filename}/images/uploads/logo-balsa.jpg)

This is the logo of the Balsamine theatre, with the old logo decomposed
and pasted into the b—  
which is finally in all b’s in the font we created for the Balsamine.
The font, by the way, was based on an existing Japanese open source
font—the fact that the accented letter is of a different size than the
non-accented letters is because it was intended for vertical display.
These kind of artefacts that are the result of a different visual
culture, we try to work with them, rather then brushing them away—in the
same way that we do not mind to be influenced by the nature of the
tools. If we use a tool, we let the tool inform us.

[![screenshot-design-in-spreadsheet]({filename}/images/uploads/screenshot-design-in-spreadsheet.png){: .alignnone .size-medium .wp-image-7460 }]({filename}/images/uploads/screenshot-design-in-spreadsheet.png)

We made a Balsamine fanzine in LibreOffice Calc, which is like Microsoft
Excel—  
you can surely imagine it is slightly gruelling to do lay-out in a
spreadsheet.

[![balsa\_website\_13-14-screenshot]({filename}/images/uploads/balsa_website_13-14-screenshot.png){: .alignnone .size-medium .wp-image-7460 }]({filename}/images/uploads/balsa_website_13-14-screenshot.png)

This is the website of the Balsamine. On the web it is much more common
to use open tools than it is in design for print: in this case, the
underlying system is called PmWiki but other Open tools like Wordpress
are really popular as well. They have thrived because of their open
nature: because people are able to open these tools up and study the way
they work, it becomes more easy to create add-ons, plugins and
extensions. So such a tool can become the centre of its own little
ecosystem.

So you see all tools bring a certain heritage, culture and ecosystem.
For the 2014/2015 season of the Balsamine we wanted to align ourselves
with the ecosystem of the web. Exactly because the internet is built on
a ‘view-source’ logic, open standards like HTML, CSS and JavaScript.

So it brought us to this new challenge: let’s try HTML, as a tool for
layout. Of course web designers use HTML for layout all the time. We
wanted to take that tool and extend its reach, and design with it for
print.

If you choose to work with HTML and CSS, it means a different model of
layout than in most DTP software. You have a formatting model, called
the ‘box model’, that is oriented towards reflowing. Another reason to
work with HTML and CSS is the existing ecosystem. A tool like ConText is
used by only a handful of users. Web design is booming. Even if
designers using web technologies for print are a minority, these
technologies themselves are in full development, and we can lift on some
of the vitality of that ecosystem.

[![slide-from-madrid-presentation-try-html]({filename}/images/uploads/slide-from-madrid-presentation-try-html.png){: .alignnone .size-medium .wp-image-7460 }]({filename}/images/uploads/slide-from-madrid-presentation-try-html.png)

[![relearn\_publication\_12]({filename}/images/uploads/relearn_publication_12.png){: .alignnone .size-medium .wp-image-7460 }]({filename}/images/uploads/relearn_publication_12.png)

So we started to write the HTML and CSS together in Etherpad—  
which is like the O.G. of collaborative text editors, and which is Open
Source.

[![balsa\_programme\_13-14-cover]({filename}/images/uploads/balsa_programme_13-14-cover.png){: .alignnone .size-medium .wp-image-7460 }]({filename}/images/uploads/balsa_programme_13-14-cover.png)

This is what it looks like.

[![balsa\_programme\_13-14\_spread]({filename}/images/uploads/balsa_programme_13-14_spread.png){: .alignnone .size-medium .wp-image-7460 }]({filename}/images/uploads/balsa_programme_13-14_spread.png)

[![balsa\_programme\_13-14\_spread\_3]({filename}/images/uploads/balsa_programme_13-14_spread_3.png){: .alignnone .size-medium .wp-image-7460 }]({filename}/images/uploads/balsa_programme_13-14_spread_3.png)

[![balsa\_programme\_13-14\_spread\_2]({filename}/images/uploads/balsa_programme_13-14_spread_2.png){: .alignnone .size-medium .wp-image-7460 }]({filename}/images/uploads/balsa_programme_13-14_spread_2.png)

We had to do quite some experimentation of course. We output the PDF
simply with the browser: Ctrl+P.

[![slide-from-madrid-presentation-html-challenges]({filename}/images/uploads/slide-from-madrid-presentation-html-challenges.png){: .alignnone .size-medium .wp-image-7460 }]({filename}/images/uploads/slide-from-madrid-presentation-html-challenges.png)

CSS already offers quite some features for print. Unfortunately, some of
these features are described in the CSS standard, but not yet
implemented by web browsers. So whereas methods exist to specify crop
marks and page numbers, they do not work yet. So for these features we
had to find work-arounds. Also the browser generates PDFs in RGB. This
is a problem in print, where either CMYK or spot colours are used. What
we did, finally, is to make two pdfs, for two spot colours.

[![ethertoff-write-read-print]({filename}/images/uploads/ethertoff-write-read-print.gif){: .alignnone .size-medium .wp-image-7460 }]({filename}/images/uploads/ethertoff-write-read-print.gif)

When we organised the Relearn Summer School (more on that later), we
wanted to create a tool where the participants could create a report in
real-time. Again we used the Etherpad, and hooked it up to create a
wiki-like tool called Ethertoff. Participants take notes on the
etherpad, these notes are displayed on a webpage, and the participants
can also edit the stylesheet that determines the layout of the webpage.
So writing notes and doing design can happen at the same time. All these
webpages then, are printed by the browser and collated together to form
a PDF for a print publication.

[![relearn\_publication\_11]({filename}/images/uploads/relearn_publication_11.png){: .alignnone .size-medium .wp-image-7460 }]({filename}/images/uploads/relearn_publication_11.png)

Here we see a spread from the book—What is funny that the challenge here
wasn’t as much on a technological level, but on the level of the
organisation of the process. The creation of a book is often
sequentially ordered: the text is written, the text is edited, the
lay-out is created, the proofs are checked, the designer finishes the
lay-out before it goes to the printer. In this case, everything:
writing, editing and designing—happened all the time. We could also
print at any time.

So how to manage this process? That becomes the challenge.

[![relearn\_publication\_04]({filename}/images/uploads/relearn_publication_04.png){: .alignnone .size-medium .wp-image-7460 }]({filename}/images/uploads/relearn_publication_04.png)

Here you see a detail where we see the different people who participated
in writing this text.

[![Who owns
culture?]({filename}/images/uploads/txt-who-owns-culture.png){: .alignnone .size-medium .wp-image-7460 }]({filename}/images/uploads/txt-who-owns-culture.png)

A designer does not just work with technological tools: the main
material is the visual culture a designer continuously appropriates and
re-uses and invents upon.

A culture, as such, is by its very definition communally owned, but the
artefacts that make up culture are private property. Copyright is a
right, it is accorded by default, no need to claim it. You just have to
make a ‘creative act’, and the product of your creative act is protected
by copyright until 70 years after your death, automatically. It is only
after that period, that the work is considered to become again a part of
the commons: the public domain, free for everyone to use.

[![SANS\_GUILT]({filename}/images/uploads/SANS_GUILT.png){: .alignnone .size-medium .wp-image-7460 }]({filename}/images/uploads/SANS_GUILT.png)

At the 71^st^ anniversary of the death of Eric Gill, OSP released three
typefaces, all versions of his famous Gill Sans.

One is based on drawings of Gill—juridically, that is fine. Another on
one is based on scans of lead. The last one is based on the digital
Monotype version that everyone with a Mac can find on their computer.

Typefaces have traditionally been hard to protect under copyright,
especially in American courts, because American judges figured that
making a typeface is not a ‘creative act’. So typeface companies found a
way around this: the form of the typeface might be hard to copyright,
the code, the coordinates that makes up the digital typeface is
copyrightable: it is considered a writing.

So what we did, is that we converted the font into bitmap, so that the
coordinates information of the curves would get lost. Then we traced the
outlines, so we would get another representation of (more or less) the
same curves.

We wrote a letter to Monotype, explaining what we’ve done—their lawyer
wrote us back, asking for our address and we never heard from them
since.

[![Generous
sharing]({filename}/images/uploads/txt-generous-sharing.png){: .alignnone .size-medium .wp-image-7460 }]({filename}/images/uploads/txt-generous-sharing.png)

We are Open Source Publishing so obviously we use Open Source software,
but we also try to open up our own production. For us it is important to
share generously, even if it not always easy to do so. How can we
actually invite others to use the designs and tools we have created? How
do we communicate our process?

Software comes with a manual to explain how it works. But in fact there
is also a manual within the code, describing how it is structured and
how it works. When you have access to the source code, as you do with
Free and Open Source software, you can read all the ‘comments’: lines of
code that are not meant for the machine to execute but for humans to
read. And the files produced by your software, you can open them up as
well.

[![Welcoming data
formats]({filename}/images/uploads/txt-welcoming-formats.png){: .alignnone .size-medium .wp-image-7460 }]({filename}/images/uploads/txt-welcoming-formats.png)

[![edit\_glif]({filename}/images/uploads/edit_glif.png){: .alignnone .size-medium .wp-image-7460 }]({filename}/images/uploads/edit_glif.png)

We prefer to use text-based data-formats. Normally data might be stored
in arbitrary binary encodings that fit great in the binary way of
thinking of a computer but are illegible to humans. In text-based
formats your image, your glyph remains text so you can see and
understand what is stored in the file. On the image you see a letter
drawn, and its corresponding text in a UFO font. UFO is an open,
text-based format developed by your teachers Erik van Blokland and Just
van Rossum. Together with Tal Leming. This UFO font, you can start to
modify it through a text-editor. Who needs design software?

Of course every format brings its own vocabulary; but as it remains text
you can more easily study the data and its structures. It also allows
for a more durable storage of your data. Rosa Menkman noticed this how a
file of a three year old version of Pages might not work anymore. With a
text-based formats, you would be able to open it up yourself.

Open Source is not subject to the same logic of programmed obsolescence
as commercial software. Rosa also mentioned what a wonderful piece of
software Quicktime Pro is. We agree. But it doesn’t run anymore on
today’s computers, because the company that owns the program has another
program that fills Quicktime’s niche (Quicktime X). So they have no
commercial interest in keeping it alive. Just like the vector drawing
software Freehand was killed off by Adobe once it acquired
Macromedia—because it was competition to Adobe’s Illustrator product.
You can run Freehand on your old Macintosh, but there is no version that
works on current computers. Were it to be Open Source, the community
could have adapted it to work on current computers.

There are certain Open Source softwares that date back to the 1970ies
that are still in use on computers today.

[![osp-website-portrait]({filename}/images/uploads/osp-website-portrait.png){: .alignnone .size-medium .wp-image-7460 }]({filename}/images/uploads/osp-website-portrait.png)

To collaborate we use a tool called Git. Git is a software running on a
server making sure every contributor to a project has the same version
of a file. It also keeps track of the history of a file and this whole
history is also present within every local copy. Every contributor owns
a full copy of a project’s history.

Visual Culture is an interface into this archive. It allows anyone to
explore our projects and their files, we imagine Visual Culture as a
tool that would allow for a more rich collaboration between designers.
Especially for shared graphical objects like typefaces we imagine am
opportunity for a more shared, distributed form of production.

[![Relearn]({filename}/images/uploads/relearn.png){: .alignnone .size-medium .wp-image-7460 }]({filename}/images/uploads/relearn.png)

Relearn is a summer-school we’ve organised in the past two years. As a
lot of OSP members are also teachers they try to spread the open-source
culture also in traditional art academies. This doesn’t always work out
as we’d like, because a school structure comes with so many roles and
hierarchies already established. That is why we started our own
initiative. Almost everybody who participated in Relearn was either a
student or a teacher in their daily live, but here we come together as a
group of people coming together who want to learn. And we find out the
best way to learn.

The Free and Open Source software development is inspiring when it comes
to education. Firstly, the exchange of knowledge happens mostly outside
of traditional educational institutions and art institutions. There is
an awful lot of self-organising involved. Secondly, the participants
display a surprising agility in moving between roles. A mentor on one
project can be an apprentice on another project.

For the organisation of Relearn we tried to keep the agility in mind.
The event is organised in week long workshops, a.k.a. tracks each coming
with their own themes and questions. These tracks are prepared by
‘consciences’ who set the frame and keep the exchange of knowledge
flowing. Participants are also encouraged to jump track during the week.

In closing, we would like to come back to Jonas Staal’s lecture. He
raised the question if we should be content with the role of the artist
as the one who holds mirrors and raises questions. In Jonas’ view,
artists can take an active role in facilitating social change.

In our case we don’t see ourselves as (just) questioning the culture of
Free and Open Source—we are actually participants. Because we have an
a-typical profile for typical Open Source developers, we do get
confronted with real problems in this subculture: sexism and scientism,
among other things. And we like to think, that in our way, we contribute
to re-thinking this subculture. But we do not do so as questioning
outsiders anymore—we have just as well become a part of this culture.

It is the question what happens when you get interested in a subculture
you initially are not part of. Today’s artists and designers often
position themselves as amateur-anthropologists. Fandom, religion,
activism, all are alluring because their practitioners have a sense of
conviction and purpose artists can no longer easily claim for
themselves. So the artist comes into the community, makes work about it,
maybe even collaborates with community members. Yet the artist’s reflex
might be to keep a safe distance. To keep the position of the outsider.
And then report back from the comfort of the gallery.

In Graham Greene’s ‘the Quiet American’, the protagonist, a journalist,
tries to keep his distance from the political turmoil around him in
Indochine. Yet he finds out he is in over his head already, a
participant whether he wants it or not. ‘Sooner or later’, the local
resistance leader tells him, ‘one has to take sides’.
