Title: OSP + Scribus = Prizewinning Design!
Date: 2009-06-05 20:36
Author: OSP
Tags: News, Awards, Libre Fonts, Printing + Publishing, Scribus
Slug: osp-scribus-prizewinning-design
Status: published

[![dsc03255]({filename}/images/uploads/dsc03255.jpg "dsc03255"){: .alignright .size-medium .wp-image-2891 }](http://gallery.constantvzw.org/main.php?g2_itemId=26159&g2_page=2)

Yesterday OSP received a [Plantin-Moretus prize
2009](http://www.plantinmoretusprijzen.be/?p=deprijs) for best designed
book in the category non fiction. The jury about
[Cross-over](http://www.lannoo.be/Content/lannoo/wbnl/listview/1/index.jsp?titelcode=14298&fondsid=16):
"*It was love at first sight*". The book, published by BAM and Lannoo
and edited by Liesbeth Huybregts, uses Libre Fonts and is produced with
Scribus and many other F/LOSS tools. We think it is probably the first
publication made in Free Software crowned 'best designed book' and we
are sure it is not going to be the last!

The prize is named after [Plantin
Moretus](http://en.wikipedia.org/wiki/Plantin-Moretus_Museum), a 16th
century printing and publishing house in Antwerp renowned for its high
quality cartography, typography and engraving.

OSP feels honoured :-)

\[[more
pictures](http://gallery.constantvzw.org/main.php?g2_itemId=26159)\]
