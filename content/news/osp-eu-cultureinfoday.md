Title: OSP @ EU Info Day Culture
Date: 2007-10-10 16:40
Author: Femke
Tags: News, Libre Fonts
Slug: osp-eu-cultureinfoday
Status: published

![eu.jpg]({filename}/images/uploads/eu.jpg)

Looking for ways to fund an European project on Free Fonts... OSP
participated in the Info Day on cultural grants, organised by the
European Commission in Brussels.
