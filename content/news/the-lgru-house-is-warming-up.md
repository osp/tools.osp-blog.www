Title: The LGRU house is warming up!
Date: 2012-02-21 21:44
Author: OSP
Tags: News
Slug: the-lgru-house-is-warming-up
Status: published

How can we re-imagine lay-out from scratch? What tools do we need to
support decentralized collaboration? How can we bring together canvas
editing, dynamic lay-outs, web-to-print and Print On Demand in more
interesting ways?

These are the questions developed at the Libre Graphics Research unit
[Co-position
meeting](http://lgru.net/archives/276 "LGRU Co-Position research meeting program").
The [LGRU](http://lgru.net/ "Libre Graphics Research Unit") is a
research project developed by a number of European Media Art
institutions, inspired in part by the practice of OSP.

The Variable house fills progressively with guests that will develop
those questions the next four days! As a pre-meeting, our friends Ana
and Ricardo have lead a colorfont workshop. Many of us will also take
part in the meeting, presenting and managing work sessions.

Saturday, the 25th of February, between 13:30 and 15:30 at [Halles de
Schaerbeek](http://www.halles.be/), we conclude with a presentation of
prototypes and discussion chaired by Angela Plohman (Baltanlabs,
Eindhoven). With : Ana Carvalho, Stéphanie Vilayphiou, Camille Bissuel,
Antonio Roberts and many others.

[![]({filename}/images/uploads/IMG_7898.jpg "IMG_7898"){: .alignnone .size-medium .wp-image-7074 }](http://ospublish.constantvzw.org/news/the-lgru-house-is-warming-up/attachment/img_7898)

[![]({filename}/images/uploads/IMG_7895.jpg "IMG_7895"){: .alignnone .size-medium .wp-image-7074 }](http://ospublish.constantvzw.org/news/the-lgru-house-is-warming-up/attachment/img_7895)

[![]({filename}/images/uploads/IMG_7888.jpg "IMG_7888"){: .alignnone .size-medium .wp-image-7074 }](http://ospublish.constantvzw.org/news/the-lgru-house-is-warming-up/attachment/img_7888)

[![]({filename}/images/uploads/IMG_7857.jpg "IMG_7857"){: .alignnone .size-medium .wp-image-7074 }](http://ospublish.constantvzw.org/news/the-lgru-house-is-warming-up/attachment/img_7857)

[![]({filename}/images/uploads/IMG_7845.jpg "IMG_7845"){: .alignnone .size-medium .wp-image-7074 }](http://ospublish.constantvzw.org/news/the-lgru-house-is-warming-up/attachment/img_7845)

[![]({filename}/images/uploads/IMG_7836.jpg "IMG_7836"){: .alignnone .size-medium .wp-image-7074 }](http://ospublish.constantvzw.org/news/the-lgru-house-is-warming-up/attachment/img_7836)

[![]({filename}/images/uploads/IMG_7900.jpg "IMG_7900"){: .alignnone .size-medium .wp-image-7074 }](http://ospublish.constantvzw.org/news/the-lgru-house-is-warming-up/attachment/img_7900)

[![]({filename}/images/uploads/lgru-coposition-25-02-dualcirclespng.png "lgru-coposition-25-02-dualcirclespng"){: .alignnone .size-medium .wp-image-7074 }](http://ospublish.constantvzw.org/news/the-lgru-house-is-warming-up/attachment/lgru-coposition-25-02-dualcirclespng)
