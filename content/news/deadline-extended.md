Title: Deadline extended :-)
Date: 2011-04-03 17:27
Author: OSP
Tags: News, LGM 2011
Slug: deadline-extended
Status: published

<p>
<script language="JavaScript"><br />
TargetDate = "04/20/2011 0:00 PM";<br />
BackColor = "white";<br />
ForeColor = "red";<br />
CountActive = true;<br />
CountStepper = -1;<br />
LeadingZero = true;<br />
DisplayFormat = "%%D%% days, %%H%% hours, %%M%% minutes and %%S%% seconds";<br />
FinishMessage = "It's too late!";<br />
</script>
Good news: You can [submit your
talk](http://create.freedesktop.org/wiki/Category:Conference_Talk_2011_Proposal)
for the Libre Graphics Meeting in Montreal until April 20. It means you
have exactly

<script language="JavaScript" src="http://ospublish.constantvzw.org/blog/wp-content/uploads/countdown.js"></script>
left to tell us what developers and designers gathering at LGM need to
know about.

</p>
See you soon in Montreal!
