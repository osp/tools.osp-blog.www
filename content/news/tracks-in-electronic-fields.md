Title: Tracks in electr(on)ic fields
Date: 2009-11-15 22:09
Author: OSP
Tags: News, books, context, LaTex, Printing + Publishing, Scribus
Slug: tracks-in-electronic-fields
Status: published

Tuesday Mardi 24/11
-------------------

### Booklaunch: Tracks in electr(on)ic fields

**@ Verbindingen/Jonctions 12: *[By Data We
Mean](http://www.constantvzw.org/vj12)***  
[Zennestraat 17 Rue de la Senne, Brussels
Bruxelles](http://constantvzw.org/vj12/spip.php?article16)

<!--more-->  
We are very happy to announce the launch of the long awaited book
*Verbindingen/Jontions 10: Tracks in electr(on)ic fields*. Nearly two
years in the making, this book is the result of our adventures in TeX,
LaTex and ConTeXt plus a cover in Scribus feat. Spiro. The booklaunch
marks the opening of a new edition of [Jonctions/Verbindingen: By Data
We Mean](%20http://constantvzw.org/vj12)  
More news as soon as the books arrive from the printer :-)
