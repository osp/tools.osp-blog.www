Title: Use Cases and Affordances
Date: 2010-12-15 11:01
Author: OSP
Tags: News, Further reading, Libre Graphics Magazine
Slug: use-cases-and-affordances
Status: published

![]({filename}/images/uploads/20101212_033.jpg "20101212_033"){: .alignnone .size-medium .wp-image-5475 }

Enjoying the first issue of Libre Graphics Magazine *First Flight* too?
In the mean time, the editorial team started working on a brand new one!
The theme for the upcoming magazine is *Use Cases and Affordances*:

> Our software tools, in their affordances and potential use cases,
> define for us, to a certain extent, what we may and may not do. Those
> decisions are put in place by the people who design the tools.
> Together, as users, developers and all areas between the two extremes,
> we boil in a constantly reconfiguring sea of use possibilities,
> material and mental affordances.

Interesting :-)

Read the full call here:
<http://www.adaptstudio.ca/blog/2010/12/libre-graphics-magazine-12-call-for-submissions.html>
and don't forget to [order a printed
issue](http://libregraphicsmag.com/buy.html)!
