Title: 1 année de Graphisme, 100% libre
Date: 2007-05-06 23:40
Author: Harrisson
Tags: News, Inkscape, LGM 2007, Scribus, Usability links
Slug: 1-annee-de-graphisme-100-libre
Status: published

[![a\_robin\_logo\_ipsa.png]({filename}/images/uploads/a_robin_logo_ipsa.png)]({filename}/images/uploads/a_robin_logo_ipsa.png "a_robin_logo_ipsa.png")

La conférence d'[Alexandre
Robin](http://blogue.jaunorange.com/index.php?post/2007/05/08/Notre-premiere-annee-de-graphisme-100-libre)
a LGM

[Association internationnale en science
politique.](http://www.ipsa.ca/)  
Design Graphique, administration reseau, dévelopement web, Alexandre à
choisi de travailler en OS plus par goût que par économie.

Une présentation enthousiasmante, menée dans un protocole tout
"corporate" il apparait aussi que le sujet était: "comment j'ai caché à
mes collègues que je faisais toute la communication de la l'entreprise
en Open Source".  
Alexandre a fait son "coming out" (open source) le mois dernier, et la
philosophie Linux est presque comprise par son boss.
