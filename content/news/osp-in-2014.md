Title: OSP in 2014
Date: 2015-07-13 10:51
Author: OSP
Tags: News
Slug: osp-in-2014
Status: published

Last February, OSP officially became an asbl/vzw: a Belgian association
with no lucrative purposes. We are trying to get more structured and
this goes through making precise activity reports. Here is the one for
2014
: [OSP2014.pdf]({filename}/images/uploads/OSP2014.pdf).

![](http://gallery3.constantvzw.org/var/albums/variable%40kriekelaar/IMG_9567.JPG)
