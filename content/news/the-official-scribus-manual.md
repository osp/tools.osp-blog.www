Title: The Official Scribus Manual
Date: 2008-12-29 23:52
Author: OSP
Tags: News, Manual, Scribus
Slug: the-official-scribus-manual
Status: published

[![]({filename}/images/uploads/cover.png "cover"){: .float }](http://www.flesbooks.com/scribus1.3manual/)Just out and
hopefully in the post soon: The Official Scribus Manual! For less than
30 euros, we ordered ourselves 438 pages of printed documentation,
examples and other invaluable info. And at least €12 from each copy goes
towards the Scribus project.

Get yours here:  
<http://www.flesbooks.com/scribus1.3manual/>

<div class="clear">

</div>
