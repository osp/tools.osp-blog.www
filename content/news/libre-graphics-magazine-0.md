Title: Libre Graphics Magazine #0
Date: 2010-06-18 15:10
Author: OSP
Tags: News, LGM, LGM 2010, Printing + Publishing
Slug: libre-graphics-magazine-0
Status: published

[![]({filename}/images/uploads/lgm01-70x100.png "lgm0"){: .float width="70" height="100"}](http://libregraphicsmeeting.org/2010/download/lgmag_00.pdf)During
LGM 2010, [Ana](http://planapress.org/),
[ginger](http://adaptstudio.ca/) and [Ricardo](http://tinkerhouse.net/)
(+ a.l.e and Femke) edited, designed and published *Libre Graphics
Magazine \#0*.

You can download it here:  
<http://libregraphicsmeeting.org/2010/download/lgmag_00.pdf>

<div class="clear">

</div>
