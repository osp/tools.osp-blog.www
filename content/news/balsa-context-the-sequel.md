Title: Balsa : ConTeXt the sequel
Date: 2011-06-25 13:46
Author: ludi
Tags: News, Works
Slug: balsa-context-the-sequel
Status: published

En février Monica et Fabien du Théâtre de la Balsamine sont arrivés chez
[Speculoos](http://www.speculoos.com), et deux heures après ils sont
repartis, sans même vraiment le savoir, avec en plus OSP et
[stdin](http://www.stdin.fr). Pierre a senti d'une part qu'une
hétérogéneïté de pratiques graphiques serait mieux à même de rendre
compte de la tentative d'involution du projet théâtral naissant de la
Balsamine, et d'autre part que, pour une fois, OSP pouvait risquer une
réponse à une demande sans qu'une complicité préalable n'existe avec le
mode expérimental qui lui est inhérent.  
[![]({filename}/images/uploads/osp3-pimped.jpg "osp3-pimped"){: .alignleft .size-medium .wp-image-6564 }]({filename}/images/uploads/osp3-pimped.jpg)  
Nous avons commencé à travailler à partir d'éléments variés, un peu
comme des objets trouvés, dans un essai de se rendre sensible à tout ce
qu'on peut trouver dans un environnement donné, sans trop aller chercher
ailleurs, comme par une espèce de pudeur écologique d'éviter trop de
déplacements ou de dépenser trop d'énergie.  
— Prenons la pince des tableaux de la Renaissance dont Martine parle
avec vibrance, posons-la sur le site actuel avec l'identité de Lisa et
utilisons ce système comme outil de collection d'images à venir.  
— Prenons l'ancien logo de Lucas, démontons-le avec soin, rangeons ses
morceaux de lettres marquées du sceau infâmant des années nonantes
influencées par les années quatre-vingt influencées par les années
trente influencées par l'Arts & Crafts de la fin du siècle passé
influencé par la Renaissance encore, et joignons-y les morceaux
originels de l'amphithéâtre de Valérie, pour en faire une équation du
feed-back, qui semble en boucle mais s'oxygène avec un courant d'air
hérité du plafond troué de la salle des cartes.  
— Prenons les anciennes enveloppes A5 et coupons dedans pour les
utiliser plutôt que les jeter.  
— Prenons les fichiers de montage photo d'Hichem et extrayons-en tels
quels leurs tracés pour en faire une lecture de repérage.  
— Prenons deux polices de caractères techniques et libres dans
lesquelles la présence latine n'est requise que pour les besoins des
traductions japonaises.  
— Prenons comme à chaque fois des logiciels libres et n'utilisons qu'eux
en les combinant pour voir comment ils changeront notre rapport au
dialogue et au processus dont ont parlé Monica et Fabien en entrée de
jeu.  
Voilà où on en est.

[![]({filename}/images/uploads/Screenshot-6.png "Screenshot-6"){: .alignleft .size-medium .wp-image-6564 }]({filename}/images/uploads/Screenshot-6.png)
