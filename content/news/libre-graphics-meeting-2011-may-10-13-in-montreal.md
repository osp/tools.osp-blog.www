Title: Libre Graphics Meeting 2011, May 10-13 in Montreal
Date: 2011-03-19 17:21
Author: OSP
Tags: News, LGM 2011
Slug: libre-graphics-meeting-2011-may-10-13-in-montreal
Status: published

![]({filename}/images/uploads/LGM.png "LGM"){: .alignnone .size-full .wp-image-5964 }

The official announcement for LGM 2011!

→ Submit your talk
[here](http://create.freedesktop.org/wiki/Conference_2011/Submit_Talk)  
→ Support LGM generously [@
Pledgie](http://pledgie.com/campaigns/14610)  
→ More info on the LGM website: <http://www.libregraphicsmeeting.org>

### ☆☆☆☆☆☆☆☆☆☆☆☆☆☆☆☆☆☆☆☆☆☆☆☆

Cutting edge creative tools meet free culture
---------------------------------------------

**May 10-13, Ecole Polytechnique Engineering School**

Programmers and artists from around the world are headed to Montreal for
the 6th annual Libre Graphics Meeting in May. The Libre Graphics Meeting
is an annual gathering for the users and developers of free and open
source creative software: artistic tools that are free for anyone to use
and modify. LGM gives software developers, artists, musicians, designers
and other creative professionals the opportunity to collaborate, share
their work and learn from each other.  
<!--more-->  
**A Preview**  
Every year, Libre Graphics Meeting focuses on individual and group
interaction, emphasizing free media and the software that produces it.
This year’s attendees can expect a similar format that will include
workshops, presentations, demonstrations, and birds-of-a-feather (BOF)
sessions. Leading up to the conference, special lab sessions bring
developers and artists face-to-face to make improvements in creative
software. Gaps can form when developers and their users don’t meet face
to face; LGM gives artists and educators a voice in the building of
their tools. Both content creators and developers will present, bringing
a diverse community together to advance the technologies and strategies
of high-quality free creative software.

**Submit a talk!**  
LGM’s strength comes from the presenters who talk about their work and
inspire others by sharing their experiences, techniques, and best
practices. Whether you are a developer, an artist, or an enthusiast, you
can be a part of this year’s event by sharing your story and your work.

The LGM community wants to hear your proposal for a session. Submit your
idea any time between now and April 1, 2011 by visiting
http://create.freedesktop.org/wiki/Conference\_2011/Submit\_Talk
