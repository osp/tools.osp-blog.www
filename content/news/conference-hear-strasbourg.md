Title: Conférence @ HEAR Strasbourg
Date: 2015-03-10 23:25
Author: Colm
Tags: News
Slug: conference-hear-strasbourg
Status: published

[![IMG\_20150310\_102254]({filename}/images/uploads/IMG_20150310_102254.jpg){: .alignnone .size-medium .wp-image-7353 }]({filename}/images/uploads/IMG_20150310_102254.jpg)

### Ramène ta fraise!

We're currently giving a
[html2print](http://osp.kitchen/tools/html2print/) workshop
[@HEAR](http://www.hear.fr/) in Strasbourg that we're punctuating with a
conference presentation of OSP's work tomorrow evening in the main hall
@18:00.

Come on over, ramène ta fraise!

Results of the html2print workshop on their way and in construction, for
now know that we're reactivated *automatic frog* so meet us on
[Github](https://github.com/osp/osp.tools.html2print).

[![Screenshot from 2015-03-10
22:16:02]({filename}/images/uploads/Screenshot-from-2015-03-10-221602.png){: .alignnone .size-medium .wp-image-7353 }]({filename}/images/uploads/Screenshot-from-2015-03-10-221602.png)
