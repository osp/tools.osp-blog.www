Title: Standards and their Stories
Date: 2010-09-01 08:36
Author: Ivan
Tags: News, Further reading, Standards + Formats, SVG
Slug: standards-and-their-stories
Status: published

I'm looking forward to the stories that may be unfolding these days here
at [SVG Open](http://svgopen.org/).

\[openbook booknumber="/b/OL16960548M"\] Here are some quotes from
chapter 1 of "Standards and their Stories", edited by Martha Lampland
and Susan Leigh Star. Among other things, this introductory chapter
makes a point for the invisibility and pervasiveness of standards :)

<div class="clear">

</div>

*standards are so pervasive that they have become taken for granted in
our everyday environment, they may become completely embedded in
everyday tools of use.*

*We have to listen to infrastructure and bring imagination to
understanding its components and how they work.*

*With time, this process can lead to what Callon calls "irreversibility"
\[...\] functional irreversibility--what would it take to change the
meaning of a red light to "go" and a green light to "stop"?*

*The strangeness of infraestructure is not the usual sort of
anthropological strangeness \[...\] Infrastructural strangeness is an
embedded strangeness, a second-order one, that of the forgotten, the
background, the frozen in place.*
