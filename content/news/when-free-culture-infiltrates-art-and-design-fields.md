Title: When Free Culture infiltrates art and design fields...
Date: 2010-12-05 22:59
Author: Stephanie
Tags: News, Design Samples
Slug: when-free-culture-infiltrates-art-and-design-fields
Status: published

We are always thrilled when we see design projects around free culture.
Lately the blog [Manystuff](http://manystuff.org) has enlightened a lot
of this kind of projects, either on/with Open Source or around free of
charge culture, public domain...

Here is a summary:

-   [Free Kevin](http://www.freekevin.info/) — A Hacker screening series
    — Can't find the torrent link anymore...
-   General Public Library — 
    <http://www.artingeneral.org/exhibitions/504> and
    <http://www.generalpubliclibrary.info/> : Nguyen invited designers,
    publishers, curators, artists, galleries, and musicians to
    contribute publications to the project that reflect the donor’s
    practice, methodology, inspiration and interest. Visitors are
    encouraged to donate a favourite book to the library during
    the exhibition. — Although the project shares the same acronym as
    GNU's General Public Licence, licenses are not the subject here. But
    the project has the merit to gather projects closer to this
    philosophy: aaaarg, Free Kevin...
-   [Morf](http://www.nijhoflee.nl/selection/morf/) — A magazine for
    graphic design (in Dutch) — Free of charge for students, paying
    for non-students. Republication/translation of major articles
    together with new articles.
-   [The Dor](http://www.the-dor.org) — The Dor is an ever-growing
    digital archive of out of print artist books. Eventually, it will
    culminate in an online database of hard to find artists books, which
    users may page through and print on demand. Currently, it finds form
    as a temporary office, where loaned titles are scanned, documented,
    and sometimes reprinted. Visitors may also contribute to this
    ever-growing library of titles. The project is made possible by
    collaborations with book collectors, artists, curators and friends.
-   [Sexy Machinery](http://www.sexymachinery.com/) — Architectural
    magazine as a way of exploring issues of copyright and ownership
-   [Open Source Digest](http://www.varv.org/osd.html) — Compilation of
    texts from the public domain. But if this digest is open source,
    where *are* the sources? ;)
-   [A Wikipedia
    Reader](http://www.asdfmakes.com/projects/a-wikipedia-reader/) —
    CC-BY-NC-SA — Artists are asked to make a *parcours* of reading
    in Wikipedia.
-   [Free](http://www.newmuseum.org/free) — Exhibition "explor\[ing\]
    how the internet has fundamentally changed our landscape of
    information and our notion of public space." A lot of material
    around this exhibition is available online, with long essays. Looks
    much more interesting than the introductory speech confusing free as
    in freedom and free as in free beer. Hopefully, during the
    exhibition, artist [Steve
    Lambert](http://visitsteve.com/work/performance-lecture-at-new-museum/)
    made a speech/city-walk about the history of free culture as in
    freedom and/or as in free beer (punk rock culture, Free
    Software history...). No copyright/left mentions though, and without
    any notice it is "all rights reserved"...
-   [Re-Applied
    Art](http://head.hesge.ch/-Dexter-Sinister-Re-Applied-Art,745-#IMG/jpg/Frances_Stark-2.jpg)
    — Workshop by Dexter Sinister asking the participants to re-use a
    hundred items from *Dot Dot Dot* magazine archive.
-   [Agreement to Receive & Exchange this
    Book](http://www.ryanweafer.com/#765534/Agreement-to-Receive-Exchange-This-Book)
    — An attempt at an alternative currency during an art book fair.

We can still see some confusion in some of this project about what is
*free* or *open source*, but we welcome these initiatives. Hope this
list will grow faster and faster!
