Title: I want a green apple
Date: 2006-12-11 01:58
Author: Femke
Tags: News, Hardware
Slug: i-want-a-green-apple
Status: published

Well, technically not open-source related... but I think this campaign
deserves all designers' attention. In a nice mock of Apple's iLife
design + writing, Greenpeace reminds us that not all that is wireless
comes without footprint. Calling all bloggers, taggers, social
bookmarkers and other *cool* people to the rescue, Greenpeace thinks
they might convince Apple to change their policy on toxic waste,
production and short product cycle:

[![apple150x60.jpg]({filename}/images/uploads/apple150x60.jpg){: #image161}](http://www.greenmyapple.org/)

[**http://www.greenmyapple.org/**](http://www.greenmyapple.org/)

More in-depth information on computers and toxic waste by the [Sillicon
Valley Toxic Waste Coalition](http://svtc.etoxics.org/).
