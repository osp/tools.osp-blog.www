Title: Palais Châlet
Date: 2009-01-02 16:18
Author: Harrisson
Tags: News, Texts Music
Slug: palais-chalet-2
Status: published

[![]({filename}/images/uploads/reverbtouchante1.jpg "reverbtouchante1"){: .alignnone .size-full .wp-image-1623 }]({filename}/images/uploads/reverbtouchante1.jpg)

16 Janvier  
Liège, Belgium  
**Palais Châlet**  
*spécial de soutien*

**- Révérberations Touchantes -**  
With

Live  
**Bruno Coeurvert  
Pierre Normal**

Dj's  
JB from Paris, Athome, Atka, Le Caniche Noir, Le Diamant Tendre

20h, Rue chauve-souris 62, Liège, 2 euros.

Feel welcome to spend this lovely, romantic and supporting night. Where
OSP crew meet, where they party, where they hear their favorite music,
and where they they dance.

This party is a support to the 11nth of November arrested people.  
"L’arrestation et l’inculpation le 11 novembre de neuf personnes, sous
couvert de lois d’exception, terrorisantes, qui semblent n’avoir d’autre
raison que de maintenir, par la violence, au pouvoir ceux qui les ont
mis en place, nous concerne tous."

[Link to the support comitees](http://www.soutien11novembre.org/)
