Title: cœur + œuf
Date: 2009-11-11 20:08
Author: Femke
Tags: News, Tools, Bug reporting, Recipes, Scribus
Slug: cur-uf
Status: published

![P1060803]({filename}/images/uploads/P1060803.jpg "P1060803"){: .alignright .size-medium .wp-image-3585 }  
<small>Yi and Femke consider cooking heartless vegan food from now
on</small>

We're correcting the second edition of the [Puerto
Cookbook](http://ospublish.constantvzw.org/news/cookbook-launch) (first
edition sold out!) and stumble over a trivial but frustrating small bug
in Scribus. Maybe too long to explain here (you're welcome to read [our
bug report](http://bugs.scribus.net/view.php?id=8577)), but it means a
lot of scrolling back and forth through the Unicode chart to locate the
obligatory œ ligature and correct 'heart' (cœur) and 'egg' (œuf) in
French.
