Title: Les enfants du transparent
Date: 2009-05-21 16:44
Author: Ivan
Tags: News, Archiving, Handmade, Scribus, Thoughts + ideas
Slug: les-enfants-du-transparent
Status: published

![overheads]({filename}/images/uploads/overheads.jpg "overheads"){: .alignnone .size-medium .wp-image-2755 }

These days i'm in Malmö for [The Art of the
Overhead](http://overheads.org) festival.

For some years now, this art festival has done media archeology about
the [overhead
projector](http://en.wikipedia.org/wiki/Overhead_projector). There was a
workshop/space this week, there will be performances this weekend, there
will be a psychedelic closing party next weekend, etc.

We're happy to see that the
[print](http://webzone.k3.mah.se/k3krga/projects/oh/PDF/OH-PROGRAMME2009.pdf)
for the
[festival](http://webzone.k3.mah.se/k3krga/projects/oh/PDF/CALLforOH2009.pdf)
was done with free software: scribus, inkscape, gimp, openoffice...

From the festival's documents:  
*The reason for why we used simple overhead projectors was because of
their open surface, where you could directly work with your materials.*

*Over a two-week period, the old school overhead projector is
re-activated as a powerful instrument for projecting alternative visions
of our contemporary new media culture.*
