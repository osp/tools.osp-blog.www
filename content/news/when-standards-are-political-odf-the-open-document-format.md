Title: When standards are political
Date: 2006-10-24 15:15
Author: Femke
Tags: News, Texts Standards + Formats
Slug: when-standards-are-political-odf-the-open-document-format
Status: published

[James Love](http://www.cptech.org/) just posted this interesting report
on Nettime:  
**  
When standards are political -- ODF (the Open Document Format)**  
Yesterday I attended a meeting hosted by TACD at Harvard's Berkman
Center about a very important issue -- one that is both highly technical
and political at the same time -- the battle over the Open Document
Format (ODF).

(See links: <http://www.cptech.org/a2k/odf/odfwkshop20oct06/>, [http://  
www.tacd.org,
http://en.wikipedia.org/wiki/OpenDocument](http://%3Cbr%20%3E%3C/a%3E%20www.tacd.org,%20http://en.wikipedia.org/wiki/OpenDocument))

The technical part concerns what ODF is -- an open specification for the
formats of common documents such as those created by word processors,
spreadsheets and presentation graphics programs. The political part
concerns what ODF represents -- an end to the Microsoft monopoly in
desktop applications that are used to author and manage these
documents."

<!--more-->  
Estimates vary, but Microsoft probably controls somewhere between 90 to
95 percent of the market for word processing, spreadsheet and
presentation graphics programs. This means people use Microsoft software
to create these documents, and also to store data. The source of
Microsoft's monopoly is control over file formats, in a world where data
needs to be shared.

Lots of companies or even free software communities can create  
software to do these common tasks. Corel's WordPerfect office suite,  
Apple's iWorks, the OpenOffice.Org, and doc.google.com are just a few  
examples of "competitors" to Microsoft office, each controlling a  
tiny part of the small non-Microsoft market share. But as we all  
know, we need to exchange data. With everyone using email and the  
web, we need to consider if others can read our documents, and if we  
can read what we receive from others.

By failing to document their own (periodically modified) file  
formats, and not supporting the file formats of competitors,  
Microsoft has been able to create a very compelling reason to buy,  
and buy again, Microsoft software. Documents created in (current  
versions) of Microsoft's software are the best way to read documents  
other people create using Microsoft's software. So long as everyone  
uses a reasonably current version of Microsoft's software, everything  
more or less works.

You can try to avoid using Microsoft -- but at price. Documents might  
not look right. Sometimes the differences are small -- but sometimes  
they are almost unusable. For this reason, most of the entire  
computer using world now relies upon software from Microsoft. Other  
companies don't even bother to invest in competing products. There is  
very little choice or innovation in this product space.

Some people say this is inevitable, but of course, this is not true.  
The lack of interoperability is deliberate -- the linchpin of  
Microsoft's monopoly. But if the public could embrace an open format  
for documents, the outcome would be much different. There would be  
more competition, more innovation, better products, cheaper prices,  
etc. And there is a highly relevant example -- the web.

Web pages are build upon the foundation of open format - called HTML  
- for hypertext mark-up language. The standards for HTML are  
determined by the World Wide Web Consortium - which is not controlled  
by any one company. The formats are open, well documented, and  
designed to work with different software and hardware. It has  
probably been the most influential and important data standard in the  
history of publishing.

There are now thousands of high quality and innovative tools to  
author web pages. Microsoft offers a few, but they were never able to  
establish a significant market share. Indeed, there is no "leading"  
tool for creating web pages. Instead, there is an astonishing variety  
of methods of doing so - ranging from bare bones text based html  
editing tools to incredibility easy to use blogging software -  
offered by a variety of companies, free software projects or even  
individuals.

The "Open Document Format" (ODF) effort has been led by a large group  
of non-Microsoft software companies that are seeking to level the  
playing field for software tools to author and manage text, data and  
graphics. It is pretty new, only having been approved by ISO/IEC on  
May 8, 2006. So far, only a handful of products support ODF,  
including the much improved free software office suite called  
OpenOffice.Org, the online program docs.google.com, and some Linux  
only applications. Apple, Corel and Microsoft have yet to suport ODF.

A handful of thoughtful government officials are trying to require  
software vendors, including Microsoft, to use this new open standard,  
in order to achieve a number of important public policy objectives,  
including:

\* More competition among suppliers of software,  
\* Improved ability to manage archives of data,  
\* Enhanced ability to use and re-purpose data contained in documents.

The State of Massachusetts and the government of Belgium and Denmark  
have already put in place requirements that ODF be supported by  
software companies, and now other governments are beginning to  
consider similar initiatives. If they succeed, it could result in a  
revolution in the structure of the entire software market, and bring  
much needed competition and innovation to these important areas.

Next year Microsoft will try to sell the public on it's latest file  
format -- "Open XML", which they are marketing as a "competitor" to  
ODF as an "open" data format. Open XML was described by one expert as  
a standard that only Microsoft could implement - similar to a job  
description custom made for a single job applicant.

Next month in Athens, Greece, at the new "Internet Governance Forum,"  
there will be proposals for global norms to support open standards  
for key aspects of information technologies, including but not  
limited to data formats. Many people are nervous about these issues,  
because Microsoft is investing millions to defeat them, and to attack  
personally government officials who Microsoft sees as too friendly to  
open standards, and to reward politicians and government officials  
who back Microsoft.

This battle, which is often very difficult to follow at the level of  
the technical details, is quite important. For years we have  
tolerated the manipulation of data formats to maintain a monopoly  
that has imposed all sorts of costs of society, in terms of high  
prices, lack of innovation and poor quality software. One only needs  
to compare the innovation seen on web publishing to the dearth of  
innovation you see on the computer desktop. If ODF succeeds now,  
Microsoft will have to compete on the basis of prices and quality -  
rather than by being the only product that will not mangle a  
document. That should be a good thing for everyone in the long run.

State and federal government agencies should be asked to require that  
software vendors support ODF.
