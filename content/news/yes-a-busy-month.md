Title: yes, a busy month
Date: 2009-05-05 19:08
Author: OSP
Tags: News, Not-Courier sans, Webdesign, Website
Slug: yes-a-busy-month
Status: published

\[caption id="attachment\_2533" align="alignright" width="400"
caption="NotCourierSans in use (Nova Imaginary Property
programme)"\][![NotCourierSans in use (Nova Imaginary Property
programme)]({filename}/images/uploads/nova.jpg "nova"){: .size-medium .wp-image-2533 }]({filename}/images/uploads/nova.jpg)\[/caption\]4
+ ½ OSP's are on their way to Montreal, Canada where they will
participate in the [Libre Graphics
Meeting](http://www.libregraphicsmeeting.org/2009/) from **6-9 May**
(expect more news as soon as they land). Next Saturday **May 9**, a
special session of [Open Source Open
Course](http://www.multimedialab.be/blog/?cat=46) in the context of
[Journées du Libre](http://journeesdulibre.bxlug.be/) with amongst
others Michel Cleempoel, Stéphane Noël, Olivier di Stefano and of course
Marc Wathieu. OSP reports from the [Pixel/Print
conference](http://wdka.hro.nl/news.asp?articleid=810#lyjQg5fJDo88WgFOyrn0J6gp-0)
in Rotterdam **12-13 May** while on **May 14** Cinema Nova's [Imaginary
Property](http://www.nova-cinema.org/index.php?page=prog/112/00cover.fr.htm)
kicks off with a presentation by the Parisian editors and Scribus
champions [Le
Tigre](http://www.le-tigre.net/A-Bruxelles-autour-du-14-mai.html/). Many
more things to see and experience there but don't miss the [OSP
Screencast](http://constantvzw.org/site/spip.php?article834) on **28
May**. But not before on **May 15** [the GenderArtNet
project](http://ospublish.constantvzw.org/news/gender-art-net) has been
launched on line! May ends in style with an edition of [Palais
Châlet](http://www.pneu.org) on **May 29** in B32 artspice, Maastricht
and the distribution of the last leftovers (if any) from the [Take Away
Archive](http://ospublish.constantvzw.org/news/take-away-archive) on
**30 + 31 May**.
