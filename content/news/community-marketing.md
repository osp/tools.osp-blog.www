Title: Community marketing?
Date: 2006-02-27 18:17
Author: Femke
Tags: News, Thoughts + ideas
Slug: community-marketing
Status: published

On <http://www.spreadfirefox.com/> Firefox "Calls All Firefox Fans" to
work on posters, leaflets, e-cards, websites and videoclips in an
attempt take more market share away from Internet Explorer. One of their
most popular campaigns I've posted here, because it replicates *so* many
cliches about women and computers, that it makes me seriously doubt this
"community" approach:

[![Femfox](http://ospublish.constantvzw.org/blog/wp-content/_femfox.jpg "Femfox"){: }](http://ospublish.constantvzw.org/blog/wp-content/femfox.jpg)  
<http://www.femfox.com>

<!--more-->  
On fora and blogs I find numerous people discussing whether this is the
right campaign for Firefox (at least only partially busy asking if
targeting men more than women is actually helpful, and whether it's
possibly "pornographic" character would harm the American "market").

Funny alternatives to headless women in lace start to circulated too:

[![firemalefox](http://ospublish.constantvzw.org/blog/wp-content/_firemale.png)](http://ospublish.constantvzw.org/blog/wp-content/firemale.png)  
*\[Firefox blocks unwanted pop-ups\]*

But still... do-it-ourselve publicity using conventional marketing
strategies as the model and measure of success? Why is it so hard, after
you have reinvented the software, to reinvent software marketing too?
