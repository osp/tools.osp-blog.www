Title: Hachures Tournures Balsamine Print Party
Date: 2014-09-29 15:55
Author: Colm
Tags: News, OSP Public Meets, Print Party, Works, Balsa, Balsamine, Bits and Sounds, Hachures Tournures, hellocatfood, Nos Petites Madeleines, Print Party, Quizaine Numérique, Seoul Fonts Karaoke
Slug: hachures-tournures-balsamine-print-party
Status: published

We're happy to be having the next Print Party in the familiar Balsamine
Foyer for the second year in a row!

![](http://balsamine.be/uploads/Saison2014-2015.Madeleines-hachures-tourneurs/grenouille){: .alignnone }

This year as part of a new[seasonal
program,](http://balsamine.be/index.php/Saison2014-2015/Programmation)
but also smaller parallel program [«Nos Petites
Madeleines»](http://www.laquinzainenumerique.be/programmation/nos-petites-madeleines.htm)
and along with the 15 day long [Quizaine
Numérique](http://www.laquinzainenumerique.be/) we're going to be
presenting [Hachures
Tourneurs](http://balsamine.be/index.php/Saison2014-2015/Madeleines-hachures-tourneurs).

Following the path started with the [Seoul Fonts
Karaoke](http://ospublish.constantvzw.org/blog/news/seoul-fonts-karaoke),
continued with the [Bits and
Sounds](http://relearn.be/r/programme::worksessions.html) worksession at
Relearn 2014, OSP is going to scratch its sonic itches once more with
the re-thread of this (appropriately musical) years [visual material and
program.](http://osp.constantvzw.org/work/balsamine.2014-2015/)

We're very pleased to have [Antonio
Roberts](http://www.hellocatfood.com/) on board with us for this event;
as a glitch artist, it is in Antonio's nature to take things apart, and
put them back together again, purposely in the wrong order, to see what
will happen. Exactly what we need to bridge graphic design to audio.

[![hachures-tourneurs\_invitation]({filename}/images/uploads/hachures-tourneurs_invitation.png){: .alignnone }]({filename}/images/uploads/hachures-tourneurs_invitation.png)

The gang will do its best to convert as much of the program into sounds,
then tracks, then mix them all together, we're expecting it to be loud,
at the very least.

October 18th 17:30

Théâtre de la Balsamine - Foyer

See you there!
