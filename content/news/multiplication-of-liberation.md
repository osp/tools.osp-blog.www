Title: Multiplication of liberation
Date: 2007-09-17 22:11
Author: Harrisson
Tags: News, Works, Design Samples, Libre Fonts
Slug: multiplication-of-liberation
Status: published

OSP is testing Liberation Mono in an invitation for an exhibition in
Brussels:

[![multi\_plier\_lib1.png]({filename}/images/uploads/multi_plier_lib1.png)]({filename}/images/uploads/multi_plier_lib1.png "multi_plier_lib1.png")

(Inkscape, Gimp and Scribus)
