Title: Quelques surprises polonaises
Date: 2008-06-18 14:10
Author: OSP
Tags: News, Print Party
Slug: quelques-surprises-polonaises
Status: published

`            _          _`  
`           | | o      | |                  o`  
`   _   __  | |     ,  | |         _   ,_       _  _  _|_`  
` |/ \_/  \_|/  |  / \_|/ \      |/ \_/  |  |  / |/ |  |`  
` |__/ \__/ |__/|_/ \/ |   |_/   |__/    |_/|_/  |  |_/|_/`  
`/|                             /|`  
`\|                             \|`  
`   _   __,   ,_  _|_`  
` |/ \_/  |  /  |  |  |   |`  
` |__/ \_/|_/   |_/|_/ \_/|/`  
`/|                      /|`  
`\|                      \|`

### vendredi le 27 juin 2008

OSP est très heureux de vous annoncer le lancement de sa première fonte
en licence libre: la
[NotCourier-sans](http://ospublish.constantvzw.org/?p=471).

Nous serions ravis de vous retrouver autour d'un live lay-out, des
Jiaozi/Pierogi et de quelques surprises (polonaises) lors de cette Print
Party qui aura lieu à:

**La Compilothèque**  
vendredi le 27 juin 2008 à 20h00  
50, Quai des Péniches,  
1000 Bruxelles  
(Metro Yser)
