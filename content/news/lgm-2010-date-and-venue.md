Title: LGM 2010: date and venue
Date: 2009-07-14 16:56
Author: OSP
Tags: News, LGM 2010
Slug: lgm-2010-date-and-venue
Status: published

**27 - 30 May 2010 in De Pianofabriek, Brussels**

[![Plogger
Image](http://ospublish.constantvzw.org/image/plog-content/thumbs/lgm2010/lgm-locations/large/1159-p1060545.JPG)](http://ospublish.constantvzw.org/image/index.php?level=picture&id=1159)
[![Plogger
Image](http://ospublish.constantvzw.org/image/plog-content/thumbs/lgm2010/lgm-locations/large/1167-church.JPG)](http://ospublish.constantvzw.org/image/index.php?level=picture&id=1167)
[![Plogger
Image](http://ospublish.constantvzw.org/image/plog-content/thumbs/lgm2010/lgm-locations/large/1169-cour.JPG)](http://ospublish.constantvzw.org/image/index.php?level=picture&id=1169)
[![Plogger
Image](http://ospublish.constantvzw.org/image/plog-content/thumbs/lgm2010/lgm-locations/large/1170-expo.JPG)](http://ospublish.constantvzw.org/image/index.php?level=picture&id=1170)
[![Plogger
Image](http://ospublish.constantvzw.org/image/plog-content/thumbs/lgm2010/lgm-locations/large/1171-fortstraat.JPG)](http://ospublish.constantvzw.org/image/index.php?level=picture&id=1171)
[![Plogger
Image](http://ospublish.constantvzw.org/image/plog-content/thumbs/lgm2010/lgm-locations/large/1173-maison.JPG)](http://ospublish.constantvzw.org/image/index.php?level=picture&id=1173)
[![Plogger
Image](http://ospublish.constantvzw.org/image/plog-content/thumbs/lgm2010/lgm-locations/large/1174-zabriskie.JPG)](http://ospublish.constantvzw.org/image/index.php?level=picture&id=1174)

Still nearly 9 months to go, but preparations for a next edition of the
Libre Graphics Meeting in Brussels have started.

In our quest for the ultimate LGM 2010 venue, we checked out many
potential spaces, but finally settled for De Pianofabriek in our
neighbourhood St. Gilles. With the main Midi station and historic city
centre at walking distance, [its
location](http://www.openstreetmap.org/?lat=50.8317339420319&lon=4.34794664382935&zoom=16)
is ideal to begin with.

The Pianofabriek venue consists of several interconnected buildings
which used to belong to [piano factory
Gunther](http://www.irismonument.irisnet.be/nl.p.send.Sint-Gillis.Garibaldistraat.25.html).
Built at the end of the 19th century, a recent renovation transformed it
into an artslab / cultural center housing class rooms, an exhibition
space, meeting rooms, workshop spaces, a theatre, a dance rehearsal hall
and much more. Its lively atmosphere will be a productive context for
the kinds of workshops, presentations, work meetings and informal
exchanges that make up LGM, and we hope that in-between intensive work,
participants find time to enjoy the many nice bars, restaurants and
café's around nearby Parvis de St. Gilles!
