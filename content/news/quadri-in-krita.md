Title: Quadri in Krita
Date: 2008-03-11 14:40
Author: Harrisson
Tags: News, Krita, Printing + Publishing
Slug: quadri-in-krita
Status: published

[![picture-5.png]({filename}/images/uploads/picture-5.png)]({filename}/images/uploads/picture-5.png "picture-5.png")

*"Why do you bother with complicated stuff to convert images in CYMK?"*
asked Loic Vanderstichelen the other night "*Use
[Krita](http://www.koffice.org/krita/)!*"  
And it works! It even manages 16 bits tif!  
Now we know! ;)
