Title: acsr
Date: 2011-02-10 17:11
Author: Stephanie
Tags: News
Slug: acsr
Status: published

[acsr](http://acsr.be) (atelier de création sonore radiophonique)
website is now online!  
We are celebrating tomorrow listening to 2 radiophonic pieces and
sharing a meal at La CompilOthèQue, 50 quai des Péniches, Brussels.

[![]({filename}/images/uploads/capture-acsr.png "capture-acsr"){: .alignnone .size-medium .wp-image-5836 }](http://ospublish.constantvzw.org/news/acsr/attachment/capture-acsr)

Website and identity in collaboration with Jérôme Degive ([Pica
Pica](http://www.picapica.be/)) and WordPress. Featuring [Univers
Else](http://ospublish.constantvzw.org/foundry/univers-else/)!
