Title: Jeffrey Zeldman on Open Source and the web
Date: 2009-04-16 08:54
Author: Femke
Tags: News, Interview, Standards + Formats, Webdesign
Slug: jeffrey-zeldman-on-open-source-and-the-web
Status: published

Under the ambitious title '*Jeffrey Zeldman Discusses the Future of Open
Source*' (filed under: 'The Internet'), the [CSS
guru](http://www.zeldman.com) compares the use of Open Source Content
Management Systems to what webstandards did for the web. Looking forward
to hear more... and will he mention SVG?

[![jeffrey]({filename}/images/uploads/jeffrey.jpg "jeffrey"){: .alignright .size-full .wp-image-2408 }](http://bigthink.com/ideas/jeff-zeldman-discusses-the-future-of-open-source)

Excerpt from a longer interview, soon available here too:
<http://bigthink.com/ideas/jeff-zeldman-discusses-the-future-of-open-source>

<small>sorry, only available in fl\*sh</small>
