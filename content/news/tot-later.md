Title: Tot Later
Date: 2011-06-15 16:38
Author: OSP
Tags: News, books, Open Fonts, pocket book, Printing + Publishing, Scribus
Slug: tot-later
Status: published

*Tot Later* has arrived!  
![]({filename}/images/uploads/totlater1.jpg "totlater"){: .alignnone .size-medium .wp-image-6544 }

**Presentation this Friday 17 June 19:00-21:00**  
Constant / Rue du Fortstraat 5, 1060 Brussels  
++++++++++++++++++++++  
In 2008 An Mertens showed a beta-version of the distributed novel
**CIAO/Tot Later** in the exhibition Place@Space/NowHere in Z33 Hasselt.
Years passed by and now she's ready to present her novel CIAO/Tot Later
- the author’s cut, accompanied by a digital story narrated through
webpages. The book was designed and typeset by OSP and printed by
[Holland Ridderkerk](http://www.hollandridderkerk.nl/).

<http://www.adashboard.org>
