Title: Nancy → Poitiers
Date: 2010-11-06 11:46
Author: Pierre_M
Tags: News, dingbats, make art
Slug: makeart-poitiers
Status: published

<!-- p.p1 {: margin: 0.0px 0.0px 0.0px 0.0px; font: 12.0px Helvetica} -->After
we turned the simplest design into a complicated process involving
Fontforge, Subversion, Scribus Python scripter, PoDoFo, glue and good
wills, the *Dingbats Liberation Fest* font (rev.106) is now visible in
good company (*DejaVu* for the closer and some  amazing pieces of art
around) at *make art - in between design* festival.\[gallery\]
