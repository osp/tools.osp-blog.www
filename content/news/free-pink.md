Title: Free Pink!
Date: 2009-07-07 06:33
Author: Yi
Tags: News, Colors, filtering
Slug: free-pink
Status: published

> [![Image\_with\_pinkish\_color\_banned]({filename}/images/uploads/Image_with_pinkish_color_banned-75x75.jpg "Image_with_pinkish_color_banned"){: .alignleft > .size-thumbnail .wp-image-3183 width="75" > height="75"}]({filename}/images/uploads/Image_with_pinkish_color_banned.jpg)  
> "Green Dam Youth Escort (<span lang="zh-Hans"
> lang="zh-Hans">绿坝·花季护航</span>: Lǜbà·Huājì Hùháng) recognizes
> pornographic images by analyzing skin-coloured regions, causing the
> barring of this image of pink color pigs."

The Chinese government has mandated that, beginning July 1, every PC
sold in China must include a censorship program called [Green
Dam](http://en.wikipedia.org/wiki/Green_Dam_Youth_Escort). This software
is designed to monitor Internet connections and text typed on the
computer. It blocks undesirable or politically sensitive content and
optionally reports it to authorities. Green Dam was developed by a
company called Jin Hui and is available as a free download...  
[![WindowsXPGreenDamGirls]({filename}/images/uploads/WindowsXPGreenDamGirls.jpg "WindowsXPGreenDamGirls"){: .alignleft > .size-thumbnail .wp-image-3183 width="75" > height="75"}]({filename}/images/uploads/WindowsXPGreenDamGirls.jpg)
