Title: Cutting Edge
Date: 2010-02-08 00:54
Author: OSP
Tags: News, LGM 2010
Slug: cutting-edge
Status: published

### [Libre Graphics Meeting 2010](http://www.libregraphicsmeeting.org)!

![]({filename}/images/uploads/cuttingeddge.png "cuttingeddge"){: .alignleft .size-full .wp-image-3889 }

[![]({filename}/images/uploads/LGM_yellow02-52x75.png "LGM_yellow02"){: .alignleft .size-full .wp-image-3889 }]({filename}/images/uploads/LGM_yellow02.png)[![]({filename}/images/uploads/LGM_blue02-52x75.png "LGM_blue02"){: .alignleft .size-full .wp-image-3889 }]({filename}/images/uploads/LGM_blue02.png)[![]({filename}/images/uploads/LGM_rose02-52x75.png "LGM_rose02"){: .alignleft .size-full .wp-image-3889 }]({filename}/images/uploads/LGM_rose02.png)

Download flyers:
[cuttingedge.zip](http://ospublish.constantvzw.org/documents/LGM2010/cuttingedge.zip)
