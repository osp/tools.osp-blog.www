Title: Collaborative Futures
Date: 2010-02-01 11:47
Author: Femke
Tags: News, Texts books, Collaborative, Further reading, Printing + Publishing
Slug: collaborative-futures
Status: published

"*Collaboration can be so strong it forces hard boundaries. The
boundaries can intentionally or unintentionally exclude the possibility
to extend the collaboration. Potentially conflict can also occur at
these borders*"  
![]({filename}/images/uploads/IMG_3843.jpg "IMG_3843"){: .alignleft .size-full .wp-image-3873 }

For this years' [Transmediale Festival](http://www.transmediale.de), the
[F/LOSS Manuals project](http://en.flossmanuals.net) took up the
challenge to write, edit and publish a collaborative publication in 5
days while test driving the alpha-release of their [booki
platform](http://www.booki.cc/). *Collaborative Futures* has many
thoughts and observations on collaborative practice, happily sticking
its tongue out at the 'pleasant social terminology' of Web 2.0.

All text released under a [Creative Commons Attribution-Share
Alike](http://creativecommons.org/licenses/by-sa/2.0/) license.

Download the e-pub and pdf version:  
<http://www.transmediale.de/en/collaborative-futures>

<!--more-->  
From the announcement:

> The Book Sprint, an intensive and innovative methodology for the rapid
> development of books saw five people locked in a room in Berlin's IMA
> Design Village for five days to produce a book with the sole guiding
> meme being the title – Collaborative Futures. They had to create the
> concept, write the book, and output it to print in 5 days.
>
> Collaborative Futures was facilitated by Adam Hyde and written by Mike
> Linksvayer, Alan Toner, Marta Peirano, Michael Mandiberg and Mushon
> Zer-Aviv with a number of guests who contributed chapters and
> passages. The process opened up a new and networked discussion
> focusing on a new vocabulary of the forms, media and goals of
> collaborative digital practice. As the transmediale.10 publication the
> Book Sprint was based on an idea by Adam Hyde and Stephen Kovats to
> enact the festival notion of futurity in the form of a flash
> publication.
>
> Aleksandar Erkalovic in part developed and tested the alpha version of
> the 'booki' collaborative platform live and on-site with which
> 'Collaborative Futures' was created.
>
> The contents of the book are now available online, and a special
> limited edition of 200 copies featuring a great cover designed by
> Laleh Torabi will be available for sale during transmediale.10,
> opening next Tuesday Feb 02, at the House of World Cultures in Berlin.
