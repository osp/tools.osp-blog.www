Title: Surprise package
Date: 2007-10-19 15:13
Author: Harrisson
Tags: News, Fontforge
Slug: surprise-package
Status: published

[![fontforge.png]({filename}/images/uploads/fontforge.png)]({filename}/images/uploads/fontforge.png "fontforge.png")

While preparing Werkplaats Typografie workshop, I had the good surprise
to discover fontforge-executables in mac in an [install
package](http://fontforge.sourceforge.net/running.html#mac). This means
installation of newer and better fontforge is now pretty much easier
that it used to be. I've installed and worked on the ppc version of it.
You still need X11, but we're used to it now. No?

*"FontForge does not conform to Apple's Human Interface Guidelines.
FontForge never will. Don't expect it to look like a normal Mac
Application. It doesn't."*  
Good, we love it!
