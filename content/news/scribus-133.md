Title: Scribus 1.3
Date: 2006-02-11 18:40
Author: Femke
Tags: News, Scribus
Slug: scribus-133
Status: published

A few days ago, I installed version 1.3.3 and a quick review already
shows that the screen-mouse response has come a long way since I used
Scribus 1.2 to get this poster printed:  
[![]({filename}/images/uploads/2006/02/oclPoster.jpg)](http://www.constantvzw.com/downloads/)

Of course, using software seriously for the first time is a disorienting
experience in itself, but in this case the application responded in such
unexpected ways that it left me sort of hopeless about the possibility
that lets say... designing a book, or doing anything more 'subtle' would
be possible. With the newest version, the next project might be a lot
easier to acomplish I hope.

Download available at <http://www.constantvzw.com/downloads/>
