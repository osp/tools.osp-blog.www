Title: La Rentrée
Date: 2007-08-27 09:54
Author: Femke
Tags: News, Reading list
Slug: la-rentree
Status: published

![lurs\_th.jpg]({filename}/images/uploads/lurs_th.jpg)  
<small>View from [Les Rencontres de
Lure](http://www.rencontresdelure.org/ete/prog.html) (Lurs,
France)</small>

Welcome back! OSP is about to start a busy year, but before we begin...
here a fresh take on the application of Open Source methodologies in
design:

> "An open source methodology could aid us in moving in this direction
> for it provides a contemporary justification  
> for a publicly owned intellectual commons sustained by collaborative
> effort. The challenge would be to design the appropriate technologies
> for interface with the network. But if we merely transfer the open
> source model into the discipline of design, without first connecting
> it to a physical sense of place, we may not take advantage of the
> opportunities being offered to us".

*[Crafting the Public Realm: Speculations on the Potential of Open
Source Methodologies in Development by
Design](http://www.thinkcycle.org/tc-filesystem/?folder_id=37457)*, Prem
Chandavarkar
