Title: LGM 2011: date + place confirmed
Date: 2010-09-09 10:17
Author: OSP
Tags: News, LGM 2011
Slug: lgm-2011-date-place-confirmed
Status: published

<iframe  frameborder="0" scrolling="no" marginheight="0" marginwidth="0" src="http://www.openstreetmap.org/export/embed.html?bbox=-73.924,45.361,-73.421,45.739&amp;layer=mapnik" style="border: 1px solid black"></iframe>  
<small>[View Larger
Map](http://www.openstreetmap.org/?lat=45.55&lon=-73.6725&zoom=10&layers=M)</small>

The sixth edition of the Libre Graphics Meeting will take place from
**10-13 May 2011** in **Montreal, Canada**. The local team builds on
experience of having organised two earlier meetings in the same city,
plus this years' LGM will make a special effort to connect to the lively
art- and design scene present *sur place*. So, save the date and we hope
to meet again in Canada's Cultural Capital next spring!
