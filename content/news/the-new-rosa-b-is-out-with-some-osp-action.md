Title: The new Rosa B is out with some OSP action
Date: 2010-05-10 10:31
Author: Harrisson
Tags: News
Slug: the-new-rosa-b-is-out-with-some-osp-action
Status: published

![]({filename}/images/uploads/Edouard_Manet_sm.jpg "Edouard_Manet_sm"){: .alignleft .size-full .wp-image-4496 }

[Rosa B](http://www.rosab.net), the web magazine, released in its new
Vintage: the of Edit! colloque, which occured in Bordeau, France, in
march 2009.  
OSP was invited in this harvest and you can follow some Millesime traces
of our passage there:

The parallel publishing workshop  
The Michel Aphesbero, Francois Chastenet, Harrisson and Robin Kinross
interview

[www.rosab.net/format-standard/](http://www.rosab.net/format-standard/)
