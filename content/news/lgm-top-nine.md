Title: lgm top nine
Date: 2009-05-10 18:31
Author: Ivan
Tags: News, LGM 2009
Slug: lgm-top-nine
Status: published

![gingerheart2]({filename}/images/uploads/gingerheart2.jpg "gingerheart2"){: .alignnone .size-medium .wp-image-2589 }

1\. The show must go on.

2\. The slides are not the content.

3\. I try not to open a terminal in front of people.

4\. It's the engineers who are responsible for the bugs and explosions.

5\. The preferences are the graveyard of any good idea.

6\. The more links an architect got on the internet, the greater the rank
on the coin.

7\. When we say it's planned, it's not only planned -- the commits are
actually happening.

8\. You take a picture of a tomato, and the end user sees a lemon.

9\. If that purple is not really purple, you need to tune your monitor.
