Title: OSP ASBL VZW Celebration Print Party
Date: 2015-05-28 11:51
Author: Colm
Tags: News
Slug: osp-asbl-vzw-celebration-print-party
Status: published

We are happy to announce an OSP ASBL/VZW\* celebration evening and Print
Party.

[![osp\_asbl-vzw\_party]({filename}/images/uploads/osp_asbl-vzw_party.jpg){: .alignnone .size-medium .wp-image-7370 }]({filename}/images/uploads/osp_asbl-vzw_party.jpg)

Back from our yearly travel to the Libre Graphics Meeting, full of
Toronto stories, and soon to be tattooed with a VAT number, OSP welcomes
you in their transit tavern.  
Join us in cutting, plotting and browsing recent works. Come to
celebrate the launch of our association with us, and visit our
Schaerbeek studio before the caravan moves on!

[![IMG\_5245]({filename}/images/uploads/IMG_5245.jpg){: .alignnone .size-medium .wp-image-7370 }]({filename}/images/uploads/IMG_5245.jpg)

*De retour de notre voyage annuel aux Libre Graphics Meeting, avec
quelques expressions torontoises à partager et bientôt un numéro de TVA
à se faire tatouer, OSP vous accueille dans sa taverne de transit.
Rejoignez-nous pour découper, plotter et feuilleter nos récents travaux.
Venez fêter le lancement de l'asbl, et visiter notre studio de
Schaerbeek avant que la caravane ne se déplace à nouveau!*

![](http://www.ludi.be/DSCF8722.JPG){: .alignnone .size-medium .wp-image-7370 }

19:00 → 21:00 Machines launch + crackers  
21:00 → 22:00 Dancing  
Guests: Roland, Océane, Cameo.

**OSP asbl/vzw Print Party  
Thursday May 28, 19:00 — 22:00**  
Avenue Princesse Elisabethlaan, 46  
1030 Schaarbeek

[![IMG\_20150528\_103258]({filename}/images/uploads/IMG_20150528_103258.jpg){: .alignnone .size-medium .wp-image-7370 }]({filename}/images/uploads/IMG_20150528_103258.jpg)

\* ASBL/VZW is the Belgian form of non-profit association that we have
chosen as the legal structure for Open Source Publising.  
The general assembly will officialy create the entity, as we will vote
on the following statutes:  
http://www.ejustice.just.fgov.be/tsv\_pdf/2015/03/06/15303974.pdf
(French)  
http://www.ejustice.just.fgov.be/tsv\_pdf/2015/03/06/15303975.pdf
(Dutch)
