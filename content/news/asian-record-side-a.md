Title: Asian record, side A
Date: 2011-04-10 12:39
Author: OSP
Tags: News
Slug: asian-record-side-a
Status: published

Saturday
========

Our [Open Design Weeks](http://opendesign.asia/) in Vietnam kicked off
at the Spotted Cow where we went for social drinks. There we met mainly
people from IT with programming backgrounds. We also got to know our new
friends from Cambodia. This was our first encounter with an on-going
theme of translation issues. We found it difficult to move past a
certain point of conversation, though John managed to have several
interestingly recursive discussions about the problems in making
multilingual websites. After the long flight it was very nice to have
this chance to breath the Vietnamese air, dive in the moisture and soak
in some cold beers.

Sunday
======

![]({filename}/images/uploads/DSC_0165.jpg)  
Sunday morning began early at [Ho Chi Minh City University of Fine
Arts](http://mythuat.edu.vn/) where we started with a presentation about
OSP to a crowded hall of over 300 people. By showing pictures we
introduced ourselves as well as the members who are absent from this
trip. Then to get even more familiar we each presented an example of our
work. Alex presented the Scribus-over-IRC process begun at the RCA
workshop, Stéphanie explained the workshop [Please Computer, Make Me
Design](http://ospublish.constantvzw.org/education/posters-please-computer-make-me-design),
Pierre Marchand showed off the evolution of Nancy to
[Fonzie](http://ospublish.constantvzw.org/?s=fonzie) and hinted at what
is to come, John discussed generative typesetting with Context, while
Pierre Huyghebaert took the responsibility of giving the general
overview of OSP.  
![]({filename}/images/uploads/DSC_0163.jpg)  
In our practice we have continuous engagement with translation (between
graphic design, programming, poetics, cartography, ...) and it was quite
something to see Hong Phuc translating our translations (and sometimes
*not* translating).  
<!--more-->

Afternoon
---------

-   pictures picking on the wall
-   different smiles
-   german dingbats dictee
-   2 dingbats actually vectorised

![]({filename}/images/uploads/IMAG0644.jpg)

Monday
======

![]({filename}/images/uploads/DSC_0190.jpg)

-   Scribus irc
-   Full room of people
-   Aprons
-   A clear goal : poster for the exhibition
-   Hong Phuc embedded
-   Lily also
-   In the morning, sketches
-   Then Scribus demo
-   PierreH on the poster - others irc comments
-   but also designing their own versions
-   Poster unfinished, but lots of excitement of the added layers by
    code

![]({filename}/images/uploads/DSC_0218.jpg)

      createLine(100,100,100,400)
      VIET CAC CAU LENH LEN DAY
      createEllipse(200,200,200,200)
      loadImage("logo")
      setFillShade(shade)
      createEllipse(100,100,100,100)
      createImage(50,50,200,300)
      textFlowM
      createLine(10, 10, 200, 200)
      createLine(20, 10, 200, 200)
      createLine(30, 10, 200, 200)
      createLine(40, 10, 200, 200)
      createLine(50, 10, 200, 200)
      createEllipse(184,184,100,100)
      createEllipse(184,188,100,100)
      insertText(10,20,30)
      createRect(25,35,50,50)
      createText(1,1, 100,250)
      createLine(20,20,60,60)
      createPolyLine(list,["..."]
      createPolygon(20,20,50,50,100,100)
      setText("troi oi!!","Text1"))
      createImage(50,50,300,300)
      setFillColor("White","Polygon79")

![]({filename}/images/uploads/DSC_0231.jpg)  
![]({filename}/images/uploads/DSC_0294.jpg)  
![]({filename}/images/uploads/DSC_0308.jpg)  
![]({filename}/images/uploads/DSC_0321.jpg)  
![]({filename}/images/uploads/DSC_0323.jpg)

Tuesday
=======

![]({filename}/images/uploads/DSC_0356.jpg)

-   TMA experience
-   IT company
-   it seemed important for our hosts, Mario and Hong Phuc, that we give
    a workshop there
-   Only Windows machines
-   so command-line workshops were not possible
-   Scribus IRC workshop - no printed manual, only online documentation
-   HTML 5 demo by Singaporian friends
-   Context/Pandoc demo - Failed to install context on Windows
-   typeset an ancient vietnamese poem
-   Tola's presentation of his company's activities: marketing for
    good causes. We plan to add more on this later.

![]({filename}/images/uploads/DSC_0354.jpg)  
![]({filename}/images/uploads/DSC_0406.jpg)

      createText(10,10,10,10,"Men tests")
      createEllipse(200,200,200,200)
      loadImage("logo")

![]({filename}/images/uploads/DSC_0374.jpg)  
![]({filename}/images/uploads/DSC_0387.jpg)  
If doubt, it became clear that day that we prefer to practice instead of
demoing.  
![]({filename}/images/uploads/DSC_0413.jpg)  
Wednesday we've been to a school in between marketing and design.  
![]({filename}/images/uploads/DSC_0418.jpg)  
The morning was to presentation by us and our friends from Singapore and
Cambodia. It was interesting to have different perspectives on
free/open-source cultures. Tola (Cambodia), from a different perspective
than the one of free software, talked about the power of
design/marketing for being used for good causes. Sophaep (Cambodia)
talked about ethical outsourcing and the intertwining of free software
into a business that is at once directed at and run by swiss foreigners
while maintaining an embedded existence within Cambodia. Emphasis was
given to the fact the company actually pays for software licenses when
it uses proprietary products, which is not only pretty unique here in
Southeast Asia but also not a universal in Europe.  
![]({filename}/images/uploads/DSC_0438.jpg)  
Luther from Square Crumbs instead emphasized using free software to
distinguish yourself from others, focusing on how reliable and
business-ready open-source is and how diverse the free software choices
are. It was refreshing to see all this variety in presenting free/open
source and it may have clarified somewhat OSP's own position by showing
it in relation to the perspectives of others. The \[jam comparison\]
9http://ospublish.constantvzw.org/news/the-four-freedoms-two-rules-and-one-jam)
worked more or less, but after some searching, the pickels must work
better in Vietnam!

Afternoon
---------

![]({filename}/images/uploads/DSC_0515.jpg)  
LiteralDraw workshop --Pen & Paper for Computers, an introduction to
graphical output device-- failed to make a Windows .exe We ran the
software on our own machines, making groups of students discovering the
software with the students Translated function names in vietnamese

    change line dòng
    change stroke bút
    change end trở—lại
    change move di—chuyển
    change cubic cong
    change close đong
    change fill điền—vào
    change transform biến—đổi
    change text văn—bản
    change font kiểu—mâu

Thursday
========

Evening - mounting of the exhibition in Trung Nguyen Café the team here
prepared easels and wooden boards to display the posters and
illustrations. The exhibition format (wood panels and easels) raises
questions amongst us, *of course*. How to convey some meaning with a
format that refers to the way some people in the 17th century were
looking at pictures as depiction of the world (paintings!). The pictures
coming from Ginger, Ana, Ricardo, Dave, Andy, Jakub, Rejon, Lafkon and
others.  
![]({filename}/images/uploads/DSC_0530.jpg)  
Not much choice on how to arrange the posters: we have the central space
of the floor, everywhere else are tables and chairs people organizing
the exhibition are surprised while reading the biographies: they don't
know any of the people listed in it and wonder why we are here but not
in the bios nor the exhibition so we decide to make something last
minute next day.

Friday
======

We wanted to do something on the floor with vinyl, but we don't know
where we can produce that so fast. Next plan: making a big poster
covering the tables in the center and print a double-issue of Libre
Graphics Magazine to be able to stack the laminated print outs. In 2
hours or so, we made a cover for this not pirated official double issue
of Libre Graphics Magazine -which looks then as a strange Reader or
Fanzine- and we pulled out all our blog content (texts and images) to
put them on the huge poster, Scribus strangled by a python!  
At 20.00, "official" opening starts. Most of the attendees are graphic
design students, some we've met during the workshops, some are from
other schools. Some teachers and graphic designers as well. Some
students showed us their work, some were interested in pursuing the
workshops at home (how to install the software used? installing
Ubuntu?).

-   Marketing/Art
-   The exhibition San Art
-   LGM 2012

Week 1 - Saturday
=================

afternoon: Debrief of the week at the swimming-pool evening: Trying to
meet Merv Espina (connection through the Piet Zwart Institute in
Rotterdam) at San Art, independant artist space. Closing party of ["Open
Edit: Mobile
Library"](http://www.san-art.org/exhibitions/AAA-MOBILE-LIBRARY/AAA-MOBILE-LIBRARY.html).
Principle is to choose a book and to intervene onto it. A book of 1288
pages, history of 20th century art in China, unbound, rebound in 5
volumes according to the major color of the pictures. \[! PICTURE !\] An
art catalogue where someone pasted pink blocks with different shades or
white lines to mark the main composition of each picture or of even the
catalogue page layout. \[!PICTURE!\] We also met Zoe Butt, co-director
of the space, and we plan an interview with her next week when we come
back from Can Tho. Merv invited us to go next week to an opening at
[\[Gallery Quynh\]](http://galleryquynh.com) in HCMC.

![](http://ospublish.constantvzw.org/blog/wp-content/themes/osp/images/OIF.jpg)  
  
<small>La participation OSP de l'Open Design Week a été rendue possible
grâce au soutien de l'OIF</small>

<link href="http://fonts.googleapis.com/css?family=Allan:bold" rel="stylesheet" type="text/css"></link>
<link href="http://fonts.googleapis.com/css?family=VT323" rel="stylesheet" type="text/css"></link>

<style type="text/css" media="screen">
        body {: <br></br>             font-size: 12px;<br></br>             background-color: aqua;<br></br>             color: deeppink;<br></br>         }<br></br>
        h1, h2, h3 {: <br></br>             font-size: 12px;<br></br>             background-color: aqua;<br></br>             color: deeppink;<br></br>         }<br></br>
        code {: <br></br>             font-size: 12px;<br></br>             background-color: aqua;<br></br>             color: deeppink;<br></br>         }<br></br>
        code {: <br></br>             font-size: 12px;<br></br>             background-color: aqua;<br></br>             color: deeppink;<br></br>         }<br></br>
        p, ul, code {: <br></br>             font-size: 12px;<br></br>             background-color: aqua;<br></br>             color: deeppink;<br></br>         }<br></br>
        ul {: <br></br>             font-size: 12px;<br></br>             background-color: aqua;<br></br>             color: deeppink;<br></br>         }</p>
</style>

