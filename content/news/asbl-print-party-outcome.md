Title: ASBL Print party outcome
Date: 2015-06-05 10:55
Author: Colm
Tags: News
Slug: asbl-print-party-outcome
Status: published

When you mix travel stories with non profit association elation, within
the zero boundary format of a print party, what do you get?

[![IMG\_20150528\_173205]({filename}/images/uploads/IMG_20150528_173205.jpg){: .size-medium .wp-image-7379 .aligncenter width="100%" height="auto"}]({filename}/images/uploads/IMG_20150528_173205.jpg)

We came up with a mix of different *things* we wanted to share and show,
and mixed them in with some more down to earth writing extracted from
our Statutes. One article in particular, [Article
3](http://ospublish.constantvzw.org/blog/news/article-iii-goal-social-object-field-of-action),
we are particularly happy with, so we plotted it on a huge A1 during the
party, then offered[an
interface](http://git.constantvzw.org/?p=lgru.co-position.remote-plotter.git;a=tree)
to let anybody add in drawn
augmentations.![IMG\_20150605\_094000]({filename}/images/uploads/IMG_20150605_094000.jpg){: .size-medium .wp-image-7379 .aligncenter width="100%" height="auto"}

Meanwhile, live Laidout and cutting plotter trace, not print - draw, not
paint.

[![IMG\_20150528\_195839]({filename}/images/uploads/IMG_20150528_195839.jpg){: .size-medium .wp-image-7379 .aligncenter width="100%" height="auto"}]({filename}/images/uploads/IMG_20150528_195839.jpg)
