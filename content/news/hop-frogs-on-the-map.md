Title: Hop frogs on the map
Date: 2010-03-08 02:24
Author: Pierre
Tags: News, cartography
Slug: hop-frogs-on-the-map
Status: published

[identi.ca/osp](http://identi.ca/osp) make his best to track our recent
activity via our differents IP addresses, where Brussels seems
synthetise by 4 positions, here with the background lazy to display, osp
frogs above water.

![]({filename}/images/uploads/frogs.png "frogs in Brussels"){: .alignnone .size-full .wp-image-4208 }

A few minutes later, a new osp -Ivan- connection from Schaerbeek, North
of Brussels, strangely shrink the scale by positionning a bright new
frog on Bruges, 100km+ away, and muxing a big taurus frog on Brussels.

![]({filename}/images/uploads/frogs2.png "frogs2"){: .alignnone .size-full .wp-image-4208 }
