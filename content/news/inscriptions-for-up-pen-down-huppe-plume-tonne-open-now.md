Title: Workshop Up Pen Down
Date: 2015-10-09 11:20
Author: Eric
Tags: News
Slug: inscriptions-for-up-pen-down-huppe-plume-tonne-open-now
Status: published

Up Pen Down – Huppe Plume Tonne is a workshop linking typography and
performance. It is a collaboration between OSP and choreographer Adva
Zakai, and you are invited to participate!

The workshop will take place Saturday 24^th^ and Sunday 25^th^ of
October in the Balsamine theatre, Brussels. On the 25^th^ the doors will
be open to the public from 16:00 onwards.

[![hoppe-pume-ton-og-image]({filename}/images/uploads/hoppe-pume-ton-og-image.png){: : .alignnone .size-medium .wp-image-7486 }]({filename}/images/uploads/hoppe-pume-ton-og-image.png)  
*OSP and the students of ÉSAD Grenoble Valence getting ready to draw
letterforms.*

In Up Pen Down – Huppe Plume Tonne the participants blow up the scale of
the movements of both drawing (down) and non-drawing (up). Uprooting
both the forms produced and the movements required, the workshop is
meant both for performers and designers.

[![children\_with\_turtle\_robot]({filename}/images/uploads/children_with_turtle_robot.jpg){: : .alignnone .size-medium .wp-image-7486 }]({filename}/images/uploads/children_with_turtle_robot.jpg)

*Seymour Papert taught mathematics through drawing instructions children
would give to the computer, other children, or in this case—to a drawing
robot.*

[![din-stroke]({filename}/images/uploads/din-stroke.png){: : .alignnone .size-medium .wp-image-7486 }]({filename}/images/uploads/din-stroke.png)

*The DIN typeface is specified through drawing instructions: defined by
its stroke rather than its contour.*

Up pen down – Huppe Plume Tonne takes inspiration from the work of the
English mathematician Seymour Papert, who tried to create an embodied
understanding of mathematics through his LOGO language. Papert created a
language, understood both by humans and computers, that describes
drawings not in an absolute cartesian grid, but from the perspective of
the person that is drawing. LOGO prefigures similar languages that use
drawing instructions to communicate between computers and early drawing
devices. Up Pen Down – Huppe Plume Tonne builds on the [research
effectuated by OSP with the students of the ÉSAD Grenoble Valence in
2012-2013](http://ospublish.constantvzw.org/blog/news/up-pen-down-invited-designers-valence).

For Up Pen Down – Huppe Plume Tonne, OSP collaborates with Adva Zakai,
Brussels-based choreographer and performer. Adva’s recent work explores
how body and language are perceived through each other. Just as much as
the body seems to recede under its textual interpretation, our very
understanding of text becomes more embodied.

The first day of the workshop will be Saturday 24^th^ of October from
10:00 to 17:30. Sunday the 25<up>th</up> the workshop starts again at
10:00 also. From 16:00 onwards the doors will be open for the public.

Address:

<address>
Théâtre la Balsamine  
Avenue Félix Marchal 1  
1030 Bruxelles

</address>
*<small>Up Pen Down – Huppe Plume Tonne is part of the [Festival des
Arts Numériques](http://festivaldesartsnumeriques.be/) and made possible
by the Wallonia-Brussels Federation.</small>*
