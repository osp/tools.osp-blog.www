Title: This Fall
Date: 2010-10-03 13:11
Author: OSP
Tags: News, exhibition, public appearance, Workshops + teaching
Slug: this-fall
Status: published

![]({filename}/images/uploads/Screenshot1.png "Screenshot"){: .alignnone .size-full .wp-image-4960 }

This fall @ OSP:

-   [**Prix Fernand Baudin Prijs Tournée
    française**](http://www.prixfernandbaudinprijs.be/Public/NewsList.php?ID=3509#m3621)
    (Paris, Valence, Nancy, Besançon et Amiens 30/09/2010 → 30/01/2011)
-   Teaching **[Images numériques – Open source – Chaîne
    (typo)-graphique
    ouverte](http://www.erg.be/erg/spip.php?article1072&lang=en)** at
    ERG
-   Submitted proposal to the [The Education, Audiovisual and Culture
    Executive Agency (EACEA)](http://eacea.ec.europa.eu/) to fund a
    [**Libre Graphics Research Unit**](/lab/wiki)
-   Thinking about The Praise of Folly and the [Prize of
    Money](http://www.herdenkingsmunt.nl/)
-   06/11 → 12/11: OSP in Tel Aviv to participate in [**Open Sources
    versus Military
    Culture?**](http://www.shenkar.ac.il/english/events/events.aspx?eventsId=117)
-   Getting [Nancy](/nancy) ready for the next edition of MakeArt
    festival [**In-between design: rediscovering collaboration in
    digital art**](http://makeart.goto10.org) (Poitiers 04/11 → 07/11)

Yes, winter will probably be hot too.
