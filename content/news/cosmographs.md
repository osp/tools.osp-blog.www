Title: Cosmographs
Date: 2011-10-01 11:51
Author: Pierre
Tags: News, Tools, Works, Art
Slug: cosmographs
Status: published

A quick glimpse on one of Vincent Meessen's Cosmographs for the
exhibition of his new works at [Khiasma (Paris) during the Hantologie
des
colonies](http://www.hantologie.com/?cat=38 "Khisma - Hantologie des Colonies")
cycle. Using [dot language](http://en.wikipedia.org/wiki/DOT_language)
that Vincent began to use quite instantly and
[Graphviz](http://www.graphviz.org/) as a *pense-bête* to track
vibrating paths around Roland Barthes and his grandfather Louis-Gustave
Binger.

\[caption id="attachment\_6730" align="alignnone" width="400"
caption="Vincent
Meessen"\][![]({filename}/images/uploads/296609_292779630735593_100000106763221_1323136_736408766_n.jpg "Cosmograph 1/3"){: .size-medium .wp-image-6730 }]({filename}/images/uploads/296609_292779630735593_100000106763221_1323136_736408766_n.jpg)\[/caption\]
