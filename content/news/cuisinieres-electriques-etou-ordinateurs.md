Title: Cuisinières électriques ET?OU ordinateurs
Date: 2012-02-06 01:04
Author: Stephanie
Tags: News
Slug: cuisinieres-electriques-etou-ordinateurs
Status: published

Encore une histoire de
["et-icien"/"ou-icien"](http://ospublish.constantvzw.org/news/relearn "Relearn")
et encore une nouvelle raison de faire de la cuisine dans OSP!  
Bernard Vaudour -Faguet, ["Histoire des cuisinières électriques et des
ordinateurs"](http://www.persee.fr/web/revues/home/prescript/article/colan_0336-1500_1990_num_83_1_2207)
