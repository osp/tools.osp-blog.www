Title: README.OSP: Exhibition @ Une Saison Graphique
Date: 2016-05-07 13:15
Author: Eric
Tags: News
Slug: readme-osp-exhibition-une-saison-graphique
Status: published

[![Poster Une Saison Graphique
2016]({filename}/images/uploads/cover-1.png)]({filename}/images/uploads/cover-1.png)

As part of the yearly Design Festival [Une Saison
Graphique](http://unesaisongraphique.fr/), and in collaboration with
[l’École Supérieure d’Art et Design Le Havre-Rouen](http://esadhar.fr/),
OSP would like to invite you to its [exhibition
README.OSP](http://esadhar.fr/fr/actualitees/readmeosp-saison-graphique-2016)!

It has been 10 years since the first public OSP blog post, and
README.OSP takes the opportunity to reflect on the potential of a shared
process. For years, OSP has been publishing all source files of its
projects, even if that means material that is in a rough state. For the
exhibition, both OSP members and students of the ESADHaR have excavated
the [publicly available archives](http://osp.kitchen/) to extract
patterns and recipes, to go beyond documentation to the proposal of a
future tool.

You are cordially invited to the vernissage, Thursday 19/05 at 19h30, at
the Galerie 65 de l'ESADHaR, 65 rue Demidoff, Le Havre.

The vernissage is preceded by [a
lecture](http://esadhar.fr/fr/actualitees/conference-collectif-open-source-publishing),
19^th^ of May at 17:30, at the Maison de l'étudiant du Havre. The
exhibition is on from the 20^th^ of May to the 2^nd^ of July, from
Monday until Saturday, from 14h30 to 19h. The 21st of May a visit to the
exhibition can be combined with a number of events, including a bicycle
tour of the exhibitions, a graphical kermis and a party. For more info
see the website of [Une Saison
Graphique](http://unesaisongraphique.fr/).

[![cartonverso]({filename}/images/uploads/cartonverso.png){: .alignnone .size-medium .wp-image-7549 }]({filename}/images/uploads/cartonverso.png)
