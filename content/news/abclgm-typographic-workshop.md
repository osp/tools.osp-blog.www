Title: ABCLGM Typographic Workshop
Date: 2010-05-27 12:41
Author: OSP
Tags: News, Type, LGM 2010, Libre Fonts, Workshops + teaching
Slug: abclgm-typographic-workshop
Status: published

![]({filename}/images/uploads/abc_workshop.jpg "abc_workshop"){: .alignright .size-full .wp-image-4549 }

The people attending this workshop will learn about one way to dissect
the shapes of the latin alphabet, and use this theorical model to make a
typeface together in a fast paced design game. The game will involve
drawing letters on paper, making use of a real life version control
system, and uploading the results to the [Open Font
Library](http://openfontlibrary.org/). No previous experience of
typeface design required.  
By and with [Dave Crossland](http://understandinglimited.com/) (type
designer), [Ludivine Loiseau](%20http://www.ludi.be/) (graphic
designer), [Pierre Marchand](oep-h.fr) (font developer) and [Sebastien
Sanfilippo](http://typeandgouda.wordpress.com/) (type designer)  
In the context of [LGM - Libre Graphique Meetings
2010.](http://www.libregraphicsmeeting.org/2010/)
