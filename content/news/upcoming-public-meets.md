Title: Upcoming Public Meets
Date: 2012-09-28 14:12
Author: Eric
Tags: OSP Public Meets
Slug: upcoming-public-meets
Status: published

![An abstract line-based representation of a
Frog]({filename}/images/uploads/frog1.png "frog"){: .size-full .wp-image-7172 }

### OSP Meets

[OSP Public meets](http://ospublish.constantvzw.org/blog/meets) is where
we talk of talking internal affairs in public, where we share a space to
show and tell on open source graphic design practice with all who are
interested. We start at *18:00*, we open up the discussion at *19:30*
and we drink+dance from *21:00* onwards.

### When

*October 8^th^, October 22^nd^, November 5^th^, November 19^th^,
December 3^rd^, December 17^th^*. In 2012.

### The cooperative

The 8^th^ of October we will focus on the cooperative. We research this
as a possible legal form for OSP, and each of us will bring an example
of a Belgium cooperative to discuss. It makes me feel in touch with my
family history, my great grandfather was the director of a milk
cooperative in the Netherlands.
