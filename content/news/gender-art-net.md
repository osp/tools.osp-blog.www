Title:  Gender Art Net
Date: 2009-04-03 10:38
Author: ludi
Tags: News, Works, Berlin, Collaborative, Inkscape, SVG, Web, Website
Slug: gender-art-net
Status: published

OSP participe actuellement au développement du projet Gender Art Net.

Gender Art Net propose de créer un atlas interactif donnant accès à
différentes vues et lectures d’un ensemble de positions artistiques
feministes dans l’Europe contemporaine.

[![gender-art-net01]({filename}/images/uploads/picture-81.png "gender-art-net01"){: .alignleft .size-medium .wp-image-2366 }]({filename}/images/uploads/picture-81.png)

<!--more-->  
[![gender-art-net02]({filename}/images/uploads/gender-art-net02.png "gender-art-net02"){: .alignleft .size-medium .wp-image-2366 }]({filename}/images/uploads/gender-art-net02.png)

Soutenu par l'European Cultural Foundation le projet est conçu par la
productrice et commissaire Bettina Knaup, en collaboration avec Constant
vzw. L'équipe du projet se compose de curatrices, artistes,
statisticienne, développeurs, de Berlin, Barcelone, Amsterdam (Dunja
Kukovec, Laurence Rassel, Diana McCarty, Maria Perez, Katalin Timar,
Urska Merc) et d'étudiants le la section European Media Studies de
l'Université de Postdam  (Lenore Hipper, Inés Matres, Sebastian Moering,
Laura-Helen Rüge)

Le projet explore les relations entre genres, territoires, artistes,
œuvres, groupes, réseaux et événements à travers différentes thématiques
/ fils de discussions.  
La base de données du projet se nourrit d'informations encodées par
l'équipe éditoriale et d'informations extraites dynamiquement sur les
sites web représentatifs des artistes.

OSP travaille à rendre visibles, dynamiques et interactifs ces
différents niveaux d'informations.

En collaborations avec Anne-Laure Buisson (statistician, feminist
activist, Brussels), nous  cherchons à distribuer/spacialiser un
ensemble de mots clés significatifs en relation avec les artistes.  
Les mots extraits des sites sont filtrés via des techniques de
statistiques multi-variées pour générer des représentation en deux
dimension reprenant les mots et les artistes, indiquant les relations
entre eux et les particularités du lexique et des centres d'intérêts des
artistes. Une série de graphiques, agissants comme des clichés ou
petites cartes thématiques de l'atlas sont ainsi obtenus.

Les informations encodées par l'équipe éditoriale localisent les lieux
d'origine et de travail des artistes, associent les artistes à des fils
de discussion et réseaux, et relient des œuvres/projets à des lieux.

OSP développe une interface qui permettra de parcourir en ligne cette
carte feministe du ciel artistique européen.

Ici un aperçu de nos recherches en cours  
(layouts réalisés avec Inkscape, R, Php, MySql, et Processing)
