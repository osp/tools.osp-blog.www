Title: The Library
Date: 2010-08-03 17:48
Author: OSP
Tags: News, Further reading
Slug: the-library
Status: published

On our (virtual) bookshelves:

[![]({filename}/images/uploads/books1.png "books"){: .alignnone .size-medium .wp-image-4879 }](http://www.librarything.com/catalog.php?view=LG_lab&collection=-1&shelf=list&sort=title)

Download the list as .csv file:
[OSP-library\_030810]({filename}/images/uploads/OSP-library_030810.csv)

The OSP-library is a modest pile of books that we think is relevant to
our practice. Some [we have
read](http://www.librarything.nl/catalog.php?view=LG_lab&shelf=list&sort=title&sort=title)
and others [we
should](http://www.librarything.nl/catalog.php?view=LG_lab&shelf=list&sort=title&sort=title).
We'll keep adding titles; suggestions welcome!

After testing several ways to catalogue the collection, we settled with
LibraryThing for the time being. It's not ideal ... though it is a good
project (they allow users to export collections as .csv file (book data,
no tags), use multiple sources for bibliographic data and have a
sensible privacy policy) the software nor data is under a free license.

We tried [Alexandria](http://alexandria.rubyforge.org/) and than
exported our booklist as a static html page but the amount of times the
software crashed drove us insane. Also it is a bit disappointing that
Alexandria almost completely relies on data from amazon.com. A rewrite
is on it's way so we are looking forward to that. We most of all want to
use <http://www.openlibrary.org>, a project initiated by
<http://www.archive.org>. We like their concept "*a wiki page for each
book*" and the site contains a fair amount of information already. For
posts on individual books, we started to use [a
plugin](http://johnmiedema.ca/portfolio/openbook-wordpress-plugin/) that
can pull data from <http://www.openlibrary.org>. But we'll need a bit of
patience before jumping ship: the team is currently working on adding
the indispensable feature that allows us to make lists. And the ability
to import data from file would be nice too!
