Title: Take Away Archive
Date: 2009-04-24 11:24
Author: OSP
Tags: News, NotCourierSans, Archiving, Constant Verlag, Not-Courier sans, Printing + Publishing
Slug: take-away-archive
Status: published

**25 April to 31 May, open every weekend from 14:00-18:00**  
**Opening: Friday 24 April 18:00-24:00**  
Rue de la Senne 17, Brussels

[![posters]({filename}/images/uploads/posters.jpg "posters"){: .alignright .size-medium .wp-image-2481 }]({filename}/images/uploads/posters.jpg)

<http://ospublish.constantvzw.org/image/index.php?level=album&id=28>

For the opening of a temporary workspace at [Zennestraat 17 Rue de la
Senne](http://www.zennestraat17.be), we have installed the *Constant
Take Away Archive*, a retrospective of books, posters, CD's and flyers,
published by [Constant](http://www.constantvzw.org) since 1997. It's a
unique chance to update your printed matter collection with vintage
must-haves, some by OSP, many by Harrisson. Opening tonight!

<!--more-->  
`C O N S T A N T * P O S T E R S `

`01  Not-CourierSans (OSP: Ludivine Loiseau), 2008`  
`02  Capture [Les Halles] (Harrisson) 200?`  
`03  Stitch and Split (Harrisson), 2005`  
`04B + 04A  Jonctions/Verbindingen 4 (Harrisson), 2004`  
`05  Copy Cult (Harrisson), 2000`  
`06  Open Content Logo Repository (Femke Snelting), 2005`  
`07A + 07B  Jonctions/Verbindingen 10 (Harrisson), 2007`  
`08  The Laurence Rassel Show (Terre Thaemlitz), 2006`  
`09  Jonctions/Verbindingen 5 (Harrisson), 2001`  
`10  Jonctions/Verbindingen 8 (Harrisson), 2004`  
`11  Routes + Routines (Harrisson + Peter Westenberg), 2005`  
`12  Jonctions/Verbindingen 9 (Roger Teeuwen), 2006`  
`13  Digitales 2004 (Harrisson), 2004`  
`14  Cinema Du Reel [BPI] (OSP: Ludivine Loiseau + Harrisson), 2009`  
`15  Unravelling Histories (De Geuzen), 2005`  
`16  The Exctasy of Influence [La Mediatheque] (OSP: Harrisson), 2009`  
`17  Place @ Space [Z33] (Peter Westenberg), 2008`  
`18  Jonctions/Verbindingen 6 (Harrisson), 2002`
