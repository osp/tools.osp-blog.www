Title: Follow The Sound Fonts
Date: 2009-09-08 12:02
Author: Harrisson
Tags: News
Slug: follow-the-sound-fonts
Status: published

Band font for Follow The Sound festival were designed in collaboration
with Jean Baptiste Parre from [LPDME](http://www.lpdme.org/). It's
remixes of the Avant Garde font, semi bold, in it's GPL version: URW
Gothic, part of the Ghostscript
[urw-fonts-1.0.7pre4](http://svn.ghostscript.com/viewvc/tags/urw-fonts-1.0.7pre44/)4
package. Valek Filippov version with cyrillics can be downloaded
[here](http://linux.softpedia.com/get/Desktop-Environment/Fonts/Type1-URW-fonts-with-Cyrillics-28227.shtml).

![sample\_urwgothic]({filename}/images/uploads/sample_urwgothic.png "sample_urwgothic"){: .alignleft .size-full .wp-image-3352 }

2 fonts were designed: Whisky Jazz and Distilled Spirit, both used in
the project to give strange rythm. Please note the M letter which is a
nice graffiti inspired trick. (We love it!).

![sample\_DS\_and\_WJ]({filename}/images/uploads/sample_DS_and_WJ.png "sample_DS_and_WJ"){: .alignleft .size-full .wp-image-3352 }

Take them here:
[whiskyjazzbeta]({filename}/images/uploads/whiskyjazzbeta.ttf)
and
[distilledspirit]({filename}/images/uploads/distilledspirit.ttf)

They're raw and unfinished, but correspond to a spontaneous
enthousiasm.  
It's beta versions, only the capitals were done. If anybody feels the
pleasure of forging other letters, feel welcome!

Black lettrines comes from 19th century font samples.

This work is released under Free Art License.
