Title: A fish can't judge the water
Date: 2006-06-02 22:54
Author: Femke
Tags: News, Reading list, Thoughts + ideas
Slug: a-fish-cant-judge-the-water
Status: published

<small>[Constant's](http://www.constantvzw.com) contribution to
[OknoPublic01](http://www.okno.be/), May 26 2006</small>

`New media curator --- information architect -- theater maker -- science fiction writer -- data base programmer -- media designer or software artist: we use computer programmes to write, read, listen, publish, edit and play. More than often we do all these things at the same time and in connection to each other.`

`But do we use software to think?`

`------`  
<!--more-->

`My physiotherapist used the following analogy to explain how humans use tools to negotiate the space around their bodies:`

`"If you prepare a sauce..." she said, "and stir it with a wooden spoon... you will be able to feel at which moment exactly the starch starts to burn to the bottom of the pan".`

`A wooden spoon might not be the kind of glamour and glitter a post human cyborg is looking for, but I think it is in this unspectacular way our daily operations with software help to make sense of our environment.`

`It has become our natural habitat. We practice software until we in-corporate its choreography. We make it disappear in the background. A seamless experience. We become one with our extensions.`

`Counter to this magic vanishing, with each possibility opened up by an operating system or software package, the space within which making can take place is circumscribed.`

`Software is never politically neutral, nor are its aesthetics without colour: each product prescribes use, and results in specific forms, sounds and shapes.`

`Software produces culture at the same time as it is produced by culture. It is shaped through and locked into economic models of production and distribution. This is obviously as much true for a wooden spoon as it is for Apple Quicktime Broadcaster but in software this "lock" is apparent in the crudest way possible. Fortunately no Anti Piracy Police is interested in my kitchen. Nor are the products of my cooking subject to Digital Rights Management. `

`Do You Have The Right Plug-in Installed? It Looks Like You Are Writing A Letter! You Have Unused Icons On Your Desktop... PLEASE READ THIS SOFTWARE LICENSE AGREEMENT CAREFULLY BEFORE DOWNLOADING OR USING THE SOFTWARE.`

`Could we understand what software does to our work and working patterns without being able to step away from it? What if our work is not only made with, but also through software? What if our work IS software?`

`Can we think ourselves outside it?`

`------`

`"someone can know how to type but not know how to read the words produced (...) just as someone can be able to read a typescript without knowing how to type" (Katherine Hayles - How We Became Posthuman)`

`Because we want to be both typist and reader, Constant is committed to Open Source Software. Or more precisely: we are interested in the tension between those two positions.`

`Constant is not a community of programmers. We do share a desire to adjust, reinvent, change and examine the instruments we use and we like to create situations which allow for that kind of reflection.`

`Our decision to use and produce Open Source tools is therefore as much political, as it is in line with the nature of our artistic and intellectual interests.`

`As each tool is scripted with use, we very much enjoy to be immersed in a milieu (or in fact: a stockpile of milieus) which emanates collaboration rather than individual authorship, which builds on exchange rather than on exclusivity. A milieu which supports biodiversity; a rich mixture of programmes and approaches.`

`Of course we get frustrated sometimes using Open Source software; one does not always have the time and energy to not know what to expect. But it is a luxury to find other experiences than those we were used to; it offers an opportunity to rethink “user-friendly-ness” to start with. "Usability" might mean something else all together depending on who is using something, and what she is using it for.`

`We are ultimately interested in making differences, glitches, misunderstandings and hick ups productive. Our work is, as much as the software we use and produce, “work in progress” and this means it’s cut-off points are not necessarily concealed.`

`We like to cross boundaries, but we don't want to erase them. We traverse different worlds, we do not make them the same. In fact, we are interested in everything that shows up in the cracks.`
