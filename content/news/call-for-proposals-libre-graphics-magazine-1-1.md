Title: Call for proposals: Libre Graphics Magazine 1.1
Date: 2010-09-11 14:14
Author: OSP
Tags: News, call, LGM, submit
Slug: call-for-proposals-libre-graphics-magazine-1-1
Status: published

> Libre Graphics Magazine is seeking submissions for its first numbered
> issue, which loosely follows the theme First Encounters/Taking Flight.
> Submissions can range from the written, to the visual, to the
> interactive. If it can be flattened and printed, it could appear in
> our print edition. If it moves or requires user input to be seen
> properly, it might just be a good fit for our web edition. Proposals
> for articles or works (or even already completed works), may be
> submitted until October 3, 11:59 Eastern time. Proposals for articles
> should be no more than 100 words, although the articles themselves may
> be up to 1000 words. Proposals and work may be sent to
> submissions@libregraphicsmag.com

Read the full call at:
[http://libregraphicsmag.com](http://libregraphicsmag.com/)
