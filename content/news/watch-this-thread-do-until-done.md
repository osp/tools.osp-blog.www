Title: Watch this thread: do until done
Date: 2007-05-25 10:18
Author: Femke
Tags: News, Licenses, Patents, Watch this thread
Slug: watch-this-thread-do-until-done
Status: published

Do free software developers need lawyers, when reverse engineering
formats such as Corel Draw? How to work on the compatibility of file
formats, when risking law suits from patent holders? Should we forget
about opening up proprietary formats altogether? Jon Philips takes the
position that developers should 'do until done':

> Its good to know generally the law, but not to be encumbered by it.

Read full thread in the Create-list archives:  
<http://lists.freedesktop.org/archives/create/2007-May/000787.html>
