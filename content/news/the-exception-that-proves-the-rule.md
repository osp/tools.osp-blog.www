Title: The exception that proves the rule
Date: 2009-03-18 10:33
Author: Femke
Tags: News, Awards
Status: published

[![wire]({filename}/images/uploads/wire.jpg "wire"){: .float }]({filename}/images/uploads/wire.jpg)

[Felix Kubin](http://www.felixkubin.com/) writes:

"As an antidote to globalisation, Ich Bin suggest local megalomania:
Mulhouse for capital! '*Avec le Gewuerz et le Schnaps*.'

The record was designed by the talented Monsieur Harrisson, a French
refugee now residing in Brussels</em> (...)"

<div class="clear">

</div>

So, OSP makes an exception for Harissons' \***last ever**\* design in
proprietary software: we're proud to announce that his cover for Ich Bin
is featured as sleeve-of-the-month in [Wire](http://www.thewire.co.uk/)!
