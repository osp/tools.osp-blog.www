Title: Support LGM2010!
Date: 2010-02-22 08:00
Author: OSP
Tags: News, Tools, LGM 2010
Slug: support-lgm2010
Status: published

[![](http://stdin.fr/lgm/pledgie/pledgie_banner.png)](http://pledgie.com/campaigns/8926)

At the yearly [Libre Graphics
Meeting](http://www.libregraphicsmeeting.org), developers and users of
our favorite tools get together to work on better software. For some of
them, a trip to Brussels is easy to fund, others cannot afford the
journey without our help. Let's pull our resources together and [raise
10.000 \$](http://pledgie.com/campaigns/8926) (7350 €) so that they can
meet face-to-face, exchange and collaborate on the kinds of tools we
like to use.  
<!--more-->  
Pledgie campaign page: <http://pledgie.com/campaigns/8926>  
Press release: <http://create.freedesktop.org/wiki/Press_Release_2>

**How can you help LGM2010?**

-   Donate to the [Pledgie campaign](http://pledgie.com/campaigns/8926)
    (small contributions welcome too!)
-   Spread this message on your (micro)blog (tag: !lgm), social network,
    mailinglist
-   Copy + paste the code below and add a dynamic banner to your site:

<form>
<textarea cols="55" rows="3" name="snippet">
&lt;a style='border-style:none'
href='http://pledgie.com/campaigns/8926'&gt;&lt;img
src='http://stdin.fr/lgm/pledgie/pledgie\_banner.png'&gt;&lt;/a&gt;

</textarea>
</form>

