Title: The four freedoms, two rules and one jam
Date: 2010-06-05 16:31
Author: Pierre
Tags: News
Slug: the-four-freedoms-two-rules-and-one-jam
Status: published

At our Bauhaus Weimar presentation, we decide to make jam. So an
adaptation of the [GPL](http://www.gnu.org/philosophy/free-sw.html)
principles seems useful.

[![Freedom 0 - use
it]({filename}/images/uploads/P1040639rr.jpg "P1040639rr"){: .alignleft .size-medium .wp-image-4566 }]({filename}/images/uploads/P1040639rr.jpg)

[![Freedom 1 - study
it]({filename}/images/uploads/P1040640rr.jpg "P1040640rr"){: .alignleft .size-medium .wp-image-4566 }]({filename}/images/uploads/P1040640rr.jpg)

[![Freedom 2 - modify
it]({filename}/images/uploads/P1040641rr.jpg "P1040641rr"){: .alignleft .size-medium .wp-image-4566 }]({filename}/images/uploads/P1040641rr.jpg)

[![Freedom 3 - share
it]({filename}/images/uploads/P1040642rr.jpg "P1040642rr"){: .alignleft .size-medium .wp-image-4566 }]({filename}/images/uploads/P1040642rr.jpg)

[![Attribution]({filename}/images/uploads/P1040643rr.jpg "P1040643rr"){: .alignleft .size-medium .wp-image-4566 }]({filename}/images/uploads/P1040643rr.jpg)

[![Do not enclose
it]({filename}/images/uploads/P1040644rr.jpg "P1040644rr"){: .alignleft .size-medium .wp-image-4566 }]({filename}/images/uploads/P1040644rr.jpg)

[(Free Art Licence)](http://artlibre.org/licence/lal/en)
