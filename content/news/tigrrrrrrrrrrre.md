Title: Tigrrrrrrrrrrre!!!
Date: 2006-03-20 23:50
Author: Harrisson
Tags: News, Tools, Design Samples, Scribus
Slug: tigrrrrrrrrrrre
Status: published

![tigre2.jpg](http://ospublish.constantvzw.org/blog/wp-content/tigre2.jpg)

Le Tigre, here, is not the translation of a Mac os in french, nor a
No-Wave grrrrl band. It's a brand new generalist weekly french magazine
that released its first issue 3 days ago. The big thing here is that
this mag is entirely set on Scribus, and proove by fact that this FLOSS
can be incorporated on a professionnal workflow. The 24 pages magazine
took choice of incorporating no ads, and thus depends on the readers.

[www.le-tigre.net](http://www.le-tigre.net/)

Articles are described as wide and various, but precise and
entertaining, from comment of worldnews to science fiction. More once
touched and seen in real!
