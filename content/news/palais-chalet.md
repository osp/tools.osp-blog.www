Title: Le Palais Châlet
Date: 2008-10-05 16:05
Author: Harrisson
Tags: News, Works, Gimp, Music
Slug: palais-chalet
Status: published

[![]({filename}/images/uploads/corneille_noire_web_400.png "corneille_noire_web_400"){: .alignnone .size-full .wp-image-950 }]({filename}/images/uploads/corneille_noire_web_400.png)

This flyer has been done by hand and by gimp with our beloved DJ
[Atka](http://www.pneu.org/spip.php?article48), to promote a new
clubnight: **Le Palais Chalet**.

Palais Chalet first event will be this 24th of October, and is called
**La Corneille d'Or**. **[Pierre
Normal](http://www.pneu.org/spip.php?article39)**, from
[pneu](http://www.pneu.org) record label and
[**Dolina**](http://profile.myspace.com/index.cfm?fuseaction=user.viewprofile&friendid=233487710)
will play live. Atka, [DJ Athome](http://www.djathome.org/) and [Le
Caniche Noir](http://www.pneu.org/spip.php?article47) will put records.  
It will be in Brussels, 20:00, at compilothèque, 50 quai des péniches,
where we've done our previous print party. Don't hesitate to come, no
doubts we'll hear there future OSP hymns, and sure we'll get inspired by
boccocini and coktails.
