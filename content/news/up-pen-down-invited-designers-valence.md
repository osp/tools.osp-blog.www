Title: Up Pen Down, 2012/2013 invited designers at the ESAD Valence Grenoble
Date: 2013-10-06 17:42
Author: Eric
Tags: News
Slug: up-pen-down-invited-designers-valence
Status: published

[![poster0](http://schr.fr/get/2013/06/poster0.jpeg){: .alignnone .size-medium .wp-image-226 }](http://schr.fr/get/2013/06/poster0.jpeg)

After Metahaven (2008/2009), Åbäke (2009/2010), Paul Elliman (2010/2011)
and LUST (2011/2012), OSP are honoured to join the ESAD’s program as
invited designers, giving workshops throughout the first semester.

<!--more-->

The intervention of OSP is situated around the notion of stroke, in the
sense of path—as opposed and informed by the notion of form. Allowing
itself to be explored through the different modalities of drawing,
typography and cartography, the stroke is intimately related to the tool
by which it is drawn, thus influencing the visual language.

As technical, intellectual and cultural constructions, software embodies
specific conceptions of the objects they are designed to manipulate
(stroke and form, in this case). The PostScript format, for example,
describes letterforms by their contour instead of their skeleton. Yet
other, lesser known file formats might take an inverse approach.

By virtue of the availability of the source code and more importantly
still by the open discussions around their conceptions (mailing lists,
collaboration platforms), free and open source software allows the
designer the better grasp the implications of the tools for their
process.

These are some of the projects developed during OSP’s collaboration with
the school:

TURTLE Graphics
---------------

<iframe src="http://player.vimeo.com/video/68146834?title=0&amp;byline=0&amp;portrait=0"  frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>

The English mathematician Seymour Papert tried to create an embodied
understanding of mathematics through his LOGO language. Papert created a
language, understood both by humans and computers, that describes
drawings not in an absolute cartesian grid, but from the perspective of
the person that is drawing. He called this TURTLE graphics.

Using the [Seymour](https://gitlab.com/pzi-prototyping/seymour)
extension, we run TURTLE scripts within the vector drawing program
Inkscape to create letterforms. We then follow the lead of [Papert’s own
experiments](https://www.youtube.com/watch?v=bOf4EMN6-XA), by having
participants take upon them the role of the turtle, having the others
instruct them where to draw.

We use Dishu, the Chinese practice of large scale ephemeral typography
with sponges and water, as introduced into the context of occidental
graphic design by [François Chastanet](http://francoischastanet.com/).
In a performance, all students and OSP members present gather in the
main hall of the workshop. The first participants leave towards the
bridge overseeing the main hall. They instruct the others on how to draw
letters. Having drawn a letter, they in turn go to the bridge and
instruct others. The words ‘UP PEN DOWN’ are written.

NO-FEATURE Type Foundry
-----------------------

[![http---no-feature.github.com-](http://schr.fr/get/2013/06/http-no-feature.github.com-.jpg){: .alignnone .size-medium .wp-image-226 }](http://no-feature.github.io/)

Inkscape is a fine program to create drawings, but does not have the
possibility to create font files. Using FontForge we turn the turtle
fonts into typefaces.

The [NO-FEATURE foundry](http://no-feature.github.io/) is a space around
typographical exchange and collaboration for (ex-)teachers and
(ex-)students of the ESAD. The TURTLE fonts developed during the first
session are the first fonts to be published on this platform, along with
the typeface Dauphine developed for the school’s identity. Publishing
the sources in the UFO format, the fonts invite new contributions and
adaptations.

Git, ce connard: Retrospective Publication
------------------------------------------

[![valence-magazine-book-mockup]({filename}/images/uploads/valence-magazine-book-mockup.jpg){: .alignnone .size-medium .wp-image-226 }]({filename}/images/uploads/valence-magazine-book-mockup.jpg)

A publication documents the four preceding sessions. The publication is
created with ConText and a small edition is printed in the school’s
printing workshop. Both the
[](https://github.com/no-feature/up_pen_down/raw/master/TEX/Publitexion.pdf)PDF
version</a> and the [source
files](https://github.com/no-feature/up_pen_down) are available on-line.

Participants
------------

Alexandra Rio, Alice Jauneau, Anaïs Alauzen, Camille Chatelaine, David
Vallance, Éléonore Jasseny, Gwenaël Fradin, Jordane Cals, Lorène Ceccon,
Margot Baran et Vincent Duché

Bibliography
------------

-   Fernand Baudin, L’écriture au tableau noir, Retz, 1984
-   Walter Crane, Line and Form, 1900 --
    <http://www.gutenberg.org/files/25290/25290-h/25290-h.htm>
-   Tim Ingold, Une Brève histoire des lignes, Zones Sensibles, 2011
-   Vilém Flusser, "Le geste d’écrire", in Flusser Studies, n°8, Mai
    2009 -- <http://www.flusserstudies.net/pag/08/le-geste-d-ecrire.pdf>
-   Donald Knuth, "The Concept of a Meta-Font", in Visible Language,
    issue 16.1, January 1982; traduction "Le concept de métafonte", in
    Communication et langages, n°55, 1983, pp. 40-53 --
    <http://www.persee.fr/web/revues/home/prescript/article/colan_0336-1500_1983_num_55_1_1549>
-   Donald Knuth, "Lessons learned from MetaFont", in Visible Language,
    issue 19.1, December 1985
-   Gerrit Noordzij, The Stroke - theory of writing, Hyphen Press, 2006=
-   Femke Snelting, "Scenes of Pressures and relief", 2009 --
    <http://snelting.domainepublic.net/texts/pressure.txt>

