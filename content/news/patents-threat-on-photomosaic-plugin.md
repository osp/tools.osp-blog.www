Title: Patents threat on photomosaic plugin
Date: 2009-08-07 19:28
Author: nicolas
Tags: News, Gimp, Patents
Slug: patents-threat-on-photomosaic-plugin
Status: published

Found on the [developper's
website](http://www.kirchgessner.net/photo-mosaic.html):

> The image mosaic plug-in for the GIMP is no longer supported or
> distributed. Mr. Robert Silvers, the holder of a patent related to the
> technology that was used in the plug-in, argued that the software
> would directly infringe his patent rights. It is not clear if the
> patent is applicable in this case. But I have neither the time,
> interest or money for legal action. So I complied with the cease and
> desist request.
>
> PHOTOMOSAIC® is a registered trademark of Runaway Technology, Inc. The
> photomosaic process is patented (US Patent No. 6,137,498) and
> protected by the patent, copyright, and other intellectual property
> laws of the United States and other major countries.

It seems though that [someone
else](http://thebigmachine.org/gimp/pmosaic/) has taken the development
further:

> This software is currently being updated for Gimp 2.2. Please check
> back regularly. In the meantime, the Gimp 1.x version and instructions
> can be found below, although they may be somewhat unreliable.

This second version of the plugin avoided the technology covered by the
patent or ... sanely ... ignored it?
