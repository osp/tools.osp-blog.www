Title: Open Printing Summit
Date: 2007-07-17 08:43
Author: Femke
Tags: News, Printing + Publishing, Standards + Formats
Slug: open-printing-summit
Status: published

The Linux Foundation OpenPrinting work group, organises a get-together
24-27 September in Montreal.  
<http://www.linux-foundation.org/en/OpenPrinting/SummitMontreal>

As stated on their site, the printing activities of the Linux Foundation
revolve around a few focal points:

-   For printer manufacturers, we want to make it easier to produce
    drivers that work across distributions and to get those drivers in
    the hands of end users, to reduce problems that result from running
    outdated drivers.
-   For application developers, we want to provide better facilities
    than we do today to add printing to applications in a portable and
    consistent way.
-   For operating system vendors, we want to put together a shared
    repository of printer drivers, so the OS/distribution vendors can
    share the burden of maintaining the (largely common) driver database
    with each other.
-   A major theme of the Printing Summit will be ensuring we have the
    proper standards in place at both the application and driver level
    to accomplish the goal of making printing on Linux "just work". In
    addition, we will discuss related topics, such as developer tools,
    developer documentation, certification, and testing, that will be
    important to the goal of making the standard actionable.

