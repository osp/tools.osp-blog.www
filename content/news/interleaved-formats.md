Title: Interleaved formats
Date: 2010-07-23 22:21
Author: Ivan
Tags: News, Texts Further reading, Interview, Standards + Formats, SVG
Slug: interleaved-formats
Status: published

[![svg
logo]({filename}/images/uploads/post11.png "interview"){: .alignnone .size-full .wp-image-4775 }](http://www.w3.org/blog/SVG/2010/06/04/interview_ie)

This [interview](http://www.w3.org/blog/SVG/2010/06/07/ie9_suport) may
be a good read. It deals with some aspects of the "current state" (June
2010) of SVG implementations.

It's got a really sweet format. It's a two-sided interview, meaning that
Doug Scheppers and Patrick Dengler interview each other. It's not often
that I come across interviews that are *interleaved*:

> I asked Patrick some questions, and he asked me some.

To read about technologies that I may (or may not) get a chance to
explore is super enjoyable. This time, my favorite was: *an HTML version
of SMIL called HTML + Time*.

The interview has an
[addendum](http://www.w3.org/blog/SVG/2010/06/07/ie9_suport) :)
