Title: Read Feeds
Date: 2008-07-13 10:02
Author: OSP
Tags: News, Free Software Community
Slug: feed-readers
Status: published

**Planet Open Fonts**  
"*Planet Open Fonts is a window into the lives and work of people
forming the open font community*" writes Nicolas Spalinger in the
editorial of this very useful aggregration page, and OSP is proud to be
included: <http://planet.open-fonts.org/>

**Create @ f.d.o.**  
Another useful collection of feeds is at
<http://create.freedesktop.org/> bringing news from projects connected
to the Free Desktop organisation, "*working on interoperability and
shared technology for X Window System desktops; a* collaboration zone
*where ideas and code are tossed around, and de facto specifications are
encouraged*".
