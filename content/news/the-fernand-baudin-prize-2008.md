Title: The Fernand Baudin Prize 2008
Date: 2009-01-02 14:18
Author: Harrisson
Tags: News, Printing + Publishing
Slug: the-fernand-baudin-prize-2008
Status: published

[![]({filename}/images/uploads/logofb.png "logofb"){: .alignnone .size-full .wp-image-1614 }]({filename}/images/uploads/logofb.png)

“The Fernand Baudin Prize” has been initiated by several graphic
artists-teachers who work in the book world. The prize is supported by
Brussels-Export. The principle aim of this initiative is to encourage
the creation of the contemporary book in the Brussels-Capital region by
honouring the most beautiful books. It is also intended to bring this
creativity to the notice of a wide public and give it an international
dimension through an exhibition and a catalogue which will be
distributed abroad.  
<!--more-->  
Belgium and particularly the Brussels-Capital region have undeniable
roots in the tradition of the book. This know-how, which has been
under-valued compared to that of its Dutch, Swiss or German neighbours
(who, for a long time, have honoured their books), will now come to the
fore on the international scene through the existence of a prize which
will support its innovations and its creators.

**Participants**  
The following may participate:  
1) Graphic artists (workshops, design bureaux etc. are also admitted)  
2) Publishers  
3) Printers and/or book-binders  
4) Artists

[Rules and application
form](http://www.prixfernandbaudinprijs.be/en/home.php?page=reglement)

OSP is part of this competition  
(and has been commissionned to create the FBP logo ;) )
