Title: Vote for Scribus
Date: 2008-05-21 12:09
Author: Femke
Tags: News, Tools, Scribus
Slug: vote-for-scribus
Status: published

![etoy]({filename}/images/uploads/etoy.jpg){: .float}Our
favourite killer-app Scribus is competing to be included in
[Hackontest](http://hackontest.org), a "*24 hour programming competition
between teams of three open source software projects*".  
[A container designed by
E-Toy.corporation](http://www.etoy.com/projects/etoy-tanks/) will house
the competition, which takes place at the Google sponsored
[OpenExpo](http://www.openexpo.org/en) (the Swiss conference and trade
show for Free and Open Source Software). "*This room will be equipped
with desks, sofas, Internet, pizza etc.*"  
Besides putting up a reward for the best code muscle rolling, Hackontest
asks the audience to judge 'soft factors' such as team work and
collaboration.

Heaven or hell, on the Hackontest website you can propose or vote for
features you would like to be worked on. Of course it would be even more
constructive to file requests in the [Scribus Bug
Tracker](http://bugs.scribus.net) but it is a quick and easy way to let
people know that Scribus matters. So, log in and vote!

<http://hackontest.org/index.php?action=Root-projectDetail%282%29>
