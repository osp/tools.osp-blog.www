Title: L'édition comme l'expérience 
Date: 2018-03-05 12:00 
Author: Ludi, Gijs
Tags: News 
Status: published


Gijs and me joined [L'édition comme expérience](https://villa-arson.xyz/edition-experience/) at La Villa Arson last week.
Monday 12th and Tuesday 13th of March, two days to thread relations from *Other ways*, *Consciousness raising* to Montessori's esthetic, au pays de Célestin Freinet.

![](http://ospublish.constantvzw.org/images/var/albums/edition-experience/20180312_114623_HDR.jpg)

In parallel to the conferences and library books speaking, we crashed into *learning by doing* with a mixed group of students (from La Villa and Drap BTS).
As their first lines of html and css we built a live publication of the event: gathering texts from the presentations, pictures, indexes of books and drawings.
Our kit: Ethertoff on a raspberry, markdown, html, and css. In preparation for the event we designed the [programme](https://villa-arson.xyz/edition-experience/), a webpage using CSS grids that can be printed and folded into a booklet.

![](http://ospublish.constantvzw.org/images/var/albums/edition-experience/20180313_221210.jpg)
![](http://ospublish.constantvzw.org/images/var/albums/edition-experience/20180313_225540.jpg)
![](http://ospublish.constantvzw.org/images/var/albums/edition-experience/20180313_204052.jpg)
![](http://ospublish.constantvzw.org/images/var/albums/edition-experience/20180313_230134.jpg)

Based on OSP experiences during [relearn](http://relearn.be/2017/), [What's the Matter with Cooperation](http://osp.kitchen/work/buda-wtmw-cooperation/) and [<o>f-u-t-u-r-e</o>](http://f-u-t-u-r-e.org/) ethertoff felt like a fitting tool. We brought it installed on a raspberry pi only accessible from the local network. For only two days this added a layer of software but also brings the question of the infrastructure or the omniprescence of the cloud: “What? You've shut down the raspberry and now everything's gone from the web?”.

![](http://ospublish.constantvzw.org/images/var/albums/edition-experience/20180314_000126.jpg)

CSS grid appeared as a new playground for us and also as web layout track opening good discussion between the InDesign swiss grid and the web flow.
Our enthousiasm with it still hit quite some bricks in layouting for the print in such a quick time but Gijs already started some scripts to unfold the rows according to the lenght of the content.

We recognized ourselves in the setup of Freinet, put the machine in the middle of the room and starting directly with complexity.

Thanks Céline Chazalviel for the organisation and invitation. Bravo aux étudiant.e.s!

Sources files are available on [osp.kitchen/villa-arson/edition-experience](http://osp.kitchen/villa-arson/edition-experience/).