Title: Mute Magazine: open soon
Date: 2007-01-30 10:11
Author: Femke
Tags: News, Works, Printing + Publishing, Scribus
Slug: mute-magazine-open-soon
Status: published

[![books.JPG]({filename}/images/uploads/books.JPG){: #image181}]({filename}/images/uploads/books.JPG "books.JPG"){: #image181}

[Mute Magazine](http://www.metamute.org/) develops Open Source software,
and has recently installed Ubuntu on all office machines. Now they want
to move their lay-out away from QuarkXpress too.

Last week, Simon Worthington and Darron Broad from Mute came over to
discuss the project. First of all we will need to make sure their
original templates can be migrated to Scribus - with a little help of
Python, this will hopefully not be such a big problem.

In parallel we started thinking about ways to connect Content Management
Systems ([Drupal](http://drupal.org/) in their case) to Scribus, so that
part of the editing process can be eventually automated. Printing On
Demand would not be far off from that -- exciting developments ahead.

But first things first: from the June 2007 issue onwards, with the help
of the Open Source Publishing team, Mute will be entirely produced with
free software. Mute will also make sure the project/process will be
properly documented and fed back into the Scribus community.

Good news!
