Title: Mute: now available in Free Software
Date: 2007-09-16 07:57
Author: Femke
Tags: News, Works, Design Samples, Scribus
Slug: mute-vol-2-6-in-free-software
Status: published

[](http://www.metamute.org/en/Mute-vol-2-6-Living-in-a-Bubble-Credit-debt-and-crisis)[![cover\_mute.jpg]({filename}/images/uploads/cover_mute.jpg){: .float}](http://www.metamute.org/en/Mute-vol-2-6-Living-in-a-Bubble-Credit-debt-and-crisis "cover_mute.jpg")**Mute
Vol 2 \#6 - Living in a Bubble: Credit, debt and crisis** is out!

After a few months of testing and trying, the work flow of the entire
magazine was converted to Free Software (OpenOffice, Inkscape, Gimp and
Scribus on Kubuntu and Ubuntu systems) and... with succes.

<div class="clear">

</div>

Read low-graphic pdf's online or order your own copy to see it with your
own eyes:
[http://www.metamute.org/](http://www.metamute.org/en/Mute-vol-2-6-Living-in-a-Bubble-Credit-debt-and-crisis)
