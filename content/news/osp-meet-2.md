Title: OSP Meet #2
Date: 2012-02-15 21:45
Author: Eric
Tags: News, OSP Public Meets
Slug: osp-meet-2
Status: published

[![]({filename}/images/uploads/balsa-affiche2012-print.png "balsa-affiche2012-print"){: .alignnone .size-medium .wp-image-7054 }]({filename}/images/uploads/balsa-affiche2012-print.png)

### This Monday 20th of February

Our first public OSP-meet was a success and we are expanding on the
formula. Instead of just discussing the work of OSP, we invite you to
bring on the table any graphic design project that involves or has been
produced with free / libre / open source software.

### We start at 18:30

An ongoing project is the visual identity for the [Balsamine
Theatre](http://www.balsamine.be/ "Balsamine | Main / HomePage"). We
will show a new set of posters fresh of the printer.

We are working on a more visually communicative way to share our project
files and we’ll provide a sneak peek at the meet.

Our friends of [Manufactura
Independente](http://blog.manufacturaindependente.org/ "Manufactura Independente | Libre Graphics & Design research studio")
will share their recent work on multi-coloured typefaces.

And then the meeting is open to you! You can let us know in advance if
you want to show something, or tell us the evening itself.

We will spin open source tunes. There will be light snacks.

*Address:*  
Constant Variable  
Rue Gallait 80  
1030 Brussels

If you can’t be there live, join the IRC (Internet Relay Chat):

<iframe src="http://webchat.freenode.net?channels=osp-meet&amp;uio=d4" ></iframe>

( [\#osp-meet on
freenode](http://webchat.freenode.net/?channels=osp-meet&uio=d4 "#osp-meet on Freenode webchat")
)
