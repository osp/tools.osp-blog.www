Title: Open Clip Art Library
Date: 2010-03-09 02:39
Author: OSP
Tags: News, clipart, Digital drawing, Licenses, repository, SVG, vector
Slug: open-clip-art-library
Status: published

[![]({filename}/images/uploads/Screenshot-1.png "Screenshot-1"){: .alignright .size-medium .wp-image-4279 }]({filename}/images/uploads/Screenshot-1.png)  
A brand new version of the [Open Clip Art
Library](http://www.openclipart.org/) just came out!

26175 (and growing) scalable vector graphics, all available under a
[public domain
license](http://creativecommons.org/licenses/publicdomain/): they may be
freely reproduced, distributed, transmitted, used, modified, built upon,
or otherwise exploited by anyone for any purpose, commercial or
non-commercial, and in any way, including by methods that have not yet
been invented or conceived.  
[http://www.openclipart.org](http://www.openclipart.org/)
