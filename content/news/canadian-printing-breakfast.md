Title: Canadian Printing Breakfast - Impressions canadiennes au petit-déjeuner 
Date: 2007-06-10 14:05
Author: Femke
Tags: News, Inkscape, Print Party, Printing + Publishing, Scribus, Scripting
Slug: canadian-printing-breakfast
Status: published

### Samedi 16 Juin

`Nepomuk Bar - City Mine(d) - Rue Saint-Jean Nepomucen, 17, Bruxel`

<small>Click
[**here**](http://ospublish.constantvzw.org/?p=257#more-257) for
English</small>

![siropderable.JPG]({filename}/images/uploads/siropderable.JPG){: .float}  
Open Source Publishing est fier de vous inviter à son petit-déjeuner
canadien, où vous seront servies leurs dernières aventures dans le monde
des Logiciels Libres. Au menu: mise en page animée d’un conte de fée,
programmeur chercheur de singes à Madagascar, et A qui change tout. Ne
ratez pas ce mélange détonant de typographie, de scripts en python et de
sirop d’érable!  
  
  
`Language: FR / EN Le service commencera à 10:00, la présentation à 11:00. Gratuit, avec le petit déjeuner. Réservations: femke at constantvzw.com`

<!--more-->

### Saturday June 16

`Nepomuk Bar - City Mine(d) - Sint-Jan Nepomucenusstraat 17, Brussels`

![siropderable.JPG]({filename}/images/uploads/siropderable.JPG){: .float}The
Open Source Publishing team would like to serve you their latest
adventures in free software for breakfast. With amongst other things a
lay-out application animating a fairy tale, a software developer looking
for monkeys in Madagascar and a letter A that changed everything. Don't
miss the miraculous mixture of pancakes, typography and python
scripting!  
  
  
`Language: FR / EN Doors open for breakfast at 10:00 / presentations start 11:00 Entrance free, including breakfast - please reserve at: femke at constantvzw.com`
