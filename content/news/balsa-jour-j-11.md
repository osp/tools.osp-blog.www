Title: Balsa Jour J-11
Date: 2011-09-08 15:13
Author: Pierre
Tags: News, Tools, Type
Slug: balsa-jour-j-11
Status: published

"À l'extérieur de cette boîte si tranquille, de bouillonnantes
transformations s'opèrent également!"

— From hand movement to vector curves to hand movement

[![]({filename}/images/uploads/MarcdeMeyerpeintre.jpg "MarcdeMeyerpeintre"){: .alignnone .size-medium .wp-image-6721 }]({filename}/images/uploads/MarcdeMeyerpeintre.jpg)
