Title: la bouche de la reine
Date: 2010-11-05 16:26
Author: OSP
Tags: News, casting, counter-punch, face, folly as an inversion, Johann Froben, letterpress, matrix, mirror, printing, punch, text directionality
Slug: la-bouche-de-la-reine
Status: published

![]({filename}/images/uploads/bouche.jpg "bouche"){: .alignnone .size-full .wp-image-5237 }

<small>Courtesy: Inkscape parallel-stroke hatching</small>
