Title: Xara Xtreme Xbecomes Xopen Xsource.
Date: 2006-03-22 12:16
Author: Harrisson
Tags: News, Tools
Slug: xara-xtreme-xbecomes-xopen-xsource
Status: published

Xara Xtreme is a vector based software. It runs under Windows and Linux
environment. It is a "crossover" software, means it manages pixels and
vectors at the same time.

After 15 years of proprietary software status, Xara is operating a
strategical migration to open source. The fact that Adobe purchased
Macromedia put the developper in a uncomfortable concurrencial
situation.  
<!--more-->  
Opening the software to GPL is hoped to bring "consolidation" via FLOSS
communities, inspired by the succesfull Inkscape model.

The 2 societies are currently in contact to synchronise on a common file
format, and to develop a converter which would enable to turn Xara (Xar)
into SVG (Scalable Vector Graphics - around which Inkscape is
constructed). This would give the ability for both sotwares to import
and export numbers of other formats as well.

The Mac OSX version is still in development.  
**Xara Xtreme on the Mac**  
*We have a new cross-platform code base, WXWidgets based, that is cross
platform C++ and should work on the Mac, but we're short of Mac
developers and testers that can help progress this. If you're a Mac
developer willing to help us please contact us. Our first stage goal is
simply to get a working viewer to the same level as the Linux build.*

[www.xaraxtreme.org](http://www.xaraxtreme.org/)
