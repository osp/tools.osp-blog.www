Title: Compatible dates
Date: 2009-03-10 09:05
Author: Femke
Tags: News
Slug: compatible-dates
Status: published

**Monday 24 March 2009**  
***Ada Lovelace Day***  
"I will publish a blog post on Tuesday 24th March about a woman in
technology whom I admire but only if 1,000 other people will do the
same."  
<http://www.promessotheque.com/AdaLovelaceDay>

**Friday 27 March 2009**  
***Thank A Developer Day***  
"The general idea is to appreciate the creators behind your favorite
Free Software application or/and component (preferred target is a not so
well known piece of code) by sending an email to the author."  
<http://thankadev.wordpress.com/>
