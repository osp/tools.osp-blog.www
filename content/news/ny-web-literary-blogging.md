Title: nY-web, literary blogging
Date: 2010-01-18 12:56
Author: Lauren
Tags: News, Works, Webdesign
Slug: ny-web-literary-blogging
Status: published

Alexandre Leray and Lauren Grusenmeyer just finished an intense
collaboration for the website of the literary platform
[nY—](http://ny-web.be)*[website en tijdschrift voor literatuur, kritiek
& amusement, voorheen yang & freespace Nieuwzuid](http://ny-web.be).*

nY-web exists indepently next to the paper edition with it's own
editorial staff, writers categories and settings. While nY is a magazine
with a profound literary history it confirms it's existence as an
independent web platform, that is to say — it uses full virtues of
publishing online.

Coded within the Django framework the nY website covers four zones with
independent qualities. Showtime for publishing news and debates. Transit
zone covers translations for literary texts, reconnecting the field of
flemish literature to more languages. Untimely meditations offers a
platform for short essayist reflections on images, texts and others.

The homepage functions as a timeline record registering the activity of
the four major zones — showtime, untimely meditations, transitzone and
long hard looks. Articles grow in length showing the quantity of
comments.

Alexandre built a custom module for paragraph commenting— based on a
concept of Michael Murhtaugh — which enables readers to interfere on
texts in the side margins. This module replies on the demand of nY for
being not only a platform of publishing but also a platform of
discussion and interaction.

Finally nY-web came to it's existence. The art of blogging in a
different light, that is sure!
