Title: Bram Crevits
Date: 2018-04-06 12:41
Author: OSP
Tags: KASK, Bram, Cimatics
Slug: bram
Status: published

![Algue Print Brussels]({filename}/images/uploads/biocolorlab-l.jpg "One of the first images plotted in the OSP studio with “living colours” from algues grown in KASK’s Biolab")

It is with great sadness that we learned of the unexpected death of our board member Bram Crevits.

We will miss Bram’s energy and his persistent interest in the parcours of OSP. We first met him in 2009, as director of the Cimatics festival for which we made [a typeface](http://osp.kitchen/foundry/cimatics/). Many encounters followed, among which the [Open Tools](http://opentools.schoolofarts.be/) workshop Bram organised at the KASK in Gent. We were honoured when Bram accepted to be a board member when we [formed an association](article-iii-goal-social-object-field-of-action.html). Most recently Bram has connected OSP with the “[Color research](http://laboratorium.bio/)” project between art and science, where we experiment on a playground drawn by a plotter that uses a set of colours based on living organisms—like the algues produced by María Boto Ordóñez in her Biolab at KASK.

Our condolences go to his family and close friends. A non-religious ceremony will be held this Saturday 7 April at 10:30 am - KASK - Zwarte Zaal / Louis Pasteurlaan 2 - 9000 Gent.

> Lieve Bram,  
> Dearest Bram,
>
> Je omarmde projecten voor een betere wereld, vaak was je de eerste. Je hebt ons je vertrouwen geschonken, je hebt ons project verdedigd, je scherpe blik heeft ons vooruit geholpen. Zoals wij zijn er zovelen. Vandaag treuren we samen.
>
> You embraced projects for a better world, often you were the first. You gave us your confidence, you defended our project, and your sharp mind helped us advance. And there are so many like us. Today we mourn together.
>
> We missen je.  
> We miss you.
>
> Open Source Publishing VZW  
> Alexandre, Catherine, Colm, Eric, Femke, Gijs, Ivan, John, Joël, Ludivine, Maxime, Nicolas, Nik, Pierre, Pierre, Sarah, Seb, Stéphanie, Yi
