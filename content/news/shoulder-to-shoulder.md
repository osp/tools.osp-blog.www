---
Title: Shoulder-to-shoulder (or not)
Date: 2023-05-04 17:00
Author: Manetta 
Tags: residency, shoulder-to-shoulder
Slug: shoulder-to-shoulder
---

It's such a pleasure to be able to contribute to this blog (again) and
to announce my residency period at OSP here. :--)

From a very nice freshly installed desk in the back of the OSP studio,
I'm taking the time to reflect and write about design
practices that actively question their way of working, both in relation
to the tools they use and from an interest in collective practices.

The desk that I am working from is a slightly wobbly very nice prototype
of an open source desk, which in a way resonates with me doing a
proto-residency at OSP between April and June 2023, 
to explore how such format could possibly work out for them.

Since 2016, I have been working with F/LOSS publishing tools as well,
mostly in the context of a collective space called
[Varia](https://varia.zone) in Rotterdam. There, my practice of design
and publishing crossed with questions emerging from feminist approaches
and collective infrastructure. At Varia we run a collective digital
infrastructure for ourselves and accidentally also for nodes and people
around us. We work with a set of publishing tools such as
[octomode](https://git.vvvvvvaria.org/varia/octomode),
[logbot](https://git.vvvvvvaria.org/varia/bots/src/branch/master/LogBot),
a [Multifeeder](https://git.vvvvvvaria.org/varia/multifeeder),
[wiki-to-print](https://cc.vvvvvvaria.org/wiki/Wiki-to-print) and
[distribusi](https://git.vvvvvvaria.org/varia/distribusi/). With these
tools we made things such as the [SomeTimes/Af en
toe](https://varia.zone/en/sometimes-af-en-toe.html) and [Toward a Minor
Tech](https://varia.zone/toward-a-minor-tech.html). We started to refer
to these ways of working as *resonant publishing* and became interested
in finding *minimal viable* approaches for collective work. Next to
working on/with software and publications, Varia also organises collective learning sessions
and other type of events, such as the [Publishing
Partyline](https://varia.zone/en/publishing-partyline.html), the [Read
and Repair](https://varia.zone/en/category/readrepair.html) series, or the
[Feminist Hack
Meetings](https://varia.zone/en/category/feminist-hack-meetings.html).

While scrolling through the OSP archives during this first week of my residency, I found a blog post written by former member Harrison in 2006 called [Ok, it is time now.](http://blog.osp.kitchen/news/is-it-possible.html), in which he writes: "Is it possible to get a graphic design professionnal workflow with open source softwares?". Now, 17 years of OSP practice later, there is a lot to speak about and many perspectives to take. And while I'm personally not too interested to work from the question when a practice is "professional" (or not), it feels important to try to articulate (again) what implications this way of working has today. 

I will work from the lens of **(inter-)dependency relations** and focus
on how they shape these design and publishing practices in specific
ways. You can think of (inter-)dependency between for example
practitioners, tools, web standards, and the community of practice
around F/LOSS design in Europe (and/or beyond).

Where do these practices (inter-)depend on? 
Which stories or moments in time make (inter-)dependency relations visible? 
What does it mean, really, to inter-dependent on someone or something else? 
How to understand the difference between dependency, inter-dependency, in-dependency, or other variations? 
How can design or publishing practitioners co-shape their dependencies? 
What makes these practices precarious (or not)? 
And how does F/LOSS based and collective work have an impact on working conditions?

Or, to phrase it differently: what can be (re)learned from operating *shoulder-to-shoulder* (or not)?

I'm writing "(inter-)dependency" on purpose in this way btw, as there
are probably many examples to be encountered in this research where it
is difficult to unravel who depends on who exactly, and when or how a
dependency turns, or could turn, into a multi-directional situation and
become an inter-dependency.

Concretely, I will depart from a moment in time that has a big impact on
the practice of OSP: the story around the CSS Regions, a CSS property
that was removed from modern browsers in 2013, triggering 10 years of
html2print practice based on workarounds and alternate technological
realities.

Publishing environments such as web-to-print are being held together
with awkward and joyful hacks, and I'm looking forward to spend time with them. 

And each morning when I arrive at the OSP studio, I am reminded to cherish such mini-inventions.

<img src="/images/uploads/broken-pencil-desk.jpg" style="width: 300px;margin-top:1em;">
