Title: Launching OSP-works
Date: 2010-03-08 00:00
Author: OSP
Tags: News, Works, Design Samples, Type, Webdesign
Slug: launching-osp-works
Status: published

[![]({filename}/images/uploads/frog-e1267540929456.png "frog"){: .alignright .size-full .wp-image-4176 }](/works)  
5 years of Free, Libre and Open Source design experiments. Now available
in chronological order:
[http://ospublish.constantvzw.org/**works**](http://ospublish.constantvzw.org/works)
