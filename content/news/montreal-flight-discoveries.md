Title: Montreal Flight Discoveries
Date: 2007-05-03 03:41
Author: Femke
Tags: News, Tools, LGM 2007, Scribus
Slug: montreal-flight-discoveries
Status: published

[![af.jpg]({filename}/images/uploads/af.jpg){: .float}]({filename}/images/uploads/af.jpg "af.jpg")While
on our way to the [Libre Graphics
Meeting](http://www.libregraphicsmeeting.org/) in Montreal we've done a
quick test of Scribus 1.3.4cvs and ... good news!

Major New Feature No.1: possibility to add character styles (besides
paragraph styles) and a general clean up of the way styles work. More
testing needed, but interface and direction taken look promising.  
For a description of the character style feature have a look at this
article on the Scribus Developers' blog: [Putting styles in
context](http://rants.scribus.net/2006/11/19/putting-styles-into-context/)

Major New Feature No.2: cropmarks, bleedmarks, and most of all: the
possibility of exporting pdf's including overlap. This solves many of
the '[Rock In Your
Shoe](http://ospublish.constantvzw.org/wiki/doku.php?id=scribus:riys)'
problems we were running up against while converting Mute Magazine into
Scribus.

This version, which is a development version, is not altogether stable
to say the least, but the future looks bright.
