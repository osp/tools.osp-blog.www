Title: Expanded Service Airway
Date: 2010-04-29 18:45
Author: OSP
Tags: News, LGM 2010
Slug: expanded-service-airway
Status: published

Was it volcanic dust? Or something else? Important documents that were
mailed weeks ago from Brussels to Moscow unfortunately never arrived.
Today we sent them again by courier to make sure
[this](http://prokoudine.info/) indispensable LGM-participant will be
able to get a visa for Belgium in time.  
\[gallery link="file" orderby="rand"\]  
All we can do now is wait and hope they arrive from our door to his in
less then 4 days...
