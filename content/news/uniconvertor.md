Title: UniConvertor!
Date: 2007-12-10 21:27
Author: OSP
Tags: News, Tools, sK1, Standards + Formats
Slug: uniconvertor
Status: published

A late announcement for an ultra useful tool: [Igor
Novikov](http://ospublish.constantvzw.org/?p=245) and [Valek
Philippov](http://ospublish.constantvzw.org/?p=245) have released
**[UniConvertor](http://www.sk1project.org/modules.php?name=Products&product=uniconvertor)**,
a universal vector graphics translator. It uses the sK1 engine to
convert one format into another.

With
[UniConvertor](http://www.sk1project.org/modules.php?name=Products&product=uniconvertor)</strong>
you can now convert files from: *CorelDRAW ver.7-X3
(CDR/CDT/CCX/CDRX/CMX), Adobe Illustrator up to v. 9 (AI postscript
based), Postscript (PS), Encapsulated Postscript (EPS), Computer
Graphics Metafile (CGM), Windows Metafile (WMF), XFIG, Scalable Vector
Graphics (SVG), Skencil/Sketch/sK1 (SK and SK1), Acorn Draw (AFF)*...  
into: *AI (Postscript based Adobe Illustrator 5.0 format), SVG (Scalable
Vector Graphics), SK (Sketch/Skencil format), SK1 (sK1 format), CGM
(Computer Graphics Metafile), WMF (Windows Metafile)*...

Uniconvertor is a commandline tool - after installation you simply type:
`$ uniconv drawing.cdr drawing.svg`  
to convert a CorelDraw file to svg for example.

Igor and Valek will keep adding new file formats to this list as they
manage to reverse-engineer them and we'll for sure put it to use.
Thanks!
