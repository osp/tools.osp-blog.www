Title: Old News
Date: 2006-06-04 12:25
Author: Femke
Tags: News, Type
Slug: old-news
Status: published

Discovering that there are so little free fonts\* available, keeps
surprising us. But did you know that even the logo for Debian, the
version of Linux that amongst others Ubuntu is based on, was done in a
proprietary font ([Poppl Laudatio
Condensed](http://www.bertholdtypes.com/bq_library/90090.html))?

[![openlogo-100.jpg]({filename}/images/uploads/openlogo-100.jpg){: #image175}]({filename}/images/uploads/openlogo-100.jpg "openlogo-100.jpg"){: #image175}

<small>\* There is a lot of Freeware around, but hardly any fonts
explicitly allow for re-modification or derivative works.</small>
