Title: FOSDEM 2011
Date: 2011-02-06 19:57
Author: Femke
Tags: News, fosdem, LGM 2011
Slug: fosdem-2011
Status: published

[\[caption id="attachment\_5752" align="alignnone" width="400"
caption="2008"\]![2008]({filename}/images/uploads/2008_2.jpg "2008"){: .size-full .wp-image-5752 }\[/caption\]](http://ja.wikipedia.org/wiki/%E3%83%95%E3%82%A1%E3%82%A4%E3%83%AB:FOSDEM_2008_KeySigning.jpg)[\[caption
id="attachment\_5753" align="alignnone" width="400"
caption="2009"\]![2009]({filename}/images/uploads/2009.jpg "2009"){: .size-full .wp-image-5752 }\[/caption\]](%20http://ben.liveforge.org/2009/02/13/fosdem-2009)[\[caption
id="attachment\_5754" align="alignnone" width="400"
caption="2010"\]![2010]({filename}/images/uploads/4362326634_b55e00c8d8_b.jpg "2010"){: .size-full .wp-image-5752 }\[/caption\]](http://gxben.wordpress.com/2010/02/09/fosdem-2010)[\[caption
id="attachment\_5755" align="alignnone" width="400"
caption="2011"\]![2011]({filename}/images/uploads/20111.jpg "2011"){: .size-full .wp-image-5752 }\[/caption\]](http://www.flickr.com/photos/iraw/5418739675/in/photostream/)

Gaffer-tape galore at the ULB-campus again, it is that time of year. As
always it takes a deep breath before I push the battered swing doors of
the main location and dive under in the fluorescent-lit hallways filled
with thousands of developers. The yearly *Free and Open source Software
Developers' European Meeting* gathers participants to any F/LOSS project
you can think of.  
<!--more-->  
Any project? This year, none of the dev-rooms, lightning-talks, BOFs,
keynotes or tracks was devoted to Libre Graphics; fonts and visuals are
missing from the overloaded program except for a [lightning-talk on
Libre Graphics
Magazine](http://fosdem.org/2011/schedule/event/libre_graphics_magazine)
by ginger coons. Someone behind me whispers: "A yellow slide, and blue
hair ... that must be interesting!", and of course it was.

\[caption id="attachment\_5766" align="alignnone" width="400"
caption="ginger coons getting ready for her
lightning-talk."\]![]({filename}/images/uploads/P1110671.jpg "P1110671"){: .size-full .wp-image-5752 }\[/caption\]

In the opening-speech, [Eben
Moglen](http://emoglen.law.columbia.edu/now) rallies participants to
"bring Freedom to their friends in the street", and the gigantic lecture
hall, filled-up to the last seat, responds excitedly to the connection
Moglen makes between Free Software and current protests in Tunesia and
Egypt. "There's no time to waste," he says; "Free Software *must* bring
Freedom to the people". I'm happy too, to see FOSDEM connect to a larger
political reality than the F/LOSS-community itself (the 2008 opening
speech [Tux with Shades, Linux in
Hollywood](http://archive.fosdem.org/2008/schedule/events/250) being an
all-time low) and hoping that the [OS Telephony
track](http://fosdem.org/2011/schedule/track/open_source_telephony_devroom)
will put a bit of nuance to the statement that freedom is "when people
bring their data home".

\[caption id="attachment\_5759" align="alignnone" width="400"
caption="Massive lecture hall fills up for Eben
Moglen"\]![]({filename}/images/uploads/P1110659.jpg "P1110659"){: .size-full .wp-image-5752 }\[/caption\]

We meet up with Claudia, Nicu, Michael, Agnez, Juliane, Amalia, Natalia,
Pierros, Koen (inviting OSP to [Void
Warranties](http://voidwarranties.be/) in Antwerp), Wauter, John, Jon,
Jakub (documenting FOSDEM this time), ginger, Donna (she is now [LPI
certified](http://lpi.org/)!), Peter (happily manning the
[openSUSE](http://www.opensuse.org/) stand), Wynke (organised a
[LinuxChix](http://en.wikipedia.org/wiki/LinuxChix) meet and explained
how the [Anti-harassment
policy](http://geekfeminism.org/2010/11/29/get-your-conference-anti-harassment-policy-here/)
made it to the FOSDEM booklet against all odds) and many more. Some of
them we'll meet again tonight for dinner -- it is an opportunity to
start preparing LGM 2011.

In-between, I manage to see some talks and presentations; as always a
mixed bag. The interesting observations from Dave Neary ("[Community
Anti-Patterns](http://blogs.gnome.org/bolsh/2011/02/01/cross-desktop-devroom-at-fosdem/)")
keep running through my head, and I was fascinated by the talk on [Crash
Data Analysis](http://fosdem.org/2011/schedule/event/mozilla_crash_data)
in the Mozilla Dev Room.

\[caption id="attachment\_5763" align="alignnone" width="400"
caption="Dave Neary proposes treatment for community
anti-patterns"\]![]({filename}/images/uploads/P1110678.jpg "P1110678"){: .size-full .wp-image-5752 }\[/caption\]

When all talks have ended, I wonder along the row of stands, trying to
decide on this years' souvenir: sticker or mug or T-shirt? I settle for
a gold-colored pin: Free Software Foundation. Established in 1985.

[![]({filename}/images/uploads/P1110687.jpg "P1110687"){: .size-full .wp-image-5752 }]({filename}/images/uploads/P1110687.jpeg)
