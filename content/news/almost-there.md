Title: Almost there!
Date: 2010-06-23 09:33
Author: OSP
Tags: News, LGM 2010
Slug: almost-there
Status: published

[![Click here to lend your support to: Libre Graphics Meeting 2010 and
make a donation at www.pledgie.com
!](http://stdin.fr/lgm/pledgie/pledgie_banner.png)](http://www.pledgie.com/campaigns/8926)

We're busy closing the books on LGM 2010 and soon deciding where to go
for LGM 2011 (Brasil? Montreal? Vietnam? Follow the discussion at:
<http://lists.freedesktop.org/mailman/listinfo/create>) so we almost did
not notice that it's only a *very* little stretch to our initial goal of
raising 10.000\$ on Pledgie :-)
