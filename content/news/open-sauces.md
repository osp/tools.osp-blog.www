Title: Open Sauces
Date: 2010-10-25 14:27
Author: OSP
Tags: News, Texts Cooking, Further reading, Printing + Publishing, Recipes
Slug: open-sauces
Status: published

[![]({filename}/images/uploads/IMG_6858.jpg "IMG_6858"){: .alignnone .size-medium .wp-image-5116 }]({filename}/images/uploads/IMG_6858.jpg)

Just out: The [fOam](http://f0.am/) Open Sauces book + insert!

The book contains essays, recipes and images documenting [Open
Sauces](http://fo.am/open_sauces):

> "a sequence of experimental courses, matched with drinks, improvised
> music and esteemed guests. While savouring the foods, the guests will
> be engaged in table conversations, sharing experiences, recipes and
> ingredients needed to demystify cultural, environmental, technical and
> ethical aspects of contemporary food systems"

The scenario for [Semiotics of the Kitchen
Radio](/news/semiotics-of-the-kitchen-radio), an OSP broadcast that fOam
hosted in their space, is published in a separate booklet.

-   Download the Open Sauces book:
    <http://opensauces.cc/open.sauces.pdf>
-   Text on Stoemp: <http://snelting.domainepublic.net/texts/stoemp.odt>
-   Scenario for Semiotics of the Kitchen Radio:
    [semioticsofkitchenradio\_semk\_short\_0608]({filename}/images/uploads/semioticsofkitchenradio_semk_short_0608.odt)

