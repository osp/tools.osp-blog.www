Title: Governmental support for LGM
Date: 2010-03-02 13:39
Author: OSP
Tags: News, LGM 2010
Slug: governmental-support-for-lgm
Status: published

Remember that we entered [a subsidy
dossier](http://ospublish.constantvzw.org/news/keep-your-fingers-crossed)
in October 2009?

The Design and Architecture committee of the Flemish ministry for
culture has decided to grant our application and support LGM 2010.

Besides the fact that any financial support is more than welcome in hard
times like these, it is above all an encouraging expression of support
for Libre Graphics as a whole, and the importance of Free, Libre and
Open Source tools for cultural production in particular.

[  
![]({filename}/images/uploads/leeuwsteunVO-G+G-e1267529728332.png "leeuwsteunVO-G+G"){: .alignright .size-full .wp-image-4158 width="200" height="58"}  
](http://www.kunstenenerfgoed.be)
