Title: Accessibility
Date: 2008-12-01 16:35
Author: Femke
Tags: News, accessibility, Standards + Formats
Slug: accessibility
Status: published

(a postcard from Vienna)

*Wishing you (OpenOffice, Ubuntu, Free Software Foundation, Electronic
Frontier Foundation, Debian, Scribus, Creative Commons, One Laptop per
Child, Open Clip Art, DejaVu project, Open Font Library or* any *other
F/LOSS or Open Content project) were here, at the [European Ministerial
e-Inclusion
Conference](http://ec.europa.eu/information_society/events/e-inclusion/2008/index_en.htm)
...*

\[caption id="attachment\_1552" align="alignnone" width="300"
caption="Sir Tim Berners Lee addresses the audience in a
videomessage"\][![Sir Tim Berners Lee addressing the audience in a
videomessage]({filename}/images/uploads/tim.jpg "Sir Tim Berners Lee"){: .size-medium .wp-image-1552 }]({filename}/images/uploads/tim.jpg)\[/caption\]
