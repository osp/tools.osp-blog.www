Title: The ecstasy of influence
Date: 2009-03-29 21:14
Author: Harrisson
Tags: News, NotCourierSans, Works, Inkscape, Not-Courier sans, Printing + Publishing, Scribus
Slug: the-ecstasy-of-influence
Status: published

[La Selec](http://www.lamediatheque.be/mag/selec/) is a magazine edited
by enthousiasts of [La Mediatheque](http://www.lamediatheque.be), a
public service for renting records, movies, games and so on. Each issue
La Selec invites a local artist to produce a poster inspired by their
recommended selection of medias. This month it was Harrisson, so was
consequently OSP. Here's the poster we did for this mythical
institution.

![poster\_selec\_ok]({filename}/images/uploads/poster_selec_ok.png "poster_selec_ok"){: .alignleft .size-full .wp-image-2310 }

<!--more-->  
Slogan comes from an Harpers magazine Jonathan Lethems article, which is
really worth to read:  
<http://www.harpers.org/archive/2007/02/0081387>

Poster was done with Inskape 0.46 and Scribus 1.3.3.12 on Ubuntu 8.10.
Font is NotCourier Sans. Illustration inspired by the work of Czech
painter [Josef Lada](http://en.wikipedia.org/wiki/Josef_Lada) (1887 -
1957), Jean de la Fontaine (1621 - 1695), and many many other
influencing things...

LONG LIVE THE MEDIATHEQUE!  
=============

Poster Playlist:

MUSIQUES CLASSIQUES TITILLEES

FS3255 – Salvatore SCIARRINO Variazioni/Framento e adagio  
XC800R – Carl CRAIG  
& Moritz VON OSWALD Recomposed  
XT240J – Jimi TENOR Recomposed  
XA518L – ANGEL  
+ Hildur GUNADOTTIR In Transmediale  
XV398B – Esther VENROOY  
+ Heleen VAN HAEGENBORGH Mock Interiors  
XV184E – Jozef VAN WISSEM A priori  
Xx ou Hx – Anne-James CHATON  
+ Andy MOOR Le Journaliste  
NC0897 - Arnaud CATHRINE  
+ Florent MARCHET Frère animal  
HB4746 – Charles PENNEQUIN  
+ Jean-François PAUVROS Tué mon amour  
XD139B – Myra DAVIES Cities and Girls  
HB4850 – Denis PODALYDES Voix off  
HA0049 – Fantaisie littéraire

CLASSIQUE

BA6296 – R. BALLARD, L'Astrée – Musiques d'après  
A. BOËSSET… le roman d'Honoré d'Urfé  
VA0413 – Eric ROHMER Les Amours d'Astrée et de Céladon  
EC5680 – Béla BARTOK /  
Dmitri CHOSTAKOWITCH  
par Yossif IVANOV Concertos pour violon

CHANSONS FRANCOPHONES

NB1079 – BATLIK En mâchant bien  
NL1506 – Loïc LANTOINE A l'attaque!  
NL4414 – LIBEN Tout va disparaître

MUSIQUES DU MONDE

MH7075 – LENINE Labiata  
MN8937 – YOM New King of Klezmer Clarinet  
MP2242 – Erwan KERAVEC Urban Pipes

JAZZ / POST-JAZZ

UC2695 – Thomas CHAMPAGNE Charon's Boat  
UM0267 – Rudresh MAHANTHAPPA Kinsmen  
UE6442 – ErikM + AKOSH S. Zufall

RAP / HIP-HOP / GRIME / BLUES / etc…

KP9392 – PUPPETMASTAZ The Takeover  
KF5141 – FOOD FOR ANIMALS Belly  
KA6501 – ARABIAN PRINCE Innovative Life  
KA3112 – AGNOSTIC MOUNTAIN  
GOSPEL CHOIR Ten Thousand

POP / ROCK

XPxxxx – PATTON Héllénique chevaleresque récital  
XA544X – ANIMAL COLLECTIVE Merriweather Post Pavilion  
XG672A – GRAMPALL JOOKABOX Ropechain

FICTION

\[4 cotes\] – Jacques ROZIER \[coffret 5 DVD\]  
VR0220 – Dominique ABEL  
+ Fiona GORDON Rumba  
VE0261 – Victor ERICE L'Esprit de la ruche  
VB0737 – Victor ZVIAGUINTSEV Le Bannissement

DOCUMENTAIRES

TB4501 – Bruce WEBER Let's Get Lost  
TB7645 – Steven SEBRING Patti Smith – Dream of Life  
TW2751 – Jack HAZAN A Bigger Splash  
TN2811 – Sandrine BONNAIRE Elle s'appelle Sabine

------------------------------------------------------------  
You can download the SVG file of the fox and crow here:  
[![poster-selec-renard]({filename}/images/uploads/poster-selec-renard.svg "poster-selec-renard"){: .alignleft .size-full .wp-image-2310 }]({filename}/images/uploads/poster-selec-renard.svg)
