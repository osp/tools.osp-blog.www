Title: Typeface in the making: W Drogę
Date: 2008-05-07 14:16
Author: OSP
Tags: Education, Live, Type, LGM 2008, Libre Fonts
Slug: typeface-in-the-making-w-droge
Status: published

[image/index.php?level=album&id=18](http://ospublish.constantvzw.org/image/index.php?level=album&id=18)

David Bargenda's photos:
[http://flickr.com/photos/opt-art/tags/opensourcepublishing/  
](http://flickr.com/photos/opt-art/tags/opensourcepublishing/)

With a group of 20 courageous participants and the help of open type
activists Dave Crossland, Alexandre Prokoudine and Nicolas Spalinger we
are using Inkscape, Gimp and FontForge to produce a typeface in a day.
We have named it **W Drogę** (On Our Way, En Route in Polish) and it
should be available for download ~~by the end of the day~~ ~~tomorrow~~
soon!

Workshop description: <http://ospublish.constantvzw.org/?p=454>
