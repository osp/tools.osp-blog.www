Title: Kick out the jam
Date: 2010-06-14 10:00
Author: ludi
Tags: Education, Live, News, Type, Libre Fonts, Licenses, Presentations, Type, Webdesign
Slug: kick-out-the-jam
Status: published

![]({filename}/images/uploads/Picture-8.png "Picture 8"){: .aligncenter .size-medium .wp-image-4588 }

7 OSP funky geeky sheeps players in[Typogravieh
lebt's](http://www.typogravieh-lebt.de/lebt_6/bin/)trailer.  
**Vielen Dank Martin & the typovieh-team.**  
Our Bauhaus typo symposium was more than leben.

<!--more-->  
![]({filename}/images/uploads/Picture-9.png "Picture 9"){: .aligncenter .size-medium .wp-image-4588 }

![]({filename}/images/uploads/1533-p1040770.jpg "1533-p1040770"){: .aligncenter .size-medium .wp-image-4588 }

[Here](http://ospublish.constantvzw.org/image/index.php?level=album&id=45)
is a color record of the adventure to the Bauhaus University, Weimar.

*viehturing: Ligatier, Cascading Stylesheeps, Captain Futura u.a. Nach
einjäriger Pause hat sich das Typogravieh endlich auscouriert und lebt
wieder!*
