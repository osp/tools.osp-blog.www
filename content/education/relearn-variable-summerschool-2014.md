Title: Relearn Variable summerschool 2014
Date: 2014-04-15 18:52
Author: Colm
Tags: Education, News, Tools
Slug: relearn-variable-summerschool-2014
Status: published

[![Screen Shot 2013-08-29 at 2\_54\_32
PM]({filename}/images/uploads/Screen-Shot-2013-08-29-at-2_54_32-PM.png){: .aligncenter .size-medium .wp-image-7241 }]({filename}/images/uploads/Screen-Shot-2013-08-29-at-2_54_32-PM.png)  
Good news! Considering how well last years OSP [Relearn Summer
School](relearn.be) went, we're reiterating and *extending* the school
with all of the people and labs that make up
[Variable](http://variable.constantvzw.org/). We're really exited to be
able to make this happen again, especially considering all the extra
people that will be joining in this year. **Relearn starts on July 6th**
(Sunday) **and ends 11 July** (Friday). You might have received some of
this info by email already, so here is an overview of what we're
attempting to do:

**Relearn** is a summerschool with as many teachers as it has
participants. It is about sharing and trying Free Culture practices, and
is entirely done with Free, Libre and Open Source software.  
It starts from the idea that tools are increasingly mediated by software
and the algorithmic and legal dynamics that constraint and shape its
use.  
Reconnecting to the physical and digital material of those tools is a
means of (re)asserting ownership and ensuring active participation in
the future of our work practices.

Relearn welcomes participants — artists, students, teachers,
practitioners — from all ages and disciplines.**  
**

[![relearn\_eric\_2141]({filename}/images/uploads/relearn_eric_2141.jpg){: .aligncenter .size-medium .wp-image-7241 }]({filename}/images/uploads/relearn_eric_2141.jpg)

→ see some more [pictures taken during Relearn
2013](http://ospublish.constantvzw.org/images/Relearn-2013)

→ [Check out the
publication](http://relearn.be/media/relearn-2013.pdf) that was put
together by gathering all of our common written experiences from last
year  
(and even [print your
own](http://www.lulu.com/shop/relearn-2013/relearn-2013/paperback/product-21561611.html)
on Lulu )

*To participate, please send an e-mail
to <registration@relearn.be> <wbr></wbr>before April 25, and include a
short description of your hopes for this years worksessions and what
kind of learning experiences you would like to be part of.  
Participation is free but the number of places is limited to 60!  
We'll be in touch by the 5th of May.*

*This year we'd like to open up the process and let anybody submit a
*proposal for a* worksession; we've detailed how to do so
on [relearn.be](http://relearn.be/)*
