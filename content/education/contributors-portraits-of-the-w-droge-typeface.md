Title: Contributors portrait of the W Drogę typeface
Date: 2008-05-12 12:53
Author: Pierre
Tags: Education, Live LGM 2008, Workshops + teaching
Slug: contributors-portraits-of-the-w-droge-typeface
Status: published

Thanks to Andy Fitzsimon for the picture (in CC full open)!

![](http://ospublish.constantvzw.org/image/plog-content/thumbs/snapshots/w-drog__/large/700-img_5867.JPG)
