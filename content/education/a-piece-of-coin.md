Title: A pièce of coin
Date: 2008-10-31 11:57
Author: Pierre
Tags: Education, News, Texts, Type, stroke font
Slug: a-piece-of-coin
Status: published

[![]({filename}/images/uploads/coin-75x75.jpg "Coin"){: .float width="75" height="75"}](http://pythonide.blogspot.com/2008/10/how-to-make-money-with-free-software.html)The
[hard and well documented
work](http://pythonide.blogspot.com/2008/10/how-to-make-money-with-free-software.html)of
a ~~Dutch~~ Belgian Python artist and designer -
[Stani](http://www.stani.be) - to produce a coin devoted to Dutch
contemporary architecture, using only
[floss](http://en.wikipedia.org/wiki/FLOSS). The stroke font he design
for it is simply beautiful (currently no info on availability). Via
[Dave](http://understandinglimited.com/archives/)'s understanding blog.
