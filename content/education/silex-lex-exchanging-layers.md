Title: SILEX-LEX: exchanging layers
Date: 2010-04-30 08:32
Author: OSP
Tags: Education, Live, News, Tools, Works, Inkscape, LGM 2010, Risographe, SVG, Workshops + teaching
Slug: silex-lex-exchanging-layers
Status: published

[Here](http://ospublish.constantvzw.org/image/index.php?level=album&id=43)
are snapshots of
[SILEX-LEX](http://ospublish.constantvzw.org/live/silex-lex) round 1.
Artists, designers and illustrators experimented with Inkscape, a
Rhizograph and each other's layers. Round 2 follows next week in
preparation of the vernissage on May 7.  
\[gallery link="file" orderby="rand"\]
