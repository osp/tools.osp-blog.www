Title: please computer | make me design
Date: 2010-04-18 19:13
Author: OSP
Tags: Education, Live Command Line, Education, Workshops + teaching
Slug: please-computer-make-me-design
Status: published

![]({filename}/images/uploads/OpenCourse.png "OpenCourse"){: .alignright .size-medium .wp-image-4444 }  
\[[Full
programme]({filename}/images/uploads/OpenCourse.pdf)\]

OSP @ **Journée Open-Course/Open-Source**

N’avez-vous jamais rêvé que votre ordinateur fasse tout ce que vous
voulez?  
C’est possible!  
Osez prendre les commandes de votre ordinateur en participant à cet Open
Course/Open Source axé sur le design par le texte.  
De la monoculture à la diversité logicielle. Un panorama des logiciels
libres pour la création (typo)graphique avec le groupe Open Source
Publishing
