Title: A challenging programme
Date: 2011-06-05 18:18
Author: OSP
Tags: Education, News, Education, Rotterdam
Slug: a-challenging-programme
Status: published

*Our friends from the Master Media Design & Communication: Networked
Media are accepting candidates for next academic year until **July 1
2011**. OSP enthusiastically recommends this exceptional design program,
where F/LOSS is the norm:*

[Master Media Design & Communication: Networked
Media](http://pzwart.wdka.nl/networked-media/)  
Institute for Postgraduate Studies and Research  
Willem de Kooning Academy / Rotterdam University Netherlands  
\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*  
Call for Applications  
Deadline July 1st, 2011 for EU/EEA students
http://pzwart.wdka.nl/networked-media  
For application info & tuition fees see below  
\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*

The Master Media Design & Communication: Networked Media is
internationally known as the new media study programme at the Piet Zwart
Institute. We offer a Master programme focused on a critical engagement
with the culture of the Internet and computer media, their artistic
design, technology and theory. We are a small, English-taught, two-year
full-time Master course with a multidisciplinary community and have a
strong emphasis on personal contact and collaboration. Our students,
faculty members and guests come, among others, from the USA, France,
Serbia, Italy, Austria, Pakistan, Turkey, and our 24/7 studio facilities
allow you to make our school your workplace.  
<!--more-->  
We offer a challenging programme with trimestrial thematic projects,
theoretical discussions and technical sessions. Next to studying
networked media through practice and theoretical research, you will
share select courses and group critiques with Lens-based Media staff and
students. This other specialisation within the Master Media Design &
Communication programme, explores the potential of digital lens-based
imagery. Parallel to group learning, students also receive strong
tutorial support through weekly individual meetings with staff and
guests.

Whether you are a graphic designer wanting to develop your digital media
practice more thoroughly, an audiovisual artist interested in a more
informed and self-made media practice, an architect interested in media
as systems that impose social possibilities and constraints, a media
activist wanting to develop both practical and theoretical tools of
intervention, a computer and electronics experimenter wanting to sharpen
her or his artistic concepts, a media artist/designer looking for
research-oriented study in an international community of peers - this is
what our programme makes possible.

\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*  
Free & Open Source / Thematic Projects / Staff / Guest Lecturers  
\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*  
Free and Open Source Software:  
Our Master program strives for the creation of media, instead of
creating with off-the-shelf and out-of-the-box technology. This is why
Free and Open Source Software are an important aspect of our course,
philosophically and technologically.

Thematic Projects:  
For an overview of some of our most recent Thematic Projects see:
http://pzwart.wdka.nl/networked-media/category/study-programme/thematic-projects/

Staff:  
Renee Turner: Course Director, member of De Geuzen, MFA (University of
Arizona), MA in Creative Writing and New Media (De Montfort University),
Laureate in Theory, Jan Van Eyck Academie  
Aymeric Mansoux: Core Tutor/Co-supervisor, artist and software
developer, Puredyne GNU/Linux, make art festival, FLOSS+Art, MFA (Ecole
Europeenne Superieure and de l'Image, Poitiers), MA Digital Arts
(Universite de Poitiers et La Rochelle)  
Michael Murtaugh: Core Tutor/Prototyping (Master from the MIT Media Lab,
USA), interaction designer, Active Archives, member of Constant (BE)  
Steve Rushton: Writing Tutor, founding member of Signal:Noise - a
cross-disciplinary research project exploring the influence of
cybernetics and feedback  
Stock: Technical Tutorial Support, hardware guru, formerly V2\_ labs,
Mr. Stock Interfaces  
Brigit "Evo" Lichtenegger: Systems Administrator/Technical Tutorial
Support, interactive artist  
Leslie Robbins: Project Coordinator, MFA (HdK Berlin), artist and member
of artist initiatives Duende and Occupying Space

Selection of previous guest lecturers and thematic project tutors:  
Arie Altena, Inke Arns, Aram Bartholl, Mez Breeze, Michel Chevalier,
Theo Deutinger , Rod Dickinson , De Geuzen, Andreas Leo Findeisen,
Daniel Gross + Joris Maltha, Rui Guerra, Seda Guerses, Christoph Haag,
jodi, jaromil, Dmytri Kleiner, Olia Lialina, Nicolas Maleve, Luna
Maurer, Metahaven, Mirko Tobias Schaefer, Edward Shanken, Lukas Simonis,
Femke Snelting, Michelle Teran, Jon Thomson + Alison Craighead, Roel
Wouters, Danja Vasiliev + Gordan Savicic / moddr\_

\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*  
Deadlines / Tuition Fees / Applications / Contact  
\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*  
Deadlines:  
European students who will receive their Bachelor degrees in summer 2011
can already apply now. The next deadline for non-EU students is May 1st
2012.

Tuition:  
For EU/EEA students, (who have applied for their first master course in
the Netherlands) the annual tuition fee for the full-time course in
2011/12 is 1713 Euro.

Applications:  
For application forms, application criteria and further information on
our curriculum or public programme, please see
http://pzwart.wdka.nl/networked-media/category/apply/application/

Contact:  
For further questions contact Leslie Robbins: l.j.drost-robbins -at-
hro.nl
