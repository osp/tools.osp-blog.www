Title: Panik Party
Date: 2009-05-16 18:46
Author: Harrisson
Tags: Education, News, NotCourierSans, Works, Music, Party
Slug: panik-party
Status: published

[![33\_posterpanikok]({filename}/images/uploads/33_posterpanikok.jpg "33_posterpanikok"){: .alignleft .size-medium .wp-image-2607 }](http://school.collerettecocofilllsd.com/index.php?/project/310309---the-masks-j0/)  
Among the 100 people involved in the creation and realisation of this
poster, OSP.  
2 of them are hidden in the picture (free poster for the one who spot
them) and 3 other helped the setup...  
This poster was done during the first year graphic design course at ERG,
Brussels.  
This course has been commissionned by [Radio
Panik](http://www.radiopanik.org/spip/) to design the poster of their
annual (and legendary) party.  
Panik is a Brussels associative radio, one of the last of the real Free
Radio. It is a fresh and colorfull wind in the grey landscape of
commercial broadcast.

Everything on the poster is done by hand.  
It can be cut in 49 different flyers, as the verso is printed with infos
for the evening (line up, logos...). Once cut, the poster becomes a
gigantic puzzle...  
Back is done in Scribus, with handrawings processed in Gimp.

You can check some more pics
[here...](http://school.collerettecocofilllsd.com/index.php?/project/310309---the-masks-j0/)  
And listen to Panik
[there...](http://www.radiopanik.org/spip/spip.php?article221)

Hope to see you at this party that will be one of the hottest of the
year!
