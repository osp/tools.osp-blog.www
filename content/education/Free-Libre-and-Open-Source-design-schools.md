﻿Title: Free, Libre and Open Source design schools
Date: 2009-02-13 16:12
Author: OSP
Tags: Education
Slug:  

Parsons school of design teaches webdesign for and with open source
software. Students learn to work with indexhibit and wordpress asking
questions such as: How do we learn and how can we teach through
interface? What is the role of design in the open source world? And how
can we give back to this new collaborative movement?

On the day he was named as president of RISD, John Maeda launched an
internal blog on which everyone there can talk openly to him about the
school. Maeda addressed the moral responsibility of designers. He
stressed the importance of transparency in design, and of extending the
participatory "open source" development process now popular in software
design to other sectors. He maintained that although keeping secrets
creates the illusion of power, if nobody knows you have a secret, its
value is worthless.
