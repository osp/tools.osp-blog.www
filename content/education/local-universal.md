Title: Local Universal
Date: 2008-04-29 15:21
Author: OSP
Tags: Education, Live LGM 2008, Workshops + teaching
Slug: local-universal
Status: published

**Wednesday May 7 in Wrocław, Poland**

Running up to the [Libre Graphics meeting
2008](http://www.libregraphicsmeeting.org/2008/), OSP invites designers,
programmers and typographers to work on a collaborative design project.
Inspired by modular systems for [road sign
lettering](http://ospublish.constantvzw.org/image/?level=album&id=11),
and a long interest in fonts designed by engineers, we would like to
work with you on an impossible project: **to design a whole alphabet in
a day**.

<!--more-->  
How do you 'make things' across styles, tastes and traditions? Does it
always mean to average differences or a sampling of parts? How do you
design through translation? What will be lost and what could be gained?

Depending on the needs and interests of the participants, we form small
working groups that are each joined by one or more OSP members. For
inspiration or as actual source material, we will photograph as many
characters as possible on our roadtrip from Brussels to Wroclaw. Each of
the groups adopts a few letters to experiment with, using free software
tools (Gimp, Inkscape, Fontforge + some Spiro flavor...) to draw,
digitize and transform. At the end of the day we will gather all glyphs
into one font and festively release the collection under an Open Font
License.

**With**

-   Pierre Huyghebaert (BEL): typographer, designer
-   Harrisson (FRA/BEL): typographer, designer
-   Yi Liang (CHN/BEL): designer, illustrator
-   Femke Snelting (NED/BEL): artist, designer
-   Ivan Monroy Lopez (MEX/NED): programmer, artist
-   Ludivine Loiseau (FRA/BEL): typographer
-   Nicolas Malevé (BEL): programmer, data activist
-   Ricardo Lafuente (POR/NED): designer, programmer

Plus the help of: Dave Crossland, Alexandre Prokoudine and Nicolas
Spalinger.

**For who**  
This workshop is for anyone interested in the combination of design,
free software and typography. Computer skills come in handy but if you
are courageous and interested: everything is possible!

Working language: English, with multi-lingual plugins installed

**Practical**  
There is a limited amount of computers available in OPT, but if you have
a laptop with Linux installed, please bring it!  
Entrance: free

Wednesday May 7 11:00 - 18:00  
Ośrodek Postaw Twórczych (Creative Art Center)  
ul. Działkowa 15, 50-538  
Wrocław, Poland  
<http://www.opt-art.net/>
