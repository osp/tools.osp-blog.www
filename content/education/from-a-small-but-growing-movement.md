Title: From a small but growing movement
Date: 2006-10-27 15:40
Author: Harrisson
Tags: Education, Type, Libre Fonts
Slug: from-a-small-but-growing-movement
Status: published

![revised\_banner.gif]({filename}/images/uploads/revised_banner.gif){: #image141}  
Due to my recent task of teaching typography, I was looking around for
courses and experiences in that domain. This drove me to Ellen Lupton's
website, teacher at Maryland Institute College of Art (MICA):
[www.designwritingresearch.org](http://www.designwritingresearch.org/index.html).
There, in an impressive generosity, you can consult her current syllabi
and exercices of hight quality courses.

She's is also a responsive designers awared of the problematic of
proprietary fonts in design. Her website host a free font manifesto
page: [http://www.designwritingresearch.org/free\_fonts.html  
](http://www.designwritingresearch.org/free_fonts.html)  
Lupton wrote several books on teaching typography. The manual "thinking
with type" is a reference in the field. This book is accompanied by a
rich teaching website:
[thinkingwithtype.com](http://www.thinkingwithtype.com/) which gives
number of exercices and "adaptative" syllabus.

She's editor in [Freefontmanifesto
Blog](http://freefontmanifesto.blogspot.com/)
