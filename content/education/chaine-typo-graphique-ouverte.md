Title: Chaîne typo-graphique ouverte
Date: 2010-12-09 11:14
Author: ludi
Tags: Education, News, Tools, Type, Fontforge, Gimp, Inkscape, Tools, Workshops + teaching
Slug: chaine-typo-graphique-ouverte
Status: published

Nous fêtons demain le premier cours Open Source de
l'[ERG](http://www.erg.be/erg/).  
In September started a fisrt "arts numériques" open source course at the
École de Recherche Graphique.  
Visit the course [wiki](http://www.ludi.be/erg/doku.php).  
The students will be proud to present the first specimens of their
pictures-fonts.  
And in exclusivity we will play with Pierre Marchand's new friend
[Fonzie](http://www.oep-h.com/fonzie/) - live !  
Come with a picture and live with a font.

![]({filename}/images/uploads/P1010820.jpg "P1010820"){: .aligncenter .size-medium .wp-image-5442 }  
`flyers d'invitation réalisés par les étudiants`

**Tomorrow, Friday December the 10th  
at 11:00  
central corridor  
near room 12  
École de Recherche Graphique,  
87, rue du page - 1050 Bruxelles**
