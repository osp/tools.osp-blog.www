Title: The most useful book
Date: 2008-11-06 15:39
Author: Harrisson
Tags: Education, News, Works, Cooking, Handmade, Printing + Publishing, Recipe, Teaching, Tools, Type
Slug: the-most-useful-book
Status: published

[![]({filename}/images/uploads/poster1.jpg "poster1"){: .alignnone .size-full .wp-image-1419 }]({filename}/images/uploads/poster1.jpg)

Nous sommes heureux de vous inviter à la présentation du livre collectif
:

- - - - - - - - - - - - -  
**Vomit' can ich i'neun dî'neun?  
en quoi puis-je vous etre utile?**  
- - - - - - - - - - - - -  
qui accompagne l'exposition  
- - - - - - - - - - - - -  
**Les 86 livres les plus utiles - pour nous  
The 86 most useful books - for us**

- - - - - - - - - - - - -  
Vendredi 7 Novembre 2008  
15:00  
Patio - Abbaye de La Cambre  
Bruxelles  
- - - - - - - - - - - - -  
<!--more-->  
[![]({filename}/images/uploads/pasta3.jpg "pasta3"){: .alignnone .size-full .wp-image-1419 }]({filename}/images/uploads/pasta3.jpg)  
Il y aura nous, le livre, l'exposition, un drink et plein plein de
trucs.  
On pourra y apprendre à peler un oeuf avec une fourchette, gagner 1000
euros avec une photographie, faire du feu, être invisible, rester
éveillé sans café, et plein encore!

Tirage ultra limité prévu à 46 exemplaires, sans luxe ni détours, mais
super beaux, et surtout bien utiles.

- - - - - - - - - - - - -  
Vomit' can ich i'neun dî'neun?  
est un livre réalisé collectivement  
entre le 3 et le 7 Novembre 2008,  
dans l'atelier de typographie de La Cambre,  
Ecole Nationale Supérieure des Arts Visuels,  
dans le cadre du CASO Livre.

[![]({filename}/images/uploads/booksweb.jpg "booksweb"){: .alignnone .size-full .wp-image-1419 }]({filename}/images/uploads/booksweb.jpg)

------------  
Starring  
------------  
Espace Urbain:  
Cecile Vandernoot  
Larissa Vanhee

Sculpture:  
Marion Fabien  
Cyril Verde  
Laure de Selys

Dessin:  
Gabrielle Weissen  
Szymon Dabrowski

Photo:  
Maxime Brygo  
Candice Cellier  
Ludivine Sibelle

Typographie:  
Aude Metz  
Chloé Vargos  
Pauline Gervasoni  
Elise Borel  
Mathilde Boucher  
Kevin Cocquio

Design industriel:  
Julien Beutter

L'assistant:  
Harrisson

= = = = = = =

[![]({filename}/images/uploads/pasta.jpg "pasta"){: .alignnone .size-full .wp-image-1419 }]({filename}/images/uploads/pasta.jpg)

Logiciels utilisés pour la réalisation de cet ouvrage:

Ubuntu 7.10  
- Xsane Image Scanner  
- Gimp  
- Open Office  
- Gthumb

Mac OsX, OsX86  
- Text Edit  
- Photoshop CS3  
- Indesign CS3

Appareil photos:  
- Lumix X10  
- Nikon D1X  
- Canon G9  
- Polaroid

Scanner:  
Canon CanoScan 1240U

Papier:  
Offset de photocopie de base 80g  
Clairefontaine Trophée 80g  
Fardes récupérées

Imprimante:  
Canon LBP 810

Fontes:  
Spectrum 1959  
Not Courier Sans (http://openfontlibrary.org/media/files/OSP/309)

Impression des couvertures sur une presse épreuve FAG Control 625

[![]({filename}/images/uploads/print1.jpg "print1"){: .alignnone .size-full .wp-image-1419 }]({filename}/images/uploads/print1.jpg)

= = = = = = = =

Le Maurice:

Pour 1 litre de Maurice:  
Faire mariner pendant 12h au frais, dans 1.5l de thé vert infusé 3mn:  
1 verre de sucre brun  
4 citrons pressés  
1 botte de menthe  
150g de gingembre coupé très fin.

Servir en base/sirop avec de la bière, de l'eau pétillante ou du vin
blanc.

= = = = = = = =
