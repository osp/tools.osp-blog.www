Title: Bauhaus meets F/LOSS
Date: 2011-04-12 09:00
Author: Femke
Tags: Education, Texts Education, Further reading, Manual
Slug: bauhaus-meets-floss
Status: published

[![]({filename}/images/uploads/vorkurs_eng.gif "vorkurs_eng"){: .alignnone .size-medium .wp-image-6173 }]({filename}/images/uploads/vorkurs_eng.gif)  
Taking it's inspiration from the Bauhaus *Vorkurs* ((Itten, Johannes.
Design and form: the basic course at the Bauhaus. New York: Van Nostrand
Reinhold)), Xtine Burrough and Michael Mandiberg wrote ***Digital
Foundations***
(([http://www.blog.digital-foundations.net](http://www.blog.digital-foundations.net/))),
a textbook for teaching software to designers and artists. Their idea
was to not just talk about which button to click, but to connect
technical training to color theory and composition. After publisher New
Riders had been convinced to apply a Creative Commons license to the
book ((HOWTO Negotiate a Creative Commons License: Ten Steps
<http://www.blog.digital-foundations.net/?p=53>)), it was only a matter
of time before the F/LOSS manuals community
((<http://www.flossmanuals.net>)) took up the challenge and rewrote
*Digital Foundations* for Gimp, Inkscape, Scribus and other Libre
Graphics tools.  
<!--more-->  

> Originally, this book was printed as a manual to the Adobe Creative
> Suite, the software found in classrooms and labs around the country.
> Just a month after the book was published, we teamed up with Floss
> Manuals to convert our manuscript into one that teaches the same
> design principles using open source software.
> </p>

In addition to explaining well-known Bauhaus principles such as
'simultaneous contrast' and 'quality contrast', the authors have mixed
in snippets from art history, post-modern philosophy, an introduction to
fair use and net-art plus more to spice up their detailed step-by-step
software exercises. Even though the book was published a few years ago,
if you are teaching or learning F/LOSS: this is a nice place to start!

<http://en.flossmanuals.net/digital-foundations/>
