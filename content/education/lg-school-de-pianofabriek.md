Title: LG-school @ De Pianofabriek
Date: 2010-05-18 21:41
Author: OSP
Tags: Education, Education, LGM 2010
Slug: lg-school-de-pianofabriek
Status: published

**Wednesday 26 May \*\*\* A day with: ginger coons, LUST, Akkana Peck &
OSP**  
*Introduction + discussion: De Pianofabriek*  
*Workshops: Various locations*

A day before the international [Libre Graphics
Meeting](http://www.libregraphicsmeeting.org) starts, art/design
students and their teachers are welcome to join LG-school for an intense
day. LG-school looks at what Free Software could mean for teaching,
learning, experimenting and making creative work.

The day starts with an introduction on why Free Software could be
interesting for designers and artists, followed by hands-on parallel
sessions. The day ends with a plenary discussion moderated by Pierre
Huyghebaert and Femke Snelting. We'll share experiences with each other
and with Free Software developers already arriving in Brussels for the
Libre Graphics Meeting.

At LG-school we will speak mainly English and some French and Dutch; the
discussion has simultaneous translation in French.

If you haven't inscribed yet, please e-mail to femke @ constantvzw.org

There will be several computers available with Linux installed -- bring
your own laptop if you can. If you plan to participate in the LUST
workshop, try to arrive with Processing installed \[2\].  
<!--more-->

SCHEDULE

11:00 Introduction  
11:30 Presentations  
12:30 -- lunch  
13:30 Parallel workshops  
16:30 -- break  
17:00 Discussion  
18:30 -- end

WORKSHOPS

**Akkana Peck** \[US\]  
Akkana Peck is a Linux software developer and writer who has worked on
projects that include web browsers, HTML editors, system software, email
programs, scientific visualization and, of course, GIMP. She is also the
author of Beginning GIMP: From Novice to Professional  
<http://shallowsky.com>  
<http://gimpbook.com>

Workshop: GIMP and GIMP Scripting: an introduction to the GNU Image
Manipulation Program, what it can do, and how to extend it by writing
custom scripts and plug-ins.  
<http://create.freedesktop.org/wiki/Writing_GIMP_scripts_and_plug-ins>  
Location: Atelier du Web (Rue du Fort 37)  
\*\*\*

**ginger coons** \[CA\]  
ginger coons is a designer, artist, pseudo-academic, occasional writer
and all-round renaissance woman. Her work specializes in topics of
intellectual property, civil liberties and truth in production. This
applies just as much to her writing as to her visual practice. As a
research mercenary, she has examined the implications of e-readers on
the environment, dynamic typography and geosocial networking, among
others. She blogs regularly on the above topics and with illustrations
at [adaptstudio.ca/blog](http://www.adaptstudio.ca/blog).

Workshop: Digital illustration, lazy tactics and the public domain.  
Location: De Pianofabriek  
\*\*\*

**LUST (Daniel Powers)** \[NL\]  
LUST is a studio for graphic and interactive design based in The Hague
(Den Haag), Netherlands. Their design philosophy revolves around
Process-based and Generative-based Design. Interested in exploring new
pathways for design at the precarious edge where new media and
information technologies, architecture and urban planning and graphic
design overlap. Topics include: graphic design, typography, abstract
cartography, mapping, architecture, urban media installations, archiving
data-visualizations, random mistake-ism, fonts, type design, new media
interactive webdesign, internet art, big bang chaos.  
<http://lust.nl>

Workshop: an introduction to free software/building your own tools, The
workshop which will focus primarily on processing.  
Location: De Pianofabriek  
\*\*\*

**OSP (ALexandre Leray, Ludivine Loiseau, Stephanie Vilayphiou e.a.)**
\[BE\]  
OSP is a graphic design agency using only Free and Open Source Software.
Closely affiliated with the Brussels based digital culture foundation
Constant, OSP aims to test the possibilities and realities of doing
graphic design using an expanding range of tools. OSP is serious about
testing the possibilities and limitations of F/LOSS in a professional
design environment, without expecting to find (or offer!) the same
experience as the ones they are used to. Actually, they are interested
in experimenting with everything that shows up in the cracks.  
<http://ospublish.constantvzw.org>

Workshop: please computer | make me design. A workshop on fun poster
generation, using simple command-lines (especially GNU textutils
tools).  
Location: Interface3 (Rue du Méridien 30)

\*\*\*

De Pianofabriek  
Fortstraat 35, Brussels (at 10 mins. from Brussels South Station)

---

\[1\] <http://www.libregraphicsmeeting.org>  
\[2\] <http://processing.org/download>
