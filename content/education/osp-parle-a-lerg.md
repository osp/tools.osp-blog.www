Title: OSP parle à l'ERG
Date: 2008-03-05 11:55
Author: Harrisson
Tags: Education, Live Presentations
Slug: osp-parle-a-lerg
Status: published

[![osperg.png]({filename}/images/uploads/osperg.png)]({filename}/images/uploads/osperg.png "osperg.png")

Répondant à l'invitation du cours de Typographie de Mme M.C. Lambert,
OSP présentera des outils open source s'inscrivant dans la chaine
graphique. Cette petite conférence abordera les problématiques de la
typographie ouverte, ainsi qu'une présentation de nos recherches en
fontes publiques, comme le DIN Project.

Cela se passera ce vendredi 7 mars 2008  
à l'**ERG**, Ecole de Recherche Graphique.  
rue du Page 87, 1050 Brussels, de 9:00 à 12:00  
[www.erg.be](http://www.erg.be)
