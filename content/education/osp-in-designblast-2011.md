Title: OSP in Designblast 2011
Date: 2011-11-16 17:19
Author: Harrisson
Tags: Education, Live, News
Slug: osp-in-designblast-2011
Status: published

[![]({filename}/images/uploads/Designblast.png "Designblast"){: .alignleft .size-full .wp-image-6916 }](http://designblast.de/2011/)  
OSP is invited to talk on friday at  
**Designblast'11** — Toys, Tricks & Tools — 18.11.2011  
"Is it better to restrict yourself or to allow everything?"  
Stand up talk by OSP Canal Historique. An open code to Love, Destiny,
Fate, Passion, Rage, Pain, Fulfilment. With explicit pictures and
language.

On 17/11/11, we'll do a day workshop on installing a non digital font.
[HFG Karlsruhe](http://www.hfg-karlsruhe.de/).
