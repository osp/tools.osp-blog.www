Title: OSP @ LGM 2008
Date: 2008-04-22 06:39
Author: OSP
Tags: Education, Live LGM 2008, Presentations, Print Party
Slug: osp-lgm-2008
Status: published

[![]({filename}/images/uploads/wroclaw1.jpg)](http://www.wroclaw.pl/p/3200/)[![]({filename}/images/uploads/lgm2.jpg)](http://www.libregraphicsmeeting.org/2008)[![]({filename}/images/uploads/opt1.jpg)](http://www.opt-art.net/)  
OSP is preparing for a hectic week in Wrocław, Poland (3 t/m 11 May). At
this years [Libre Graphics
Meeting](http://www.libregraphicsmeeting.org/2008) we give a one day
workshop in [Ośrodek Postaw Twórczych](http://www.opt-art.net/)
(Creative Arts centre), perform a Print Party and present our work. The
LGM
[program](http://www.libregraphicsmeeting.org/2008/index.php?lang=en&action=program)
is packed with talks and we'll hopefully record a few more
[interviews](http://ospublish.constantvzw.org/?cat=18) as well. The
extended OSP delegation includes: Harrisson, Pierre Huyghebaert, Nicolas
Malevé, Yi Liang, Femke Snelting, Ludivine Loiseau, Ricardo Lafuente and
Ivan Monroy Lopez.
