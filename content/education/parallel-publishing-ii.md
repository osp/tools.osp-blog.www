Title: parallel publishing II
Date: 2009-03-06 11:06
Author: Ivan
Tags: Education, News, Education, Printing + Publishing, Standards + Formats, Workshops + teaching
Slug: parallel-publishing-ii
Status: published

the
[workshop's](http://ospublish.constantvzw.org/news/tales-of-interrogated-type-parallel-publishing)
first publication is ready. every participant has created a fictional
petites annonces for it. some pictures of making, collating and
distributing.

we're also on irc at \#crying\_room on irc.freenode.org :)  
![01]({filename}/images/uploads/01.jpg "01"){: .alignnone .size-medium .wp-image-2052 }![11]({filename}/images/uploads/11.jpg "11"){: .alignnone .size-medium .wp-image-2052 }![2]({filename}/images/uploads/2.jpg "2"){: .alignnone .size-medium .wp-image-2052 }

<http://ospublish.constantvzw.org/image/?level=album&id=26>
