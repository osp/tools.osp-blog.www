Title: Looking for students
Date: 2011-02-01 13:37
Author: OSP
Tags: Education, News, Education, media studies, Schools
Slug: looking-for-students
Status: published

Mirko Tobias Schaeffer (from the excellent text [*Bastard
Culture*](http://www.mtschaefer.net/entry/bastard-culture-2/) and [a
great talk](http://river-valley.tv/designing-a-better-tomorrow/) at LGM
2010) wrote us about the one year master programme he is teaching with
a.o. Marianne van den Boomen and Ann-Sophie Lehmann at Utrecht
University:

> We combine academic analysis and theory with hands on experience in
> new media. We're looking for enthusiastic students who would like to
> join our programme in the Netherlands.

Have a look at the Call for students:
[NMDC\_Call-f-students\_eng]({filename}/images/uploads/NMDC_Call-f-students_eng.pdf)

Registering deadline for next term is March 1 2011.
