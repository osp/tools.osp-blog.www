Title: Seoul fonts karaoke
Date: 2014-01-29 00:54
Author: OSP
Tags: Live, News, fonzie, Language, Music, ocr, Print Party, Type
Slug: seoul-fonts-karaoke
Status: published

[![DSCF0749]({filename}/images/uploads/DSCF0749.jpg){: .aligncenter .size-medium .wp-image-7214 }]({filename}/images/uploads/DSCF0749.jpg)

This print party was wildly exciting.  
Thanks again to the [collectif
SIN](http://www.youtube.com/user/collectifsin1/videos) and [De La
Charge](http://delacharge.com/) gallery.  
Find the recipe and generic of the event on the [README page of the
workshop](http://osp.constantvzw.org/workshop/typojanchi-seoul/) and
**download the 2 hits interpreted by our voice Ms. Funzie Korocr,
[다누보강의 작울결 Remix
1](http://git.constantvzw.org/?p=osp.workshop.typojanchi-seoul.git;a=blob_plain;f=print-party/hits/seoul-fonts-song_1.wav)
and [다누보강의 작울결 Remix
2](http://git.constantvzw.org/?p=osp.workshop.typojanchi-seoul.git;a=blob_plain;f=print-party/hits/seoul-fonts-song_2.wav)**

[![Screenshot from 2014-01-28
23:27:34]({filename}/images/uploads/Screenshot-from-2014-01-28-232734.png){: .aligncenter .size-medium .wp-image-7214 }]({filename}/images/uploads/Screenshot-from-2014-01-28-232734.png)
