Title: Software history mapping night
Date: 2008-10-13 08:59
Author: OSP
Tags: Live, Digital drawing, History, Tools
Slug: software-history-mapping-night
Status: published

### Wednesday 22 October 20:30-22:00

![]({filename}/images/uploads/map1.jpg "map"){: .float}In
a first attempt to draw a collaborative map of the many different tools
that together define the practice of digital design, OSP attempts to
recollect facts and anecdotes gathered over the last few years.

<div class="clear">

</div>

At **Software history mapping night** we'll gather around the table to
start a visual cartography of design software.  
<!--more-->  
Fontlab, Fontforge, Pagemaker, Sodipodi or Ghostscript: When did it
start? Who was involved? Where did it connect?

We will live-broadcast the session from a central Brussels location and
if you have stories to tell, facts to add or experiences to share, you
are more than welcome to join the discussion through video stream and/or
chat.

\[stream- and chat addresses announced on this site next week\]
