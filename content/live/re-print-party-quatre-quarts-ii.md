Title: Re-Print party: Quatre Quarts II
Date: 2009-03-30 23:07
Author: OSP
Tags: Live, Print Party, Recipe, Workshops + teaching
Slug: re-print-party-quatre-quarts-ii
Status: published

**Tuesday 31 March 13:00-16:00**  
Auditoire P7 • Erg, 87, Rue du page, 1050 Bruxelles

![tools]({filename}/images/uploads/tools.jpg "tools"){: .float }At [Open course / Open
source](http://www.multimedialab.be/blog/?p=1204), OSP performs a second
run of a Print Party that happened almost a year ago at the exact same
location. This time we have changed roles to serve you another assorted
collection of stories about engineered fonts, a live cooking lesson,
design 9 new numbers in Inkscape and FontForge, prepare a fresh 16 page
booklet in Scribus and of course end with commandline, cake and ...
print!

Here's one we prepared earlier:
[quatre-quarts-multi-track-print-party](http://ospublish.constantvzw.org/live/quatre-quarts-multi-track-print-party)
