Title: Read The Fine Manual
Date: 2011-03-09 19:34
Author: OSP
Tags: Live, News, Tools
Slug: remote-publishing
Status: published

Today, 1st day out of 3 of workshop at Royal College of Art in London.

We are doing collective remote publishing by sending commands to Scribus
from an IRC channel. We are on irc.freenode.net, channel \#rcaws if you
want to have a glimpse at it, or do blind design! From 10am to 5pm,
London time.

Tomorrow, we'll pass as many versions of Gill Sans through Fonzie!  

We all worked on the same document to make a poster to announce our
public presentation on Friday at 3pm.  

[![]({filename}/images/uploads/screenshot_09-03-11_18-14-22.png "screenshot_09-03-11_18-14-22"){: .alignnone .size-medium .wp-image-5942 }]({filename}/images/uploads/screenshot_09-03-11_18-14-22.png)
