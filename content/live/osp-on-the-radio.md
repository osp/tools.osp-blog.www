Title: OSP on the Radio
Date: 2008-03-27 18:51
Author: Harrisson
Tags: Live, News, LGM 2008, Licenses, Music, Radio
Slug: osp-on-the-radio
Status: published

[![bruitpanik.gif]({filename}/images/uploads/bruitpanik.gif)]({filename}/images/uploads/bruitpanik.gif "bruitpanik.gif")

OSP va parler dans le poste  
ce mercredi 2 avril,  
19:00 - 20:30,  
sur [radio Panik](http://www.radiopanik.org/spip/) (il y a du
[stream](http://www.radiopanik.org/ecouter/)), 105.4 fm a Bruxelles.  
dans l'émission "le Libre en Questions"

Bonne écoute!
