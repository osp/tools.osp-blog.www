Title: Edit: Norms, formats and supports
Date: 2009-01-25 14:45
Author: OSP
Tags: Live, News, DIN, Standards + Formats, Tools, Workshops + teaching
Slug: norms-formats-and-supports
Status: published

### 5 to 7 March, Bordeaux

We're preparing a journey to Bordeaux, France where OSP will contribute
to the conference **[Edit: Norms, formats and
supports](http://www.rosab.net/edit)** with a workshop and a lecture.

![edit]({filename}/images/uploads/edit.jpg "edit"){: .float }From the conference description:

"*Signs and pictograms which organize the flow of movement in cities and
towns according to graphic “pie charts” and other diagrams, which serve
to guide political decisions and economic diagnoses – a multitude of
visual norms which are ignored and “invisible” and have been chosen by
some unknown person at some arbitrary time – control and direct our
daily lives.  
Only when new supports appear (on internet in particular) is our
attention drawn to this world of norms and codes which generally remain
unquestioned. Why?"  
*

Other participants include: Ruedi Baur, Bruno Latour, Edward Tufte,
Robin Kinross, Steve Rushton and Norm.

More info at: <http://www.rosab.net/edit>
