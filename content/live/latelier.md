Title: l'Atelier 
Date: 2011-01-06 12:32
Author: ludi
Tags: Live, News, Works, Drawing, In the pipeline, The Gimp, Webdesign
Slug: latelier
Status: published

[![]({filename}/images/uploads/acsr-preview.png "acsr-preview"){: .aligncenter .size-medium .wp-image-5575 }]({filename}/images/uploads/acsr-preview.png)  
In collaboration with Jérôme Degive ([Pica
Pica](http://www.picapica.be/)), OSP is pleased to announce the upcoming
launch of the new Atelier de Création Sonore Radiophonique website.  
Tracks details and generic to follow.
