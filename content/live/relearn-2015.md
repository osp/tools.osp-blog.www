Title: Relearn 2015
Date: 2015-06-28 17:36
Author: Colm
Tags: Live, News, Culture of work, Education, relearn, relearn-2015, summer-school, Tools, Workshops + teaching
Slug: relearn-2015
Status: published

Relearn is back for 2015!

![](http://www.ralphmag.org/1/bosch-duck-fan281x233.gif){: : .alignnone}

We're very happy to see the summer school continue and morph into a new
being again this year, with a new set of people taking care of the
organization. Quick recap: OSP set up the first edition of Relearn in
2013. In 2014, Relearn was a jointed venture with all the Variable labs
taking part, just before we had to vacate the house. This time, there
are no immediate ties between the organizers, yet there is a very solid
body of exciting and able people taking care of this summer's school.

This year, we go to
[Zinneke](http://www.zinneke.org/Plan-d-acces?lang=fr), to broaden our
horizons.

You can read all about the selected tracks on
[relearn.be/2015/](http://relearn.be/2015/) and peruse through the
archives for [/2014/](http://relearn.be/2014/) and
[/2013/](http://relearn.be/2013/)

**!Applications close on the 1st of July!**

See you there?
