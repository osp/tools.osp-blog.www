Title: LGM radio
Date: 2010-05-26 23:45
Author: Pierre
Tags: Live, LGM, Radio
Slug: lgm-radio
Status: published

![LGM on radio
panik]({filename}/images/uploads/radio2.jpg "radio2"){: .alignnone .size-full .wp-image-4545 }

The Radio Panik show "Good morning Stallman" interviews 3 LGM fellas.
Audio condensed. [Listen to the
podcast](http://www.radiopanik.org/spip/Emission-du-26-mai-Libre-Graphics).
