Title: Tonight: SHMN (update)
Date: 2008-10-22 09:23
Author: OSP
Tags: Live, News, History, Inkscape, SVG, Tools
Slug: ce-soir-tonight-vanavond-shmn
Status: published

[![]({filename}/images/uploads/screenshot.png "software history"){: .alignnone .size-full .wp-image-1236 }](http://ospublish.constantvzw.org/map)

The OSP's have gathered around their respective working tables in
Brussels and Barcelona, to prepare **Software History Mapping Night**,
tonight from 20:30 - 22:00 (GMT+1).

For this occasion, Nicolas has created a rudimentary, quick, lo-tech
collaborative mapping tool:  
<http://www.ospublish.constantvzw.org/map>  
<http://www.ospublish.constantvzw.org/map/sandbox> (crazy layers!)\*

<!--more-->  
In a SVG compliant browser (Firefox works but Opera is more stable),
this tool displays the SVG files that have been uploaded to
[ospublish.constantvzw.org/map/svg](http://ospublish.constantvzw.org/map/svg).
It does this in order of upload, and allows you to navigate layer by
layer!

We are drawing our map layers in Inkscape on A3 size, landscape (which
is interpreted as 1488.19 x 1052.36 pixels for some reason).  
If you want to add a layer yourself, be sure to not flow text in a box
(will result in a black rectangle). Select *text&gt;unflow* in Inkscape.
You'll also need to use webfonts (times, arial, georgia, verdana,
courier etc.)

To add files: upload them to map/svg (or to test use: map/sandbox/svg).
Use this ftp info:

> `server: ospublish.constantvzw.org username: ospublish_map login: softhistmap`

(The ftp access has now been disabled; if you want to contribute:
[e-mail us](http://ospublish.constantvzw.org/?page_id=241)!)

For a webcam view on our working table, view:  
[http://www.giss.tv:8000/history.ogg](http://www.giss.tv:8000/history)  
Sorry, there is no sound.

If you need help or want to stop by to say hello, we're all here:  
<irc://freenode/softwarehistory>

OSP is looking forward to your layers!

\*<small>More about what, how and why [in this
post](http://ospublish.constantvzw.org/?p=1182).</small>
