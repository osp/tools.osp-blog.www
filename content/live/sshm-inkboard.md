Title: SHMN (Inkboard)
Date: 2008-10-26 19:55
Author: Ivan
Tags: Live, Tools, Inkscape, Scripting, SVG, Tools
Slug: sshm-inkboard
Status: published

Inkboard is an Inkscape extension that allows remote collaboration over
the network. In inkscape-devel, there's a
[thread](http://sourceforge.net/mailarchive/message.php?msg_id=44608A6B.2020006%40ekips.org)
where people get all excited over it, and manage to bring down a server
as a result :-)

It's hard not to get overexcited over tools that have collaboration
built into them. They give rise to book covers that I'll repost
[again&again](http://ospublish.constantvzw.org/?p=544), and to LAN
parties, like last week's **Software History Mapping Night**.  
<!--more-->  
Strange, that the tech back kitchen for this was also about software
history. As far as I could tell, Inkboard isn't part of Inkscape's
lifeline at the moment. Nothing wrong with that. This was a constraint
that Nicolas answered with very nice
[php](http://www.ospublish.constantvzw.org/map).

Inkscape compiles fine with Inkboard support in both Ubuntu and Gentoo.
We have been able to see the extra menu between *Effects* and *Help*:

[![]({filename}/images/uploads/inkboard-300x24.png "inkboard"){: .alignnone .size-medium .wp-image-1185 width="300" height="24"}]({filename}/images/uploads/inkboard.png)

We have been chatting away with the Pedro XMPP client, but we have not
been able to share and work on a common SVG document, which was one of
the ideas for the SHMN. Another idea: collaboration is not entirely in
the tools :)

In Gentoo the Inkboard useflag is called *jabber*, and this is the
[recipe](http://www.inkscape.org/wiki/index.php/CompilingUbuntu#Configuring_and_Compiling)
for compiling Inkscape with Inkboard support (and other things) in
Ubuntu.
