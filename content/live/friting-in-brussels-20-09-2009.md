Title: Friting in Brussels 20 09 2009
Date: 2009-09-14 10:59
Author: Harrisson
Tags: Live, News, Works
Slug: friting-in-brussels-20-09-2009
Status: published

Organised by a little open source minded people crew driven by fixed
gear bikes, this [Alley Cat
Race](http://en.wikipedia.org/wiki/Alleycat_races) will occur next
sunday in Brussels.

[![alley\_frite\_flyer]({filename}/images/uploads/alley_frite_vh.png "alley_frite_flyer"){: .alignleft .size-full .wp-image-3378 }]({filename}/images/uploads/alley_frite_vh.png)

Starts  
20/09/09 — 14:00 (day without car)  
Rue de la Victoire 96 — 1060 Bxl map  
Distribution of spoke cards + checkpoint map

Ends  
Place du Jeu de Balle — Aperitive, music, results etc.  
Inscription: 3€

Trip:  
+/- 35km, 9 checkpoints (incl. 3 task checkpoints)

Recommended:  
Lights, Water, Helmet, Lock, Pen, Map of Bxl (19 communes)

Registration:  
[www.fixedgearbrussels.com](http://www.fixedgearbrussels.com)

Flyer done in Inkscape 0.46!
