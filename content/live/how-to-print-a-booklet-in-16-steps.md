Title: How To Print A Booklet In 19 Easy Steps
Date: 2006-06-04 14:36
Author: Femke
Tags: Live, Tools, Printing + Publishing
Slug: how-to-print-a-booklet-in-16-steps
Status: published

The focus of this recipe is on the last bit: rearranging pages so that
you can easily print out nice booklets. For a quick-and-dirty solution
you can use Abiword or OpenOffice for the page-lay out part but Scribus
is essential when you want to be precise with typography.

The recipe is based on the How-To posted on the Scribus Wiki:  
<http://wiki.scribus.net/index.php/How_to_make_a_booklet>.

To make this recipe, you need to open a *terminal*, *shell* or work in
the *commandline*. If you have never done this before, have a look a
this tutorial: <http://linuxcommand.org/>

You can of course print texts of any length, but folding and stapling
more than 12 sheets of paper gets really hard so we suggest making
booklets of 48 pages maximum.

The tools mentioned are all available in most software repositories, and
can be installed using Ubuntu's Synaptic.

[![sample](http://ospublish.constantvzw.org/blog/wp-content/_sample.jpg "sample"){: }](http://ospublish.constantvzw.org/blog/wp-content/Maternal_PoliticsB.pdf)  
<small>[Download sample .pdf
file](http://ospublish.constantvzw.org/blog/wp-content/Maternal_PoliticsB.pdf);
if you simply want to print this document, start the recipe at step 13.  
The text used in this example is available here:
[http://www.constantvzw.com/cyberf/book...](http://www.constantvzw.com/cyberf/book/articles.php?pg=art26).</small>

<!--more-->  
**Ingredients**

-   Linux operating system\* \[Debian / Ubuntu\]
-   Browser \[Firefox\]
-   A text available under an open license
-   xpdf-utils (includes: pdftotext, pdftops, *ps2pdf?*)
-   Texteditor \[Gedit\]
-   Lay-out software \[Scribus 1.3.3.1\]
-   Font \[Bitstream Charter\]
-   psutils (includes: psnup, psbook)
-   Printer
-   Paper
-   Stapler
-   A piece of soft cardboard (side of a box for example)

**Print A Booklet In 19 Easy Steps**

1.  Choose any text that is available under an open license
2.  Download the text to your harddisk in .pdf format or copy the text
    into a text editor
3.  If you have downloaded a .pdf file, you need to convert the .pdf to
    a plain text file using the commandline:  
   `~$ pdftotext infile.pdf`
4.  Clean up the file as much as possible (remove unneccessary white
    lines, check whether any other corrections need to be made) in a
    text editor and save the document as .txt
5.  Open *Scribus* and start a new document with the following options
    selected: *Size: A5*, *Number of pages: 48*, *Page Layout: double
    sided* and *Automatic Text Frames*
6.  Import the .txt file in the Automatic Text Frame and do the
    necessary lay-out; add page numbers etc.
7.  Remove all empty pages so that you end up with a multiple of 4 pages
    (either 4, 8, 12, 16, 20, 24, 28, 32, 36, 40, 44 or 48 pages).
8.  Save / export the document as .pdf with fonts embedded
9.  Using the commandline, convert the pdf file to postscript:  
   `~$ pdftops -paper match infile.pdf outfile.ps`
10. Rearrange the pages so that when printed and folded, each page ends
    up in the right place (when your booklet has 8 pages, page 1 should
    be placed opposite of page 8, page 2 opposite of 7 and 4 opposite
    of 5). *n* is the amount of pages in your booklet.  
   `~$ psbook -sn infile.ps outfile.ps`
11. Arrange two A5 pages next to each other on one A4 sheet (-2 refers
    to the amount of pages on the A4):  
   `~$ psnup -2 -PA5 infile.ps outfile.ps`
12. Convert the document back to .pdf format (This seems a redundant
    step, but without it I had problems with placing, so...)  
   `~$ ps2pdf infile.ps outfile.pdf`
13. Also use the commandline to print first the even pages (*myprinter*
    is the name of your printer, *n* is the amount of copies)  
   ` ~$ lpr -P myprinter -o page-set=even -#1 infile.pdf`
14. When the even pages are printed, you need to re-arrange the order of
    the pages so that the first page comes last.
15. Put the pages upside down back in the printer
16. Now print the odd pages  
   `~$ lpr -P myprinter -o page-set=odd -#1 infile.pdf`
17. Fold the pages from A4 to A5
18. Fold the stack back open and place it on the piece of cardboard with
    the cover facing you. Click open your stapler so you can staple the
    stack in the middle
19. Gently remove the stack (which is now stuck to the cardboard) and
    fold the staples back in.

**FINISHED!**

<small>\*It should work on OSX too, but I have not tested this
yet</small>
