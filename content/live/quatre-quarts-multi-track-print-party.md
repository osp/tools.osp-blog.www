Title: Quatre/Quarts: multi track Print Party 
Date: 2008-04-13 23:57
Author: OSP
Tags: Live, Print Party, Recipe
Slug: quatre-quarts-multi-track-print-party
Status: published

![](http://ospublish.constantvzw.org/image/thumbs/public-appearance/quatre-quarts/small/498-p1010310.JPG){: .float}![](http://ospublish.constantvzw.org/image/thumbs/public-appearance/quatre-quarts/small/494-p1010335.JPG){: .float}![](http://ospublish.constantvzw.org/image/thumbs/public-appearance/quatre-quarts/small/500-img_3235.JPG){: .float}![](http://ospublish.constantvzw.org/image/thumbs/public-appearance/quatre-quarts/small/492-p1010356.JPG){: .float}

<div class="clear">

</div>

more images here:
<http://ospublish.constantvzw.org/image/?level=album&id=13>  
and here:  
[http://www.flickr.com/photos/marcwathieu/...](http://www.flickr.com/photos/marcwathieu/sets/72157604530267258/)

<!--more-->

**Track 1: Narrate**

[![]({filename}/images/uploads/narrate.jpg "narrate"){: .float}]({filename}/images/uploads/narrate.jpg)  
Pierre guided us through his assorted collection of stories about
engineered fonts.  
→ [Images  
](http://ospublish.constantvzw.org/image/?level=album&id=10)  
**Track 2: Bake**

[![]({filename}/images/uploads/ingredients.jpg "ingredients"){: .float}]({filename}/images/uploads/ingredients.jpg)  
With the help of Yi, Femke prepared a Quatre Quarts and explained what
chemical processes where going on while mixing and baking.  
→ [Recipe](http://ospublish.constantvzw.org/recipes/Quatre%20Quarts.htm)
(improved formula)  
→ [What Happens When You Bake a
Cake?]({filename}/images/uploads/whathappenswhenyoubakeacake.txt)

**Track 3: Design**

[![]({filename}/images/uploads/numbers-300x64.jpg "numbers"){: .float}]({filename}/images/uploads/numbers.jpg)

Harrisson designed 9 numbers in Inkscape and FontForge, based on 4
different elements: a square, a circle, a triangle and a rectangle. Than
he laid out a 16 page booklet in Scribus, and added the Quatre Quart
recipe.  
→ [Download
PDF]({filename}/images/uploads/qq.pdf)  
→ [Download
Font]({filename}/images/uploads/qq.ttf)

**Track 4: Print**

[![]({filename}/images/uploads/book.jpg "book"){: .float}]({filename}/images/uploads/book.jpg)

The finished booklet after commandline imposition + printing.  
→ [19 easy steps to print a
booklet](http://ospublish.constantvzw.org/?p=90) (version française:
voir 'comments')

[![]({filename}/images/uploads/lrg-485-p1010402.jpg "lrg-485-p1010402"){: .float}]({filename}/images/uploads/lrg-485-p1010402.jpg)

[![]({filename}/images/uploads/eat.jpg "eat"){: .float}]({filename}/images/uploads/eat.jpg)

→ [Ingredient list](http://ospublish.constantvzw.org/?p=413)

Many thanks to the ERG tutors who helped us gather materials (cake tins,
whisks, printers, mixing bowls, extension chords...)!
