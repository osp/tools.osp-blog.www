Title: Views from the kitchen
Date: 2008-11-18 12:53
Author: OSP
Tags: Live, Cooking, Music, Radio, Recipe
Slug: views-from-the-kitchen
Status: published

[![]({filename}/images/uploads/write12.jpeg)](http://gallery.constantvzw.org/main.php?g2_itemId=22398)

**Recorded in [FoAM's Open Kitchen](http://fo.am/open_kitchen), 15
November 2008.**

Sounds (raw!):
<http://ospublish.constantvzw.org/documents/sound/kitchen>  
Playlist & credits:
[conduite.pdf]({filename}/images/uploads/conduite.pdf)  
Images:  
<small>Peter Westenberg:</small>
<http://gallery.constantvzw.org/main.php?g2_itemId=22398> <small>(Free
Art License)</small>  
<small>Alex Davies:</small>
<http://flickr.com/photos/alexdavies/sets/72157609143437321/>
<small>(All rights reserved)</small>
