Title: OSP public meet
Date: 2012-01-20 18:15
Author: Eric
Tags: Live, News, OSP Public Meets, Works
Slug: osp-public-meet
Status: published

January 30th
------------

Finally a moment of meeting with you for 9000 km of drawings, tools,
stories.  
Interested in the *libre*? Curious about our practice? OSP studio
welcomes you for a first public session at Variable.

**18h30** - Welcoming  
Presentation and discussions around recent and ongoing OSP studio
works.  
[![]({filename}/images/uploads/radio-panik-0.jpg "radio-panik-0"){: .size-medium .wp-image-6961 .aligncenter }]({filename}/images/uploads/radio-panik-0.jpg)  
[![]({filename}/images/uploads/balsa-badges-0.jpg.png "balsa-badges-0.jpg"){: .size-medium .wp-image-6961 .aligncenter }]({filename}/images/uploads/balsa-badges-0.jpg.png)

**19h30**  
**Exiled Cuisine** (Ivan Monroy Lopez, Mexico City)  
Lecture/performance +
[dinner](http://cooking.stackexchange.com/questions/20691/in-this-recipe-what-is-a-possible-substitute-for-serrano-pepper "cooking!").  
Cookbook files are open when there’s a chance of working. The work is
not technological in that nothing of this has not been done before. The
literature that feeds into this has been contentionally written in the
shall we say mmh street. The public space of technical forums is related
in some way to network protocols. It could be good that these HTML files
be exported into cookbooks. I don’t like it when these books are sold. A
standard regime of the kitchen is easier for the exiled to understand.

`Variable rue Gallait straat 80 1030 Bruxelles`
