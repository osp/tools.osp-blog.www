Title: Territorial practice
Date: 2010-12-09 11:50
Author: Pierre
Tags: Live, Presentations, Thoughts + ideas, Workshops + teaching
Slug: territorial-practice
Status: published

It's hard to believe that we are back for more than three weeks from Tel
Aviv, where we participated in *[Open Code Versus Military Culture?
Aspects in Israel Digital
Culture](http://www.shenkar.ac.il/digitalcultureconf/digitalcultureconf_eng.html)*.
It feels both close and very far away.

\[caption id="attachment\_5416" align="alignnone" width="400"
caption="Arriving late night at Dizengoff square, Tel
Aviv"\][![]({filename}/images/uploads/imag1426.jpg "Dizengoff, Tel Aviv"){: .size-medium .wp-image-5416 }]({filename}/images/uploads/imag1426.jpg)\[/caption\]

*Open Code Versus Military Culture?* was initiated by digital artist
[Tsila Hassine](http://www.missdata.org/), asking pertinent questions
about the relationships among technology, politics, culture and art.
These questions are obviously ultra relevant in the light of the
political situation in the Middle East and even more pressing in Israel
where technological culture is largely shaped by defence industries and
commercial software corporations.

The conference and work session gave us the opportunity to sense the
connections between military, culture and commerce in the Israeli
context. It happened through the voices and gestures of the very people
involved in the decision making, each having different interests.  
<!--more-->  
**Two-day conference: Open Code Versus Military Culture?**

The morning sessions came as a bit of a shock, especially for those of
us who had just arrived in Israel. A retired brigadier spoke about the
importance of 'Thinking out of the box' in response to security threats,
and an entrepreneur explained how the civilian market could profit from
military research and innovation in a nice way. Their understanding of
the dynamics of technological development had a lot in common: both the
military and the entrepreneur impose the emergency of a 'time to market'
to developers, thinking in an abstract world. Through this phraseology,
the cost of human life, in the case of the military, is understood in
terms of commodity and the presence of a product delivered timely on the
market, for the industrial, as a military operation. Logically then, the
computer industry in Israel would sell security-related services,
benefiting from the reputation of its army. And due to a constant
assimilation of war and marketing, as a response to immediate threat,
the Israeli industry would lack time and investment to create
infrastructure. As the military culture is one of secrecy, no wonder
that it doesn't provide a fertile ground for open source development. As
a contrast, a second entrepreneur representing the RedHat-Israel company
came to present their open source model for the computer industry. This
capitalist with a friendly face celebrated the rebirth of centralized
mainframes through the buzzword of cloud-computing, arguing this was the
first true innovation driven by F/LOSS. The first question mark was
expressed in a presentation about
[Tsofen](http://www.tsofen.org/?lang=en), an organisation trying to open
up the labour market of the Israeli High Tech Industry for qualified
Arab IT engineers living in Israel.

By the time the second session *Critical Perspectives on the Uses and
Discourses of Digital Technology and the Military* had started,
entrepreneurs and military representatives unfortunately had already
left the premises. Dr. Eyal Weizman (flown back to London right after
his lecture before any questions could be asked) held a stunning talk on
the issue of proportionality, presenting the design of the separation
wall on the West Bank as a form of collaborative design. "Our
algorithmic society has become irrevocably obsessed with calculating and
reducing the evil it has itself perpetrated" ((Talk partially overlapped
with this text: 
<http://www.allvoices.com/s/event-6743881/aHR0cDovL3d3dy5vcGVuZGVtb2NyYWN5Lm5ldC9leWFsLXdlaXptYW4vbWF0ZXJpYWwtcHJvcG9ydGlvbmFsaXR5LXBhdWwtaGlyc3QtbWVtb3JpYWwtbGVjdHVyZS0yMDEw>))

Sheizaf Rafaeli from Haifa University toured us through Digital Culture
in Israel and showed that it is at the same time vibrant, diverse and
dispersed. The reality of the World Wide Web in fact allows very
different communities to exist in parallel, without being connected at
all. The presentation by Eran Sachs was both impressing and moving, as
he related his experience in military service (where he was taught
signal theory and analysis by ear as part of his service with the IDF
Electronic Intelligence unit) to his art sound projects, connecting both
with his ad hoc slogan "want + can → will": about driving habits of
israeli roads; an art sound project in the evacuated buildings of Hebron
where he produced sound to visualize space by throwing stones on walls;
a precise, humorous and detailed explanation about what the convolution
of waves means; to a sudden final, beautiful and vibrant assembly of
some of these parts in a call for convolution as a civilian strategy of
smooth but strong counter-power to current politics. After that, the
Minister of Improvement of Government Services talked to us about the
new government website for a while until he finally was called away to
appear on tv. The day ended with an exposé from Professor Langdon Winner
about how Open Source might in the end be the only hope for saving
democracy in The United States.

The second day of the conference was devoted to cultural practice. The
first panel was called *Cultural and Artistic Approaches to Code
Practices* and as you might have guessed, this was the panel we were in.
Matthew Fuller started off with Executing Software Studies, and the need
to look at software as a culture. For the OSP presentation we had
decided to speak through very concrete and detailed examples about the
ways F/LOSS tools help unimagine practice. We were trying to be sincere
but it also felt rather futile. Ayelet Karmon and Yair Reshef gave an
informative overview of various projects applying code-practices to
physical objects and the session was concluded by Mushon Zer-Aviv
((based on ideas discussed here:
<http://www.smashingmagazine.com/2010/09/01/the-case-for-open-source-design-can-design-by-committee-work>)),
a graphic designer based in Tel Aviv. He ended his talk Towards Open
Source Design with the statement that "\[T\]he substantial parts of
design that still cannot be easily quantified or assessed on shared
rational ground should be managed  through trust and leadership. A
resilient community of practice must be  able to develop design
leadership whose work and guidance is respected  and appreciated even
without the convenient meter of coding meritocracy.", an idea we tried
to challenge at the dinner table (with not much success yet, but we
might have another chance at an upcoming LGM). The last sessions were
devoted to data visualization. It started with a short presentation of
[openknesset.org](http://www.openknesset.org), a group of activists
*monitoring* the Israeli Parliament. In the same vein, [Roberto
Therón](http://usal.academia.edu/RobertoTheron/) stated that data
visualization is a tool to foster change. Interestingly, his practice of
data viz is related to collective methodologies as the ones developed in
collaboration with Media Lab Prado that welcome open contributions,
modifications of the project through a dialogue with the participants;
the production of data visualisation is the occasion for an exchange
within emerging communities of researchers ranging from academics,
hackers, activists and artists. Ted Byfield impressed with a sharp
analysis that ended with a call for politically oriented graphs ((some
of it can be found
here: <http://streamingculture.parsons.edu/parsons-the-new-school-for-design-050710-0516pm/>)).
Vinca Kruk from Metahaven Design Collective did an attempt to unravel
the Eden Abergill case, but unfortunately only managed to scratch the
surface of the complicated context in which these images continue to
appear.

**Worksession: Territorial Practice**

The two day OSP-workshop Territorial Practice took place in Holon, a
suburb south of Tel Aviv. As we hoped, working collectively with maps
proved to be a productive interface to speak with participants on
different levels.

\[caption id="attachment\_5407" align="alignnone" width="400"
caption="Francis Alys, The Green Line. “Sometimes Doing Something Poetic
Can Become Political, and Sometimes Doing Something Political Can Become
Poetic”"\][![The Green Line, Francis
Alys]({filename}/images/uploads/The-Green-Line-by-Francis-006.jpg "The-Green-Line-by-Francis"){: .size-medium .wp-image-5416 }]({filename}/images/uploads/The-Green-Line-by-Francis-006.jpg)\[/caption\]

We discussed perspective, delieniation, territory and cartography as a
subjective process. We consequently tried to construct the map of the
Holon neighbourhood through observation and notetaking on the ground and
than combined our observations into multi-layered collective images.

[![]({filename}/images/uploads/img_6900.jpg "img_6900"){: .size-medium .wp-image-5416 }]({filename}/images/uploads/img_6900.jpg)

[![]({filename}/images/uploads/p1080309.jpg "p1080309"){: .size-medium .wp-image-5416 }]({filename}/images/uploads/p1080309.jpg)

We were happy to have a chance to work for two days with Nira, a retired
urban and regional planner who used to work for the government in
Jerusalem; with Orna, a photographer, architect and activist living with
her daughter in Jaffa and with Sva, a multidisciplinary artist whose
activities range from dance to practical philosophy.

\[caption id="attachment\_5414" align="alignnone" width="400"
caption="Orna gave us a selection of her beautiful textile-mounted maps
of the area, now de-classified but in use by British military in the
1910-20's"\][![]({filename}/images/uploads/p1080242.jpg "p1080242"){: .size-medium .wp-image-5416 }]({filename}/images/uploads/p1080242.jpg)\[/caption\]

The second day, a group of interaction design students from Shenkar
School of Art and Design joined. They energetically updated the
OpenStreetMap data on the Holon area and stated that 'they will never
look at maps the same way again'.

\[caption id="attachment\_5434" align="alignnone" width="400"
caption="Notes on Walking Paper print"\][![Notes on Walking Paper
print]({filename}/images/uploads/imbal-bas.jpg "imbal-bas"){: .size-medium .wp-image-5416 }]({filename}/images/uploads/imbal-bas.jpeg)\[/caption\]

The Open Street Map project and the wonderful Walking Papers project
((<http://walking-papers.org/>)) proved to be interesting tools for
dialog. They provided us with a provisional and fragile basis for a
conversation between participants, but also helped understand better our
own approaches/practices/sensibilities vis-a-vis questions of mapping.

\[caption id="attachment\_5415" align="alignnone" width="400"
caption="Participants re-enact Esther Ferrer's performance: Walking is
the
way"\][![]({filename}/images/uploads/p1080264.jpg "p1080264"){: .size-medium .wp-image-5416 }]({filename}/images/uploads/p1080264.jpg)\[/caption\]

More images:
<http://ospublish.constantvzw.org/image/index.php?level=album&id=48>

**Examples discussed in the workshop**

Francis Alÿs, The Green Line:
<http://www.nytimes.com/2007/03/13/arts/design/13chan.html>  
Peter Westenberg, Op, van, -se:
<http://westenberg.constantvzw.org/?p=57>  
The Towards project: <http://www.towards.be>  
Text: Cartography as a common good
<http://www.towards.be/site/spip.php?article367>  
List of Free, Libre Open Source cartographic resources:
<http://www.stormy-weather.be/wiki/index.php/MappingRessources>  
Animation of 2008 commits to Openstreetmap: <http://vimeo.com/2598878>  
Jaffa, Autobiography of a City: <http://www.jaffaproject.org/>  
Map Compare, comparing openstreetmap to Google Maps:
<http://tools.geofabrik.de/mc/>  
Ben Fry, All streets: <http://benfry.com/allstreets/>  
Esther Ferrer, Walking is the way:
<http://www.arteleku.net/estherferrer>  
The relation of territory to maps:
<http://www.wordiq.com/definition/Map-territory_relation>  
Ohad Matalon:  
<http://www.taviartgallery.com/ViewExPhoto.php?p=0&n=84&pic=956>  
OpenStreetMap of [our small area in
Holon](http://www.openstreetmap.org/?lat=32.01723&lon=34.79323&zoom=16&layers=M)
after edition by us, the Hebrew version of streetnames were added by
[Talkat](http://www.openstreetmap.org/user/talkat).

**Notes**
