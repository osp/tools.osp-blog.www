Title: OSP Mexican branch
Date: 2011-04-09 15:49
Author: OSP
Tags: Live, News, branch, future, Mexico
Slug: osp-mexican-branch
Status: published

[![]({filename}/images/uploads/ivan_biker-75x100.jpg "ivan_biker"){: .float width="75" height="100"}]({filename}/images/uploads/ivan_biker.jpg)
Next Tuesday our co-worker [Ivan Monroy Lopez](http://textzi.net/)
relocates to Mexico City to set up the first Latin American branch of
OSP. Using 10 hours of time difference in our advantage, we now can
serve Free, Libre and Open Source design (almost) around the clock! If
you want to join the Mexican revolution, please write to
mail@ospublish.constantvzw.org and we put you in contact with our local
representative.
