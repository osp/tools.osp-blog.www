Title: Road to South-Wrocław
Date: 2008-05-05 19:42
Author: Pierre
Tags: Live, News, LGM 2008, Libre Fonts, Map, Workshops + teaching
Slug: road-to-south-wroclaw
Status: published

This Wednesday, join us for our [type
workshop](http://ospublish.constantvzw.org/?p=443) in the [OPT cultural
center](http://www.opt-art.net/) in Wrocław from 11am to ongeveer 6pm.
For those who arrive directly from the airport, you can begin with [this
path](http://maps.google.com/maps?f=d&hl=en&geocode=&saddr=wroclaw+airport&daddr=wroclaw,+dzialkowa+15&sll=51.07809,17.035226&sspn=0.01038,0.025063&ie=UTF8&z=13)
then please print this map, because it seem that it is not easy to find
for taxi driver (our journey was epic) or even for Google Maps (just
waiting for [Towards](http://www.towards.be)).  
See ya!

[![]({filename}/images/uploads/map-real.jpg "map-real"){: .alignnone .size-medium .wp-image-467 }]({filename}/images/uploads/map-real.jpg)
