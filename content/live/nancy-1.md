Title: Nancy -1
Date: 2010-03-24 23:22
Author: ludi
Tags: Live, News, dingbats, expo, Font, public appearance, Unicode
Slug: nancy-1
Status: published

[![]({filename}/images/uploads/grille.jpg "grille"){: .alignright .size-medium .wp-image-4316 }]({filename}/images/uploads/grille.jpg)  
[![]({filename}/images/uploads/smoke-75x56.jpg "smoke"){: .alignright .size-medium .wp-image-4316 }]({filename}/images/uploads/smoke.jpg)
[![]({filename}/images/uploads/detail-75x56.jpg "detail"){: .alignright .size-medium .wp-image-4316 }]({filename}/images/uploads/detail.jpg)
[![]({filename}/images/uploads/boite-75x56.jpg "boite"){: .alignright .size-medium .wp-image-4316 }]({filename}/images/uploads/boite.jpg)

OSP se prépare à [my.monkey](http://www.mymonkey.fr)  
Dessin/grattage Unicode à la mine de plomb, scriptage Scribus vers
étiquettes, collection d'objets, préparation d'un mode d'emploi et
suivit du projet en place en ligne.

Encore beaucoup de quoi s'occuper. Demain premier tests debuggage de
Nancy.
