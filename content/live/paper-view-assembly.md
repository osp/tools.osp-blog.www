Title: PA/PER VIEW: Assembly
Date: 2011-03-28 11:26
Author: OSP
Tags: Live, News, Licenses, Printing + Publishing, public appearance
Slug: paper-view-assembly
Status: published

**Assembly: Friday April 1 (18:00 - 20:00) at PA/PER VIEW art book
fair**  
WIELS, Avenue van Volxemlaan 354, Brussels (Belgium)

![]({filename}/images/uploads/thing.jpg "thing"){: .alignnone .size-full .wp-image-6046 }

This Friday, OSP participates in a discussion initiated by Agency.
Agency is the generic name of a Brussels-based agency that was
established in 1992 by Kobe Matthys. Agency constitutes a growing “list
of things” that resist the binary division between culture and nature,
expressions and ideas and consequently between creations and facts,
subjects and objects, humans and non-humans, originality and banality,
individuals and collectives, etc...  
<!--more-->  
Agency calls things forth from it’s list via varying “assemblies” inside
exhibitions, performances, publications, etc... Each assembly explores
in a topological way a different aspect of the performative consequences
of the apparatus of intellectual property for an ecology of art
practices.

For Assembly (PA/PER VIEW), Agency will call things forth, speculating
on the question: How to include book making in art practices? How are
typeface designers, book binders, lay-outers, etc... taken into
consideration by copyright law? Thing 001452 (Orion) and Thing 001535
(Introductory Language Work) will convene an assembly at PA/PER VIEW in
order to bear witness. On April 1, 2011 Agency will invite a diverse
group of concerned guests to “translate” on these things: Thorsten
Baensch (publisher Bartleby), Sari Depreeuw (legal scholar VUB), Manuel
Raeder (graphic designer), OSP (Open Source Publishing: Ivan Monroy
Lopez/Seb Sanfilippo/Femke Snelting/etc..), Ziga Testen (graphic
designer), etc...
