Title: VoidCon 2011
Date: 2011-07-08 10:13
Author: OSP
Tags: Live, Texts Antwerp, Conference, hackerspace, public appearance
Slug: voidcon-2011
Status: published

![]({filename}/images/uploads/void.png "void"){: .alignnone .size-medium .wp-image-6606 }  
Saturday July 16: find OSP at [VoidCon
2011](http://www.voidwarranties.be/index.php/VoidCon2011), organised by
the Antwerp hackerspace [VoidWarranties](http://www.voidwarranties.be).
