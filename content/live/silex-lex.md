Title: SILEX-LEX
Date: 2010-04-08 15:18
Author: OSP
Tags: Live, Digital drawing, expo, Inkscape, LGM 2010, Printing + Publishing
Slug: silex-lex
Status: published

[![]({filename}/images/uploads/logo_silex_lex.png "logo_silex_lex"){: .alignright .size-medium .wp-image-4382 }]({filename}/images/uploads/logo_silex_lex.png)

**Exhibition: 8 May → June 4 2010  
Opening: May 7 18:00-21:00  
De Pianofabriek, Fortstraat 35, 1060 Brussels  
**

What if a team of talented artists, illustrators, graphic designers and
graffiti writers starts working with Free, Libre and Open Source
softwares for the first time? What if their trials and errors are
printed on the Plus-tôt-Te-Laat Rhizograph and then handed over to
students of the Ecole de Recherche Graphique (ERG) for a make-over?

Results of this serial experiment on show until June 4 at De
Pianofabriek in the context of the [Libre Graphics Meeting
2010](http://www.libregraphicsmeeting.org).

With: Nurse, Laurent Baudoux, Manuel Falcata, Gwenola Carrere, Lodovico
Corsini, Jerome Degive, Julien Meert, Martin Meert, Benoit Plateus,
Jonathan Poliart, Emilie Seron.  
Assistants: Harrisson, Pierre Marchand, Ludivine Loiseau

<small>An initiative of OSP (Open Source Publishing) in collaboration
with [Plus-tôt Te Laat](http://www.pttl.be/) and support of De Vlaamse
Gemeenschap, De Vlaamse GemeenschapsComissie and
[http://www.multiseatcomputer.be](http://www.multiseatcomputer.be/).</small>
