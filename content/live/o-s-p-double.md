Title: O S P   double
Date: 2010-01-14 17:26
Author: OSP
Tags: Live, News, Works, Inkscape, SVG
Slug: o-s-p-double
Status: published

Pour le numéro de janvier consacré à la visualisation de données, le
magazine français [étapes:](http://www.etapes.com/) ouvre une double
page à OSP pour notre carte [Cinéma du réel
2009](http://ospublish.constantvzw.org/news/osp-full-scale-in-beaubourg).

[![osp-etapes\_int\_small]({filename}/images/uploads/osp-etapes_int_small.jpg "osp-etapes_int_small"){: .alignleft .size-medium .wp-image-3750 }]({filename}/images/uploads/osp-etapes_int_small.jpg)

&gt; [voir
+](http://ospublish.constantvzw.org/news/o-s-p-double#more-3749)  
<!--more-->  
[![osp-etapes\_cover\_small]({filename}/images/uploads/osp-etapes_cover_small.jpg "osp-etapes_cover_small"){: .alignleft .size-medium .wp-image-3750 }]({filename}/images/uploads/osp-etapes_cover_small.jpg)

[![osp-etapes\_texte01\_small]({filename}/images/uploads/osp-etapes_texte01_small.jpg "osp-etapes_texte01_small"){: .alignleft .size-medium .wp-image-3750 }]({filename}/images/uploads/osp-etapes_texte01_small.jpg)

[![osp-etapes\_texte02\_small]({filename}/images/uploads/osp-etapes_texte02_small.jpg "osp-etapes_texte02_small"){: .alignleft .size-medium .wp-image-3750 }]({filename}/images/uploads/osp-etapes_texte02_small.jpg)

[![osp-etapes\_texte03\_small]({filename}/images/uploads/osp-etapes_texte03_small.jpg "osp-etapes_texte03_small"){: .alignleft .size-medium .wp-image-3750 }]({filename}/images/uploads/osp-etapes_texte03_small.jpg)
