Title: Print Party Berlin
Date: 2006-09-18 09:33
Author: Harrisson
Tags: Live, Works, Print Party, Printing + Publishing
Slug: print-party-berlin
Status: published

![panel.jpg]({filename}/images/uploads/panel.jpg){: #image132}

![orga.jpg]({filename}/images/uploads/orga.jpg){: #image132}

A report from the whole day is here:
[http://www.stormy-weather.be/wiki/in...](http://www.stormy-weather.be/wiki/index.php/FREEdom_and_OPENness%2C_workshop)  
<!--more-->  
For Wizard of OS Constant Printing Team members Harrisson and Pierre
Huyghebaert showed that designing and printing booklets with Open Source
software is nowhere near Science Fiction.

Wednesday the 13th of September, Pierre and Harrisson presented a Print
Party during Wisard of Os 4. This event was taking place in Berlin. The
critical panel, organised by Cornelia Solfrank and Nicolas Malevé at
Tesla (former Podewil) gathered Laurence Rassel, Simon Yuill, Harrisson,
Pierre Huyghebaert, Simon Worthington, Adam Hyde, Saul Albert, Gisle
Froysland, Malte Steiner, Gordon Duggan, Eberhard Ortland, Hinrich
Sachs, Aileen Derieg, Goran Djordevic, Gergers Petersen, Felix Stalder,
Inke Arns, Jacob Lillemose, Annette Schindler, Dorothea Carl, Christian
von Borries, …

This compact presentation was focused on the imposition of a 8 pages
leaflet, that we succeded in printing, using non graphical interface
softwares on Ubuntu. This little operation replaced what used to be done
by a 10000 euro software 5 years ago.

![matos.jpg]({filename}/images/uploads/matos.jpg){: #image132}

![laurence.jpg]({filename}/images/uploads/laurence.jpg){: #image132}

Text set on paper is Femke Sneltings "Open Source Software for design"
and OsBlogs "How To Print A Booklet In 19 Easy Steps", resulting in a
"meta" publication on Constant Verlag.

![gnu\_manifesto.jpg]({filename}/images/uploads/gnu_manifesto.jpg){: #image132}
