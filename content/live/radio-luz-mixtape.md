Title: Radio Luz mixtape
Date: 2008-09-12 18:56
Author: Ivan
Tags: Live, LGM 2008, Music, Radio, Thoughts + ideas
Slug: radio-luz-mixtape
Status: published

This was the playlist when OSP went on a radio station in Wrocław. A few
months after, it's hard to reconstruct the exact sequence of events that
led us there. It was something like this: there was a gig at an
alternative space, David met us there, he had helped us set up the
pre-LGM workshop at [](http://www.opt-art.net)OPT, and he invited us to
his friend's show on [](http://radioluz.pwr.wroc.pl)Radio Luz.

Our compilation was about the long tradition of appropriation in pop.
Afrika Bambaataa sampled Kraftwerk, everyone sampled The Winstons, and
M|A|R|R|S sampled everyone. Though I ~~didn't~~ find the pictures of OSP
donning the big headphones \[[found
them!](http://ospublish.constantvzw.org/image/?level=picture&id=842)\],
I did find a lofi recording of the show. The typography
[workshop](http://ospublish.constantvzw.org/?p=454) had just taken place
a few days before:  
<!--more-->

> There was another font made with letters from around the OPT buiding.
> We took pictures of graffitti, of old signage, of shops, of these
> kinds of things. We extracted the letters of the alphabet, and we
> constructed a new alphabet with them. It's the same principle as the
> music that we heard. Music with samples. Taking things that others
> have made, and you create a new thing that then you can share and that
> others can modify and use again...

1\. Kraftwerk. [Trans-Europe
Express](http://en.wikipedia.org/wiki/Image:TEE.png).  
2. Afrika Bambaata and the Soulsonic Force. [Planet
Rock](http://en.wikipedia.org/wiki/Image:PlanetRockSingle.jpg).  
3. Public Enemy. [Fight the
Power](http://www.discogs.com/viewimages?release=610705).  
4. The Winstons. [Amen,
Brother](http://en.wikipedia.org/wiki/Image:Amen_break_sample_image.png)  
5. M|A|R|R|S. [Pump Up the
Volume](http://en.wikipedia.org/wiki/Image:MarrsPumpUpTheVolumeAD707.jpg).  
6. Atari Teenage Riot. [Destroy 2000 Years of
Culture](http://www.discogs.com/viewimages?release=224214).  
7. Pierre Normal. [Attraction](http://www.pneu.org/spip.php?article46).
