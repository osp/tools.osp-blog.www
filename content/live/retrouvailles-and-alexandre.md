Title: Retrouvailles and Alexandre
Date: 2009-05-06 21:45
Author: Pierre
Tags: Live, Images, LGM 2008, LGM 2009, Photos, Pictures
Slug: retrouvailles-and-alexandre
Status: published

Now that we cross
[Alexandre-Prokoudine-magic-glue-between-developers](http://prokoudine.info/blog/)
again in the corridors of [LGM
2009](http://www.libregraphicsmeeting.org/2009), we've been able to
physically get pictures taken by him in his sans-fatigue hunt for images
at LGM last year! I don't know if he gimped the files but [on the
protraits we're looking like out of a shiny Rock&Folk
magazine](http://ospublish.constantvzw.org/image/?level=picture&id=995)...
(but it's a beautiful surprise, thank you Alexandre!)

![Kitchen during the
Printparty]({filename}/images/uploads/img_9261.jpg "Kitchen during the Printparty"){: .alignnone .size-medium .wp-image-2569 }
