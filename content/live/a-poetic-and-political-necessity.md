Title: a poetic and political necessity
Date: 2010-10-29 19:17
Author: Pierre
Tags: Live, News, Map, Presentations, public appearance, Tools, Workshops + teaching
Slug: a-poetic-and-political-necessity
Status: published

![]({filename}/images/uploads/8.gif "8"){: .alignnone .size-full .wp-image-5138 width="200" height="50"}![]({filename}/images/uploads/8.gif "8"){: .alignnone .size-full .wp-image-5138 width="200" height="50"}

Next week another delegation of OSP travels to Tel Aviv to present at
the conference **[Open Code Versus Military Culture? Aspects in Israel
Digital
Culture](http://www.shenkar.ac.il/digitalcultureconf/digitalcultureconf_eng.html)**
organised by the [Shenkar College for Engineering and
Design](http://www.shenkar.ac.il). Following the talk, there will be a
worksession at the [Israeli Center for Digital
Art](http://digitalartlab.org.il).

From the conference description:

> "The interest in the effects of technology on culture, and
> reciprocally, the effects of culture on technology, peaked with the
> unprecedented spread of the Internet. This conference seeks to promote
> public discussion about the significant relationships among
> technology, politics, culture and art. Israel has a very successful
> hi-tech industry. However, the local digital-technological culture is
> largely shaped by defense industries and software corporations."

<http://www.shenkar.ac.il/digitalcultureconf/digitalcultureconf_eng.html>

<!--more-->  
Talk description:  
**Unimagining practice**  
Contemporary creative work depends largely on software tools. Full of
"accepted ideas" about the way things ought to be done, tools are
unavoidably shaped by conventional models of production and
distribution. In 2006, OSP decided to use Free, Libre and Open Source
Software only. We had grown frustrated with the perceived neutrality of
the default designer toolset and wanted to take part in the construction
of software that circumscribes what we make. In this talk we'll go
through some of the consequences of that choice: how it changed our
habitual vocabulary, radically reorganised our workflow and in what way
we engaged with the F/LOSS community developing those tools.

Workshop description:  
**Territorial practice**  
A two day worksession on F/LOSS cartographic tactics at the [Israeli
Center for Digital Art](http://digitalartlab.org.il)  
Even if cartography is generally produced from a bird's eye perspective,
details cannot be drawn from a remote location; they must be
confirmed/corrected/added on the spot. The territory must be literally
practiced by annotating and drawing.

Working on location with territories and the people living onto it,
offers both beautiful and efficient ways of exchanging practiced of
space, and builds connections while mediating localized knowledges.
Being able to consult, create, publish and exchange maps, to have access
to cartographic data and know precisely where the openings are is both a
poetic and a political necessity.

On Thursday 10 November, in the morning, OSP starts with a presentation
of maps we are interested in, some of the projects we have worked on and
the questions they raise. For the afternoon session, we invite
participants to bring samples of maps they like/dislike, data they think
that deserves geo-location, ideas for maps or edits to existing maps.  
On Friday 11 November, we will group participants according to their
interests and work together in response to needs, questions and ideas.  
Depending on the input of the participants, the work will concentrate
more on practical issues or conceptual ones.

Tools and materials we could decide to work with on day 2: OpenLayers,
OpenStreetMap, drawing, geonames, GPS-tracking, gpsbabel, web services
and API's...
