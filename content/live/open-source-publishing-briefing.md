Title: Open Source Publishing Briefing
Date: 2007-11-18 16:03
Author: Femke
Tags: Live, London, Presentations
Slug: open-source-publishing-briefing
Status: published

[![]({filename}/images/uploads/nmx3.jpg)]({filename}/images/uploads/nmx3.jpg)
[![]({filename}/images/uploads/nmx1.jpg)]({filename}/images/uploads/nmx1.jpg)
[![]({filename}/images/uploads/nmx2.jpg)]({filename}/images/uploads/nmx2.jpg)

With Mute's Simon Worthington and Laura Oldenburg, OSP participated in a
[NM-X network
evening](http://nm-x.com/event/2007/11/open-source-publishing-briefing).
Following our presentations, we discussed the problems and potential of
installing free software on proprietary platforms, whether it was
interesting to develop a free software 'design suite', the position of
free fonts and much more.
\[[Notes](http://ospublish.constantvzw.org/wiki/doku.php?id=presentations:nm-x_notes)\]
\[[Images](http://flickr.com/search/?q=NMX421)\]

If you happen to be in London on Thursday November 22 and 29, feel
welcome to participate in the workshop that follows this presentation:
<http://nm-x.com/event/2007/11/open-source-publishing-workshops>

<small>Pictures: Saul Albert</small>
