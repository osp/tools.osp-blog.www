Title: Building archives landscapes plotter
Date: 2011-10-22 11:37
Author: OSP
Tags: Live, News, Tools
Slug: building-archives-landscapes-plotter
Status: published

Invited at [Interpunctie project van
Bolwerk](http://www.ooooo.be/interpunctie), we excavate into the 10+
years of archives by building [the first bricks of
tools](http://git.constantvzw.org/?p=osp.residency.masereel.git) to draw
a landscape thanks to bash and python scripts + the [legacy Fontforge
scripting
language](http://fontforge.sourceforge.net/scripting-tutorial.html).

\[gallery link="file"\]
