Title: A postcard from LGM 2008
Date: 2008-05-10 16:12
Author: OSP
Tags: Live, News, Tools, LGM 2008
Slug: a-postcard-from-lgm-2008
Status: published

[![yi](http://ospublish.constantvzw.org/image/plog-content/thumbs/snapshots/crossing-lgm-wroc__aw/large/666-a-dsc02271.JPG)](http://ospublish.constantvzw.org/image/?level=picture&id=666)
[![talk](http://ospublish.constantvzw.org/image/plog-content/thumbs/snapshots/crossing-lgm-wroc__aw/large/679-p5080070.JPG)](http://ospublish.constantvzw.org/image/?level=picture&id=679)
[![dave](http://ospublish.constantvzw.org/image/plog-content/thumbs/snapshots/crossing-lgm-wroc__aw/large/689-p1050928.JPG)](http://ospublish.constantvzw.org/image/?level=picture&id=689)

The end of our participation in the Libre Graphics Meeting 2008 is in
sight...

Over the last few days we have seen many intriguing, surprising and
interesting talks; we discovered new tools, rediscovered 'old' ones and
made connections with developers, users and standards-officials -- LGM
has been again an inspiring adventure.

We interviewed [Dave Crossland](http://understandinglimited.com) about
the history of font editing software, [Denis
Jacquerye](http://home.sus.mcgill.ca/~moyogo/) about internationalized
typography, [Ralph Giles](http://people.xiph.org/~giles/) about the
story of Nimbus and Courrier, Michael Terry about
[Ingimp](http://www.ingimp.org/) and usability development in Open
Source and [Chris Lilley](http://www.w3.org/People/chris/) about W3C
standardizing processes and the way it facilitates (or not) interaction
between developers and designers. All this we will make available at
some point on this weblog.

Apart from that, [Cedric Gémy](http://www.le-radar.com/) has initiated a
Free Software user group for graphic design professionals (most
certainly needs a better name), we started packaging our first open font
for distribution via the [Open Font
Library](http://openfontlibrary.org/) and thought of dozens of new
projects to work on.

<http://ospublish.constantvzw.org/image/?level=album&id=19>

It will take us days, weeks, months to process all this material so
please be patient :-) In the mean time, a big thank you to everyone and
enjoy the growing collection of images as we upload.
