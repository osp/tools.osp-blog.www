Title: Audio + screencast of our intervention at LGM 2008
Date: 2008-05-13 11:26
Author: Pierre
Tags: Live, LGM 2008, Presentations
Slug: audio-screencast-of-our-intervention-at-lgm-2008
Status: published

[![]({filename}/images/uploads/flash.gif "Screencast LGM OSP")]({filename}/images/uploads/flash.gif)

Kaveh Bazargan has patiently recorded [the sound and the screen of every
interventions at LGM](http://www.river-valley.tv/conferences/lgm2008/).
Here is ours:
<http://media.river-valley.tv/conferences/lgm2008/quicktime/0103-Harrisson.html>.
(At the end of the conference, we discovered that Kaveh is a TeX guy for
20 years, so we've asked him a few questions. Transcript will come.)
