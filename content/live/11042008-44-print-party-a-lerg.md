Title: 11 avril: Quatre/Quarts Print Party à l’ERG
Date: 2008-04-09 07:52
Author: OSP
Tags: Live, Ingredients, Print Party
Slug: 11042008-44-print-party-a-lerg
Status: published

Info: <http://ospublish.constantvzw.org/?p=407>  
Recette:
[http://ospublish.constantvzw.org/recipes](http://ospublish.constantvzw.org/recipes/Quatre%20Quarts.htm)

**Pour faire une 4/4 Print Party, il faudra:**

-   2 fours électriques
-   16 œufs
-   1 kg de beurre
-   1 kg de sucre blanc fin
-   1 kg de farine
-   3 tabliers
-   1 raclette en caoutchouc (pour racler les plats avec facilité)
-   1 rouleau de papier cuisson
-   1 pair de ciseaux
-   2 cuillères à soupe
-   4 fourchettes
-   4 spatules en bois
-   1 mixeur
-   2 maniques
-   1 tamis (pour la farine)
-   2 fouets de cuisine
-   2 grands saladiers
-   4 moules à cake identiques (qui rentrent dans les susdits fours)
-   36 assiettes
-   36 tasses
-   36 cuilleres
-   36 morceaux sucres
-   4 boilers électriques
-   400 g de thé noir
-   1 litre de lait
-   3 ordinateurs
-   1 imprimante laser
-   1000 feuilles A4
-   1 projecteur avec ses cables de connection
-   2 grandes tables stables
-   2 prises électriques
-   1 rallonge
-   1 triplette
-   1 planche à découpe
-   1 cutter bien affuté
-   1 agrapheuse
-   1 bout de carton

