Title: Pancakes & Python
Date: 2007-06-18 23:42
Author: Femke
Tags: Live, Print Party, Printing + Publishing
Slug: pancakes-python
Status: published

[  
![p1020758.JPG]({filename}/images/uploads/p1020758.JPG)
![p1020810.JPG]({filename}/images/uploads/p1020810.JPG)  
![p1020849.JPG]({filename}/images/uploads/p1020849.JPG)
![breakfast.jpg]({filename}/images/uploads/breakfast.jpg)  
![p1020818.JPG]({filename}/images/uploads/p1020818.JPG)
![p1020838.JPG]({filename}/images/uploads/p1020838.JPG)  
](http://gallery.constantvzw.org/main.php?g2_itemId=15696)

Impressions from **Canadian Printing Breakfast** in Nepomuk Bar,
Brussels. More images in [Constants Image
Repository](http://gallery.constantvzw.org/main.php?g2_itemId=15696).

<small>Special thanks: An, Wendy & Pierre (serving), Ivan (Python),
Nurse (records), Peter (pictures), Nicolas (printing), Veronique (City
Min(e)d).</small>
