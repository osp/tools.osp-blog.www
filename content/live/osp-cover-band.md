Title: Print party: OSP Cover Band
Date: 2008-05-05 20:09
Author: OSP
Tags: Live, News, LGM 2008, Music, Print Party
Slug: osp-cover-band
Status: published

We are [preparing](http://ospublish.constantvzw.org/?p=443) our print
party for tomorrow... Introducing ***Not Courier Sans*** (a re-take
font), serving ***Original Covers*** (a playlist specially prepared for
you) plus homemade [***jiaozi, ravioli and/or
pierogi***](http://ospublish.constantvzw.org/recipes/jiaozi_ravioli_pierogi.htm).
Join us at 19:00 in [Café
Mleczarnia](http://www.mleczarnia.wroclaw.pl/), Wroclaw (Poland)!  
[![]({filename}/images/uploads/jiaozi-extract.jpg "jiaozi-extract"){: .alignnone .size-full .wp-image-469 width="400" height="94"}]({filename}/images/uploads/jiaozi1.jpg)
