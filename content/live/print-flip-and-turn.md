Title: Print, flip, and turn
Date: 2008-06-29 15:11
Author: Ivan
Tags: Live, Works, Print Party, Python, Recipe, Scribus
Slug: print-flip-and-turn
Status: published

At the Polish print party, we tried a possible automation of the
[nineteen steps](http://ospublish.constantvzw.org/?p=90). We gave out
numbered tickets at the entrance, and at the end there was a numbered
copy of the booklet for everyone. The booklets were all different via
Python and Scribus. The imposition and printing was very simple Perl.

If you just want to print a number of copies of the same booklet, grab
the modified versions of the
[scripts](http://pzwart2.wdka.hro.nl/~ilopez/print_flip_turn.tar.gz),
and skip step 1. The `$people` variable is the number of copies to be
printed, and the `$pubName` variable is the name of your pdf file, minus
.pdf extension ;)

So and then:

1.  Create a .sla file by following 1 to 7 of the nineteen steps. For
    the importing to pdf, use
    [layout.py]({filename}/images/uploads/layout.py),
    or better yet write your own script for making all the pdf documents
    different :)
2.  Steps 9-13 are now handled by the
    [printprintprint.pl]({filename}/images/uploads/printprintprint.pl) script.
    This is imposition and the printing of even pages.
3.  When the printing is done, take the warm stack of paper and \`flip
    and turn' it so that your printer knows that it's upside down.
4.  Finally, the printing of the odd pages is handled by
    [flip\_and\_turn.pl]({filename}/images/uploads/flip_and_turn.pl)  
   `~% perl flip_and_turn.pl`

What
[this](http://ospublish.constantvzw.org/image/?level=picture&id=765)
looks like.
