Title: Dirty variables workshop
Date: 2018-03-27 17:00 
Author: OSP
Tags: Live
Status: published

Dirty variables workshop is about variable fonts with some distance and manual interpolation (with stroke fonts inside), with La Cambre master students in type media, Brussels, on the 26 + 27 March 2018.

We are using Fonttools and the ttx format, which is a clear xml dump of every table present in a (variable) font. See [github.com/fonttools/fonttools](https://github.com/fonttools/fonttools).

The custom made inspector web tool displays the content of ttx files, including all deltas. The repository is on [http://gitlab.constantvzw.org/osp/workshop.dirty-variables](http://gitlab.constantvzw.org/osp/workshop.dirty-variables)

All the documentation we gathered before and during the workshop follows, in a quite rough way!

![Inspector showing deltas](https://gitlab.constantvzw.org/osp/workshop.dirty-variables/raw/b0a89a0c9fb8de2267f7fe975eeaff5744ab8d52/iceberg/Screenshot%20from%202018-03-26%2016-51-04.png "Inspector showing deltas")

## Variables fonts past and present

OpenType Font Variations is the new feature of the OpenType Font Format specification version 1.8. The first public announcement of this new version happened in Warsaw during ATypI 2016.

To be a bit more clear on what and who :

OpenType format was developped by Microsoft, Apple and Adobe since 1996, to stop the war between Postscript (Adobe) and Truetype (Microsoft + Apple) formats. Because of their need for emoji fonts, Google join the group more recently. Apple and Microsoft share the same concern.

«The new OpenType format allows for a single file to contain an entire family, or multiple related families, of font instances. 
Previously if you wanted to license an entire font family with a range of weights for your website (like Light, Regular, Medium, Bold, Extra Bold, and Ultra) the type foundry would generate a separate font file for each style. But now it's possible for type foundries to put all of these styles into a single file.» 
— from [cjtype.com/dunbar/variablefonts](http://cjtype.com/dunbar/variablefonts), an article on Dunbar “n”, a single glyph font 'n' as the "first" var font !

[The "historical moment" by Roxane Gataud on Twitter @RoxaneGataud at Studio Dumbar with @kupfers @youknowcj @tassiana_costa #variablefonts"](https://twitter.com/RoxaneGataud/status/777209352025407488)

Variable fonts are also called “responsive typographies”.

For the moment, this font format is mostly working on the web. Compatble web browsers are listed on [https://caniuse.com/#feat=variable-fonts](https://caniuse.com/#feat=variable-fonts)

For the print world :

* [Adobe Photoshop and Illustrator now support Variable Fonts and the user can find the controls in the font panel by pressing the Variable Font button next to the Font Style menu](https://blog.typekit.com/2017/10/19/new-variable-fonts-from-adobe-originals/)

* Colorfont support in Photoshop - link to find one day

* [A recent announcement from Inkscape](https://t.co/2bvR3uMGuU - https://twitter.com/axis_praxis/status/976112486209421313?s=09)

* [You can buy The Zeitung Flex InDesign extension by Underware for Adobe suite, to play with Zeitung Flex variations](http://www.underware.nl/fonts/zeitung/features/Flex/ → https://twitter.com/variablefonts/status/799150169791102976)

![Variable geometrics with bodies](https://gitlab.constantvzw.org/osp/workshop.dirty-variables/raw/673b2cbe180d11473460895eab97ebd16090c662/iceberg/game-20180326_134231.jpg "Variable geometrics with bodies")

##  Design spaces, axis, tables?

To dive into the variable fonts also brings us to revisit the different spaces and tables that are around and contained in a font.

In the mid-nineties, Adobe has proposed Multiple Masters fonts, an interpolation system to generate fonts from two masters embeded into the font itself.
In the same time, Apple introduce a more refined but similar system called Apple Advanced Typography (AAT).
Both system proves the technical feasability but were not largely used by designers.
[https://en.wikipedia.org/wiki/Multiple_master_fonts - https://en.wikipedia.org/wiki/Apple_Advanced_Typography](https://en.wikipedia.org/wiki/Multiple_master_fonts - https://en.wikipedia.org/wiki/Apple_Advanced_Typography)

Designers have not catch up these tools, but type designers begon to use the same kind of tools, but externally of the font, in the font editors* mainly. The designation “design space” one of way to name it. This space gathers or deploys the informations describing the relations between the masters, the sources of a font. It defines for exemples the relations between the light and the bold version of a same font. A design space store all the datas needed for variable font interpolations. It it was is not into the font, meaning it is not about the drawing of the font, the meta infos... 

We could say that now with Open Type 1.8 specification, this design space become more concretely embeded into the font and not depending on extrapolation tools. The design space is called variation space in the specification and it is the range of variability in the variable font.

The design space is defined by 

* axes (names and dimensions of the axes)

* sources (links to the ufo, otf, ttf...)

* instances (the steps you wants to generate)

* rules (sketche on how conditional stuff could work)

And those element are tidy into tables :

* glyph - Glyph shapes → table containing the shape data for the glyphs [https://docs.microsoft.com/en-us/typography/opentype/spec/glyf](https://docs.microsoft.com/en-us/typography/opentype/spec/glyf)

* gvar -Glyph Variations→ table containg the delta's of the glyphs [https://docs.microsoft.com/en-us/typography/opentype/spec/gvar](https://docs.microsoft.com/en-us/typography/opentype/spec/gvar)

* fvar - Font Variations → table desribing the variation axes of the font, and describing instances (pre-set values per axis) - [https://docs.microsoft.com/en-us/typography/opentype/spec/fvar](https://docs.microsoft.com/en-us/typography/opentype/spec/fvar)

* avar - axis variations → table allowing to modify how a variation behaves, mainly the 'speed', it can become non-linear - [https://docs.microsoft.com/en-us/typography/opentype/spec/avar](https://docs.microsoft.com/en-us/typography/opentype/spec/avar)

* cvar - CVT (control value table) variations [https://docs.microsoft.com/en-us/typography/opentype/spec/cvar](https://docs.microsoft.com/en-us/typography/opentype/spec/cvar)

* stat - style attributes → ? [https://docs.microsoft.com/en-us/typography/opentype/spec/stat](https://docs.microsoft.com/en-us/typography/opentype/spec/stat)

Axis names have to be a string, that can't be longer than 4 letters. Axis tags are predefined axes. 'wght': weight, 'wdth': width, 'ital': italic, 'slnt': slant, 'opsz': optical size.

See a talk by [Erik van Blokland at TYPO Labs 2017 about designspaces](https://www.youtube.com/watch?v=3RRoIYeJ3YQ)

![How to plot an A, skeleton versus contour, projection](https://gitlab.constantvzw.org/osp/workshop.dirty-variables/raw/673b2cbe180d11473460895eab97ebd16090c662/iceberg/a-lola-20180326_151522_HDR.jpg "How to plot an A, skeleton versus contour, projection")

##  More complete introductions

To ensure a good start of the technology (everyone was remembering the failed attempt of MM and AAT...), the four giants agreed to announce it in one go, along with external commenters

* [Introducing OpenType Variable Fonts, John Hudson (W3C), Sep 14, 2016](https://medium.com/@tiro/https-medium-com-tiro-introducing-opentype-variable-fonts-12ba6cd2369)

* [Adobe annoucement](https://blog.typekit.com/2016/09/14/variable-fonts-a-new-kind-of-font-for-flexible-design/)

* [Microsoft annoucement](https://docs.microsoft.com/en-us/typography/opentype/font-variations)

* [Google annoucement](https://opensource.googleblog.com/2016/09/introducing-opentype-font-variations.html)

* Apple annoucement (is there an Apple annoucement?)

* [From TrueType GX to Variable Fonts, Tom Rickner, Head of Tools at Monotype - 29. 11. 2016](https://www.monotype.com/resources/articles/part-1-from-truetype-gx-to-variable-fonts/)

* [Why Variable Fonts Will Succeed, Thomas Phinney (FontLab, ATypI) - 27. 01. 2018](http://www.thomasphinney.com/2018/01/why-variable-fonts-will-succeed/)

* [OpenType variable fonts discussion on TypeDrawers](http://typedrawers.com) - search for "[otvar]"

##  Documentation

* The very clear Microsoft documentation of the OpenType® specification is on [https://docs.microsoft.com/en-us/typography/opentype/spec/font-file#otttables](https://docs.microsoft.com/en-us/typography/opentype/spec/font-file#otttables)

* More general, a blog post with some details - [https://simoncozens.github.io/fonts-and-layout/opentype.html#opentype-font-variations](https://simoncozens.github.io/fonts-and-layout/opentype.html#opentype-font-variations)

* And 99 articles from 1991 (!) to 2018 on [https://www.axis-praxis.org/resources](https://www.axis-praxis.org/resources)

![At work, in the tower](https://gitlab.constantvzw.org/osp/workshop.dirty-variables/raw/673b2cbe180d11473460895eab97ebd16090c662/iceberg/group-IMG_20180327_103245.jpg "At work, in the tower")

## (All-male) actors

Here are some profiles to follow that document and follow all the rebounds of the variables

* Laurence Penney (Bristol, UK)
    * Axis-praxis [https://www.axis-praxis.org](https://www.axis-praxis.org)
    * [https://twitter.com/axis_praxis](https://twitter.com/axis_praxis)
    * [https://www.youtube.com/watch?v=16QIZrRxafY](https://www.youtube.com/watch?v=16QIZrRxafY)

* Nick Sherman (New York, USA) - At the occasion of Robothon 2018 (8–9 March), Nick launched [https://v-fonts.com](https://v-fonts.com), a website collecting and displaying variables fonts. On twitter Nick is on [https://twitter.com/NickSherman](https://twitter.com/NickSherman) and [https://twitter.com/variablefonts](https://twitter.com/variablefonts)

* Erik van Blokland from Letterror, part of the OpenType 1.8 comitee

* Just Van Rossum from Letterror, (probably) the creator of ttx, neffew of Guido van Rossum, programmer who creates Python

* Thomas Phinney, font and typography expert and consultant. In his day job, Thomas is President of FontLab, the font creation/editing software company. He is also treasurer of ATypI, the international typographic association. From 1997-2008 he did type at Adobe, lastly as product manager for fonts and global typography. After that he spent five years as senior technical product manager (a.k.a. “guru”) of fonts and typography at Extensis, including managing the font library for the WebINK web font solution.  His typeface Hypatia Sans is an Adobe Original (with help from Robert Slimbach, Miguel Sousa and Paul Hunt). His latest typeface is the Kickstarter-funded Cristoforo.

* John Hudson of Tiro Typeworks, design wonderful top-of-the-line fonts in Vancouver. Tiro is increasingly involved in font technologies, and are avid advertisers for OpenType and work often with Microsoft and Linotype on projects. John has created or collaborated on typefaces for many writing systems. He is an expert contributor to Unicode, and a member of the W3C Web Fonts Working Group.

* Adam Twardoch and Yuri Yarmola : programmers of FontLab

* Georg Seifert and Rainer Erich Scheichelbauer : programmers of Glyphs

(Yes, Twitter remains a good node for typographic discussions)

## Some critical approaches

* [Le point de vue de Jean-Baptiste Levée aux TYPO Labs 2017](https://www.youtube.com/watch?v=0V-AeI4yaQQ)

* [Boring font is the title of a conference Indra Kupferschmid recently gave at typemedia.org/robothon2018 - “Warning: type family regularization (that interpolatable outlines encourage) is BORING!”](http://kupferschrift.de)

* ["Of course there are skeptics and believers. A very interesting statement from Indra Kupferschmid in her talk during this year’s Robothon (2018) was when she warned type designers of the sometimes boring results of this new technology. I’m one of the believers and in this article I’ll try to explain why" - Irene Vlachou] (https://www.type-together.com/index.php?action=portal/viewContent&cntId_content=3614)

* [Causticity from "the department of silly demos"](https://pixelambacht.nl/2017/variable-hover-effects/)

## Usage exemples

* A variable font that responds to sound on [goertek.kontrapunkt.com/online/](https://goertek.kontrapunkt.com/online/)

* Web use collection of [Mandy Michael codepen.io/collection/XqRLMb/#](https://codepen.io/collection/XqRLMb/#)

* Fake on [website Dumbar studiodumbar.com](https://studiodumbar.com/) and on [github.com/RoelN/Color-Variable-Emoji](https://github.com/RoelN/Color-Variable-Emoji)

![Inspector showing deltas](https://gitlab.constantvzw.org/osp/workshop.dirty-variables/raw/b0a89a0c9fb8de2267f7fe975eeaff5744ab8d52/iceberg/Screenshot%20from%202018-03-27%2014-34-41.png "Inspector showing deltas")

## Toolkit

* Fonttools
   * [github.com/fonttools/fonttools](https://github.com/fonttools/fonttools)
   * [groups.google.com/forum/#!forum/fonttools](https://groups.google.com/forum/#!forum/fonttools)
   * [On Mac, where every install is a bit more complicated, install Fonttools with brew on terminal](https://brewinstall.org/install-fonttools-on-mac-with-brew)

* PyGObject
   * [pygobject.readthedocs.io/en/latest](https://pygobject.readthedocs.io/en/latest/)
   * To install it, you'll also need libgirepository1.0-dev and libcairo-dev

* Fontforge

* Non-free : Fontlab, Robofog, Robofont, and Glyphs.

![When it's working!](https://gitlab.constantvzw.org/osp/workshop.dirty-variables/raw/673b2cbe180d11473460895eab97ebd16090c662/iceberg/hop.gif "When it's working!")