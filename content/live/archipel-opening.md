Title: Archipel Opening
Date: 2010-12-01 22:43
Author: ludi
Tags: Live, News, Works, 3D, Handmade, Music
Slug: archipel-opening
Status: published

![]({filename}/images/uploads/archipel-open-01.jpg "archipel-open-01"){: .aligncenter .size-medium .wp-image-5357 }

[Archipel](http://www.lamediatheque.be/dec/archipel/index.php) is both a
permanent collection, a website and a travelling exhibition. It intends
to propose an intuitive exploration of adventurous music and images
appeared since the early twentieth century.  
Archipel has chosen to group these discs and films as "islands"
involving artists (or works) that share similar creative practices.  
An archipel that can be visited, where one can get lost without being an
expert or already initiated to art known as "experimental".

A project by the Médiathèque de la Communauté française de Belgique.  
Graphic identity - Harrisson OSP  
Website through SVG interface - [Michael
Murtaugh](http://www.automatist.org/)  
Installation, furniture and plans - [Mathieu
Gabiot](http://www.mathieu-g.be/)

Discover the third dimension of Open Source Publishing.  
The furniture pieces will be released under a Free Art-based license.

Wednesday December the 1st :
[www.archipels.be](http://81.246.38.119/media/archipel/pub/) online (web
site preview)  
**Thursday December the 2nd - 18:30 - 21:30** : Installation Opening  
+ Baudouin Oosterlynck concert

![]({filename}/images/uploads/P1140353m.jpg "P1140353m"){: .aligncenter .size-medium .wp-image-5357 }

Passage 44  
Boulevard du Jardin Botanique, 44  
1000 Bruxelles
