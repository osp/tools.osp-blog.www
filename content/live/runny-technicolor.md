Title: Runny technicolor
Date: 2009-05-28 00:55
Author: ludi
Tags: Live, News, Collaborative, Colors, LGM 2009, Standards + Formats
Slug: runny-technicolor
Status: published

![Dairy
Queen]({filename}/images/uploads/dsc00006.jpg "dsc00006"){: .size-medium .wp-image-2789 }

Dans le cadre du festival ["Imaginary
Property"](http://www.nova-cinema.org/index.php?page=prog/112/cover.en.htm),
OSP vous invite au [Nova](http://www.nova-cinema.org/)le jeudi 28 mai
2009 à 19 h. Au programme : le jaune interdit et le rouge bientôt libre,
un décodage de morceaux de vie montréalais au [LGM
2009](http://www.libregraphicsmeeting.org/2009/), une projection vers
des formats inconnus et un aperçu de montage vidéo en mode texte wiki
party avec hacking d'[Active Archives](http://activearchives.org).

As a contribution to the [Nova](http://www.nova-cinema.org/) festival
[’Imaginary
Property’](http://www.nova-cinema.org/index.php?page=prog/112/cover.en.htm),
OSP presents a selection of cutting video wiki party, real life [LGM
2009 Montreal](http://www.libregraphicsmeeting.org/2009/), instructional
videos, live-drawing-from-far, interviews, color and colour clips,
digital slide shows and other moving material for the screen. While
serving Canadian cocktails, they’ll browse through some or their
adventures in free software, open content licensing and collaborative
work.
