Title: What’s the thinking here?
Date: 2008-05-30 17:00
Author: OSP
Tags: Conversations, Texts
Slug: what%e2%80%99s-the-thinking-here
Status: published

Matthew Fuller: *One of the things that is notable about OSP is that the
problems that you encounter are also described, appearing on your blog.
This is something unusual for a company attempting to produce the
impression of an efficient ’solution’. Obviously the readers of the blog
only get a formatted version of this, as a performed work? What’s the
thinking here?*

Read the interview:
[http://www.spc.org/fuller/interviews/open-source-publishing-interview](http://www.spc.org/fuller/interviews/open-source-publishing-interview-with-femke-snelting/)

<small>Matthew Fuller writes about software culture and has a contagious
interest in technologies that exceed easy fit solutions. He is David Gee
reader in Digital Media at the Centre for Cultural Studies, Goldsmiths
College, University of London, edited *Software Studies, A Lexicon* (MIT
Press, 2007), wrote *Media Ecologies: Materialist Energies in Art and
Technoculture* (MIT Press, 2005) and *Behind the Blip: Essays on the
Culture of Software*.</small>

<small>Many of his essays are available on line:
[http://www.spc.org/fuller](http://www.spc.org/fuller/)</small>
