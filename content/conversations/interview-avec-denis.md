Title: Interview avec Denis
Date: 2008-09-18 19:00
Author: ludi
Tags: Conversations, Type, Fontforge, LGM 2008, Libre Fonts
Slug: interview-avec-denis
Status: published

L'ATypI 08 est en route à St Petersburg. [Denis Moyogo
Jacquerye](http://moyogo.blogspot.com/), co-leader du projet DejaVu [y a
présenté ce
matin](http://atypi.org/05_Petersburg/20_main_program/view_presentation_html?presentid=519)
le projet sur lequel il travaille actuellement.

C'est l'occasion pour nous de publier une interview OSP de Denis,
rencontré en mai dernier aux Libre Graphics Meeting 2008 de Wrocław.

\[caption id="attachment\_916" align="alignnone" width="300"
caption="OSP avec Denis et Dave Crossland à FOSDEM 2008,
Bruxelles"\][![Rencontre avec Denis et Dave Crossland à
Fosdem]({filename}/images/uploads/picture-16.png "OSP avec Denis et Dave Crossland à FOSDEM 2008, Bruxelles"){: .size-medium .wp-image-916 }]({filename}/images/uploads/picture-16.png)\[/caption\]

<!--more-->

**Ludi :** Hello Denis. Peux-tu nous raconter comment tu es entré dans
le projet DejaVu?

**Denis :** \[...manquant...\] une des langues du Congo, et j'ai
commencé à m'intéresser à savoir comment est-ce qu'on l'écrivait. Je me
suis rendu compte qu'il y avait un orthographe avec des caractères
spéciaux et des accents, qui sont plus ou moins les mêmes que ceux
utilisés pour l'alphabet phonétique. Donc un jour j'ai décidé de me
mettre au travail. J'ai regardé les polices qu'il était possible
d'améliorer. Pour DejaVu il y avait déjà un certain nombre de gens qui
avait l'air assez actifs, donc j'ai téléchargé les sources, j'ai ouvert
FontForge et j'ai commencé à chipoter toute une après-midi et puis le
lendemain j'ai envoyé un patch. On m'a dit « Oui c'est bien, mais il
faudra aussi dessiner la version bold, l'italic et le bold italic »
(rires) Donc, comme c'était les vacances, j'ai passé toute la semaine à
faire ça. J'ai envoyé le patch et puis c'est entré dans les sources, les
gens ont pu l'avoir un mois après, dans la distribution. Puis j'ai
commencé à ajouter des choses nécessaires pour d'autres langues. J'ai
commencé à corriger un peu ce que j'avais fait. De fil en aiguille, je
suis plus ou moins resté très actif dans le projet. Et un jour, le
leader principal du projet a annoncé qu'il ne pouvait plus s'en occuper
à plein temps...

**Pierre :** Du projet DejaVu?

**Denis :** Oui. Il a demandé si les gens étaient prêts à reprendre sa
place. Personne n'a vraiment répondu. Donc une semaine après il a dit «
Bon eh bien, les trois personnes les plus actives sont nommées d'office.
Et c'était Ben Laenen et moi et une troisième personne pour maintenir la
liste de courriels.

**Pierre :** Et le projet DejaVu, son histoire avant toi?

**Denis :** Le projet DejaVu, à la base c'est lorsque Bitstream a vendu
Vera au projet Gnome. Ils ont fait une licence libre.

**Pierre :** Est ce qu'ils en ont tiré du cash?

**Denis :** Oui. Donc plusieurs personnes, un peu chacune de leur côté
ont commencé à compléter cette police de caractère, en rajoutant les
deux trois caractères qui manquaient pour leur langue. Il y avait plus
ou moins une quinzaine de versions différentes. Štěpán Roh a mis tout ça
ensemble et a créé le projet DejaVu. C'est comme ça que c'est devenu le
plus gros projet de police dérivée de Bitstream.

**Pierre :** Vous avez encore des contacts avec Bitstream?

**Denis :** Non. On a pas de contacts directs. Il y a plus ou moins un
an, des gens de la fondation Gnome avaient pris contact avec eux pour
essayer de voir si Bitstream serait intéressé de rajouter d'autres
caractères et donc de continuer à améliorer les polices mais pour
l'instant...  
Ils seraient prêts à le faire s'il y avait différents payements.

**Ludi :** À chaque fois, il y a eu des ajouts de glyphes. Mais le
dessin de base, reste inchangé par rapport à la Vera ou il a été
modifié?

**Denis :** Oui. Le dessin de base est inchangé. Les seules choses qui
ont peut être changé c'est quelques bugs, un ou deux glyphes qui
n'étaient pas bons, enfin pas exactement ce que les gens s'attendaient à
avoir dans leur langue. On a aussi modifié deux trois choses dans
l'espacement de certains caractères. En dehors de ça c'est exactement la
même chose. Pour tous les caractères qu'on a créé ou rajouté par
nous-même, soit on est parti des caractères de Vera ― par exemple pour
certaines langues africaines, il y a beaucoup de caractères avec des
crochets en plus, un b avec un crochet au dessus ou un d avec un crochet
― donc là c'est assez simple, il suffit de prendre le b et de lui
ajouter un crochet.  
Aussi, la police Bitstream Vera a été optimisée pour la lecture à
l'écran. Donc chaque caractère a un petit programme, un petit logiciel,
le hinting qui permet de rentrer le caractère dans les pixels selon la
taille, donc ça aussi on est en train de l'ajouter pour les caractères
qu'on a fait.

**Pierre :** Tu as fait une après-midi sur le regular mais lequel? La
version sans empattements, la version serif?

**Denis :** Sans serif.

**Pierre :** Et donc on t'a demandé la version bold, italic et bold
italic, on te les a demandé aussi pour la serif et la mono?

**Denis :** Non. Ça c'est moi par après. Parce qu'en fait je comptais
faire toutes les versions, c'est juste que j'ai commencé par la sans
serif.

**Ludi :** Actuellement ce sont des TrueType, des OpenType?

Denis ; On génère des fichiers .ttf mais avec des tables OpenType. On a
des ligatures, on a les positionnements de caractères et d'accents.

**Pierre :** Vous vous conformé le plus possible à l'Unicode? Ou
entièrement?

**Denis :** On suit le standard Unicode donc on évite de mettre des
caractères dans des glyphes non spécifiés. On en a quelques-uns mais
c'est juste parce qu'on sait qu'ils vont bientôt être implémentés dans
Unicode.

**Pierre :** Oui, peut-être. On peut poser la question dans l'autre sens
: Est-ce qu'Unicode vous suffit?

**Denis :** Oui, il y a des gens qui ont proposé d'ajouter des
caractères qui n'étaient pas encore dans Unicode et qui ne sont pas
encore planifiés. Par exemple les caractères médiévaux pour lesquels il
y a un standard parallèle à Unicode qui a des caractères en plus et donc
ça c'est dans le PUA (Private Usage Agreement).

**Pierre :** Mais qui peuvent être élus potentiellement pour être
ensuite intégrés à Unicode?

**Denis :** Oui.

**Ludi :** Comment ça se passe? Vous avez beaucoup de participants? Il y
a beaucoup de demandes?

**Denis :** On a plus ou moins 5 personnes qui sont très actives. Et
puis 5 autres qui aident de temps en temps.

**Ludi :** Peut-être que ça va augmenter après ces rencontres. \[NDLR :
à Wroklaw\]

**Denis :** Oui, j'espère aussi.

**Pierre :** Tu avais un intérêt dans la typographie au départ en tant
que développeur et linguiste?

**Denis :** Moi, au départ l'intérêt est juste venu par le besoin.
J'avais besoin d'une police de caractères. Donc j'ai commencé à regarder
comment la faire et je me suis rendu compte que c'était assez compliqué
et assez intéressant aussi.

**Ludi :** Pour quelle langue tu en avais besoin?

**Denis :** Le lingala et aussi la phonétique.

**Pierre :** Phonétique, ça existait déjà, non?

**Denis :** Oui mais c'est juste parce que en fait... à la base quand je
lisait les pages web, ça utilisait la police de caractère par défaut,
donc Bitstream et les caractères phonétiques étaient substitués d'une
autre police de caractères. C'était pas uniforme. Pour moi c'est juste
plus simple si tout est dans une seule police de caractères, avec le
même style. C'est juste plus agréable à lire.

**Ludi :** Et maintenant, est-ce qu'en dehors du projet DejaVu, tu t'es
mis à dessiner d'autres fontes?

**Denis :** Oui. J'avais aussi commencé à ajouter des caractères au
projet Free Fonts. J'ai ajouté pas mal de caractères à Nimbus. Mais là
depuis un an, on a commencé à planifier un projet, avec le CRDI (Centre
de Recherches et de Développement International). C'est l'aide au
développement canadienne. Ils veulent financer des projets de
localisation de logiciels informatiques en langues africaines. Et donc,
une partie du projet, c'est de produire plus de polices de caractères
qui supportent les langues africaines. Là, je vais partir de plusieurs
polices de caractères et ajouter les caractères africains.

**Pierre :** Peut-être pour être plus précis : DejaVu a un axe très
clair en terme typographique, de rester sur les 3 polices de base de
Vera?

**Denis :** Oui.

**Pierre :** Donc l'extension se fait dans le sens de l'ajout de glyphes
des tables Unicode mais pas dans le sens de rajouts typographiques.

**Denis :** Non. On a les 3 familles : sans, serif et sans mono. Si
quelqu'un veut rajouter un caractère dans une police, il peut juste le
faire dans Sans par exemple, mais s'il le fait pour DejaVu Sans il doit
aussi le faire pour Sans Bold, Sans Italic et Sans Bold Italic. C'est le
minimum demandé. Le problème est que certains systèmes d'écriture n'ont
pas la notion d'italique. L'écriture arabe par exemple a juste le
regular et le gras. Aussi, certaines écritures qui n'ont pas ces notions
de sans et de serif. Mais ils ont peut être un style plus traditionnel
et un style plus coupé.

**Pierre :** Oui. Quels sont les rapports avec le modèle typographique
occidental dominant et les modèles d'écritures dont certains doivent...
Moi je connais assez mal, même quasi rien des scripts africains, je vois
un petit peu l'ancien éthiopien, des choses qui... strictement rien à
voir.

**Ludi :** Par rapport aux participants, est ce qu'il y a quelqu'un qui
contrôle ce qui est fait, qui valide?

**Denis :** Il y a des gens qui ont déjà accès aux sources, pour les
autres c'est assez simple, il suffit de soumettre les modifications à la
liste public, et donc généralement on jette un œil dessus, on voit si
c'est correct ou pas, on donne un commentaire ou on dit directement que
c'est bon et on l'inclut. Une fois que la personne a fait ça quatre ou
cinq fois sans trop de problèmes, on lui donne accès directement aux
sources.

**Nicolas :** Donc oui il y a un petit temps de test.

**Denis :** Oui parce que généralement, enfin moi par exemple, chaque
fois que j'envoyais un patch au début il y avait toujours des erreurs...

**Pierre :** Vous donnez aussi une espèce de mini formation? Par mail
ou...?

**Denis :** Oui, par mail. Les gens posent des questions soit par mail
ou bien sur IRC.

**Ludi :** C'est des gens qui viennent d'où par exemple?

**Denis :** D'un peu partout : États-Unis, Europe, Russie.

**Harrisson :** Est ce qu'il y a des chinois?

**Denis :** Dans le projet, non. On a un français qui a des origines du
Laos. C'est lui qui a ajouté le laotien dans la DejaVu Sans.

**Harrisson :** Y a-t-il des glyphes chinois dans DejaVu?

**Denis :** Non. En fait on a très peur du chinois parce que c'est quand
même beaucoup de caractères.

**OSP :** (rires)

**Yi :** Faut pas avoir peur.

**Denis :** Mais on a déjà eu pas mal de gens qui demandaient pour qu'on
puisse avoir les caractères chinois. Moi personnellement je serais prêt
à le faire mais il me faudrait le temps et les connaissances surtout.
Dans notre équipe actuellement il n'y a personne avec les connaissances
nécessaires pour pouvoir dessiner les caractères.

**Nicolas :** La question qui se pose pour moi quand je vois l'étendu de
ces fontes c'est : quel sens ça a de garder une sorte d'identité de la
fonte sur tous ces langages? Comment est-ce qu'on peut dire que ça reste
une Courier, je sais pas, une Vera Sans... en arabe et en chinois aussi?
Est-ce-qu'il y a des guidelines, quelque chose qui permet de...

**Denis :** Ce qu'on essaye de faire, si par exemple on écrit un texte
bilingue, c'est que ça s'affiche correctement et qu'il n'y ait pas de
différences de contraste dans le gris des textes.

**Pierre :** Oui, par comparaison, par juxtaposition.

**Denis :** Mais parfois on se rend compte que c'est quasiment
impossible. Par exemple il y a des systèmes d'écriture qui peuvent avoir
des lignes assez courtes et il y en a qui ont vraiment besoin d'espace,
qui ont certains caractères qui montent très haut ou descendent très bas
ou qui ont plusieurs diacritiques au dessus. C'est assez complexe parce
qu'en plus Bitstream Vera à la base est faite pour être lue à petites
tailles et donc par économies ils sauvent beaucoup d'espace entre les
lignes. En arabe on a du raccourcir les caractères en bas. C'est un
problème qu'on aimerait résoudre parce que c'est assez important pour la
lecture.

**Ludi :** C'est pas possible d'encoder à l'intérieur d'une même fonte,
des hauteurs différentes?

**Denis :** Si c'est possible. OpenType définit une table base qui
permet de définir ça, même par langues. Par exemple le latin en français
avec une certaine hauteur de ligne et le latin en vietnamien avec une
hauteur de ligne différente.

**Pierre :** Mais la difficulté c'est quand tu mélanges les deux. Tu
dois quand même trouver une certaine harmonie. Par exemple j'ai un petit
peu composé de l'arabe et c'est chaque fois «wao!»... Ça ne marche pas
quoi.

**Denis :** Personnellement si c'était à refaire je crois que je me
limiterais simplement au latin au cyrillique et au grec, et peut-être
les autres systèmes d'écriture qui sont compatible.

**Pierre :** Oui, un critère stylistique.

**Denis :** Parce que ce sont des systèmes qui supportent vraiment tout
Unicode. Pour beaucoup de systèmes d'écriture c'est vraiment difficile
de trouver le juste milieu.

**Pierre :** Donc il y a quand même beaucoup de critères typographiques.
Est-ce que sur la liste s'il y a un caractère en éthiopien qui apparaît,
est-ce qu'il y a des discussions sur des critères typographiques, genre
«Non là vraiment le contraste est trop faible, ici l'empattement est
décalé»?

**Denis :** Non, malheureusement. On en parle beaucoup sur IRC mais il
n'y pas d'archives là dessus. Parce que des fois on est assez méchants.
(rires OSP) On est là «Mais c'est quoi ce truc? Ah mais c'est moi qui
l'ai fait? Non.» Mais c'est vrai que parfois, quand je regarde certains
caractères, je vois que certaines courbes ne sont pas belles, que le
contraste est faux à certains endroits. Il faudrait qu'on passe plus de
temps aussi pour l'espacement.

**Pierre :** Oui c'est le parent pauvre. C'est le plus laborieux.

**Denis :** Oui, parce qu'au début on se dit qu'il faut juste dessiner
les caractères puis après on commence à se documenter un peu et on se
rend compte du travail que c'est.

Pierre: 50% du travail, non?

**Denis :** Ce qui est intéressant aussi ce sont les technologies
avancées avec OpenType.  
À partir d'un moment on peut avoir un caractère différent selon le
contexte, selon la langue.  
Depuis que nous sommes devenu une police par défaut sur différentes
distributions Linux, on peut commencer à pousser un peu la technologie
parce que il y a beaucoup de choses qui ne sont pas supportées, parce
qu'il n'y a pas de polices de caractères qui le font en fait. Là, comme
nous on peut dire, voilà, nous on a une police qui intègre ces
spécificités et ça serait bien si l'utilisateur pouvait y avoir accès.
Ce qu'on a commence à faire c'était le positionnement et la substitution
de caractères pour le latin, le cyrillique et le grec, donc ça c'était
partiellement supporté avec Pango par exemple. Et donc depuis qu'on s'y
est intéressé ça a un peu fait bouger les choses. Il y aussi la
substitution selon la langue. Là on a mis un peu la pression pour que ça
soit implémenté. Maintenant on va commencer à se mettre au système où on
peut avoir différentes hauteur de ligne selon le système d'écriture ou
la langue. Aussi, avec les noms de polices de caractères. Parce que dans
les vieux systèmes informatiques il y avait juste une police de
caractères avec 4 styles : normal, gras, italique et gras italique,
ensuite OpenType est arrivé et a ajouté tout un système pour avoir une
dizaine de gras différents et différentes chasses. On a ajouté ça et ça
a créé pas mal de bugs dans pas mal d'applications. Par exemple
OpenOffice substituait le condensé au lieu de prendre le regular et donc
il y des gens qui se plaignaient chez nous. On leur disait «C'est pas
notre faute, nous on suit les spécifications Opentype».

**Pierre :** Vous produisez aussi des largeurs, des chasses différentes?

**Denis :** Oui ça c'est parce que quelqu'un a proposé l'idée et donc on
a fait un petit hack rapide ou c'est automatiquement généré à partir
d'une version régulière. Une réduction de 90% ça passe encore bien à
l'œil. Personnellement, je préférerais si c'était fait manuellement à
partir des dessins originaux mais...

**Harrisson :** Oui, le condensé est particulièrement difficile.

**Pierre :** il y a aussi des systèmes semi-automatiques qui condensent
en gardant le contraste, en ne faisant pas un stretch.

**Denis :** Oui, FontForge a une fonction pour faire ça. Parfois ça
marche très bien et parfois ça passe pas. À un moment je me suis amusé à
faire DejaVu Sans extra light et c'était assez intéressant comme
expérience. Enfin c'est pas super beau mais c'est utilisable.

**Harrisson :** Est-ce qu'il y a des serif, sans serif dans des systèmes
comme l'arabe? Des systèmes ou justement c'est un peu plus difficile
pour nous de voir s'il y a des spécificités du genre.

**Denis :** Dans ces cas là, on s'inspire de ce qui existe déjà, comme
par exemple le Tahoma. On a essayé de suivre le même style. Pour le sans
serif on a pas encore commencé. Je sais que pour le projet de
localisation à Farsi ils ont fait une police de caractères basée sur
Bitstream Vera en arabe et ils l'ont fait pour le serif et le sans
serif. On est en contact avec une des personnes qui a travaillé
là-dessus. On aimerait aussi pouvoir l'avoir en serif .

**Harrisson :** Tu es aussi impliqué avec le projet Nimbus?

**Denis :** Non pas directement.

**Harrisson :** Parce qu'on a travaillé sur une police Nimbus, sur la
mono. Et c'est pas très clair au niveau des licences, au niveau du
dessin. Ça ressemble très fort à d'autres. On aurait voulu savoir si tu
avais quelques infos à ce sujet?

**Denis :** Les polices Nimbus, si je me souviens bien, elle sont sous
licence GPL. Elles ont été données ou achetées pour Ghostscript. Nimbus
Serif et Nimbus Mono sont en GPL. Parce que justement le projet Free
Fonts les a utilisé comme base.

**Pierre :** À propos de cette base, vous connaissez le nom de la
personne qui a dessiné le Vera chez Bitstream? C'est crédité?

**Denis :** Oui c'est crédité oui. J'ai un nom en tête mais je suis pas
sûr alors... Je devrais le savoir effectivement.

**Pierre :** Non, non, mais je pose la question parce qu'elle est quand
même assez spécifique. Je veux dire que c'est quand même la typo la plus
anonyme.

**Denis :** Mais le dessin est aussi assez proche de Frutiger.

**Pierre :** Et tu as mentionné ton autre activité dans l'association
panafricaine? Ça consiste en quoi? C'est relié?

**Denis :** Je vais plus ou moins refaire le même travail pour les
langues africaines. Dans les deux mois qui viennent, je vais prendre les
polices Liberation et je vais ajouter ce qu'il faut pour les gras des
langues africaines, les langues officielles.

**Pierre :** Et ça c'est du boulot qui est financé par l'extérieur?

**Denis :** Oui là c'est le projet avec le CRDI.

**Pierre :** Est-ce que tu as des contacts avec des utilisateurs,
proches ou lointains du travail que vous faites? Est ce que par exemple
vous le voyez dans le domaine académique être utilisé, mais aussi chez
un coiffeur quelque part au Congo ou...

**Denis :** Le problème c'est justement qu'en Afrique, il y a des
standard qui ont été publiés par les académiciens ou parfois par le
gouvernement, mais il y très peu de gens qui les utilisent en fait.
C'est à dire que les standards ne sont pas bien diffusés, publiés. Par
exemple, dans le cas du Zaïre, dans les années 70, ils ont travaillé sur
un standard pour uniformiser l'orthographe dans toutes les langues
officielles du Congo.

**Pierre :** Parce qu'il y avait quand même l'idée politique de
zaïrisation.

**Denis :** Depuis, seuls les universitaires l'utilisent. Les gens
utilisent encore l'alphabet qu'ils ont appris à l'école primaire.
Certains manuels scolaires sont publiés avec l'orthographe standard mais
ça n'est pas utilisé largement. Dans les autres pays africains ça dépend
aussi. Souvent il y a différents orthographes possibles dans la même
langue. Malheureusement il n'y a pas assez de culture littéraire où il y
aurait un gros corpus écrit dans un même orthographe.

**Nicolas :** Une autre question. On s'intéresse aussi beaucoup par
ailleurs aux questions de cartographie et justement par rapport aux
polices de caractères, quand on voit le projet OpenStreetMap ou des
projets similaires, on a déjà eu pas mal de discussions sur quelles
types de polices, qu'est ce que serait une police pour ce genre
d'activités et Femke me disait qu'elle t'as vu éditer OpenStreetMap,
donc on s'est dit qu'on pouvait te poser la question si tu avais des
idées ou si dans le type de recherches que tu as fais, il y aurait des
polices intéressantes à apporter dans ce genre de projet et aussi à
partir du moment où on a des cartes qui commencent à devenir
multilingues, qu'est-ce que ça veut dire au niveau de la gestion?  
Parce qu'il y a une idée très occidentale de la carte et je vois très
bien comment nous par défaut on place les caractères à partir du moment
où on les lit autrement, etc. Je pense que ça pose des questions. Pas
juste «est-ce que la fonte est disponible?» mais aussi quel est son lien
avec une sorte de lecture de l'image, de lecture de direction, de
l'espace.

**Denis :** Oui, je suis aussi actif sur le projet OpenStreetMap. Pour
les polices de caractères, avant ils utilisaient Arial. Quelques
personnes se sont plaintes et maintenant ils utilisent DejaVu. Ce n'est
pas moi ni Ben qui avons poussé pour ça, mais c'était assez marrants
parce qu'on se disait «Ah ça serait cool si ils utilisaient DejVu» et le
lendemain ils utilisaient Deja Vu. «Ah, cool !». Personnellement je ne
sais pas si DejaVu est la meilleure police pour les cartes en fait parce
que c'est plus une police pour l'écran en petite taille. Comme les
cartes sont générées à l'avance, je pense qu'il mieux de prendre des
caractères moins larges parce que ça prend beaucoup de place. Je pense
que pour la lecture, elle se fait bien avec un police plus fine
(condensée).

**Pierre :** Oui, il y a différentes discussions à ce sujet. Entre le
rapport de réduire le corps et espacer plus les caractères pour arriver
en fait à la même longueur qu'une version condensée qui est plus grande
mais qui est pas forcément plus lisible. Il y des écoles qui s'opposent
sur la question. Mais je ne suis pas sûr que DejaVu soit une si mauvaise
candidate.

**Denis :** C'est vrai que d'un point de vue international, multilingue
c'est quand même assez important d'avoir un style en commun. Pour
l'instant dans OpenStreetMap, il y a deux engins qui génèrent les
cartes, il y a Mapnic et Osmarender, les deux utilisent DejaVu mais
Mapnic ne fait pas de substitution. Pour tout ce qui est écrit en
chinois, coréen, japonais et autre langues asiatiques, il n'a pas de
caractères, il place juste des carrés. Tandis qu'Osmarender substitue
avec des polices qui ont ces caractères là.

**Pierre :** Par contre ce que tu disais au sujet du foisonnement
vertical (différentes hauteurs de glyphes selon les langues), ça oui
c'est important que ça soit limité. C'est difficile de placer des objets
texte qui ont des proportions...

**Denis :** Oui sans pour autant cacher la carte. Moi ce que je trouve
intéressant c'est le problème, justement avec la Belgique, surtout
Bruxelles, étant bilingue. Bon déjà il y a des noms kilométriques comme
Molenbeek Saint Jean et donc en plus si on les met dans les deux langues
et si en plus la police de caractère est grasse et/ou large. (Rires)
Oui, il faudrait plus de souplesse dans leurs engins de rendu pour
qu'ils puissent sélectionner différentes polices de caractères selon la
longueur du texte.

**Pierre :** En cartographie manuelle il y a cette espèce de paradigme
de couper le nom en morceaux, des systèmes pour éviter les obstacles
mais c'est très compliqué à programmer en automatique.

**Yi :** Sur logiciels libres, il y a les fontes Open Source mais aussi
les fontes comme Arial ou Times qui cohabitent. Comment ça se passe?

**Denis :** Arial, Times et Courier font partie des des core fonts?
C'est Microsoft qui avait décidé de rendre disponible un paquet de
polices de caractères comme ça elles pouvaient être utilisées sur les
pages web.

**Pierre :** Quelle est la licence exacte? J'ai jamais regardé.

**Denis :** Non c'est une licence classique, tu peux juste utiliser la
police de caractère, tu peux pas la modifier, la redistribuer. Le
problème c'est que c'est limité à une version fixe, c'est figé dans le
temps.

**Pierre :** Il n'y a pas de contradiction formelle à l'utiliser sauf
que si on l'intègre dans un logiciel qui a un autre type de licence et
où ça va rentrer en conflit.

**Denis :** Le problème aussi c'est qu'il n'y a pas moyen de rentrer en
contact, de communiquer avec les personnes qui l'ont conçu. Parce que si
c'était possible, moi à la limite ça me dérange pas d'avoir une licence
fermée mais si on pouvait communiquer pour que la prochaine version soit
bien faite.

**Pierre :** Mais il y a un autre problème avec l'Arial c'est qu'elle
est moche. C'est un rip-off mal fait.

**Harrisson :** En parlant de dérivés, est ce qu'il y a des dérivés de
la DejaVu? Des personnes qui se la sont approprié et que en ont fait
d'autres?

**Denis :** De DejaVu même non. Mais il y a des gens qui ont pris
Bitstream et qui ont plus ou moins le même niveau que DejaVu. Il y Arev.
Lui a rajouté un certain nombre de caractères pour les mathématiques.
Donc, à un moment on s'en est rendu compte et on en a repris quelques
uns. Et quelques personnes qui ont travaillé pour faire une serif mono.

**Harrisson :** Nous, lors d'un workshop, on avait montré les
possibilités de faire des changements à l'intérieur d'une fonte. On
avait juste changé le a par exemple. On a fait un autre a sur la DejaVu
Serif. Mais je sais pas si vous avez des feedback sur ce genre de jeux.

**Denis :** Non, pas de DejaVu. Mais Bitstream Vera il y en a beaucoup.
Enfin moins qu'avant puisqu'on les a reprise ensemble mais dérivées de
Deja Vu même, non. Généralement si les gens veulent faire des
modifications ils nous les envoie directement. Il y a un grec qui a
refait le grec pour DejaVu. Il nous avait envoyé un pdf avec ce qu'il
avait fait. Ça avait relancé un peu la discussion sur notre travail à
nous et on a commencé à refaire des modifications.

**Harrisson :** Mais vous avez toujours un dessin par glyphe? Vous
n'avez pas plusieurs...

**Denis :** Si on a quelques variantes, par besoin linguistiques. Pour
le a on peut choisir le a scolaire et le a typographique. Il y aussi le
g à double contre-poinçon et le ŋ. Le n avec un crochet utilisé dans les
pays nordiques, en sami puis dans quelques langue africaines mais le
problème c'est que la forme majuscule est différente. Pour le sami c'est
un N majuscule normal avec le crochet tandis qu'en Afrique c'est souvent
un n minuscule agrandi avec le crochet. Et certains ont le crochet sous
la ligne de base, d'autre au dessus. On a trois variantes.

**Pierre :** Qui existent dans Unicode? Déjà référencés comme des choses
séparées?

**Denis :** Non, pour Unicode c'est un seul caractère. Donc on a un
caractère et 3 glyphes pour ce même caractère, qui sont référencés selon
la langue, selon l'interface aussi si elle le permet.

**Yi :** J'ai encore une question. Par exemple si on ouvre une page
internet en chinois, parfois il y a des problèmes d'affichage. Les
caractères manquant ne sont pas substitué par des carrés mais il manque
carrément des caractères et même en changeant le codage, rien ne change.
Et d'autre fois il n'y a pas de problèmes d'affichages. Je veux dire sur
un même site, le même jour.

**Denis :** C'est bizarre. Là le problème peut venir du navigateur
utilisé, la librairie qui gère Unicode ou les polices de caractères. Ce
qu'il faut savoir avec le codage c'est qu'une page html peut être
définit par un codage, mais ce que le navigateur va d'abord lire c'est
ce que le serveur lit. Dons si le serveur dit que j'utilise le codage x
et que la page html dit codage y, c'est le codage x qui est pris en
compte.

**Pierre :** Ce qui n'est pas toujours idéal.

**Nicolas :** Ça veut dire que si tu veux une page en chinois sur un
serveur américain, il faut que le serveur ai détecté à l'origine que ta
page est en chinois.

**Denis :** Je sais pas si tous les navigateurs suivent ça à la ligne.

**Nicolas :** Mais le problème que tu décris est plus étrange, parce
qu'il y a des trous.

**Yi :** Oui, c'est même pas une erreur d'affichage, il manque des
signes.

**Ludi :** Mais peut être bientôt le DejaVu chinois?

**Harrisson :** Ou reprendre ça d'ailleurs?

**Denis :** Oui, il y a Arne Götje qui travaille sur une Sans Serif qui
serait compatible avec DejaVu, il a accès à une base de donnée avec les
traits et il aimerait faire une police de caractères qui soit compatible
avec DejaVu. En parlant de caractères asiatiques, il y a une police de
caractères pour le japonais qui est assez bien, c'est M+. Avec un
licence spécifique, mais une licence libre. Ce qui est intéressant c'est
qu'ils font 4 gras différents.

**Yi :** Oui il y en a une en chinois aussi qui vient de sortir il n'y a
pas très longtemps mais avec une seule version pour l'instant qui
correspond à la base chinoise.

**Denis :** Ce qui pourrait aussi être intéressant c'est Droid.
Apparemment c'est une licence libre. Ils ont une police fallback avec
des caractères chinois en regular.

**Yi :** J'ai même essayé dans Scribus de comparer avec les typos
classiques de Microsoft pour voir la différence et c'est vrai que...
enfin l'auteur lui-même précise que ce n'est pas encore au point, il
reste des choses à améliorer.

**Denis :** Ce qui vraiment intéressant aussi c'est dans Pango, il y a
un an et demi, ils ont implémenté pour qu'on puisse travailler en lignes
verticales.

**Ludi :** Moi j'ai une dernière question, pour revenir à la base du
fonctionnement de DejaVu, au niveau de l'organisation, comment ça se
passe? Est-ce que vous travaillez parfois ensemble dans un même lieu ou
c'est toujours à distance?

**Denis :** Pour l'instant c'est toujours à distance. On est quasiment
toujours sur IRC, quand il fait jour. (rires) Il y a aussi des gens sur
différents fuseaux horaires. Mais donc oui assez souvent on travaille
sur IRC. On fait des modifications et puis on fait une capture d'écran,
on la met sur un serveur temporaire et on passe le lien sur IRC et on a
des commentaires ou bien directement on met ça dans les sources et puis
on voit les commentaires.

**Pierre :** Donc vous avez un mode de communication, un mode
humoristique uniquement verbal?

**Denis :** Oui, on a déjà pensé à as se rencontrer mais ça c'est jamais
concrétisé. Avec Ben, on s'est déjà vu plusieurs fois.

**Pierre :** Ben était à Fosdem aussi non? (Free and Open Source
Software Developers' European Meeting )

**Denis :** Oui.

**Pierre :** Donc il y a eu un moment où vous étiez ensemble.

**Denis :** Mais c'est vrai que nous deux on est pas loin, 40 mn... mais
toujours par IRC.

**OSP :** OK. Merci beaucoup Denis.

**Denis :** C'était un plaisir.

**OSP :** À bientôt.
