Title: How to catch Ukrainian TV in Moscow
Date: 2007-05-15 14:46
Author: Femke
Tags: Conversations, Tools, LGM 2007, sK1
Slug: how-to-catch-ukrainian-tv-in-moscow
Status: published

**Conversation with Igor Novikov and Valek Philippov (SK1)**

[![igor.JPG]({filename}/images/uploads/igor.JPG)]({filename}/images/uploads/igor.JPG "igor.JPG")[![valek.JPG]({filename}/images/uploads/valek.JPG)]({filename}/images/uploads/valek.JPG "valek.JPG")  
Excerpts from a conversation with **Igor Novikov** (Ukraine) and **Valek
Philippov** (Russia) about how and why they are involved in [SK1
pre-press software](http://sk1.sourceforge.net/); the joy of reverse
engineering and a handy tip for receiving Russian TV in Ukraine too.

[igor\_valek.mp3](http://ospublish.constantvzw.org/documents/sound/igor_valek_corr.mp3)
\[25mb\]
