Title: GRRRR - objectivity of the unperfect, 
Date: 2009-02-24 10:35
Author: Harrisson
Tags: Conversations, Tools, Digital drawing
Slug: grrr-objectivity-of-the-unperfect
Status: published

During an internet wandering, and thanks to excellent
[K-SET](http://www.k-set.net/) website, I found the link to swiss
drawing artist [GRRRR](http://www.grrrr.net/) website I was looking for
a long time. Though I'm fan for a long time of his work, from Maika 2
([www.noraduester.net](http://www.noraduester.net/) =&gt; music) record
sleeve to Vitra 2006 catalogue, I hardly found traces of his activity.
(GRRRR - 4 Rs - is not an easy keyword for google search! ).

![sea\_perseus]({filename}/images/uploads/sea_perseus.gif "sea_perseus"){: .alignleft .size-full .wp-image-1919 }

<!--more-->

![pelle]({filename}/images/uploads/pelle.png "pelle"){: .alignleft .size-full .wp-image-1919 }

Impressive urban landscapes, where structure, destruction and chaos are
harmoniously gathered through organic black lines. Between meditative
observation, and witnessing nervous and versatile urban activity, GRRRR
works oscillate between ligne claire and expressionism, linking harmony
and defaults...

![maika2]({filename}/images/uploads/maika2.jpg "maika2"){: .alignleft .size-full .wp-image-1919 }

Drawings also express a web cultural background infused with squats,
human photocamera and alternative comics. Those images are the humble
wanderer and patient retranscription of unspectacular scenes, half
molded by human hands, half by traces of complexity of nature.

If drawings shows industrial objects, it is with the defaults of the
human hand. If they show human construction, it is with long and patient
observation and retranscription of its activity.

![grrr1]({filename}/images/uploads/grrr1.png "grrr1"){: .alignleft .size-full .wp-image-1919 }

I was surprised to find digital version of his illustrations, where
crude pixelated treatment to drawing emphases radicaly and harmoniously
his drawing style. Moreover, animated movies and [music
videos](http://www.grrrr.net/bigzis/index.html) testimonates of a
serious activity, out of the boundaries of inked paper.

The GRRRR link page was also a good surprise. Among others, there are
links to few open source applications we're used to play with, such as
Gimp, Inkscape and so on... Curiosity convinced me to go over shyness,
and contact him, though I'm not use to such intrusive behavior...

After an exchange of few emails, GRRRR kindly accepted to give a little
interview.  
Here it is.

= = = = = =

*- May you introduce your work?*

i do drawings:

i start my work with pen, paper and a nice spot for a little rest and to
have a seat... then my goal is to fill the very last corner of that
sheet of paper with observations my surroundings, these drawings later
evolve into murals, picturebooks, animations and other media.

i started with comics which lead me into drawing on the street to have
any backgrounds for the stories, this than turned into a large
collection of "urban-landscape"-drawings from where i started a research
in density/patterns/"bildrauschen" which will lead into...?  
...and very soon after i self-published comics i began to put my work on
the internet (see www.[GRRRR.net](http://www.GRRRR.net/)), i really like
the anarchistic, self-expressive and low-cost aspect of this worldwide
network and update my website regularly.

*- What is the background you're coming from?*

i have been raised lowerclass by my single mother but in switzerland -
quite a rich country, so im more like from the middleclass... and then
came comics, squats, artschool, internet, extensive travelling,
artmarket...

right now im high on books ;)...

*  
- How did you get into open source softwares?*

out of curiosity, because open-source belongs to everyone and the best
things in life are free... :)

*- Did the use of open source softwares changed the way you work?*

Not directly, but its generally better to work with open structures,
when i started publishing on the net i also thought about using (at that
time still proprietary) flash-graphics, but vectors didn't fit my
drawings, i much prefered the pixelated gifs and pngs, and the
HTML-structure proofed to be much more extendable,
cross-platform-friendly and able to stand the time...

i was about to learn linux but then mac went unix too and i got lazy
;)...  
i work on mac os x, with programs like:  
gimp  
cyberduck  
firefox  
vlc  
mpeg streamclip  
scribus  
neooffice  
burn  
pure data extended  
gawker  
copernikus  
posterazor

*- Why do you consider FLOSS softwares more appropriate to your practice
than commercial ones?*

1\. free of charge, you pay what you want/can...  
2. it is public property, i love public space in general...

*- Is it a problem to use those softwares compared to print workflow or
standards?*

yes and no; as artist i try to do things differently, some disadvantages
can turn out to be inputs for new ideas...  
but till now i haven't found an opensource-program for my
animation-work, so I still have to this with an old apple-software...

*- Do you know other artists or designers working with FLOSS?*

just a few, generally its only those creative people that come from the
conceptual, the computer-programing side who are into linux and
opensource... visual designers working on macs don't care, for example
they're too lazy to install first x11 and then the gimp etc... though
its really not that complicated: i just managed to install scribus with
fink :) (though my first attempt, installing it  
through macports failed...)

all you need is an internet-connection, a little time but most
important: curiosity!

![ingo\_giezendanner]({filename}/images/uploads/ingo_giezendanner.png "ingo_giezendanner"){: .alignleft .size-full .wp-image-1919 }
