Title: Interview with George Williams (FontForge)
Date: 2007-05-07 13:32
Author: Femke
Tags: Conversations, Type, Fontforge, LGM 2007
Slug: interview-with-george-william-fontforge
Status: published

[![george.JPG]({filename}/images/uploads/george.JPG){: .float}]({filename}/images/uploads/george.JPG "george.JPG")
Interview (~~unedited files~~) with George Williams, developer of
[FontForge](http://fontforge.sourceforge.net/), the open source font
editing tool. Conversation about Shakespeare, Unicode, the pleasure of
making beautiful things and pottery. Enjoy!

Listen to the recording : [  
GW\_dl1.mp3](http://ospublish.constantvzw.org/documents/sound/GW_dl1.mp3)
\[8.1 mb\] and
[GW\_dl2.mp3](http://ospublish.constantvzw.org/documents/sound/GW_dl2.mp3)
\[16.6 mb\]

... or read the transcription: "[I think the ideas behind it are
beautiful in my
mind](http://ospublish.constantvzw.org/blog/typo/i-think-the-ideas-behind-it-are-beautiful-in-my-mind)"

This interview is part of the book [I think that conversations are the
best, biggest thing that Free Software has to offer its
user](http://conversations.tools).
