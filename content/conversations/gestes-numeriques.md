Title: Gestes Numériques
Date: 2009-06-01 16:14
Author: OSP
Tags: Conversations, Education Discussion, Education, Video
Slug: gestes-numeriques
Status: published

[![Stephane](http://gallery.constantvzw.org/main.php?g2_view=core.DownloadItem&g2_itemId=25736&g2_serialNumber=2)![Loic](http://gallery.constantvzw.org/main.php?g2_view=core.DownloadItem&g2_itemId=25742&g2_serialNumber=2)![Marc](http://gallery.constantvzw.org/main.php?g2_view=core.DownloadItem&g2_itemId=25744&g2_serialNumber=2)![Michel](http://gallery.constantvzw.org/main.php?g2_view=core.DownloadItem&g2_itemId=25738&g2_serialNumber=2)](http://gallery.constantvzw.org/main.php?g2_itemId=25728)

Our colleagues from [Open Source Video](http://osvideo.constantvzw.org)
published a video-registration of the discussion that took place at the
yearly [Journées du Libre](http://journeesdulibre.bxlug.be/).  
Enjoy Loic Vanderstichelen, Stéphane Noël, Michel Cleempoel and Marc
Wathieu as they present with humour and enthusiasm why F/LOSS is
relevant for art- and design education (in French).

<http://osvideo.constantvzw.org/journees-du-libre-09/>
