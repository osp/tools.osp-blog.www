Title: Meaningful Transformations
Date: 2012-01-12 12:20
Author: Femke
Tags: Conversations, Lay-out, LGM 2011, Tools
Slug: meaningful-transformations
Status: published

**A conversation with Tom Lechner**

[![]({filename}/images/uploads/sphere.jpg "sphere"){: .alignnone .size-medium .wp-image-6954 }](http://blogs.lgru.net/ft/conversations/meaningful-transformations)

We discovered the work of [Tom Lechner](http://www.tomlechner.com/) at
the Libre Graphics Meeting 2010 in Brussels. Tom has traveled from
Portland, US to present
[Laidout](http://river-valley.tv/laidout-and-strange-interfaces/), an
amazing tool that he made to produce his own comic books and also to
work on three dimensional mathematical objects. His software interests
us for several reasons. We are excited about how it represents the
gesture of folding, love his bold interface decisions plus are impressed
by the fact that Tom has decided to write his own programming framework
for it. A year later, we meet again in Montreal, Canada for the 2011
Libre Graphics Meeting where he presents a follow-up. With Ludivine
Loiseau (amateur bookbinder and graphiste) and Pierre Marchand
(artist/developer, contributing amongst others to podofoimpose and
Scribus) we finally find time to sit down and talk.

Read the interview:
<http://blogs.lgru.net/ft/conversations/meaningful-transformations>
