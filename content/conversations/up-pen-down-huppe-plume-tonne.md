Title: Up Pen Down — Huppe plume tonne
Date: 2015-08-07 18:54
Author: Colm
Tags: Conversations, Education, Live, News, Print Party, Workshop
Slug: up-pen-down-huppe-plume-tonne
Status: published

Announcing *Up Pen Down — Huppe plume tonne*
--------------------------------------------

[![IMG\_1389]({filename}/images/uploads/IMG_1389.jpg){: .aligncenter .size-medium .wp-image-7421 }]({filename}/images/uploads/IMG_1389.jpg)  
a workshop and preformance during the Quinzaine Numérique 2015:

specifics of the workshop are on their way, but for now, here is the
introduction text:

En 2012, OSP (Open Source Publishing) construit un workshop de plusieurs
mois avec des étudiants de l'École Supérieure d’Art et Design de
Valence. La proposition au centre des tables s'intéresse en détail au
«trait», dans le sens de «chemin» (path en anglais) par opposition et en
dialogue avec la notion de forme. Si ce point de départ offre plusieurs
perspectives — celles du dessin, de la typographie, de la cartographie —
ce «chemin» pose aussi la question des outils et en particulier des
outils numériques, et de leur relation avec le langage visuel.

Il y a trois ans donc, des étudiants en design graphique, en typographie
et nous-mêmes sont amenés par le trait à la performance. Aujourd'hui,
nous allons réitérer l'expérience en modifiant le chemin de
l'apprentissage. Nous proposons un workshop d'expérimentation permettant
deux approches :  
– pour les participants avec une expérience graphique, s'intéresser à
produire du lettrage par des mouvements à échelle du corps;  
– pour les participants avec une expérience chorégraphique, s'intéresser
à la manière spécifique par lequel le dessin (down) et le non-dessin
(up) questionne le mouvement et ses différentes échelles;

Nous chercherons à nous intéresser spécifiquement à une série de
questions, et aux spécificités qui y répondent.

La chorégraphe et danseuse Adva Zakaï nous accompagnera dans ce travail
de débroussaillage. Son travail de recherche fondé sur l'hypothèse que
le corps mute progressivement en texte numérique, et que le numérique
mute en retour au contact intime des corps.
