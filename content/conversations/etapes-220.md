Title: étapes 220
Date: 2014-09-29 12:44
Author: Colm
Tags: Conversations, News, Texts, Works, étapes, co-working, Interview, libreobjet
Slug: etapes-220
Status: published

*étapes* magazine issue 220 focused on Co-Working, so we were pleased to
be asked for an interview for the Portrait section of the publication.

[![Screenshot from 2014-09-27
14:03:19]({filename}/images/uploads/Screenshot-from-2014-09-27-140319-e1411819484217.png){: .wp-image-7267 .size-medium }]({filename}/images/uploads/Screenshot-from-2014-09-27-140319.png)

In proper OSP fashion, after an initial set of questions from Caroline
Bouige, the interviewer, we answered collaboratively, both to the
questions and to each other.

You can read the full Q&A up on
<http://osp.constantvzw.org:9999/p/etapes>

For the full article: <http://etapes.com/etapes-220>

Our friends from [Libre Objet](http://libreobjet.org/) were also
featured in this issue, so grab yourself a copy!
