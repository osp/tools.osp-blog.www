Title: Médor
Date: 2014-09-29 12:49
Author: Colm
Tags: Conversations, News, Works, crouwd funding, deep journalism, independant publishing, journalism, Médor, Not a dog
Slug: medor
Status: published

![](http://www.halles.be/website/images/dbfiles/4829/large/M%C3%A9dor.png){: .alignnone }

**Medor is not a dog !**
------------------------

It's a trimestrial, Belgian magazine of inquiries and stories, 128 pages
long. *Médor* contains long-term investigations, reports and portraits
focused on Belgium.

*Médor* digs the heart of issues. It inquires and it is persistent. It
takes the time needed to be further, beyond appearances. It seeks to
understand the facts and to give opinions about uncovered truths.

*Médor* intends to cultivate the pleasure of reading and to inform while
entertaining. *Médor*'s journalists make sense of current events and are
proud and passionate about their work. Today the profession of
journalism is threatened by job pressures that erode the search for
meaning and by small news items that jostle the priorities of the press.

So *Médor* aims to reinvent the mechanisms which produce information.
*Médor* is innovative and differentiates itself through its creative
process as well as its editorial line. It invents its own, suited
ecosystem : original, free style graphics ; teams of journalists and
graphic designers or photographers who work together in pairs ;
collective decision-making ; rotating chief editors ; readers organized
in a co-op ; and decent salaries for all.

It functions with a horizontal structure and open-source software, pays
everyone, print on FSC (ecolabel), guarantees transparency,
independence, etc.

Join *Médor* on the full website: [medor.coop](http://medor.coop)
