Title: OSP @ Princesse!
Date: 2014-09-29 15:56
Author: Colm
Tags: Conversations, News, OSP Public Meets hackbase, Home, Princesse, Schaerbeek
Slug: osp-princesse
Status: published

[![DSCF3587]({filename}/images/uploads/DSCF3587.jpg){: .alignnone .size-medium .wp-image-7273 }]({filename}/images/uploads/DSCF3587.jpg)

Since the early days, OSP has changed form over and over, from asking
simple questions right through to organizing week-long workshops to
share answers its members have found.

There have been a fair amount of turning points in the past, and some of
the biggest ones have happened over the last few years. [The
Variable](http://www.hulu.com/lost "Lost"){: .alignnone .size-medium .wp-image-7273 } residency was
certainly an interesting step, it brought in means to extend and
collaborate in an environment that called for growth on the personal and
group level. The three years in Variable were key in making OSP what it
is today, but not only OSP, all of the Constant research experiments
have taken from the shared house and gone forth.

Variable went out on a big bang with [Relearn
2014](http://ospublish.constantvzw.org/blog/news/relearn-variable-summerschool-2014)
but all good things come to an end. We were all extremely happy, and we
left with as many plans as questions for what the next steps would be.
Even before leaving Variable, we knew of the forthcoming
[F-LAT](http://www.f-lat.org/) project and other ideas regarding the
maintenance of the Brussels (whatever that means) FLOSS network of
people. It's going to be yet another exciting year!

For OSP, it was clear that we enjoyed the idea of a studio space and
wanted to follow up on that, so after a few weeks of searching we found
a perfect fit in the old [Hackbase.be](http://hackbase.be) working
space. Hackbase is still moving into a new directions, but thanks to
them, we're equally happy with the space and its history (and its
unremovable shelves filled with old computer gear from all eras!). OSP
has a new home.

[![DSCF3575]({filename}/images/uploads/DSCF3575.jpg){: .alignnone .size-medium .wp-image-7273 }]({filename}/images/uploads/DSCF3575.jpg)

There certainly is still a lot of work to do there, but we're very
happy. The added bonus is that it's only a few streets away from the
re-purposed Variable building, so we're not too nostalgic.

The caravan is also in the process of setting up its own fully fledged
legal structure (in its own name) as a non profit organisation, or ASBL
as they say in French. As soon as we're up and running, we'll have you
all over for a studio-warming (even though we can't, at this point
guarantee the room temperature) party, Loop events, and any other reason
too!

Av. Princesse Elisabeth 46

Schaerbeek Brussels

Welcome!

[![DSCF3586]({filename}/images/uploads/DSCF3586.jpg){: .alignnone .size-medium .wp-image-7273 }]({filename}/images/uploads/DSCF3586.jpg)
