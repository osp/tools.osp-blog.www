Title: the Univers Else is yours
Date: 2011-02-08 14:28
Author: Antoine
Tags: Fonts, Foundry (blog), News, Type
Slug: the-univers-else-is-yours
Status: published

**[![]({filename}/images/uploads/600x450-R00118961.jpg "Univers Else is yours"){: .alignnone .size-medium .wp-image-5809 }  
](http://ospublish.constantvzw.org/news/the-univers-else-is-yours/attachment/exif_jpeg_picture-2)Fresh**
new display case for [Univers
Else](http://ospublish.constantvzw.org/foundry/univers-else/ "Univers Else")
font, with original Crickx vinyl lettering.  
[![]({filename}/images/uploads/600x450-R0011892.jpg "Dead drop"){: .alignnone .size-medium .wp-image-5809 }](http://ospublish.constantvzw.org/news/the-univers-else-is-yours/attachment/exif_jpeg_picture-3)  
Check out Speculoos and OSP's [dead
drop](http://deaddrops.com/ "dead drop") and get Univers Else.
