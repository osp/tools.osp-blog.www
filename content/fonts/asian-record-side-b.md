Title: Asian Record, side B
Date: 2011-04-21 14:58
Author: OSP
Tags: Fonts, News, OSP-DIN Fontforge, LiteralDraw, Unicode, Workshops + teaching
Slug: asian-record-side-b
Status: published

Monday
======

\[caption id="attachment\_6221" align="alignnone" width="400"
caption="[opendesign.asia](\%22http://opendesign.asia/)"\][![]({filename}/images/uploads/DSC_0633.jpg "DSC_0633"){: .size-medium .wp-image-6221 }]({filename}/images/uploads/DSC_0633.jpg)\[/caption\]

\[caption id="attachment\_6222" align="alignnone" width="400"
caption="Vi Mã Đăng installing OSP's posters at Café
3D."\][![]({filename}/images/uploads/P1060327.jpg "P1060327"){: .size-medium .wp-image-6221 }]({filename}/images/uploads/P1060327.jpg)\[/caption\]

Its akward, it's unconfortable, but we want to put ourselves in a
position of welcoming a happy accident, a *trong ruì có may*.

[![]({filename}/images/uploads/IMAG0123.jpg "IMAG0123"){: .size-medium .wp-image-6221 }]({filename}/images/uploads/IMAG0123.jpg)  
<!--more-->  
[![]({filename}/images/uploads/DSC_0665.jpg "DSC_0665"){: .size-medium .wp-image-6221 }]({filename}/images/uploads/DSC_0665.jpg)

*(Tomorrow, not forget :  
— printer  
— walking  
— reformat usb sticks  
— osm account)*

[![]({filename}/images/uploads/DSC_0666.jpg "DSC_0666"){: .size-medium .wp-image-6221 }]({filename}/images/uploads/DSC_0666.jpg)

We make a tour with GPS and cameras to chase stencil letters in
priority, and some hand-painted letters.

[![]({filename}/images/uploads/P1060378.jpg "P1060378"){: .size-medium .wp-image-6221 }]({filename}/images/uploads/P1060378.jpg)

\[caption id="attachment\_6246" align="alignnone" width="400"
caption="Redrawn stencil letters, on lots of
walls"\][![]({filename}/images/uploads/P1060405.jpg "P1060405"){: .size-medium .wp-image-6221 }]({filename}/images/uploads/P1060405.jpg)\[/caption\]

[![]({filename}/images/uploads/P1060368.jpg "P1060368"){: .size-medium .wp-image-6221 }]({filename}/images/uploads/P1060368.jpg)

\[caption id="attachment\_6244" align="alignnone" width="400"
caption="Some less stencils, even if the reverse leftover of a vinyl
cutting operation is barely always throw away, it can be used also, and
is composed of islands and sea. Stickers are
everywhere."\][![]({filename}/images/uploads/P1060400.jpg "P1060400"){: .size-medium .wp-image-6221 }]({filename}/images/uploads/P1060400.jpg)\[/caption\]

\[caption id="attachment\_6245" align="alignnone" width="400"
caption="Maybe at a more high pace than on other places, machine vinyl
cutting letters make hand-drawn lettering goes
rare"\][![]({filename}/images/uploads/P1060408.jpg "P1060408"){: .size-medium .wp-image-6221 }]({filename}/images/uploads/P1060408.jpg)\[/caption\]

Participants ask what is so interesting in those daily life pictures.
Pierre Marchand says that, this is exactly daily life as they say, but
if they learn how to look at it and how to change it, then it will
change radically this daily life/landscape.

\[caption id="attachment\_6220" align="alignnone" width="400"
caption="When possible to wrap it, wrap it. Here the ceiling of a
taxi."\][![]({filename}/images/uploads/P1060228.jpg "P1060228"){: .size-medium .wp-image-6221 }]({filename}/images/uploads/P1060228.jpg)\[/caption\]

Tuesday
=======

[![]({filename}/images/uploads/P1060330.jpg "P1060330"){: .size-medium .wp-image-6221 }]({filename}/images/uploads/P1060330.jpg)

We work with bare feet  
Maybe are we at the meristem (Méristème) where cells are not yet
specialised - where we are in danger  
The professor illustrator who draws beautiful curves so easily on
Coreldraw ask for more step by step operations  
(Tape, Ferrer maybe)  
Connecting with the city, with the territory  
- Bring a flat surface to be able to work less bended

[![]({filename}/images/uploads/DSC_0817.jpg "DSC_0817"){: .size-medium .wp-image-6221 }]({filename}/images/uploads/DSC_0817.jpg)

We are making fonts based on previous day pictures. The morning we learn
how to use gimp/inkscape/fontforge to autotrace a letter from a picture
and how to produce a font.

Wednesday
=========

"Unicode's normative definition is not as informative as it might be
(many of the Greek accents are "unified" with Latin accents that the
don't really look like, the result is that following Unicode slavishly
will yield the wrong glyph). So in some cases FontForge will use a
slightly different set of glyphs than the normative decomposition." —
Georges Williams (http://fontforge.sourceforge.net/accented.html)

<http://vietunicode.sourceforge.net/charset/vietalphabet.html>

\[caption id="attachment\_6252" align="alignnone" width="400"
caption="how to define the thickness and the curve of a tilde relative
to letters thickness in a non-typographic system as the DIN
one?"\][![]({filename}/images/uploads/DSC_0931.jpg "DSC_0931"){: .size-medium .wp-image-6221 }]({filename}/images/uploads/DSC_0931.jpg)\[/caption\]

"Too much excited to put the vietnamese diacritics everywhere, Nhan
Nguyễn and the 2 Pierres decide to spent the afternoon in the garden of
the "3D Café" in Can Tho under the endless loop of vn muzak
(<http://www.youtube.com/watch?v=igqtCvoBXck> seems a must) impossible
to avoid if you want to have access to an electrical outlet. Prince
prepared a py + qt script to inject automatically the needed  
AàÀảẢãÃáÁạẠăĂằẰẳẲẵẴắẮặẶâÂầẦẩẨẫẪấẤậẬ  
bBcCdDđĐeEèÈẻẺẽẼéÉẹẸêÊềỀểỂễỄếẾệỆfFgGhH  
iIìÌỉỈĩĨíÍịỊjJkKlLmMnN  
oOòÒỏỎõÕóÓọỌôÔồỒổỔỗỖốỐộỘơƠờỜởỞỡỠớỚợỢ  
pPqQrRsStTuUùÙủỦũŨúÚụỤưƯừỪửỬữỮứỨựỰ  
vVwWxXyYỳỲỷỶỹỸýÝỵỴzZ  
Nhan propose some ways to draw them and Pierreh lost himself in the
osp-foundry to find the source files. Organically, after editing most of
our fonts, all more or less experimental in some way, we've been
attracted by our old chap Din, and ends up by editing most of the
diacritics to make them fits best with the Din principles (from what
font came the previous ones?). The night falls suddenly
-near-equator-style- without finishing, but with all the actors ready
for tomorrow's first activity of the morning : the "up, more down, a bit
less at the right, slightly to the left" collective game of positioning
ready-to-use component diacritics. Back to the hotel with a discussions
about the iterative but programmatic process of producing a font, itself
a program, a script in some way, or more mathematically,
factorisation... Let's go for another motor ride in town, now."

Thursday
========

Morning: We are practicing LiteralDraw in groups. Participants draw
letters.

[![]({filename}/images/uploads/DSC_0821.jpg "DSC_0821"){: .size-medium .wp-image-6221 }]({filename}/images/uploads/DSC_0821.jpg)

[![]({filename}/images/uploads/DSC_0829.jpg "DSC_0829"){: .size-medium .wp-image-6221 }]({filename}/images/uploads/DSC_0829.jpg)

Afternoon:  
We take advantage of the crazy fluidity of riding a motorbike in the
city. Some of the participants ride their motorbike and go draw letters
in the city, communicating by phone their position and movements to the
ones staying at the working space. Those ones report the instructions in
LiteralDraw to make letters in order to make a complete font.

\[caption id="attachment\_6254" align="alignnone" width="266"
caption="Chien Phan Quoc leaving for his first road
session"\][![]({filename}/images/uploads/DSC_0960.jpg "DSC_0960"){: .size-medium .wp-image-6221 }]({filename}/images/uploads/DSC_0960.jpg)\[/caption\]

\[caption id="attachment\_6259" align="alignnone" width="400"
caption="Lilly Nguyễn on her patient, smily and persistant work of
multitranslation"\][![]({filename}/images/uploads/DSC_0001.jpg "DSC_0001"){: .size-medium .wp-image-6221 }]({filename}/images/uploads/DSC_0001.jpg)\[/caption\]

[![]({filename}/images/uploads/P1060546.jpg "P1060546"){: .size-medium .wp-image-6221 }]({filename}/images/uploads/P1060546.jpg)

First series of letters ended up being quite easy to do -- only skeleton
of letters, instructions given by phone were not so clear. Second
series: we want a real letter with skin and flesh, why not serifs or
stencil letters?

We add a
[LOGO](http://en.wikipedia.org/wiki/Logo_%28programming_language%29)/Turtle
mode to LiteralDraw.

Friday
======

Chien lost a complex LiteralDraw drawing yesterday for he didn't save
it. Discussions around the missing fishing net when you quit without
saving. Is LD a software like the others, or a more rough tool, an
in-between commandline and GUI tool?

LD was modified yesterday in two interesting ways.

For an even more "direct" or "straightforward" mode : no need for an end
anymore.  
So

    line 40 60

will not wait for a

    end

to draw the line (of course the end instruction is still very useful to
separate shapes)

For our rides in the city hunting letters shapes, we introduced the turn
instruction, with right, straight, left arguments, the distance and a
nuance (a deviation from the main direction)

Maybe more important is the introduction of variables, with the var
instruction! So we're now able to write

    change turn tourne
    var 150 unpeu
    var -10 presque

then

    tourne R unpeu presque

which is not real natural language programming, but which sounds quite
poetical for me.

Pushing it a bit, to go to typed variables to be able to

    tourne àdroite unpeu presque

or

    tourne à-droite un-peu presque

or even

    tourne à droite un peu presque

using non-breaking spaces between "un" et "peu" as a trick. So we
introduced number and string as replacements for var. LD is more and
more deeply connected to language.

The extention of OSP-DIN is a neverending work.

Sometimes we've used previous week's dictionary

    change line dòng
    change stroke bút
    change end trở—lại
    change move di—chuyển
    change cubic cong
    change close đong
    change fill điền—vào
    change transform biến—đổi
    change text văn—bản
    change font kiểu—mâu

    biến—đổi 0.6 0 0 0.6 0 -300 
    bút 180 0  200 1
    điền—vào 250 100 100
    di—chuyển 135.00 732.00 
    cong  134.00 691.00  178.00 660.00  241.00 659.00 
    cong 290.00 661.00  328.00 701.00  328.00 755.00 
    cong  378.00 665.00  441.00 643.00  521.00 677.00 
    cong 524.00 681.00  576.00 759.00  497.00 889.00 
    cong  496.67 891.67  411.67 960.00  330.00 986.67 
    cong 230.00 926.67  163.33 845.00  133.33 735.00 
    trở—lại
    đong
    di—chuyển 135.00 732.00 
    kiểu—mâu 30 0 0 Alfphabet
    văn—bản we

\[caption id="attachment\_6256" align="alignnone" width="400"
caption="With the golden frog at the
back"\][![]({filename}/images/uploads/DSC_0141.jpg "DSC_0141"){: .size-medium .wp-image-6221 }]({filename}/images/uploads/DSC_0141.jpg)\[/caption\]

[![]({filename}/images/uploads/DSC_0191.jpg "DSC_0191"){: .size-medium .wp-image-6221 }]({filename}/images/uploads/DSC_0191.jpg)

    Lilly Nguyễn     Thong Tran
    Thanh Tri        Loi Kim
    Trung Tran Minh  Ngân Trương
    Dat Huynh Phat   Nam Pham
    Quoc Nam         Chien Phan Quoc
    Vansau Duong     Kiet Le              
    Lê Quốc Tuấn     Xuan Phuong      
    Nhan Nguyễn      Minh Đương Nguyễn

![](http://ospublish.constantvzw.org/blog/wp-content/themes/osp/images/OIF.jpg)  
  
<small>La participation OSP de l'Open Design Week a été rendue possible
grâce au soutien de l'OIF</small>

<link href="http://fonts.googleapis.com/css?family=Allan:bold" rel="stylesheet" type="text/css"></link>
<link href="http://fonts.googleapis.com/css?family=VT323" rel="stylesheet" type="text/css"></link>

<style type="text/css" media="screen">
        body {: .size-medium .wp-image-6221 }<br></br>
        h1, h2, h3 {: .size-medium .wp-image-6221 }<br></br>
        code {: .size-medium .wp-image-6221 }<br></br>
pre {: .size-medium .wp-image-6221 }<br></br>
        p, ul, code {: .size-medium .wp-image-6221 }<br></br>
        ul {: .size-medium .wp-image-6221 }</p>
</style>

