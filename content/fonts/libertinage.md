Title: Libertinage
Date: 2008-11-02 23:03
Author: ludi
Tags: Fiche, Libertinage, News, Type, Works, Fontforge, Libre Fonts, Type
Slug: libertinage
Status: published

The Libertinage font set we developed for FLOSS+Art book is available on
the [Open Font Library](http://openfontlibrary.org/media/files/OSP/322)
for several days.

We built Libertinage by copying and pasting parts of Linux Libertine
glyphs or simply by all-turning glyphs.  
There are 27 variations, one for each latin letter in the alphabet + the
'Full' version, containing all modifications.

Un petit goût nonante.

[![]({filename}/images/uploads/osp_-_libertinage1.png "osp_-_libertinage1"){: .alignleft .size-medium .wp-image-1313 }]({filename}/images/uploads/osp_-_libertinage1.png)
