Title: POLSKU UPDATE
Date: 2011-02-07 14:34
Author: Seb
Tags: Fonts, Foundry (blog), News, Tools, Type, Works
Slug: polsku-update
Status: published

[![]({filename}/images/uploads/thanks-paulo.png "thanks-paulo"){: .alignnone .size-medium .wp-image-5784 }](http://ospublish.constantvzw.org/news/polsku-update/attachment/thanks-paulo)  
Thanks to Paulo Silva aka nitrofurano, Polsku has now a Latin 1
diacritics  
and ligatures set. The new version is available for download on [OSP
Foundry](http://ospublish.constantvzw.org/foundry/seb-font/).
