Title: Spring font OSP day
Date: 2011-03-21 21:37
Author: Pierre
Tags: Fonts, Foundry (blog), Type
Slug: spring-font-osp-day
Status: published

Spring cleaning with a long awaited operation : the release of our [OSP
DIN](http://ospublish.constantvzw.org/foundry/osp-din/).

Spring listing our current favorite fonts, in use for projects or
planned to be used :

-   [Bentham](http://www.google.com/webfonts/family?family=Bentham&subset=latin)
-   [Copse](http://www.fontsquirrel.com/fonts/copse)
-   [OSP Din
    Engshrift](http://ospublish.constantvzw.org/foundry/osp-din/)
-   [Droid Serif](http://www.fontsquirrel.com/fonts/Droid-Serif) and
    [Sans](http://www.fontsquirrel.com/fonts/Droid-Sans)
-   [Sorts Mill
    Goudy](http://www.theleagueofmoveabletype.com/fonts/6-sorts-mill-goudy)
-   [Inconsolata](http://www.fontsquirrel.com/fonts/Inconsolata)
-   [Libertine](http://www.linuxlibertine.org/)
-   [Latin
    Modern](http://www.gust.org.pl/projects/e-foundry/latin-modern/download)
-   [Molengo](http://www.google.com/webfonts/family?family=Molengo&subset=latin)
-   [NotCourierSans](http://ospublish.constantvzw.org/foundry/notcouriersans/)
-   [PT
    Serif](http://www.google.com/webfonts/list?family=PT+Serif&subset=latin)
-   [Puritan](http://www.google.com/webfonts/family?family=Puritan&subset=latin)
-   [Quicksand](http://www.fontsquirrel.com/fonts/Quicksand)
-   [Ume](http://sourceforge.jp/projects/ume-font/releases/)
-   [Univers-Else](http://ospublish.constantvzw.org/foundry/univers-else/)
-   [Vollkorn](http://www.google.com/webfonts/family?family=Vollkorn&subset=latin)

Spring switching on this blog layout, featuring our new [Sans Guilt
fonts](http://git.constantvzw.org/?p=osp.workshop.rca.git;a=snapshot;h=2279b9774d5de40fb894447827db5a59f81f3750;sf=tgz)
from the [RCA
workshop](http://ospublish.constantvzw.org/type/not-gillty). Two
versions are used, with some overlaying for the titles emphasising on
the hidden dimension of kernings. The body text rendering is provided by
the fidel Latin Modern Roman, accompanying the TeX project from the
start.
