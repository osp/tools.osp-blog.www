Title: As cool as it can be
Date: 2011-01-14 19:05
Author: Pierre_M
Tags: Fonts, Tools, Font, fonzie, ocr
Slug: as-cool-as-it-can-be
Status: published

Fonzie benefits of the preparation for the next iteration of the
(?)-annual Constant main event. With
[Tesseract](http://code.google.com/p/tesseract-ocr/) (an OCR program and
library), there's a new friend around, the first meeting with
[Potrace](http://potrace.sourceforge.net/) was quite impressive, we
can't wait to see what will happen
next.[![]({filename}/images/uploads/Screen-shot-2011-01-14-at-16.24.37.png "fonzie + tesseract"){: .alignnone .size-medium .wp-image-5592 }](http://ospublish.constantvzw.org/tools/as-cool-as-it-can-be/attachment/screen-shot-2011-01-14-at-16-24-37)

You can dowload the source code of Fonzie with Git on the osp repository
with the command

`git clone http://git.constantvzw.org/osp.git`
