Title: Colored beehive
Date: 2012-02-11 15:45
Author: OSP
Tags: Fonts, Live, Micro, News
Slug: colored-beehive
Status: published

Ana and Ricardo, our friends from
[Manufacturaindependente](http://www.manufacturaindependente.org/ "Manufacturaindependente"),
have join us at Variable house for a February residency busy with the
preparation of the [LGRU Co-position research
meeting](http://lgru.net/archives/276). In the beehive, maybe a
pre-workshop about Colorfonts?  
[![manufacturaindependente -
Bonjourvariable]({filename}/images/uploads/Bonjourvariable.png "Bonjourvariable"){: .alignleft .size-medium .wp-image-7003 }]({filename}/images/uploads/Bonjourvariable.png)
