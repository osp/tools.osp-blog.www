Title: New work on Fonts at W3C
Date: 2009-05-13 08:44
Author: Femke
Tags: Fonts, LGM 2009, Standards + Formats, Type, Watch this thread
Slug: new-work-on-fonts-at-w3c
Status: published

[Chris
Lilley](http://ospublish.constantvzw.org/conversation/even-when-you-are-done-you-are-not-done)
writes: "W3C is collecting ideas for work related to downloadable fonts
on the Web. This email summarizes the current situation, and asks for
feedback on a draft charter for a future W3C Font working group or
interest group. Please send feedback on the charter to the publicly
archived mailing list www-style@w3.org."

Read his overview and respond on the CREATE mailinglist:  
[http://lists.freedesktop.org/archives/create/2009-May/001752.html  
](http://lists.freedesktop.org/archives/create/2009-May/001752.html)

(it's interesting to see how a relatively modest event such as
[LGM](http://www.libregraphicsmeeting.org), continues to produce waves
in many related domains)
