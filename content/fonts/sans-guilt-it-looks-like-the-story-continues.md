Title: Sans Guilt, it looks like the story continues
Date: 2011-05-27 20:21
Author: OSP
Tags: Fonts, Foundry (blog), News, SansGuilt, Texts, Type
Slug: sans-guilt-it-looks-like-the-story-continues
Status: published

![]({filename}/images/uploads/letter_2.jpeg "letter_2"){: .alignnone .size-full .wp-image-6200 }
![]({filename}/images/uploads/letter_1.jpeg "letter_1"){: .alignnone .size-full .wp-image-6200 }

<small>Do you remember that we sent [a
letter](http://ospublish.constantvzw.org/typo/it-looks-like-we-are-sending-a-letter)
a few weeks ago. We are finally expecting an answer from our
correspondent and we feel it is time to reveal the content of this
letter. Feel free to react to this letter either on this blog, by mail
or on any other [type centric
sites](http://typophile.com/node/82567).</small>

========================================  
From:  
Open Source Publishing  
Rue du Fortstraat 5  
1060 Bruxelles, Belgium  
mail@ospublish.constantvzw.org

To:  
Monotype Imaging headquarters  
500 Unicorn Park Drive  
Woburn, MA USA 01801  
us@monotype.com

Brussels, 11 April 2011

Dear Monotype,

We are writing you because we just published Sans Guilt \[1\], which is
a reinterpretation of the Gill Sans released under an Open Font license.

We are OSP \[2\], a design collective based in Brussels that has been
working with Free and Open Source software since 2006.

We believe that the 71th anniversary of the death of Eric Gill implies
that his work is now in the public domain. To mark this anniversary we
decided to liberate the Gill Sans and make a free and open source
release of it.

We created three variants from different sources. One was scanned from
original drawings (Sans Guilt Drawing Based — DB), another from
hand-printed letterpress (Sans Guilt Lead Based — LB) and a third had a
digital file as a basis (Sans Guilt Monotype Based — MB). The work was
done in collaboration with students from the Royal College of Art in
London.

Our liberation of the Gill Sans raises legal and ethical questions
surrounding proprietary font software and works of typography that are
in the public domain. As designers, we can not answer those questions
theoretically so the Sans Guilt project is an attempt at finding
practical answers.

To us it seems that the central question about any font reinterpretation
is that of identifying and transforming material sources. Throughout
history fonts have always been adapted from previous renderings, often
transposing the design from one technology to another.

In order to explore this historical context and to understand how to
work with this complicated matter, we released Sans Guilt. The
DB-variant is based on original drawings by Eric Gill that we found in
the RCA library. The images were scanned and then traced using a custom
software to produce PostScript outlines. The LB-variant started from the
Gill Sans letterpress available at the RCA printshop. Students manually
printed the movable type and than scanned the result.

The third variant is possibly the most problematic. Because to us the
digital is material too, we wondered if we could use a digital source as
a starting point for liberating the Gill Sans. For the MB-variant we set
a text in a page layout software with a licensed Monotype Gill Sans.
This text was then turned into a bitmap and ran through a software that
converted each glyph back into a PostScript outline.

Although Gill Sans as a design work is in the public domain, the font
software that we used to create the material sources for our
reinterpretation is not. In the case of computer fonts there is a
blurring of boundaries between technology and visual design. By turning
the text into a bitmap image we did not reuse any part of the digital
information of the Monotype Gill Sans itself. We believe that the bitmap
is solely a representation of the design work of Eric Gill which we
consider to be in the public domain since 2010.

With the creation of Sans Guilt and by writing you this letter we are
trying to clarify the complicated legal matter around typographical
heritage, technology and intellectual property and we are looking
forward to discuss the implications with you.

Kind regards,

OSP

\[1\] http://ospublish.constantvzw.org/foundry/sans-guilt  
\[2\] http://ospublish.constantvzw.org/about
