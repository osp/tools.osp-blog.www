Title: OSP print party as bootstrap IV for the Balsa
Date: 2013-02-26 02:09
Author: Pierre
Tags: Fonts, Live, News
Slug: osp-print-party-as-bootstrap-iv-for-the-balsa
Status: published

↓  
*Hello,  
Sorry to disturb you. We are a [group of
designers](http://ospublish.constantvzw.org) working in Brussels,
Belgium with free software and we've used for 18 months the Ume fonts
for the graphical and typographic identity of [a
theatre](http://www.balsamine.be). We are preparing a
presentation/performance in which we would love to insert some details
about the Ume fonts, their history, anecdotes, technical details,
anything. We found your 4 names spread out in [the log of the Ume
font](http://sourceforge.jp/projects/ume-font/history/?limit=20&skip=640)
from 2006 till now. Maybe do you know some part, even very little, of
the of the journey of that font family?  
Many thanks in advance for any help!  
*

↱ Bootstrap IV  
Open Source Publishing  
Print Party  
28/02 - 20h30  
<http://www.balsamine.be/index.php/Saison2012-2013/BootstrapIV>

Depuis 2 ans, la caravane OSP fait l'identité typographique de la Balsa.
Depuis 7 ans, la caravane OSP fait des print parties. Dans les deux cas,
ces pratiques utilisent des outils libres qui, par leur genèse
collaborative, changent les pratiques, qui à leur tour changent les
outils. OSP invite Mme Ume, l'ingénieure japonaise dessinatrice d'une
famille de police en usage à la Balsa. Sous ses instructions, ils
rejouent les journaux intimes des opérations graphiques. Les dialogues
donnent voix aux pratiques logicielles muettes et à leur écologie
culturelle. Un bootstrap qui crochette les serrures de l'identité
balsamique en mode repeat. ↪

[![pantographe\_tablette]({filename}/images/uploads/pantographe_tablette.png){: .alignleft .size-medium .wp-image-7192 }]({filename}/images/uploads/pantographe_tablette.png)

"La plupart des auteurs contemporains (artistes, designers, cinéastes,  
auteurs, scénographes), créent leur oeuvre à l'aide d'ordinateurs. Ils  
utilisent les logiciels par défauts, les mêmes outils que tout le monde  
utilise. Imaginez que ces logiciels n'étaient pas seulement faits pour  
faire le travail, mais seraient des outils à penser, des instruments à  
développer l'imagination, des objets qui repensent le monde dont ils  
font partie?" Femke Snelting, "Toolbending", 2012  
•  
[Réservations](http://billetterie.balsamine.be/cgi?lg=fr&pag=1727&rec=42)
