Title: Year - Mousse
Date: 2011-07-21 19:43
Author: Pierre
Tags: Fonts, News, Works
Slug: year-mousse
Status: published

A few weeks ago, a [365 pages
magazine](http://www.mottodistribution.com/site/?p=15870) has been
published by [Komplot](http://www.kmplt.be/project.php) and David Evrard
in Brussels. Even if it is not strictly an OSP job (it miss, by exemple,
the necessery plural workers), it smells libre graphics around its
square ears. Another magazine, [Mousse](http://www.moussemagazine.it),
ask questions, multivoices amongst publishers and designers answer.  
\[gallery\]  
<!--more-->  
*What triggered the need to publish something like YEAR?*  
The need to make the story. Something like that. Tell the story in your
own words. When you do something like that, at a point, you can’t do
something else. No choice. And the absence of choice is, by definition,
necessity! Héhé… You know it’s just something growing on a table with a
few people are sitting around…

*The interesting feature is its apparent non-linear structure, meaning
that it can be read and enjoyed randomly. What do you think are the
advantages of a publication based such loose structure?*  
There are two ideas of time. One of these is : time is a line, and you
can highlight some points on this big, general and academic line. This
is History. This is competition. The second idea is : time is a collage
of moments, a suite, a partition. This is Stories. Using this second
idea, you can see time as different layers where time is space. You need
time to read/make ? a book. YEAR is about time. We separated the book in
12 parts like a calendar, with internal covers copying the design of
existing revues titled with months’ name… i think readers of Mousse
easily recognized the US magazine October or the French one May, for
example… A calendar is physically made out of this idea of time. The
setting is made out of layers… Layers that can be projective… No matter
what’s the first. This is not a competition.  
The 12 parts and covers refer also to what is call in French a
"recueil", a compendium that some publisher produce from all the
magazines of a year, pack in a cheap (or luxury, but contrasted from the
rest) cover et resold as a different item. So yes, it prays to be read
asynchroniously. And so the strange number of pages.

*How did you select the contributions for YEAR?*  
Just like for a surprise party… a scene as an experimental
constellation… We also invited people to curate part of the books from
their own activities : Devrim Bayar from the Wiels, Yann Chevallier from
Le Confort Moderne, Jean Paul Jacquet from La Chaussette or Margot from
Etablissement d’en face …

*Did you have to give up or compromise with the printing process and
binding? Why did you choose a yellow pages kind of paper?*  
Because it's the cheapest. No kidding. When you decide to produce a 365
pages magazine with quite nothing at the start, very few advertisers,
you're in a straight economy. Others decisions came up from that
standing point too : rotary printing press, only a quarter of the pages
in colour, the stickers on blanco cardboard. The play was then to
stretch these rough options to somewhat strange variations, with no
additional costs, only by allocating time to convince printers and
binders. So the uncut bottom of pages and gluing. About the conception
and design process, some parts has follow the same ecology of choice
with the use of free not as-in-free-beer handmade software. But only a
few, because it seems that no discipline can be relevant for some
South-West Brussels inhabitants. At the opposite side, no fonts used has
been in contact with a lawyer to write his license. And yes, you spotted
it, the main font was grabbed where it's called "a typeface for the
nations".
