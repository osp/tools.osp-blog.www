Title: Libre Fonts
Date: 2009-06-07 10:40
Author: Femke
Tags: Fonts, Libre Fonts, Terminology, Watch this thread
Slug: libre-fonts
Status: published

The [Open Font Library](http://openfontlibrary.org/) is preparing a
brand new site (an idea of what's in store:
[http://openfontlibrary.org/wiki](http://openfontlibrary.org/wiki/Main_Page))
and this sparked off an interesting discussion about terminology. How to
name fonts that are made available on the OFL site?  
<!--more-->  
***Free Fonts*** sounds nice, but risks to blur with gratis (and
non-free) fonts promoted under the same term. Others are all for using
it because even if confusing, *Freedom* is important. There are many
arguments against the use of the word ***FLOSS Fonts***: it is an
acronym, a software term (there is no consensus about whether fonts are
software), and it sounds ugly. The software-argument works against
***Free Software Fonts*** too, of course. ***Open Fonts*** mixes with
*Open Type* (which again has nothing to do with their license) and is
felt to be ambiguous and evasive. But for the same reason, others are in
favour of the term because *Open* is more inclusive and alludes to the
'open endedness' of a font or process. The current proposal is to use
***Libre Fonts***, even when it introduces an unfamiliar term to English
speakers and would need some explanation.

OSP likes this nicely awkward cross-language invention so we have
adopted the term ***Libre Fonts*** and renamed our tags. We hope it
sticks around.

Thread on the openfontlibrary list:
<http://lists.freedesktop.org/archives/openfontlibrary/2009-May/002097.html>
