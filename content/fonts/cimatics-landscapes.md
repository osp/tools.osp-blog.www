Title: Cimatics landscapes
Date: 2009-11-09 16:33
Author: ludi
Tags: Fonts, Foundry (blog), News, Type, Works, Digital drawing, Fontforge, In the pipeline, Recipe, Standards + Formats
Slug: cimatics-landscapes
Status: published

[![Cimatics-preview01]({filename}/images/uploads/Cimatics-preview01.png "Cimatics-preview01"){: .alignnone .size-full .wp-image-3560 }]({filename}/images/uploads/Cimatics-preview01.png)  
Earlier this year, we were invited to work on the identity of the
Brussels-based [Cimatics A\\V Platform](http://cimatics.com).

Cimatics is a framework for initiating and facilitating audiovisual
productions, events, publications and workshops.  
To launch the new identity, we developed a new family of fonts! We hope
you're ready for the libre dingbats attack URW Gothic L dusty noisy text
bloc jam?

We'll go more into the design process and release these fonts at two
live events later this month. Stay tuned.

☔☺☔ ✈ ❍ ✌
