Title: Pelgrimage to Pragma
Date: 2009-06-29 23:34
Author: Pierre
Tags: Tools, context, LaTex, Tools
Slug: pelgrimage-to-pragma
Status: published

**Designing with TeX: episode IV**

[![pragma]({filename}/images/uploads/pragma.jpg "Click here for more images"){: .alignnone .size-medium .wp-image-3130 }](http://ospublish.constantvzw.org/image/?level=album&id=34)

Today we drove up North to the headquarters of
[Pragma](http://www.pragma-ade.nl/) in Hasselt (NL), *La Place* from
where [ConTeXt](http://contextgarden.net/), a document markup language
and document preparation system based on TeX, is being developed. The
goal of the journey was to resolve some of the issues we encounter while
designing a multi lingual publication for
[Constant](http://www.constantvzw.org). We returned a little wiser about
ConTeXt and the things it can do well, and cannot (yet) do. We also
brought back a few working installations, a beautiful solution for image
linking, the understanding that CMYK is easy, some answers to the
problem of line-height switching plus a basic hack for multiple language
streams. This all against the usual tarif.

So, in case you were wondering what happened to our project to design
with TeX: we are still working on it!
