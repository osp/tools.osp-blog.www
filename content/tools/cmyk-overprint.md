Title: CMYK overprint
Date: 2007-06-28 14:10
Author: Femke
Tags: Tools, Inkscape, Scribus
Slug: cmyk-overprint
Status: published

Wonderful Inkscape unfortunately does not support black overprinting.
You can define colors in CMYK but it will not allow values such as 40% C
+ 40% M + 40% Y + 100% K (Inkscape for some reason automatically
converts these back to 0% C + 0% M + 0% Y + 100% K).

[![bad\_circles.jpg]({filename}/images/uploads/bad_circles.jpg)]({filename}/images/uploads/bad_circles.jpg "bad_circles.jpg")
[![good\_circles.jpg]({filename}/images/uploads/good_circles.jpg)]({filename}/images/uploads/good_circles.jpg "good_circles.jpg")

[![bad\_tiger.jpg]({filename}/images/uploads/bad_tiger.jpg)]({filename}/images/uploads/bad_tiger.jpg "bad_tiger.jpg")
[![good\_tiger.jpg]({filename}/images/uploads/good_tiger.jpg)]({filename}/images/uploads/good_tiger.jpg "good_tiger.jpg")

<small>Left: cyan plate after importing .svg in Scribus.  
Right: cyan plate after adjusting colors.</small>

But with the help of excellent Scribus, this is easy to fix. Simply
import the .svg into Scribus, and go to edit &gt; colors. All colors in
imported Inkscape illustrations, will be RGB; most likely black has
ended up as 'FromSVG\#000000', or 'Grey0'. Change this color from RGB to
CMYK and change values to 40% C + 40% M + 40% Y + 100% K. Check in print
preview and ... voilà!
