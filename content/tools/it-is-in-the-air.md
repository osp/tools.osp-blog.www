Title: It is in the air
Date: 2006-03-21 18:41
Author: Femke
Tags: Tools, Scribus
Slug: it-is-in-the-air
Status: published

Reporting bugs is frustrating work. I feel pretty stupid when a bug
apparently was already reported months ago (was it worth reporting? Am I
simply annoying developers by telling them once again something does not
work? Should I have spent even more time finding duplicates?), but at
the same time it would be worse when it was just me having an idea or
experience.  
<!--more-->  
Anyways, I am really happy to find many versions of the same issue:

-   **Differential paragraph styles**
-   **Cascading styles**
-   **Depending paragraph styles**

... it is in the air.

If Scribus could pull that off... it would make Open Source Publishing
radically different and exciting.
