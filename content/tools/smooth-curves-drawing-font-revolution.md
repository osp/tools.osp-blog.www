Title: Smooth curves drawing font revolution?
Date: 2007-05-07 00:40
Author: Harrisson
Tags: Tools, Design Samples, LGM 2007
Slug: smooth-curves-drawing-font-revolution
Status: published

[![spiro.png]({filename}/images/uploads/spiro.png)]({filename}/images/uploads/spiro.png "spiro.png")

Spiro is a toolkit for curve design, especially font design, created by
Raph Levien. It is a smooth alternative to the wide known Bézier
curves... It is VERY impressive using.  
<!--more-->  
Dave Crossland and Nicolas Spalinger
([OFL](http://scripts.sil.org/cms/scripts/page.php?site_id=nrsi&id=OFL)
– [Fontly](http://www.fontly.com)) demoed a chain of process: a script
that takes a scanned sample of fonts, contrast it, then recognises and
chops automaticaly the glyphs, and import them as background for Spiro
PPEDIT application. You can then use the spiro to draw the outlines, in
an easier and smoother way than beziers (and reducing the amount of
points). Automatisation of the work process is a terrible gain of time,
and made me dream the whole night. The spiros curves are accepted by
Fontforge, and tranformed as editable beziers curves... On linux
systems, files produced by ppedit are immediately send to the famous
font editor...

[levien.com/spiro](http://www.levien.com/spiro)

A MacOs version has just been released: enjoy the incredible souplesse
of the curves manipulation. The tensions angles seems to be very
automatised - and are difficult to stress, but I hardly tryed it... You
can't save the curves done... yet. Some more info will be released this
week!
