Title: Potrace --alphamax 1.334 (or the limit between artificial and natural)
Date: 2008-05-05 11:10
Author: Pierre
Tags: Tools, Digital drawing, Inkscape
Slug: potrace-alphamax-1334-or-the-limit-between-artificial-and-natural
Status: published

Since the first time I've used an autotrace program -Adobe Streamline
1.0 in the early nineties- I've been disappointed by the unavoidable
angles in curves, named kinks or cusps, that pledged the vector output.
Lots of designers and developers seem not to care about it, but for me
it is simply the difference between artificial shapes that scream "*I'm
a vector!*" and natural shapes where every sharp edge is a small curve
when you look really close. That kind of ultra detail may seem useless
and/or nerdy, but it can really make the difference in typography. Like
in my work where I use bitmaps as sources.

[![Perfectly aligned anchor
points]({filename}/images/uploads/screenshot-inkscape.gif "screenshot-inkscape"){: .alignleft .size-full .wp-image-457 }]({filename}/images/uploads/screenshot-inkscape.gif)So
for 17 years+, I've tried all options and all autotrace softwares I've
found for that simple but invisible holy grail function : a real smooth
aligned anchor for all curves. No way. The only cheat strategy I've
found was to produce polygons with no curves at all, then to round all
them... Sometimes ok, but really not satisfactory.

A few weeks ago, during my first real hands on Inkscape, I've made some
tests on the Potrace function. That FLOSS package will be more open to
custom settings? I founded the "smooth corners" setting, like in so much
other packages. After a few tests, with the maximum and strange value of
1.34, it's reveal to produce those precious precisely aligned anchors!
Ma-gni-fi-que.

And today, having a bunch of tif pictures to autotrace for a "regular"
"proprietary" job, I decided to try potrace more extensively and to try
to process them in batch. So I read the potrace documentation, and in a
flow of features I read with \*real\* emotion :  
*"`-a n, --alphamax n`  
set the corner threshold parameter. The default value is 1. The smaller
this value, the more sharp corners will be produced. If this parameter
is negative, then no smoothing will be performed and the output is a
polygon. **The largest useful value is 4/3 or 1.334, which suppresses
all corners and leads to completely smooth output.**"* (from
<http://potrace.sourceforge.net/potrace.1.html>)

[![Magic
alphamax=4/3]({filename}/images/uploads/screenshot-trace-bitmap.gif "Screenshot Trace Bitmap"){: .alignleft .size-full .wp-image-457 }]({filename}/images/uploads/screenshot-trace-bitmap.gif)

So the cryptic Alphamax setting, with a 4/3 value define precisely the
limit between artificial and natural in the autotrace and vector
world!...

And I immediately use it :

1.  rename to avoid spaces :  
   *`` for file in *; do mv "$file" `echo $file | sed -e 's/  */_/g' -e 's/_-_/-/g'`; done ``*
2.  convert tif files (not supported by potrace) to pbm files and insert
    "pbm-" at the beginning of the filename :  
   *`` for pic in `ls *.tif` ; do echo "converting $pic"; convert $pic pbm-$pic.pbm; done ``*  
   (it take some time : pbm files, as other portable anymap files not
    compressed, are \~30 x more heavy than tif)
3.  trace the pbm and produce eps files with "eps-" at the beginning of
    the filename :  
   *`` for pic in `ls *.pbm`; do echo "potrace $pic"; potrace --alphamax 1.334 --turdsize 2 --longcurve --turnpolicy black -o eps-$pic.eps $pic; done ``*

Even if it has me place three hours closer of my job's deadline, in this
rude white night in pure designer style, I continue to smile.
