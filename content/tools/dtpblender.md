Title: DTPblender
Date: 2006-06-04 13:25
Author: Femke
Tags: Tools
Slug: dtpblender
Status: published

[![blender
screenshot](http://ospublish.constantvzw.org/blog/wp-content/_blender.jpg "blender screenshot"){: }](http://ospublish.constantvzw.org/blog/wp-content/blender.jpg)  
A new kid on the block? Makers of 3D-modeling software Blender announce
that they have developed a "solution for fast and flexible creation of
2D graphics and layouts for web site design and print". Its interface
-no surprise - resembles Blender and other proprietary animation
packages such as Flash; the website mentions upfront that the package
does not offer output like CMYK or Postscript. Although a first quick
try is not immediately convincing (but this could be because I am not
very familiar with the strand of softwares DTPblender is based on), it
could be interesting to radically combine web design and page lay-out.
More after further testing.

More information and download here:
[http://dtpblender.instinctive.de](http://dtpblender.instinctive.de/cms/Main/Home)
