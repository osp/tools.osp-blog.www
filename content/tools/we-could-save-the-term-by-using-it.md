Title:  We could save the term by using it
Date: 2008-05-14 22:40
Author: Femke
Tags: Tools, Scribus, Vocabulary, Watch this thread
Slug: we-could-save-the-term-by-using-it
Status: published

A lively discussion about the terminology used in Scribus:  
<http://lists.scribus.info/pipermail/scribus/2008-May/028994.html>

All started with this post from Hans-Josef Heck, linking the language of
digital lay-out to that of historical printing techniques:

> "Master" is the perfect English term. The master masters a page, a
> paragraph, etc.
>
> The Webster (edition 1994) says:  
> 3: controlling the operation of other mechanism (e.g. master
> cylinder)  
> 4: establishing a standard for reference (e.g. master gauge).
>
> To use "page master" instead of "master page" stresses, what the
> function  
> is, namely "mastering".
>
> "master" means "ruling". There is no equivalent in German, I think,
> which could we used here. In German "Mutter" (mother) is a possible
> solution, which means "stems from" (e.g. Mutterbaum, Mutterpflanze).
> In the printing trade there are in the German nomenclature two terms,
> that stem from Latin "mater", (Mutter, mother):
>
> 1\. Mater = a mould for a founding patterns for printing. It was
> positive, as the printing block had to be negative.  
> 2. Matrize = a stencil, positive, the ink was pressed through or those,
> where the printing colour was imposed on the back and then used with a
> kind of alcohol for copying.
>
> These techniques are gone. Laser copies or digital (offset) printing
> we use instead. But we could save the term by using it.
