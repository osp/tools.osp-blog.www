Title: Take Survey: OSS Usability Improvement
Date: 2010-02-12 16:25
Author: OSP
Tags: Tools, Tools, Usability links
Slug: take-survey-oss-usability-improvement
Status: draft

### Evaluating OSS usability improvement from Industry Perspective

"The following survey is a part of my PhD research work to empirically
evaluate the effects of key factors listed below on the Open Source
Software (OSS) Usability. The International Organization for
Standardization and The International Electro technical Commission
ISO/IEC 9126-1 \[1\] defines usability as *The capability of the
software product to be understood learned, used and attractive to the
user, when used under specified conditions*"

Take the survey:
<http://www.kwiksurveys.com/online-survey.php?surveyID=BOHMN_8d5c35f8>

\[1\] International Standard ISO/IEC 9126-1 (2001) Software Engineering
– Product Quality – Part 1: Quality model (first edition,
2001-06-15):9-10
