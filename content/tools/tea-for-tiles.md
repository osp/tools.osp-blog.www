Title: Tea for Tiles
Date: 2007-06-06 19:19
Author: Harrisson
Tags: Tools, Works, Design Samples, How-to, Inkscape
Slug: tea-for-tiles
Status: published

Good way to practice softwares is getting jobs done for friends. This
week, Maluka, an excellent, enthusiasming and courageous organic shop
(placed at the corner of our office street, which helps!), asked us to
design them a logo and cards. Here is the proposal, using Vera Sans
Serif and Inkscape, and specifically its magic "[clone tile
tool](http://tavmjong.free.fr/INKSCAPE/MANUAL/html/Tiles.html)".  
[![picture-12.png]({filename}/images/uploads/picture-12.png)]({filename}/images/uploads/picture-12.png "picture-12.png")  
[![picture-13.png]({filename}/images/uploads/picture-13.png)]({filename}/images/uploads/picture-13.png "picture-13.png")

As works are also a good way to share knowledge, it is the good context
to show a specificity of the clone tile tool that [Cédric
Gemy](http://www.cgemy.com/) showed me in Montreal, after LGM Inkscape
presentation.  
<!--more-->  
Clone tile tool allows to duplicate an element on a specific area of the
page, using parameters such as shift, symetry, scale, rotation, blur,
transparency... and
[trace](http://tavmjong.free.fr/INKSCAPE/MANUAL/html/Tiles-Trace.html)

Example: vecto-pixelization (very handy for vinyl cuttings)  
1 Open an image (such as jpg) in inkscape. (image can also be another
element drew in inkscape, such as type!)  
2 Draw what will be the "pixel" (here, a star).  
3 Place this on the upper left corner of the image.  
[![picture-8.png]({filename}/images/uploads/picture-8.png)]({filename}/images/uploads/picture-8.png "picture-8.png")  
3 In the clone tile tool box, go to tab "trace"  
4 Select the caracteristic you want to be transcribed (here it is the
lightness of the jpg)  
[![picture-11.png]({filename}/images/uploads/picture-11.png)]({filename}/images/uploads/picture-11.png "picture-11.png")  
5 Select your motif and "create"  
[![picture-9.png]({filename}/images/uploads/picture-9.png)]({filename}/images/uploads/picture-9.png "picture-9.png")

[![picture-10.png]({filename}/images/uploads/picture-10.png)]({filename}/images/uploads/picture-10.png "picture-10.png")

This feature needs a bit of test and practice, but it's worth playing
with. Ressources are almost infinite.  
There are plenty of random settings you can set (as I did for Manuka)...

And... fasten your seat belt, here are few bonus
[tricks](http://tavmjong.free.fr/INKSCAPE/MANUAL/html/Tiles-Tricks.html)!
