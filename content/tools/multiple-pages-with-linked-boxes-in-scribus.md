Title: Multiple pages with (linked) boxes in Scribus
Date: 2008-03-12 13:31
Author: Femke
Tags: Tools, Python, Scribus
Slug: multiple-pages-with-linked-boxes-in-scribus
Status: published

Besides pagenumbers, Scribus masterpages can currently only hold static
elements. [Ivan Monroy Lopez](http://textzi.net/) wrote us a very handy
python script which puts as many linked or unlinked text boxes on as
many pages you want. You can also run the script multiple times...

[measurements.py]({filename}/images/uploads/measurements.py "measurements.py")  
[template.py]({filename}/images/uploads/template.py "template.py")

<!--more-->  
The script is split into two files: **measurements.py** and
**template.py**. **Template.py** should be run from inside Scribus (In
the main menu choose: Script &gt; Execute Script). Put info about boxes
(size, amount, page start etc.) into **measurements.py**.

This is what **measurements.py** looks like if you would want to end up
with a 22 page document with 2 linked columns of equal length on each
page:

`#the script will start inserting boxes at page start = 1 #the script will stop inserting boxes at page end = 22`  
`#the definition of text boxes follows the format: #[x-position, y-position, width, height] #x and y positions refer to top-left corner of boxes. #units are based on the default set for the document.`  
`#text boxes needed in even pages. edit as needed. even = [[0, 0, 100, 100], [100, 100, 100, 100]]`  
`#text boxes needed in odd pages. edit as needed. odd = [[0, 100, 100, 100], [100, 0, 100, 100]]`  
`#will the text boxes be linked? (yes or no) link = 'yes' #link = 'no'`

both **measurements.py** and **template.py** should be in the same
directory.
