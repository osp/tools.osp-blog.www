Title: How to turn a frog into a prince
Date: 2007-06-27 17:19
Author: Femke
Tags: Tools, Works, LGM 2007, Print Party, Printing + Publishing, Python, Scribus
Slug: how-to-turn-a-frog-into-a-prince
Status: published

[![frogprince\_th.jpg]({filename}/images/uploads/frogprince_th.jpg){: .float}](/wp-content/uploads/FrogPrince/StoryC.pdf)
**Frog and Prince** is a fairytale featuring free software, python
scripting and an open font. The story was premièred at [LGM
2007](http://www.libregraphicsmeeting.org/2007/) (Montréal, Canada), and
formed the centerpiece of the [Canadian Printing
Breakfast](http://ospublish.constantvzw.org/?p=257) (Brussels, June
2007). Below is everything you need to make the recipe, but you can also
browse through sample documents for each of the steps in the
transformation process. Ingredients are there to be opened up,
re-distributed, changed and improved upon. Please report back when you
use or abuse.

\[[Preview of the final
result](/wp-content/uploads/FrogPrince/StoryC.pdf)\]

<!--more-->

\[[download all sample documents as a .zip
file](/wp-content/uploads/FrogPrince.zip)\]

**Tools you will need**:

-   Internet connection
-   Gimp image editor
-   Inkscape vector editor
-   Terminal (command line) application
-   Scribus lay-out application
-   Python script <small>(scripted by [Ivan
    Monroy-Lopez](http://textzi.net/))</small>
-   Pdf toolset: psnbook, psnup, ps2pdf, pdftops
-   Printer
-   Stapler

**1. Prepare ingredients**

Find the Grimm story '[The Princess and the
frog](http://www.gutenberg.org/etext/20437)', available through the
Gutenberg project. Identify two sections: one telling the story of the
frog, and the other starting at the moment the frog turns into a prince.
Save the first section as
[Frog.txt](/wp-content/uploads/FrogPrince/Frog.txt), the second as
[Prince.txt](/wp-content/uploads/FrogPrince/Prince.txt).

Look for an image of a prince, and convert it to Scalable Vector Format
(svg) using Gimp, Inkscape and Potrace. Do the same for an image of a
frog.

**2. Install font**

In your home directory, create a folder called '.fonts'. The dot is
important (Make sure you have *view invisible files* selected in file
browser)! Copy
[CharterOSP-Roman.ttf](/wp-content/uploads/FrogPrince/CharterOSP-Roman.ttf)
into this folder. From the command line, run:

`$ fc-cache`

This is to update the fontconfig settings. The CharterOSP-Roman should
now be available to all applications.

**3. Prepare document**

Open Scribus (version 1.3.3.9 or lower) and create a new document, size
A5.

Import [Frog.svg](/wp-content/uploads/FrogPrince/Frog.svg) and
[Prince.svg](/wp-content/uploads/FrogPrince/Prince.svg) (File &gt;
Import &gt; Import SVG) into Scribus and place them on top of each other
on page 1. Convert both objects to a textframe (contextual menu:
*Convert to &gt; Text frame*).

Change name of the objects into respectively 'Frog' and 'Prince' (*View
&gt; Properties* and change name in X,Y,Z tab.)

Define the following 4 colors (*Edit &gt; Colors &gt; New*) and make
sure you use the exact same names:

FrogInitialFill =&gt; the color of the fill of Frog on page 1  
FrogFinalFill =&gt; the color of the fill of Frog on last page  
PrinceInitialFill =&gt; the color of the fill of Prince on page 1  
PrinceFinalFill =&gt; the color of the fill of Prince on last page

Before you can run the script, the 1-page .sla file
([FrogPrince.sla](/wp-content/uploads/FrogPrince/FrogPrince.sla)) must
be saved in the same directory as the Prince.txt and Frog.txt files. The
Python script ([Story.py](/wp-content/uploads/FrogPrince/Story.py))
should be saved in the same directory.

**4. Run script**

To run the script: *script &gt; execute* script and select
[Story.py](/wp-content/uploads/FrogPrince/Story.py)

Now a series of dialogue boxes will pop up.

The one about scale goes like this:  
0.1 == one tenth of the original size  
0.25 == one quarter of the original size  
0.5 == one half of the original size

Keep in mind that in the end, the script only shrinks objects. the
shrunk frog will appear on the last page, while the shrunk prince will
appear on the first page.

When running, the script creates the following sequence of colors:  
FrogFill2 ... FrogFilln, where n = (numberOfPages - 1)  
PrinceFill2 ... PrinceFilln, where n = (numberOfPages -1)  
so it's best not to have colors with those names in the sla file, to
avoid conflicts.

**5. Style text**

In the newly created document
[Story.sla](/wp-content/uploads/FrogPrince/Story.sla), make two new
styles, one for Prince and another for Frog. Choose OSP Charter, 6
points for FrogStyle and 11 points for PrinceStyle in order to fill out
the text over as many text boxes as possible.

Apply styles to Frog and Prince.

Export the document as pdf
([Story.pdf](/wp-content/uploads/FrogPrince/Story.pdf)); make sure to
embed all fonts. (*File &gt; Export as PDF*. In Font tab, select 'embed
all fonts')

**6. Prepare for printing**

For more detailed instructions, see [How to print a booklet in 19 easy
steps](http://ospublish.constantvzw.org/?p=90).

Use the command line, convert the .pdf document to a postscript
document:

`$ pdftops -paper match '/home/user/Desktop/Story.pdf' '/home/user/Desktop/Story.ps' `

Then arrange the order of pages for imposition:

`$ psbook -s16 '/home/user/Story.ps' '/home/user/Desktop/StoryB.ps' `

If all goes well, you will see the following:

`[16] [1] [2] [15] [14] [3] [4] [13] [12] [5] [6] [11] [10] [7] [8] [9] Wrote 16 pages, 5937633 bytes `

Make spreads, placing 2 A5's on an A4:

`$ psnup -2 -PA5 '/home/user/Desktop/StoryB.ps' '/home/user/Desktop/StoryC.ps' `

`[1] [2] [3] [4] [5] [6] [7] [8] Wrote 8 pages, 5944599 bytes `

Convert the document back to pdf:

`$ ps2pdf '/home/user/Desktop/StoryC.ps' '/home/user/Desktop/StoryC.pdf' `

**8. Print**  
\[this step has bugs in Ubuntu 7.04; other versions/systems should be al
right.\]

You can use the commandline to print first the even pages (myprinter is
the name of your printer, n is the amount of copies)

`$ lpr -P myprinter -o page-set=even -#1 infile.pdf `

Once the even pages are printed, you need to re-arrange the order of the
pages so that the first page comes last.

Put the pages upside down back in the printer

Now print the odd pages

`$ lpr -P myprinter -o page-set=odd -#1 infile.pdf `

Fold the pages from A4 to A5

Fold the stack back open and place it on the piece of cardboard with the
cover facing you.

Click open your stapler so you can staple the stack in the middle

Gently remove the stack (which is now stuck to the cardboard) and fold
the staples back in.

Sample documents:

-   [Story.ps](/wp-content/uploads/FrogPrince/Story.ps)
-   [StoryB.ps](/wp-content/uploads/FrogPrince/StoryB.ps)
-   [StoryC.ps](/wp-content/uploads/FrogPrince/StoryC.ps)
-   [StoryC.pdf](/wp-content/uploads/FrogPrince/StoryC.pdf)

