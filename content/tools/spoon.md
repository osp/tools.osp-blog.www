Title: Spoon
Date: 2011-04-29 15:28
Author: Femke
Tags: Tools, Further reading, Thoughts + ideas, Tools
Slug: spoon
Status: published

![]({filename}/images/uploads/spoon.jpg "spoon"){: .alignnone .size-full .wp-image-6323 }  
"Go into the kitchen and open the first drawer you come to and the odds
are you'll find the wooden spoon that is used to stir soups and sauces.
If this spoon is of a certain age you will see it no longer has its
original shape. It has changed, as if a piece had been cut obliquely off
the end. Part of it is missing.  
We have (though not all at once, of course) eaten the missing part mixed
up in our soup. It is continual use that has given the spoon its new
shape. This is the shape the saucepan has made by constantly rubbing
away at the spoon until it eventually shows us what shape a spoon for
stirring soup should be"

(Bruno Munari. *Design as art*, Penguin, 2009)
