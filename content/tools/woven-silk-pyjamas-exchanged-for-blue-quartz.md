Title: Woven silk pyjamas exchanged for blue quartz
Date: 2006-02-20 11:08
Author: Femke
Tags: Tools, Type, Scribus
Slug: woven-silk-pyjamas-exchanged-for-blue-quartz
Status: published

Instead of using the usual *The Quick Brown Fox jumped over the lazy
dog*, this rather absurd text is set as default in Scribus Font Preview:

[![scribus
preview](http://ospublish.constantvzw.org/blog/wp-content/_scribusPreview.png "scribus preview"){: }](http://ospublish.constantvzw.org/blog/wp-content/scribusPreview.png)

I wonder who decided to use this particular sentence, and why?  
Alternative *pangrams* to choose from:
<http://users.tinyonline.co.uk/gswithenbank/pangrams.htm>
