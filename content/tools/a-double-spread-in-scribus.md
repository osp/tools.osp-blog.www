Title: A double spread in scribus
Date: 2006-03-04 17:08
Author: Harrisson
Tags: Tools, Scribus
Slug: a-double-spread-in-scribus
Status: published

![](http://ospublish.constantvzw.org/blog/wp-content/babelbooksm.png)

Thanks to Philip May and Perl5 software that generated text, it was
possible to realise a double spread of a "Babels book". Text is composed
of the combinatory of the 26 letters of the alphabet, dot, comma and
space, as described in *Library of Babel*, in *Fictions* Borges book.
Those books are 410 page, 40 lines per page and 80 character per line.
Capitals were added in the script. Those 2 pages were then set with
Scribus.  
<!--more-->  
[See the
pdf](http://www.constant.irisnet.be/~constant/tbook/wp-content/babel_bookok.pdf)

Obviously, scribus needs a strong amelioration of its ergonomy. It took
something like 3 hours to compose those 2 pages.  
Here is a Scribus Bug Report, encounted during the exercice:

among other...

But the pdf generator seems to work ok so far, it was very easy to
export.
