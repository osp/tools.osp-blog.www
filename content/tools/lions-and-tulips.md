Title: Lions and tulips
Date: 2009-02-18 10:53
Author: Pierre
Tags: Tools, context, In the pipeline, LaTex, Tools, Under development
Slug: lions-and-tulips
Status: published

**Designing with TeX: episode II**

"*Users only need to learn a few easy-to-understand commands that
specify the logical structure of a document*". If only we had sooner
understood that user here is writer, not designer, we might have given
up earlier. *The Not So Short Introduction to LaTeX2* goes on to
explain: "*They almost never need to tinker with the actual layout of
the document*" ((The Not So Short Introduction to LATEX2 in 141 minutes;
by Tobias Oetiker; Hubert Partl, Irene Hyna and Elisabeth Schlegl;
Version 4.26, September 25, 2008)).

![tex\_lion1]({filename}/images/uploads/tex_lion1.png "tex_lion1"){: }![tulips-3001]({filename}/images/uploads/tulips-3001.png "tulips-3001"){: }  
<small>'Funny' lions (TeX and LaTeX) and digital tulips
(ConTeXt)</small>  
<!--more-->  
It is harder than we imagined, to start from scratch. How on earth does
one change a font? How to work across packages? Marking up a LaTex
document does resemble working with CSS or HTML, but only slightly. Each
command, each tag is particular to the magnificent world of LaTeX
itself. Many times we meet the paternalist humor of TeX's father, as it
has infected the whole TeX community it seems. We learn to understand
the tong-in-cheek concept of 'badness' (depending on the tension put on
hyphenated paragraphs, compiling a .tex document produces 'badness' for
each block on a scale from 0 to 10.000), and a long history of wonderful
but often incoherent layers of development, that envelope the mysterious
lasagna beauty of TeX's typographic algorithms. One day we will try to
draw you that on the
[map](http://ospublish.constantvzw.org/news/ce-soir-tonight-vanavond-shmn).

But however exciting to designers like us, LaTeX does resist anything
that shifts it's model of 'book', 'article' or 'thesis'. Fit for
academic publishing but too tight for the kind of publication we'd like
[Verbindingen/Jonctions 10](http://www.constantvzw.org/vj10) to be:
multilingual, multi-format, multi-layered. Small changes can be made
without much trouble, but major ones (try for example to combine a
custom paper size AND change the display of headers simultaneously)
explode the document beyond repair.

At the point we are ready to give up, we remember Pierre Marchand's
comment to [our earlier
post](http://ospublish.constantvzw.org/works/designing-with-latex).
Following his advice, we finally decide to try out
[ConTeXt](http://wiki.contextgarden.net), another 'macro package' that
uses the TeX engine. "*While LaTeX insulates the writer from
typographical details, ConTeXt takes a complementary approach by
providing structured interfaces for handling typography, including
extensive support for colors, backgrounds, hyperlinks, presentations,
figure-text integration, and conditional compilation*"
((http://wiki.contextgarden.net/What\_is\_ConTeXt)). This could be what
we were looking for.

ConTeXt was developed in the 1990's by a Dutch company specialised in
'Advanced Document Engineering'. They needed to produce complex
educational materials and workplace manuals and came up with this
interface to TeX. "*The development was purely driven by demand and
configurability, and this meant that we could optimize most workflows
that involved text editing*."
((http://www.tug.org/interviews/interview-files/hans-hagen.html))

However frustrating it is to re-learn yet another type of markup (even
if all two are based on the same TeX language, none of the LaTeX
commands works in ConTeXt and vice versa), many of the things that in
LaTeX we could only achieve by means of 'hack', now are built in and
readily available. There are plenty of questions, bugs and dark areas
still but we breath again.

We're in the middle now of typesetting the book we've been working on
for so long, so it is a bit early to know whether we will succeed in the
end.

To be continued!
