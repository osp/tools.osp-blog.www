Title: Unlock + collect for output
Date: 2006-12-11 02:29
Author: Femke
Tags: Tools, Inkscape
Slug: collect-for-output
Status: published

The same illustration that got us to post about [image
scripting](http://ospublish.constantvzw.org/?p=156), also brought up an
interesting discovery plus a feature/plug-in for Inkscape.

[![constant\_web20\_th.jpg]({filename}/images/uploads/constant_web20_th.jpg){: #image166}]({filename}/images/uploads/constant_web20.jpg "constant_web20.jpg"){: #image166}  
<small>Detail of illustration for Mute Magazine. [Click to view
.jpg]({filename}/images/uploads/constant_web20.jpg)
or [download complete zipped .svg file +
images](http://www.constant.irisnet.be/~constant/snelting/web4.0.zip)</small>

**Lock layer**  
It is often helpful to lock an object (in this case the glow in the
background), so that when you move things around, you do not have to
worry about whether it stays at the right place. Quickly done with
Inkscape, but unfortunately not as easily undone. The only way to
unlock, is to open up the document in an editor, look for the line
describing the locked object, and then delete the line:
`sodipidi:insensitive`. Pfff...

**Collect for output**  
Inkscape does not embed images, but links to them. It is therefore fast
to work with (especially when using many images and layers as is the
case with this illustration) but not easy when you have to send out the
file to a printer or someone else - there is no way to check whether one
of the images is missing.

Pim Snel [wrote an
extension](http://facility.lingewoud.nl/hacks/inkscape_save_as_zip/) to
collect all bitmaps, create relative links to them in the Inkscape
document and zip everything up in one go. Wonderful! A description of
how to use the script at [Jakub 'jimmac' Steiner's weblog](http://).
