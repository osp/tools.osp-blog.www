Title: What you won't get for 100$
Date: 2008-06-03 12:35
Author: Femke
Tags: Tools, Inkscape
Slug: what-you-wont-get-for-100
Status: published

On his weblog **[Infinite
Knots](http://www.bryceharrington.org/drupal/)**, Inkscape's Bryce
Harrington explains that if you want to have a feature implemented in an
open source project, offering money will probably not help. He describes
how "folks who aren't developers" challenged Inkscape developers to make
the application work on MacOSX, after first having tried doing it
themselves.

> if you want to get some feature or fix into an open source project,
> rather than offering money, have a go at it yourself. Even if it is
> well beyond your technical ability or time availability, your efforts
> may be enough to stimulate someone else to eventually have a go at it
> too. This could be a detailed procedure you followed that got close to
> working but had a fatal problem. Or a messy patch you made that
> \*should\* work but doesn't. Or it might be a list of possible
> solutions you've ruled out and why.

But what does he mean exactly by "*aren't developers*"?  
[  
http://www.bryceharrington.org/drupal/node/52](http://www.bryceharrington.org/drupal/node/52)
