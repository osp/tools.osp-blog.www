Title: A postcard from Amsterdam
Date: 2009-11-13 18:16
Author: Femke
Tags: Tools, Further reading, ideas, Tools
Slug: postcard-from-amsterdam
Status: published

![P1060804]({filename}/images/uploads/P1060804.jpg "P1060804"){: .alignright .size-medium .wp-image-3594 }  
<small>Alessandro Ludivico proudly presents the latest issue of
[Neural](http://neural.it/) with OSP-designed ad for **[By Data We
Mean](http://www.constantvzw.org/vj12)**</small>

At [a conference](http://networkcultures.org/wpmu/query/about/) in
Amsterdam, the [Ippolita collective](http://www.ippolita.net/) proposes
us to build (and use?) *convivial tools*, a method for users that
'neither want to rule nor to be ruled by the Society of the Query':

-   Detect and locate the borders of your local world
-   Decide in advance how many times / until when you will play the game
-   Mesh up different existing tools
-   Enjoy because no sacrifice is allowed (you are not representing
    anyone, you are not saving the world!)

Read more about *Tools for Conviviality* in this text by Ivan Illich:
<http://clevercycles.com/tools_for_conviviality/>
