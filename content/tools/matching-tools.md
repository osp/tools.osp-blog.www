Title: Matching Tools
Date: 2007-06-06 08:39
Author: Femke
Tags: Tools, Inkscape, LaTex
Slug: matching-tools
Status: published

[![chose1.jpg]({filename}/images/uploads/chose2.jpg)](http://typebye.com/test2.html)  
Today's challenge brought to you by indexer and typesetter [John
Culleton](http://wexfordpress.com/).

<em>These four covers were done using three different tools:

1.  Tex (context)
2.  Gimp
3.  Inkscape

Who can match covers 1 through 4 with the correct tool?</em>  
<http://typebye.com/test2.html>
