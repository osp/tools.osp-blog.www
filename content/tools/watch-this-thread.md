Title: Watch this thread: Scribus mailinglist
Date: 2006-06-17 10:47
Author: Femke
Tags: Tools, Scribus, Watch this thread
Slug: watch-this-thread
Status: published

The Scribus mailinglist is a good place to start when you want to find
out about printing, PDF, typography, color management and everything
else related to open source publishing. Developers and other users
discuss solutions to problems, but also give background information on
why certain technical constraints exist, what licensing issues arise.
The Scribus list seems exceptionally generous and has made it its policy
to welcome questions on all levels.

Some older and newer threads to watch:  
<!--more-->  
**Maintaining text with Scribus**  
Imagine you could connect your desktop publishing software to a weblog
or other content management system? Collaboratively edit, and lay out
the results in Scribus? Printing publications "on-demand" from your
database? If it would be possible to import and export correct xml
files, it would be not just large publishing houses that could do such
operations. Gregory Pittman shows his python script frameslist.py, and
explains what it is capable of and what not.  
[scribus/2006-June/thread.html\#18471](http://nashi.altmuehlnet.de/pipermail/scribus/2006-June/thread.html#18471)

**Scribus in the Art Lab**  
The use of open source tools in design education is not evident. Some
feel it would deprive students of "real life" experience with "what the
industry wants", others think it might make students more independent
and self-learning. Tutors report on their motivations, methods and
problems.  
[scribus/2006-April/thread.html\#16749](http://nashi.altmuehlnet.de/pipermail/scribus/2006-April/thread.html#16749)

**offtopic: microsoft must pay to adobe to include pdf
exportcapability**  
Sometimes issues related to desk top publishing, but not necessarily to
Scribus itself are brought up on the list. This is a good way to learn
about the politics of (design-)software. Here is the thread on PDF
export troubles between Microsoft and Adobe that I reported on last
week:  
[scribus/2006-June/thread.html\#18348](http://nashi.altmuehlnet.de/pipermail/scribus/2006-June/thread.html#18348)

**CMYK Processing in open source**  
One of the issues that is hard to overcome, and returns time and time
again on the list, is the problem of CMYK export. This is not a Scribus
issue in itself (color separation of graphic elements is no problem; it
is a feature missing from image processing softwares), but is obviously
frustrating when you are working with pictures in your document. The
reasons why and possible (future) solutions are discussed here:  
[scribus/2006-March/thread.html\#16421](http://nashi.altmuehlnet.de/pipermail/scribus/2006-March/thread.html#16421)
