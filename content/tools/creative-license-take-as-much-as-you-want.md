Title: Creative License: Take as much as you want
Date: 2007-10-16 09:18
Author: Femke
Tags: Tools, Licenses
Slug: creative-license-take-as-much-as-you-want
Status: published

Adobe's new Creative Suite is currently advertised with the slogan:
*Creative License - Take as much as you want*. Terms and Conditions
which apply when you submit a feature request or a bug report:

> You agree that by submitting your Idea, you acknowledge and agree that
> any such Idea is nonconfidential, and that Adobe has no obligation to
> return anything submitted, respond to, or confirm receipt of your
> Idea. <!--more-->You warrant that no other person or corporation has a
> property interest in the submitted Idea. You understand and
> acknowledge that Adobe may itself be developing and creating similar
> Ideas, and/or that Adobe may have received or may someday receive
> similar Ideas from others, and that existing or planned products and
> services independently developed without use of your Idea may contain
> Ideas or concepts similar or identical to those you submit. You
> acknowledge and agree that your submission shall not preclude Adobe
> from developing or acquiring such Ideas without obligation to you.
> Notwithstanding anything to the contrary herein, Adobe shall be free
> to use any Idea that you submit on a perpetual, royalty-free basis,
> for any purpose whatsoever, including use, modification, display, and
> distribution, and/or in the development, manufacture, marketing, and
> maintenance of Adobe products and services without any obligation to
> you.

<http://www.adobe.com/cfusion/mmform/index.cfm?name=wishform>
