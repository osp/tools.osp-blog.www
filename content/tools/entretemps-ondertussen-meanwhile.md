Title: \definetypeface
Date: 2009-03-14 10:42
Author: Pierre
Tags: Tools, context, In the pipeline, LaTex
Slug: entretemps-ondertussen-meanwhile
Status: published

**Designing with TeX: episode III**

Thanks to the [super active ConTeXt
mailinglist](http://archive.contextgarden.net/message/20090313.145131.0cc05256.en.html),
we are finally able to load our own fonts! And of course, once we know
how, we are almost disappointed that it is so easy to do.

if you compile [**this
file**]({filename}/images/uploads/fontsample.tex):

`\definetypeface[Libertinage][rm][Xserif][Libertinage] \setupbodyfont[Libertinage, 24pt] \starttext \input knuth \stoptext`

with this command:

`texexec --xtx fontsample.tex`

you end up with
[**this**]({filename}/images/uploads/fontsample.pdf).
