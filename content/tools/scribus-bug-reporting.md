Title: Scribus Bug Reporting
Date: 2006-02-12 18:00
Author: Femke
Tags: Tools, Scribus
Slug: scribus-bug-reporting
Status: published

New addiction: reading through the thousands of bug-reports on the
rigourously precise Scribus bugtracking system.

[![]({filename}/images/uploads/2006/02/scribusBugtrack.png){: width="500"}]({filename}/images/uploads/2006/02/scribusBugtrack.png)  
<http://bugs.scribus.net/view_all_bug_page.php>

It is somehow consoling to see those thousands of minor and major
problems scroll by. We will be adding our own reports over the coming
weeks (see below).  
<!--more-->

-   Looks like change of name in Paragraph style means as much as
    deleting that style. Should be prevented or not be the case.
-   Hardly any control over selecting blocks of text in preview mode.
    Can't figure out why.
-   'Paste' (text within story editor) is extremely slow; screen can
    freezes for up to 50 sec. before applying change.
-   Paste from other applications (Mozilla Thunderbird, gedit,
    Character Map) does not work.
-   After 'replace' has been executed, feedback should be 'Replaced x
    amount of elements' (not: 'Search Finished')
-   Application of changes through Properties is completely irregular.
    some changes have effect, some not... some changes trail behind,
    others are erased... Sometimes styles are overruled by others,
    sometimes not.
-   Font size, line height etc. are not forced when applied over
    selected text; it seems especially messy when that selection had
    different styles/sizes etc. to begin with. When multiple paragraphs
    are selected, styles are sometimes applied, sometimes not.
-   Feedback in Properties does not reflect the actual formatting of
    selected text.
-   When a block of text with more than one style applied (or having
    different sizes, colors etc.) is selected, the dialogue in
    properties would need to grey out/go blank for those specifications
    that are mixed. (and not suggest that all selected text has one
    particular size, color)
-   It is too risky to only be able to check the result on screen (or
    scanning the text word by word).
-   'Wordstyles' should be dominant over Paragraph styles and a
    paragraph style applied- now if f.e. a line return is removed in a
    paragraph on which a paragraph style applies, 'wordstyles'
    are removed.
-   Changes applied through story editor seem to be more consistently
    applied but hard to handle because of visual feedback lacking. Also
    it is not possible to change line-height from there.
-   When a paragraph style is removed under Menu&gt;Edit&gt;Paragraph
    styles, it remains available under Properties&gt;Style (Confusing
    because there is no way to check or correct these styles; they do
    not exist anymore. Or do they?)
-   'No styles' produces a different effect every time. It should simply
    REMOVE ALL STYLES and set text to 'default style'
-   Rendering of underlined text is very poor (line is extremely heavy)
-   When ALL text is selected in a linked text box, ALL text should
    be selected... Now sometimes the whole text is made active (Copy)
    and sometimes not; there is no feedback available to find this out.
-   'UNDO' does often not work; never for text corrections/changes (in
    Preview nor Story Editor); also not after Select all&gt;Delete; also
    not after changing specifications in Properties.
-   Line spacing can not be changed per paragraph from
    Properties&gt;Line Spacing (it changes line spacing in other
    paragraphs around it too)
-   Outlined texts: not possible to select outline color?

