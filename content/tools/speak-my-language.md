Title: Speak my language
Date: 2010-07-06 15:30
Author: Alexandre
Tags: Tools
Slug: speak-my-language
Status: published

A couple of  days ago I was looking at the OSP blog trackback list and
discovered the [Fork/Memo
blog](http://www.forkable.eu/memo/ "Forkable - Memo").  It documents the
work of Lafkon studio. Lafkon is Christoph Haag and Benjamin Stephan --
two graphic designers working with unconventional tools for graphic
design, including Latex and shell scripts among others Unix commands, to
make generative designs. With the Unix philosophy in mind, they show how
written language -- be it found in the graphic resources coded in plain
text (SVG), the scripts, and even in the filenames --  can be used as a
powerful interface/means to produce graphics.

\[caption id="" align="alignnone" width="352" caption="One of the Linux
Audio Conference 2008 posters"\][![One of the Linux Audio Conference
2008
posters](http://www.forkable.eu/generators/lacmachine/o/non-free/A3/plakatlac2008_A3_05.gif "One of the Linux Audio Conference 2008 posters"){: }](http://www.forkable.eu/memo/)\[/caption\]

\[caption id="attachment\_4706" align="alignnone" width="400"
caption="Individual SVG files get connected thanks to their
filename"\][![in
out]({filename}/images/uploads/inout.png "in out"){: }](http://ospublish.constantvzw.org/tools/speak-my-language/attachment/inout)\[/caption\]

The Icing on the cake: all the material they produce is released under
copyleft licenses!

<http://www.forkable.eu/memo/>  
<http://www.forkable.eu/>  
<http://www.lafkon.net/>
