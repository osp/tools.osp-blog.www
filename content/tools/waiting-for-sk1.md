Title: Waiting for SK1
Date: 2008-05-10 08:47
Author: nicolas
Tags: Tools, LGM 2008, sK1
Slug: waiting-for-sk1
Status: published

[![]({filename}/images/uploads/p1010817.jpg "p1010817"){: }]({filename}/images/uploads/p1010817.jpg)
[![]({filename}/images/uploads/igor2.jpg "igor2"){: }]({filename}/images/uploads/igor2.jpg)

After the presentation of [Igor Novikov](?p=245) about the new version
of [sk1](http://sk1project.org), the OSP team can't wait. It is promised
to be published for June, in the meantime we installed the current
version and are testing our svgs with it.

To install the current version on ubuntu:

grab the three .deb files from the [products
page](http://sk1project.org/modules.php?name=Products), then in a
terminal, run:  
`sudo  dpkg -i tcl8.5_8.5.0-2ubuntu10_i386.deb sudo  dpkg -i tk8.5_8.5.0-3ubuntu10_i386.deb`  
via synaptic, add the packages:  
python-imaging  
python-imaging-tk  
python-liblcms

and then,

`sudo dpkg -i sk1_0.9.0-rev335-1ubuntu10_i386.deb`  
Complementary instructions for other platforms are available on [the
project's website](http://sk1project.org/modules.php?name=Products).
