Title: Importing Inkscape in Fontforge
Date: 2007-10-20 11:03
Author: Harrisson
Tags: Tools, Fontforge, Inkscape
Slug: import-inkscape-in-fontforge
Status: published

[![a.png]({filename}/images/uploads/a.png)]({filename}/images/uploads/a.png "a.png")

Fontforge is an ideal font design program, as far we could test it out:  
Opening mac fonts (on ppc here) is more direct, and drawing tools are
really ok to take on. Soft seems more fluid than previous version and...
it can easely import inkscape svgs!  
<!--more-->  
Here is a way to make correspond inkscape size of document and fontforge
size of font. This means you don't have to resize your imported svgs,
they would pop up at the right place in FontForge when opened. Follow
those steps:

By default, fontforge glyph dimension box is 1000 x 1000 postscript
units.  
The baseline line is set at 0pt.  
ascenders goes up to 800pt  
and descender down to 200pt.

In Inkscape, create a new document  
In `document properties`, set all your units in inkscape in pixels
(px).  
Set the document dimension to 1000px x 1000px  
Set an horizontal guide at 200px  
Draw your letter.  
Save it on svg.

Import it on fontforge, selecting SVG in the file import menu box.  
Dimensions an placement should be the same as the ones in the Inkscape
document.

Super!
