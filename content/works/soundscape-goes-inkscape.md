Title: Soundscape goes Inkscape
Date: 2007-04-27 14:22
Author: Harrisson
Tags: Works, Design Samples, Inkscape, Music, Printing + Publishing
Slug: soundscape-goes-inkscape
Status: published

The new maxi vynil from [Odessa](http://odessa-music.be/) is about to be
send to production. We worked on the label design yesterday and
inkscape's "clone generator" feature did the difference. It was the
perfect tool to render an electronic disco ball - and type, evoking the
minimal dance pop of the music and the complexity of the sound
production and composition. Hopefully soon to be heard at your local
dance club!

[![donna\_web.png]({filename}/images/uploads/donna_web.png)]({filename}/images/uploads/donna_web.png "donna_web.png")

[![donna\_disco\_web.png]({filename}/images/uploads/donna_disco_web.png)]({filename}/images/uploads/donna_disco_web.png "donna_disco_web.png")
