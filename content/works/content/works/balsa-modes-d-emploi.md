Title: Balsa modes d'emploi
Date: 2018-07-02 21:07
Author: OSP
Tags: Balsa, Spiro, Html2print
Slug: balsa-modes-d-emploi
Status: published

Septième saison associés avec le Théatre de la Balsamine! 
Et pour celle-ci, sous le signe obligé mais d'une certaine jouissance, l'arte povera numérique d'OSP en auteur visuel et à l'écriture de modes d'emploi. 
Déplier une table pliante, bricoler un piège à mouche, tenir les voiles pendant la tempête, nouer ses lacets avec un dead knot, 
faire un cunnilingus, construire une barricade... Le tout en courbes spiro torréfiées bruit imagemagick.

![]({filename}/images/uploads/economies-skip.jpg "Entre autres références, *Les économies au féminin*, Skip, 1980")
↑ Entre autres références, *Les économies au féminin*, Skip, 1980

Le poster dans le métro en prémices Inkscape, avec son historique fournis de sept saisons de logos.
![]({filename}/images/uploads/balsa-18-19-metro.jpg "L'affiche de saison, métro Madou")

Le programme-pass en A6, avec html2print et deux couleurs spot décalées. À voir aussi en pdf [les 64 pages avec les couleurs spots avant traitement](https://cloud.osp.kitchen/s/TkJBATZQf6Dwgw4) ou [la couche du noir des 64 pages après traitement](https://cloud.osp.kitchen/s/aGGr3S6dsSn4d9J).

![]({filename}/images/uploads/balsa-18-19-pass01.jpg "")
![]({filename}/images/uploads/balsa-18-19-pass02.jpg "")
![]({filename}/images/uploads/balsa-18-19-pass05.jpg "")
![]({filename}/images/uploads/balsa-18-19-pass06.jpg "")
![]({filename}/images/uploads/balsa-18-19-pass07.jpg "")

L'affiche A0 en version unique sur le totem à côté de la Balsa.

![]({filename}/images/uploads/balsa-18-19-totem.jpg "")

Les premiers flyers qui inaugurent la gamme de couleur bien bam.

![]({filename}/images/uploads/balsa-18-19-flyer01.jpg "")
![]({filename}/images/uploads/balsa-18-19-flyer02.jpg "")

Et le site web installé la saison passée, [balsamine.be/#saison-1819](https://balsamine.be/#saison-1819) complètement remoulu en spot colors et timeline acide sucrée.

![]({filename}/images/uploads/balsa-site-18-19.png "")

