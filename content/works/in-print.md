Title: In Print
Date: 2007-01-03 11:48
Author: Femke
Tags: Works, Scribus
Slug: in-print
Status: published

[![p1000114.JPG]({filename}/images/uploads/p1000114.JPG){: #image171}]({filename}/images/uploads/p1000114.JPG "p1000114.JPG"){: #image171}[![p1000116.JPG]({filename}/images/uploads/p1000116.JPG){: #image171}]({filename}/images/uploads/p1000116.JPG "p1000116.JPG"){: #image171}  
<small>Contribution by [De Geuzen](http://www.geuzen.org) in [Open
magazine](http://www.skor.nl/article-2883-en.html) about *Travail
Mobile*, a workshop for
[Digitales](http://www.stormy-weather.be/digitales-2006/?lang=en).</small>

After the[Crash Test-post](http://ospublish.constantvzw.org/?p=127) a
few month ago, finally pictures of the result!

Thanks to Peter Linnell (one of the main Scribus developers), who after
I posted about some [Postscript
troubles](http://nashi.altmuehlnet.de/pipermail/scribus/2006-September/019932.html)
to the Scribus mailinglist, came to the rescue on IRC.
