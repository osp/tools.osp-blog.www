Title: Towards #2 is out
Date: 2008-06-25 13:20
Author: Pierre
Tags: Works, Inkscape, Printing + Publishing, SVG
Slug: towards-2-is-out
Status: published

[![]({filename}/images/uploads/towards2cov.jpg "Towards #2 cover"){: .alignnone .size-full .wp-image-546 }]({filename}/images/uploads/towards2cov.jpg)

[Towards](http://www.towards.be) is a cartographic project close to OSP
and the second publication is [finally out and
available](http://www.towards.be/site/spip.php?article272) after a long
9 months of preparation. Following our informations, it's the last one
that will be produced using proprietary software ;)... But the timeline
present on the cover and backcover, made from all the posts written
since the beginning of the Toward’s website, is already manufactured
with an open smile. It has been generated programmatically : a php
script has pulled all the information from the database and produced an
svg file. A shell script has spidered all the images attached to the
posts. All these elements have been glued together using inkscape. The
positioning of the blocks have been finalized by hand and with the help
of the inkscape connectors. More info and download the code on
[www.towards.be/code](http://www.towards.be/code).
