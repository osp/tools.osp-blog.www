Title: The Adventure Continues
Date: 2007-11-11 20:05
Author: Femke
Tags: Works, Batik, Design Samples, Inkscape, SVG
Slug: the-adventure-continues
Status: published

*(A post for readers with some F/LOSS stamina)*  
<!--more-->  
After producing a [flyer for
VJ10](http://ospublish.constantvzw.org/?p=335) in Inkscape without too
much trouble, we confidently decide to do a poster next. With [a three
week programme of lectures, workshops and
installations](http://www.constantvzw.com/vj10) following four
interwoven themes, scattered over five locations and of course
translated in three languages (Dutch, French and English), VJ10 might
not be the easiest event to fit on a poster but Harrisson courageously
dives in.

![]({filename}/images/uploads/goodrender.jpg){: .float}Experimenting
with the spray paint tool he soon produces a playful image mixing and
mapping the complicated Festival program. But than the trouble begins.

Finalising corrections and inevitable changes, it becomes harder and
harder to navigate the document in Inkscape until we can only view it in
preview mode. We also learn to split text and image into separate layers
and work most of the time with the image layer switched off.

To be able to check the document, we need a full size print and this we
manage with the help of
[poster](http://ospublish.constantvzw.org/?p=346) (although our colour
printer chokes on the resulting 450 MB document).

![]({filename}/images/uploads/badrender.jpg){: .float}Our
solution to [produce PDF's with the help of the Apache Batik
library](http://ospublish.constantvzw.org/?p=335) is for some reason
failing, so we tile the document on the basis of a poorly rendered EPS.
In itself not such a problem but we know to expect problems in a later
stage.

The problem with Batik originates in the 'flowroot' element that has
been added in the SVG specification 1.2. Inkscape incorrectly refers to
version 1.0 and Nicolas figures out that when you open the Inkscape
document in a text editor and change the reference to SVG 1.0, Batik
stops complaining.

[![]({filename}/images/uploads/inkscapeheader.jpg)]({filename}/images/uploads/inkscapeheader.jpg)

But... the problem itself does not go away - the resulting PDF contains
black blocks in the place of text.

![]({filename}/images/uploads/blackblocks.jpg)

We figure that 'flowroot' (text that is in a 'dynamic' textbox) is still
the problem so we manually select each textbox and un-flow it.
Converting the text to outline would also do the job but both solutions
create a new problem. We have used colours to distinguish French, Dutch
and English texts from each other but when different colors/languages
placed in the same text frame are converted, they end up having only one
single color.

![]({filename}/images/uploads/unflow_before.jpg)</a>
[![]({filename}/images/uploads/unflow_result.jpg)]({filename}/images/uploads/unflow_result.jpg)
[]({filename}/images/uploads/unflow_before.jpg)[![]({filename}/images/uploads/ttext_after.jpg)]({filename}/images/uploads/ttext_after.jpg)

We decide to give up on colour coding (it would mean to split all
languages into separate textboxes - this is too much work and too risky
with so little time left for checking and corrections), convert all text
to black and mark the transitions with hearts.

![]({filename}/images/uploads/hearts.jpg)

Now we have a document with no 'flowroot' left, we still need to convert
the SVG to PDF. With the size of this document (600 x 840 mm) Batik
starts to run out of memory.

After trying to open the background image in Gimp, importing the whole
.svg in Scribus, exporting the background as bitmap from Inkscape and
even considering to piece screenshots of the blurs together...

Nicolas helps us out again and explains how we can assign more memory to
Java so we can rasterize the file.

`java -Xmx1152M -jar batik-rasterizer.jar -m application/pdf poster_vjx_verso.svg`

The computers we normally work on, have not enough memory for this so we
decide to descend to Constant's cellar and work on the powerful dual
core machine that has been custom built by the [Open Source Video
team](http://osvideo.constantvzw.org) to process video files.

We have forgotten to switch on the image layer in the .svg file so we
need to open the document in Inkscape first before we can pass it on to
Batik. We quickly install Inkscape on this machine through the Synaptic
package manager but unfortunately it defaults to version 0.44 which does
not handle blur/transparency at all. The Inkscape website offers Linux
Autopackages of version 0.45.1 but these fail to install on this machine
(this can be the result of the specific set up of the system which is
optimized for video use) and we are getting a bit desperate.

In the mean time, Nicolas has managed to convert the file on his machine
and has uploaded it to a webserver. The verso of the poster is luckily
converted quickly on this fast machine and we are finally ready to hand
our files to the printer the next morning.

The printer experiences problems trying to output the file on high
resolution, and explains us that this is caused by our 'non standard
PDF'. It is hard to be sure whether that is true, or whether we have
simply handed in a non-Adobe PDF but in the end he decides to open the
file in Photoshop and RIP it from there... :-(

On Friday morning, we receive an e-mail from our printer. He basically
tells us that there will be a two day delay (this is partially because
of the unconventional folding we chose) but we are also asked to pay an
additional 60 euros for having our PDF opened in Photoshop.

The proof we receive on Friday afternoon shows a reasonably
well-rendered image, with one surprising detail: the size of the poster
has decreased by 98%; and as a result the outer margins are too large.

Another proof will be made with the mistakes corrected and we're
expected to do a last check tomorrow morning at 08:00. The adventure
continues...
