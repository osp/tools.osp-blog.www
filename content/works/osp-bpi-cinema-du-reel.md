Title: OSP + BPI = Cinéma du réel
Date: 2009-03-18 17:49
Author: Harrisson
Tags: Works, Inkscape, Printing + Publishing, SVG
Slug: osp-bpi-cinema-du-reel
Status: published

Carte du réel

[![4002]({filename}/images/uploads/40021.png "4002"){: .alignleft .size-full .wp-image-2202}]({filename}/images/uploads/4002.png)

OSP has been sollicitated by documentary [Cinéma du
Réel](http://www.cinereel.org/index.php?lang=en) festival to construct a
map for an alternative reading of the movie selection.  
<!--more-->  
Though book-like brochure is the most efficient and practical way to
organise complex information of a 3 day with more than 100
documentaries, gathered in 7 different fields, this form seemed too
restrictive for its director, Javier Paquer.

The linearity of pages goes against the idea that those movies have
complex and multiple relations between them, and programmation is
articulated in contexts wich need to be expressed in a way that audience
could do their own choice as if they were travelling in a heteroclite
thought meaningfull environment.

For that he asked OSP to graphically interprete this diversity and
homogeneity...

**Modus operandi**

We worked in a very close relation, and the map is more done by its
director than us.  
General image is based on a famous image from Chris Marker's “La Jetée”
movie.  
![jetee\_master]({filename}/images/uploads/jetee_master.jpeg "jetee_master"){: .alignleft .size-full .wp-image-2202}

The map reflects also a current situation of movies / documentaries /
television / cinema situation. It offers a subjective interpretation
mixing production and context.  
A pleasure to realise, especially with it's director humor.

Moreover, spending 2 weeks on inkscape was a pleasure we didn't
expected. Program is stable and pretty much reliable. If you don't go to
far in the special features (types on curves, clones...) you're ready
for print. And don't forget to vectorise ALL the fonts!

This deep breathe ino inkscape also shown the importance of a
collaboration between users and developpers. It's funny to find stitches
in interface, or counter intuitive logic when you're using the software.

For that, we'll have few suggestions for the inkscape board...

Most interesting features we used were around the clone functions.
Drawing one pictogram, cloning it all around the map and modify this
whole by just changing the mother clone is very effective.

Those special features are delicate to handle if you move out of
Inkscape. This clone feature react strangelly when svg are imported in
other programs.

**The CYMK Elephant**

Scribus is necessary to get files ready for printing. We imported the
inkscape files as SVGs, and converted colors into CYMK manually.
(Something should be done at that stage). PDFs looked good. Print
culture is not that developped in FLOSS world, but it is very
frustrating to see how archaic methods of conversions we had to invent
just to get a basic quadrichromical document. This means last minutes
corrections are compliquated, as everything is turned into paths and
ungrouped.

The biggest threat was to obtain overprint of the black color. Drawing
and lines are so thin that we couldn't dare to print in "réserve"
(traduction needed)

To avoid it, we did 2 inkscape files: one with Cyan Magenta and Yellow,
and separate one with Black. We imported those 2 files in scribus to
convert RGB inkscape file to CYMK (manually). Parisian graphic designer
“Dasein”, on charge of the brochure, gathered the films.

Check were stressy, due to the fact clones, groups, and “types on
curves” reacted very lunatiquelly. Blurs and transparencies have to be
avoided. It just don't work once exported, or with heavy and risky file
treatment.

PDF is pleasantly light. 2.6 Mega for this impressive 440 x 550 mm
document.

This map is integrated in the general programm brochure, and is
re-interpreted in large scale in Centre Georges Pompidou, where the
festival will occurs.
