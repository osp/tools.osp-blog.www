Title: Archipel is under construction
Date: 2010-05-18 20:08
Author: Harrisson
Tags: Works
Slug: archipel-is-under-construction
Status: published

![]({filename}/images/uploads/g4619.png){: .alignleft .size-full .wp-image-4500}

An organology of contemporary musics  
"Archipel is a navigation on emotions and their history through
sensorial zones."

[La Mediatheque](http://www.lamediatheque.be) asked OSP for a full scale
project: a new section called Archipel. It is an honour for us to
collaborate with this gigantic library of media institution.  
Archipel will be a serie of fictional islands where supports will be
mixed and organised through sensitive keywords and subjective browsing.  
OSP team has on charge graphics for its visualisation, identity,
graphics, web consultation and on site installation.

The website deals with complex database and break through
[SVG](http://automatist.org/blog/?p=183) interface, where [Michael
Murtaugh](http://www.automatist.org/) seems to get fun.  
Furnitures are also part of the project, And [Mathieux
Gabiot](http://www.mathieu-g.be/) leads this new design dimension of
open source publishing - we're currently working on a GPL-based license
for 3d objects.  
After an draft installation at [BPI](http://www.cinemadureel.org/)
during Cinema Du Reel 2010 festival in Beaubourg last month, public
launch will occur this fall.

More to come soon!
