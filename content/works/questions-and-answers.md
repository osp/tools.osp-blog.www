Title: Questions and answers [update]
Date: 2007-07-16 10:38
Author: Femke
Tags: Works, Scribus
Slug: questions-and-answers
Status: published

[![muteosp.jpg]({filename}/images/uploads/muteosp.jpg){: .float}]({filename}/images/uploads/muteosp.jpg "muteosp.jpg")

Over the last few months, the OSP and Mute team have established a new
design and production workflow for [Mute
magazine](http://www.metamute.org), using Open Source tools. Below are a
few of the questions we are encountering during the last stages of this
process.

<div class="clear">

</div>

<!--more-->  
**------------------------------------------------------------**  
**Mute 2 6 – Questions and answers**  
**------------------------------------------------------------**  
Storing images, should they always be kept in the same place, at present
we've made a folder for each Scribus document and then I sit an images
sub directory, this way I can move the whole set of files around without
risking losing images?

> Good idea; yes this procedure is necessary (just like old versions of
> QuarkXpress).

Fonts between OS WinXP/OSX/Linux are not being recognised in ODT or SLA.
Linux if OK but WinXP and OSX (check version numbers of Scribus) have
problems. Could this be a font TTF issue?

> What do you mean 'between'? And what are the problems exactly? (hard
> to test / recognize the problems for us because we do not have a
> Windows machine available)

On the biographies, black boxes and the title boxes, how should we bring
in the pointed angled graphics into the document?

> Re-make your basic shape in Inkscape, save as SVG and import into
> Scribus (File -&gt; Import -&gt; Import SVG). SVG's can be altered in
> Scribus.

How do we export text and second how to we make PDFs text machine
readable?

> We wished we had thought of this beforehand... ~~Scribus automatically
> outlines \*all\* .ttf fonts to outlines, i.e. renders them machine
> un-readable (Subscription pages etc. have already been converted to
> outlines btw. so there we don't have any choice).~~\*
>
> For the next issue, we will need to choose our fonts based on this,
> but testing is needed because Type1 fonts with problems (a missing
> glyph for example) might end up as outlines as well.
>
> Even if the font was \*not\* converted to outlines, the exported PDF's
> still need to undergo some treatment, but that in itself is easy to do
> with the help of our friends pdftops and ps2pdf.  
> (read [this
> thread](http://nashi.altmuehlnet.de/pipermail/scribus/2007-May/024023.html))
>
> Another option, is to extract text from the .sla; which seems a bit
> archaic ... although interesting with respect to web-to-print.
>
> \* Update: apparently the problem was in the fontfile itself. After
> using another version, the problem dissappeared.

How do we output with and without Crop Marks?

> Save as PDF &gt; check: Clip to page margins (= without cropmarks)  
> Save as PDF &gt; uncheck: Clip to page margins (= with cropmarks)

How do we join PDF pages together for the book block?

> Use pdftk (command line tool):
>
> `$ pdftk inputA.pdf inputBpdf output combined.pdf`
>
> EXAMPLE:
>
> f`snelting@station:~/Desktop/articles$ pdftk '/home/fsnelting/Desktop/articles/0005.pdf' '/home/fsnelting/Desktop/articles/0004.pdf' '/home/fsnelting/Desktop/articles/0003.pdf' '/home/fsnelting/Desktop/articles/0002.pdf' '/home/fsnelting/Desktop/articles/0001.pdf' output combined.pdf`
>
> More ideas/ways of using pdftk: <http://www.pdfhacks.com/pdftk/>, or
> see pdftk manual - you can take specific pages from certain documents,
> re-arrange order etc. etc.

How can we later replace specific pages?

> Repeat the above recipe with the new pages instead (you simply
> re-create the whole package).

How to crop an area from the cover and add it as a page to the book
block PDF file?

> Ehm... how would you do this otherwise? Our guess: Edit -&gt; Document
> setup -&gt; change to final cover size; make sure to check 'apply size
> setting to all pages'. Than re-position artwork, and export as pdf.
> Add to pdftk ingredients (see above) and merge with other pages.

How do we check and apply leading to the SLA files?

> Leading can be applied through styles or in the Properties box
> (Windows -&gt; Properties -&gt; Texttab), but surprisingly not in the
> text editor. The properties box is a bit tricky to handle because you
> will never be sure whether the displayed leading applies to a single
> frame or to multiple frames, and when you re-apply a leading here, it
> will overrule the leading applied in styles. Best is to trust styles +
> check visually with view -&gt; view baseline grid.

How do we update folios and page numbers?

> On each article: Document setup -&gt; Sections -&gt; change start
> number.
