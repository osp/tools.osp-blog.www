Title: Inkscape Poster
Date: 2006-02-22 23:41
Author: Harrisson
Tags: Works, Design Samples, Inkscape
Slug: inkscape-poster
Status: published

[![stein
poster](http://ospublish.constantvzw.org/blog/wp-content/_poster_stein.jpg "stein poster"){: }](http://ospublish.constantvzw.org/blog/wp-content/poster_stein.jpg)

*Poster done with Inkscape, the context is the visit of Bob Stein at the
Jan Van Eyck Academie*

Thursday 23 February, 15:00  
The Jan Van Eyck Academie  
kindly invites you to:  
**The Tomorrow Book**  
Robert Stein presentation
