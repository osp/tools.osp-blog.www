Title: Out of the veil
Date: 2007-04-28 11:05
Author: Harrisson
Tags: Works, Design Samples, Printing + Publishing
Slug: out-of-the-veil
Status: published

250 cm2 of printed matters OSP visit card have been delivered.  
They look bright! The file delivered to the printer was a PDF from
inkscape.  
It seems that the SVG suffered of a RGB to CYMK conversion which altered
a bit the colors. All colors were based on 100% of magenta and/or cyan
and/or yellow and/or black. And it's not not interesting.

[![p1000836.png]({filename}/images/uploads/p1000836.png)]({filename}/images/uploads/p1000836.png "p1000836.png")

[![p1000837.png]({filename}/images/uploads/p1000837.png)]({filename}/images/uploads/p1000837.png "p1000837.png")
