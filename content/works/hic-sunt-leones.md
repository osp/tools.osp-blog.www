Title: Hic Sunt Leones
Date: 2009-03-20 22:20
Author: Femke
Tags: Works, Code, Collaborative, Web
Slug: hic-sunt-leones
Status: published

... and for the same [Cinema du Réel](http://www.cinereel.org/)
festival, OSP worked with [Michael Murtaugh](http://www.automatist.org)
on HIC SUNT LEONES, a collective slideshow of on-line visual
contributions gathered during the festival.

[![capture-wwwhicsuntleonesfr-mozilla-firefox1]({filename}/images/uploads/capture-wwwhicsuntleonesfr-mozilla-firefox1-400x50.png "capture-wwwhicsuntleonesfr-mozilla-firefox1"){: .alignright .size-medium .wp-image-2243 width="400" height="50"}]({filename}/images/uploads/capture-wwwhicsuntleonesfr-mozilla-firefox1.png)  
<!--more-->  
Javier Packer, director of the festival, describes the project as
follows: "*HIC SUNT LEONES means 'Here are lions' in Latin. Romans used
to write this on maps over unexplored territories suggesting unknown
dangers could lie there. What does 'Cinéma du Réel' mean to you? What
images of the world would we like to share with others in this
improbable and undefined community created by the user of the project?
What uncharted lands? What dangers?*"

<http://www.hicsuntleones.fr>

Contributors can choose at what point they want to insert their upload,
and as a result images start to respond to each other, make and break
groups of images, build a narrative. The slideshow was screened in the
festival's public space.

The playlist-code is available here:
<http://activearchives.org/wiki/Software>
