Title: Making a living
Date: 2011-05-09 22:37
Author: OSP
Tags: Works, Crickx, LGM 2011, libre typography, Type, univers else
Slug: making-a-living
Status: published

[![]({filename}/images/uploads/makingaliving_pink-77x100.jpg "makingaliving_pink"){: .alignleft .size-thumbnail .wp-image-6376 width="77" height="100"}]({filename}/images/uploads/makingaliving_pink.jpg)
[![]({filename}/images/uploads/makingaliving_green-77x100.jpg "makingaliving_green"){: .alignleft .size-thumbnail .wp-image-6376 width="77" height="100"}]({filename}/images/uploads/makingaliving_green.jpg)
[![]({filename}/images/uploads/makingaliving_turq-77x100.jpg "makingaliving_turq"){: .alignleft .size-thumbnail .wp-image-6376 width="77" height="100"}]({filename}/images/uploads/makingaliving_turq.jpg)
[![]({filename}/images/uploads/makingaliving_blue-77x100.jpg "makingaliving_blue"){: .alignleft .size-thumbnail .wp-image-6376 width="77" height="100"}]({filename}/images/uploads/makingaliving_blue.jpg)  
[makingaliving.pdf]({filename}/images/uploads/makingaliving.pdf)  
<small>OSP contribution for the LGM Tools-exhibit in
[StudioXX](http://www.studioxx.org/), Montreal.</small>
