Title: In the pipeline
Date: 2008-04-16 12:22
Author: OSP
Tags: Works, Inkscape, Scribus
Slug: in-the-pipeline
Status: published

[![]({filename}/images/uploads/cover_bam_5_quadripdf.png "cover_bam_5_quadripdf"){: .float }]({filename}/images/uploads/cover_bam_5_quadripdf.png)
[![]({filename}/images/uploads/identiteit.jpg "cover_bam_5_quadripdf"){: .float }]({filename}/images/uploads/identiteit.jpg)

With \* new \* OSP [Yi Jiang](http://www.taidangao.org/) we are
currently working on typography and lay-out for *CROSS-over: Kunst,
Media en Design in Vlaanderen*, a publication edited by Liesbeth
Huybrechts and published by [BAM](http://www.bamart.be/) / Lannoo.

[![]({filename}/images/uploads/flowchart.png "flowchart"){: .float }]({filename}/images/uploads/flowchart.png)
[![]({filename}/images/uploads/brussel.png "flowchart"){: .float }]({filename}/images/uploads/brussel.png)

The book is layed-out in Scribus and contains various database
visualisations generated in Inkscape (in collaboration with [Michael
Murtaugh](http://www.automatist.org)). It will go to print by the end of
this month.
