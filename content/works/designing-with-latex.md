Title: Designing with LaTeX
Date: 2008-10-28 09:28
Author: OSP
Tags: Works, In the pipeline, LaTex
Slug: designing-with-latex
Status: published

**Designing with TeX: episode I**

![]({filename}/images/uploads/vj101.jpg "vj101"){: .float }OSP is currently working on a book containing
essays, interviews and images following from the Constant
festival/seminar [**Tracks in electronic
fields**](http://data.constantvzw.org/site/spip.php?rubrique46) and we
are laying it out in [LaTeX](http://www.latex-project.org/intro.html)!

LaTeX is a tool developed in the context of academic publishing,
specifically for technical or scientific documents. It combines markup à
la CSS with powerful algorithms automatizing widow / orphan control and
intelligent image placement.

We've grown more and more interested in LaTeX because it is specifically
developed for typographically sane paper publications, and as far as you
can get from regular canvas-based typesetting. We are also curious to
experience from up close the sophisticated typographic system developed
by [Donald Knuth](http://www-cs-faculty.stanford.edu/~knuth/).  
![]({filename}/images/uploads/fetchphp.png "fetchphp"){: .float }  
How will we be able to design with LaTeX? Will we need to change our
workflow? Space for experiment and playing around? Can we use the design
patterns built in to the tool, in another context? Will the book
'look-and-feel' like a scientific publication in the end?

<div class="clear">

</div>

<!--more-->

> *There are several reasons to want to know the definitions of LaTeX
> commands: from the simplest “idle curiosity”, to the pressing need to
> patch something to make it “work the way you want it”. None of these
> are pure motives, but knowledge and expertise seldom arrive through
> the purest of motives.*  
> [<small>http://www.tex.ac.uk/cgi-bin/texfaq2html?label=ltxcmds</small>](http://www.tex.ac.uk/cgi-bin/texfaq2html?label=ltxcmds)

> *Installing fonts in LaTEX has the name of being a very hard task to
> accomplish. But it is nothing more than following instructions.
> However, the problem is that, ﬁrst, the proper instructions have to be
> found and, second, the instructions then have to be read and
> understood.  
> [<small>http://www.ntg.nl/maps/29/13.pdf</small>](http://www.ntg.nl/maps/29/13.pdf)*

<p>
<center>
![]({filename}/images/uploads/lines.png "lines"){: .float }

</center>
</p>
Ivan and Pierre are the OSP's to take the first plunge. Ivan has `tetex`
installed on his Gentoo system, and Pierre `texlive` on Ubuntu. Getting
the Belgian-French Azerty keyboard working was not easy, and also
installing other fonts than the default took some time. But once done,
they are quickly producing those typical academic standard publications
from OpenOffice documents, though this time with quite a different kind
of content.

The book is planned to come out before the end of this years so we will
report back with more concrete design samples soon.
