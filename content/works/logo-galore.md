Title: Logo Galore
Date: 2009-01-10 11:49
Author: OSP
Tags: Works, Design Samples, Font, Inkscape, Libre Fonts, Printing + Publishing, Type
Slug: logo-galore
Status: published

Lots of work going on the OSP Studio.

Here are few of our recent production:

= = = = = = = = = = = = = = = =

**Logo for**  
**[Le Vecteur](http://www.vecteur.be/)**

Cultural space in Charleroi city

[![]({filename}/images/uploads/vecteur1.png "vecteur1"){: .alignleft .size-full .wp-image-1649 }]({filename}/images/uploads/vecteur1.png)

Button version:

[![]({filename}/images/uploads/vecteur2.png "vecteur2"){: .alignleft .size-full .wp-image-1649 }]({filename}/images/uploads/vecteur2.png)

This logo that can be adapted in multiform patterns:

[![]({filename}/images/uploads/vecteur3.png "vecteur3"){: .alignleft .size-full .wp-image-1649 }]({filename}/images/uploads/vecteur3.png)

= = = = = = = = = = = = = = = =

**Logo for**  
**[The Fernand Baudin Prize](http://www.prixfernandbaudinprijs.be/fr/)**

[![]({filename}/images/uploads/pfb.png "pfb"){: .alignleft .size-full .wp-image-1649 }]({filename}/images/uploads/pfb.png)

[Submit!!!](http://www.prixfernandbaudinprijs.be/fr/home.php?page=reglement)

= = = = = = = = = = = = = = = =

**Logo for**  
**Parallellipeda**

Parallellipeda will be an exhibition on Art and Science, which will
occur in Leuven next year, curated by [Edith
Doove](http://www.edprojects.be/),
[ed.project](http://www.edprojects.be/)

[![]({filename}/images/uploads/logoparallelli.png "logoparallelli"){: .alignleft .size-full .wp-image-1649 }]({filename}/images/uploads/logoparallelli.png)

For that, we developped an abstract font that creates 3D "cubic"
patterns:

[![]({filename}/images/uploads/logoparallelli_how.png "logoparallelli_how"){: .alignleft .size-full .wp-image-1649 }]({filename}/images/uploads/logoparallelli_how.png)

A second abstract font is under construction.

= = = = = = = = = = = = = = = =
