Title: Kanttekeningen / Sidemarks
Date: 2008-06-09 15:08
Author: OSP
Tags: Works, Inkscape, Scripting
Slug: kanttekeningen-sidemarks
Status: published

[![kanttekeningen]({filename}/images/uploads/kanttekeningen_th.jpg){: .float}](http://ospublish.constantvzw.org/kanttekeningen/kanttekeningen_bij_databank.pdf)*Kanttekeningen
bij een databank* documents our investigation into the inner workings of
the database of BAM, the Flemish institute for visual, audiovisual and
media art.

To visualise the data stored, we wrote a plugin in Inkscape, manually
generated graphs and scripted .svg - you can read everything about what
we found and how we worked in [16
pages](http://ospublish.constantvzw.org/kanttekeningen/kanttekeningen_bij_databank.pdf)
that are inserted in the back of the [CROSS-over
book](http://www.bamart.be/pages/detail/en/1566/) (in Dutch).

All documents, sketches, code etc. are gathered here:
<http://ospublish.constantvzw.org/kanttekeningen>.
