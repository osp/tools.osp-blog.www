Title: Open DIN: Das Ist Norm
Date: 2006-11-02 12:08
Author: Femke
Tags: Type, DIN, Standards + Formats
Slug: open-din-das-ist-norm
Status: published

**What does it mean, when a typeface is released in the public domain?
What are the legal issues surrounding typography? How can a font be
generated collaboratively, using open source software? What does it
mean, an open standard and how can such a standard fit different
contexts?**

![DIN\_Q]({filename}/images/uploads/q-32.jpg){: #image148}

To get our hands into these and many other interesting, but difficult
questions, the Open Source Publishing team has embarked on a new
adventure. In the coming year, we will be working on a new digital
rendering of the classic DIN font with the aim to release it in the
public domain.

We chose DIN (often referred to as "the German Autobahn typeface") as a
starting point for a few reasons.

First of all, because it is one of the rare typefaces that was released
into the public domain from the moment it was designed in 1932. While
the original drawings remain freely available, various type foundries
have copyrighted digital renderings (see:
[http://www.linotype.com/](http://www.linotype.com/306/din1451-family.html)
and
[http://www.fontfont.com/](http://www.fontfont.com/shop/index.ep?cview=P71702D&clist=PD)).

Secondly because its particular history brings up many questions about
standards, their political implications and relations to use. In 1936
the German Standard Committee decided DIN should be employed in
technology, traffic, administration, and business, with the idea to
facilitate the development of German engineering and industry. Our point
of departure is therefore far from neutral ground.

Collaborators: Pierre Huyghebaert, Harrisson, Philip May, Nicolas Maleve
and Femke Snelting.

<wp:comment>
  <wp:comment_id>651</wp:comment_id>
  <wp:comment_author><![CDATA[ignacio]]></wp:comment_author>
  <wp:comment_author_email><![CDATA[ignaciogarcia@platoniq.net]]></wp:comment_author_email>
  <wp:comment_author_url></wp:comment_author_url>
  <wp:comment_author_IP><![CDATA[81.36.162.242]]></wp:comment_author_IP>
  <wp:comment_date><![CDATA[2006-11-18 10:49:16]]></wp:comment_date>
  <wp:comment_date_gmt><![CDATA[2006-11-18 09:49:16]]></wp:comment_date_gmt>
  <wp:comment_content><![CDATA[this is a great initiative, please don't forget to include the spanish special characters!]]></wp:comment_content>
  <wp:comment_approved><![CDATA[1]]></wp:comment_approved>
  <wp:comment_type><![CDATA[]]></wp:comment_type>
  <wp:comment_parent>0</wp:comment_parent>
  <wp:comment_user_id>0</wp:comment_user_id>
</wp:comment>
<wp:comment>
  <wp:comment_id>3758</wp:comment_id>
  <wp:comment_author><![CDATA[Elle]]></wp:comment_author>
  <wp:comment_author_email><![CDATA[beer.brommel@hotmail.com]]></wp:comment_author_email>
  <wp:comment_author_url></wp:comment_author_url>
  <wp:comment_author_IP><![CDATA[80.200.0.105]]></wp:comment_author_IP>
  <wp:comment_date><![CDATA[2007-06-23 20:45:12]]></wp:comment_date>
  <wp:comment_date_gmt><![CDATA[2007-06-23 19:45:12]]></wp:comment_date_gmt>
  <wp:comment_content><![CDATA[I really am looking forward to this open font, since I am looking for it a long time]]></wp:comment_content>
  <wp:comment_approved><![CDATA[1]]></wp:comment_approved>
  <wp:comment_type><![CDATA[]]></wp:comment_type>
  <wp:comment_parent>0</wp:comment_parent>
  <wp:comment_user_id>0</wp:comment_user_id>
</wp:comment>
<wp:comment>
  <wp:comment_id>42774</wp:comment_id>
  <wp:comment_author><![CDATA[nitrofurano]]></wp:comment_author>
  <wp:comment_author_email><![CDATA[nitrofurano@gmail.com]]></wp:comment_author_email>
  <wp:comment_author_url>http://nitrofurano.linuxkafe.com</wp:comment_author_url>
  <wp:comment_author_IP><![CDATA[87.196.141.193]]></wp:comment_author_IP>
  <wp:comment_date><![CDATA[2008-09-11 00:28:13]]></wp:comment_date>
  <wp:comment_date_gmt><![CDATA[2008-09-10 23:28:13]]></wp:comment_date_gmt>
  <wp:comment_content><![CDATA[does it mean getting paths from Manuale Tipografico (and alike) raster scans we can have an OpenBodoni without having legal problems with those known foundries used to sell Bodoni, since a 250 year old work is a public domain?]]></wp:comment_content>
  <wp:comment_approved><![CDATA[1]]></wp:comment_approved>
  <wp:comment_type><![CDATA[]]></wp:comment_type>
  <wp:comment_parent>0</wp:comment_parent>
  <wp:comment_user_id>0</wp:comment_user_id>
</wp:comment>
<wp:comment>
  <wp:comment_id>44696</wp:comment_id>
  <wp:comment_author><![CDATA[nitrofurano]]></wp:comment_author>
  <wp:comment_author_email><![CDATA[nitrofurano@gmail.com]]></wp:comment_author_email>
  <wp:comment_author_url>http://nitrofurano.linuxkafe.com</wp:comment_author_url>
  <wp:comment_author_IP><![CDATA[87.196.147.20]]></wp:comment_author_IP>
  <wp:comment_date><![CDATA[2009-02-23 18:21:24]]></wp:comment_date>
  <wp:comment_date_gmt><![CDATA[2009-02-23 17:21:24]]></wp:comment_date_gmt>
  <wp:comment_content><![CDATA[i forgot to tell about this project: http://klepas.org/openbaskerville/

(how can we delete that mistaken previous post?)]]></wp:comment_content>
  <wp:comment_approved><![CDATA[1]]></wp:comment_approved>
  <wp:comment_type><![CDATA[]]></wp:comment_type>
  <wp:comment_parent>0</wp:comment_parent>
  <wp:comment_user_id>0</wp:comment_user_id>
</wp:comment>
<wp:comment>
  <wp:comment_id>44859</wp:comment_id>
  <wp:comment_author><![CDATA[nitrofurano]]></wp:comment_author>
  <wp:comment_author_email><![CDATA[nitrofurano@gmail.com]]></wp:comment_author_email>
  <wp:comment_author_url>http://nitrofurano.gmail.com</wp:comment_author_url>
  <wp:comment_author_IP><![CDATA[87.196.237.33]]></wp:comment_author_IP>
  <wp:comment_date><![CDATA[2009-06-18 01:50:00]]></wp:comment_date>
  <wp:comment_date_gmt><![CDATA[2009-06-17 23:50:00]]></wp:comment_date_gmt>
  <wp:comment_content><![CDATA[http://www.fontshop.be/upload/0ZC7YPAC.jpg - found this master drawing of Prussian Railways typeface - if someone have this in a better resolution, please let us know! :)]]></wp:comment_content>
  <wp:comment_approved><![CDATA[1]]></wp:comment_approved>
  <wp:comment_type><![CDATA[]]></wp:comment_type>
  <wp:comment_parent>0</wp:comment_parent>
  <wp:comment_user_id>0</wp:comment_user_id>
</wp:comment>
<wp:comment>
  <wp:comment_id>44858</wp:comment_id>
  <wp:comment_author><![CDATA[nitrofurano]]></wp:comment_author>
  <wp:comment_author_email><![CDATA[nitrofurano@gmail.com]]></wp:comment_author_email>
  <wp:comment_author_url>http://nitrofurano.gmail.com</wp:comment_author_url>
  <wp:comment_author_IP><![CDATA[87.196.237.33]]></wp:comment_author_IP>
  <wp:comment_date><![CDATA[2009-06-17 17:18:06]]></wp:comment_date>
  <wp:comment_date_gmt><![CDATA[2009-06-17 15:18:06]]></wp:comment_date_gmt>
  <wp:comment_content><![CDATA[Since this typeface were designed to be drawn over an orthogonal square grid, i didn't care about kernings - i think this would reach closelly the idea of the Prussian Railways at early 20th century - but, if the kerning needs really exists, could it be a good idea to be snapped to 50 or 25 units multiples?]]></wp:comment_content>
  <wp:comment_approved><![CDATA[1]]></wp:comment_approved>
  <wp:comment_type><![CDATA[]]></wp:comment_type>
  <wp:comment_parent>0</wp:comment_parent>
  <wp:comment_user_id>0</wp:comment_user_id>
</wp:comment>
<wp:comment>
  <wp:comment_id>44857</wp:comment_id>
  <wp:comment_author><![CDATA[nitrofurano]]></wp:comment_author>
  <wp:comment_author_email><![CDATA[nitrofurano@gmail.com]]></wp:comment_author_email>
  <wp:comment_author_url>http://nitrofurano.gmail.com</wp:comment_author_url>
  <wp:comment_author_IP><![CDATA[87.196.237.33]]></wp:comment_author_IP>
  <wp:comment_date><![CDATA[2009-06-17 17:09:00]]></wp:comment_date>
  <wp:comment_date_gmt><![CDATA[2009-06-17 15:09:00]]></wp:comment_date_gmt>
  <wp:comment_content><![CDATA[http://www.magwerk.com/mag.php?magazine=encore&amp;language=en&amp;issue=13&amp;page=34 helped me fixing http://pastebin.com/f21f16ecd (sfd) and http://pastebin.com/f4293eabb (svg) - accessing the original master drawing would help a lot indeed]]></wp:comment_content>
  <wp:comment_approved><![CDATA[1]]></wp:comment_approved>
  <wp:comment_type><![CDATA[]]></wp:comment_type>
  <wp:comment_parent>0</wp:comment_parent>
  <wp:comment_user_id>0</wp:comment_user_id>
</wp:comment>
<wp:comment>
  <wp:comment_id>44855</wp:comment_id>
  <wp:comment_author><![CDATA[nitrofurano]]></wp:comment_author>
  <wp:comment_author_email><![CDATA[nitrofurano@gmail.com]]></wp:comment_author_email>
  <wp:comment_author_url>http://nitrofurano.gmail.com</wp:comment_author_url>
  <wp:comment_author_IP><![CDATA[87.196.237.33]]></wp:comment_author_IP>
  <wp:comment_date><![CDATA[2009-06-17 00:53:12]]></wp:comment_date>
  <wp:comment_date_gmt><![CDATA[2009-06-16 22:53:12]]></wp:comment_date_gmt>
  <wp:comment_content><![CDATA[i don't know if http://pastebin.com/f200354ae (sfd) and http://pastebin.com/f69957ad6 (svg) can be considered a start - of course it's plenty of mistakes, and i'm not completelly assured about if the licence of the original DIN drawing is really public domain (i imagine it is anyway...)]]></wp:comment_content>
  <wp:comment_approved><![CDATA[1]]></wp:comment_approved>
  <wp:comment_type><![CDATA[]]></wp:comment_type>
  <wp:comment_parent>0</wp:comment_parent>
  <wp:comment_user_id>0</wp:comment_user_id>
</wp:comment>
<wp:comment>
  <wp:comment_id>44853</wp:comment_id>
  <wp:comment_author><![CDATA[nitrofurano]]></wp:comment_author>
  <wp:comment_author_email><![CDATA[nitrofurano@gmail.com]]></wp:comment_author_email>
  <wp:comment_author_url>http://nitrofurano.gmail.com</wp:comment_author_url>
  <wp:comment_author_IP><![CDATA[87.196.237.33]]></wp:comment_author_IP>
  <wp:comment_date><![CDATA[2009-06-16 19:54:07]]></wp:comment_date>
  <wp:comment_date_gmt><![CDATA[2009-06-16 17:54:07]]></wp:comment_date_gmt>
  <wp:comment_content><![CDATA[Interesting is seeing both Linotype and Fontfont versions are very different of the original Engschrift - both seems to try to fix some 'mistakes' from the original, and if some version (open, for example) follows strictly from the original (which seems to be public domain?), i think it may not have licensing problems... - i don't know if this http://en.wikipedia.org/wiki/File:DIN1451board.jpg is the original master drawing, and where can we get one with better quality...]]></wp:comment_content>
  <wp:comment_approved><![CDATA[1]]></wp:comment_approved>
  <wp:comment_type><![CDATA[]]></wp:comment_type>
  <wp:comment_parent>0</wp:comment_parent>
  <wp:comment_user_id>0</wp:comment_user_id>
</wp:comment>
<wp:comment>
  <wp:comment_id>44638</wp:comment_id>
  <wp:comment_author><![CDATA[paulo]]></wp:comment_author>
  <wp:comment_author_email><![CDATA[nitrofurano@gmail.com]]></wp:comment_author_email>
  <wp:comment_author_url>http://nitrofurano.linuxkafe.com</wp:comment_author_url>
  <wp:comment_author_IP><![CDATA[87.196.56.143]]></wp:comment_author_IP>
  <wp:comment_date><![CDATA[2008-12-28 12:05:27]]></wp:comment_date>
  <wp:comment_date_gmt><![CDATA[2008-12-28 11:05:27]]></wp:comment_date_gmt>
  <wp:comment_content><![CDATA[not exactly about Din - the original drawings of Garamond seems to be in a Belgium museum - http://en.wikipedia.org/wiki/Plantin-Moretus_Museum - would be possible an Open-Garamond being made from these drawings? http://barneycarroll.com/garamond.htm]]></wp:comment_content>
  <wp:comment_approved><![CDATA[1]]></wp:comment_approved>
  <wp:comment_type><![CDATA[]]></wp:comment_type>
  <wp:comment_parent>0</wp:comment_parent>
  <wp:comment_user_id>0</wp:comment_user_id>
</wp:comment>
<wp:comment>
  <wp:comment_id>44862</wp:comment_id>
  <wp:comment_author><![CDATA[nitrofurano]]></wp:comment_author>
  <wp:comment_author_email><![CDATA[nitrofurano@gmail.com]]></wp:comment_author_email>
  <wp:comment_author_url>http://nitrofurano.gmail.com</wp:comment_author_url>
  <wp:comment_author_IP><![CDATA[87.196.237.33]]></wp:comment_author_IP>
  <wp:comment_date><![CDATA[2009-06-18 18:24:12]]></wp:comment_date>
  <wp:comment_date_gmt><![CDATA[2009-06-18 16:24:12]]></wp:comment_date_gmt>
  <wp:comment_content><![CDATA[when trying to upload to openfontlibrary.org - "/var/www/openfontlibrary.org/htdocs/cclib/cc-debug.php"(287): error_log(../cchost_offline/cc-log.txt) [function.error-log]: failed to open stream: No such file or directory [2009-06-18 16:22 pm][127.0.0.1][/media/submit/font] - beautiful...]]></wp:comment_content>
  <wp:comment_approved><![CDATA[1]]></wp:comment_approved>
  <wp:comment_type><![CDATA[]]></wp:comment_type>
  <wp:comment_parent>0</wp:comment_parent>
  <wp:comment_user_id>0</wp:comment_user_id>
</wp:comment>
<wp:comment>
  <wp:comment_id>44863</wp:comment_id>
  <wp:comment_author><![CDATA[nitrofurano]]></wp:comment_author>
  <wp:comment_author_email><![CDATA[nitrofurano@gmail.com]]></wp:comment_author_email>
  <wp:comment_author_url>http://nitrofurano.gmail.com</wp:comment_author_url>
  <wp:comment_author_IP><![CDATA[87.196.237.33]]></wp:comment_author_IP>
  <wp:comment_date><![CDATA[2009-06-18 19:38:20]]></wp:comment_date>
  <wp:comment_date_gmt><![CDATA[2009-06-18 17:38:20]]></wp:comment_date_gmt>
  <wp:comment_content><![CDATA[last version at http://gnome-look.org/content/show.php?content=107153]]></wp:comment_content>
  <wp:comment_approved><![CDATA[1]]></wp:comment_approved>
  <wp:comment_type><![CDATA[]]></wp:comment_type>
  <wp:comment_parent>0</wp:comment_parent>
  <wp:comment_user_id>0</wp:comment_user_id>
</wp:comment>
<wp:comment>
  <wp:comment_id>45541</wp:comment_id>
  <wp:comment_author><![CDATA[Waarom betalen voor iets dat gemeen goed is? &laquo; Jeffry Baecker]]></wp:comment_author>
  <wp:comment_author_email><![CDATA[]]></wp:comment_author_email>
  <wp:comment_author_url>http://jeffrybaecker.wordpress.com/2010/05/19/waarom-betalen-voor-iets-dat-gemeen-goed-is/</wp:comment_author_url>
  <wp:comment_author_IP><![CDATA[66.135.48.201]]></wp:comment_author_IP>
  <wp:comment_date><![CDATA[2010-05-19 11:46:23]]></wp:comment_date>
  <wp:comment_date_gmt><![CDATA[2010-05-19 09:46:23]]></wp:comment_date_gmt>
  <wp:comment_content><![CDATA[[...] Waarom betalen voor iets dat gemeen goed&nbsp;is? 19/05/2010   Via OSP [...]]]></wp:comment_content>
  <wp:comment_approved><![CDATA[1]]></wp:comment_approved>
  <wp:comment_type><![CDATA[pingback]]></wp:comment_type>
  <wp:comment_parent>0</wp:comment_parent>
  <wp:comment_user_id>0</wp:comment_user_id>
</wp:comment>
