Title: Open Revival
Date: 2009-02-23 21:55
Author: OSP
Tags: Type, Contribute!, History, Type
Slug: open-revival
Status: published

![stephenson-blake\_original\_scan1]({filename}/images/uploads/stephenson-blake_original_scan1.jpg "stephenson-blake_original_scan1"){: .alignnone .size-full .wp-image-1958 }

"*With the written word an absolute fundamental component of daily
communication, typography and fonts are vital to providing aesthetic
harmony and legibility to our textual works. There are thousands of
fonts available, of which only a small number are useful or any good for
setting vast quantities of text, and of which an even smaller number are
available to be freely distributed and shared.*"

**[The Open Baskerville project](http://klepas.org/openbaskerville/)**
is an attempt to collaboratively re-create a high quality revival of
Baskerville, ultimately available under an Open Font License or GPL. The
project has an issue tracker and all font source files are available via
a repository. You can add a missing glyph or adjust the kerning; the
site at <http://klepas.org/openbaskerville/> explains in detail how this
works, and why the initiators ended up using the 1913 Stephenson, Blake
specimen. A suivre!

<small>Thanks [nitrofurano](http://nitrofurano.linuxkafe.com/) for
reminding us :-)</small>
