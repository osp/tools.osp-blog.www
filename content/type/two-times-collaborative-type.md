Title: Two Times Collaborative Type
Date: 2006-06-06 22:43
Author: Femke
Tags: Type, Fontforge
Slug: two-times-collaborative-type
Status: published

Two projects to submit fonts to, or find others to work with:

**Typeforge**  
[http://www.typeforge.net](http://www.typeforge.net/cms/)  
![fontforge](http://ospublish.constantvzw.org/blog/wp-content/_fontforge.jpg "fontforge"){: }

The *Typeforge* project was initiated by Portuguese type designer Pedro
Amado, to create a platform for "open source collaborative type design".
You are encouraged to share sketches, notes and ideas and work on
collective projects. Good Fontforge tips + tricks too!

**Open Font Library**  
<http://openfontlibrary.org/>

Based on the same principle as the [Open Clip Art
Library](http://openclipart.org/), this project simply aims to bring
together copyleft fonts. The project has just started, hence their
collection is rather small, but such a repository is so much needed that
I think it could grow quickly once people start adding.
