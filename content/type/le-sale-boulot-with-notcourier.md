Title: Le Sale Boulot with NotCourier
Date: 2008-10-14 09:19
Author: Harrisson
Tags: Type, Works
Slug: le-sale-boulot-with-notcourier
Status: published

[![]({filename}/images/uploads/saleboulot.jpg "saleboulot"){: .alignleft .size-medium .wp-image-1110 }]({filename}/images/uploads/saleboulot.jpg)  
Excellent Brussels based graphic design studio
[collerettecocofilllsd](http://collerettecocofilllsd.com/) just released
a book called "Le Sale Boulot", from ~~german~~ Franco-Chilean artist
[Vladimir Cruells](http://www.lesaleboulot.com/). Main font used is
[NotCourier](http://openfontlibrary.org/media/files/OSP/309) we
designed. We're proud!

<http://collerettecocofilllsd.com/blogblog/>
