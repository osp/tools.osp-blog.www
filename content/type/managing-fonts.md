Title: Managing Fonts
Date: 2007-04-28 09:53
Author: Femke
Tags: Type, How-to
Slug: managing-fonts
Status: published

[![dfontmgr.png]({filename}/images/uploads/dfontmgr.png){: .float}]({filename}/images/uploads/dfontmgr.png "dfontmgr.png")DEbian
Font MAnager a.k.a. dfontmgr (available through the Synaptic Package
Manager) is helpful when you want to register and unregister fonts on a
Debian / Ubuntu system.

After installation you can start the manager, which is a GUI for the
Defoma package, from the command line. Type `sudo dfontmgr` (you need to
be root to make changes to fonts). It does not look very pretty but
installing and uninstalling fonts should now be easy.
