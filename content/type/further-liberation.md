Title: Further Liberation.
Date: 2007-05-15 10:35
Author: Harrisson
Tags: Type, Libre Fonts, Standards + Formats
Slug: further-liberation
Status: published

If we check the font infos of Liberation font, on Fontforge, for
example, here is what we find:  
<!--more-->  
[![liberation1.png]({filename}/images/uploads/liberation1.png)]({filename}/images/uploads/liberation1.png "liberation1.png")  
Copyright Ascender Corp...  
and a trademark: *"Liberation is a trademark of Red Hat, Inc. registered
in U.S. Patent and Trademark Office and certain other jurisdictions."*.
In the license text of the ttf font package, we find: "*Copyright ¬©
2007 Red Hat, Inc. All rights reserved. LIBERATION is a trademark of Red
Hat, Inc.*", and the license text is GPL.  
Still, the license informations says:  
*"Use of this Liberation font software is subject to the license
agreement under which you accepted the Liberation font software."*

Interesting as well: Liberation Sans seems to have been created in 1970,
while all the others in the pack were in 2004 or 2005.

Liberated fonts are not exempt of paradoxes...

Looking forward, the owner of rights of this font are [Ascender
Corporation](http://www.ascendercorp.com/), *"leading provider of
advanced font products specializing in type design, font development and
licensing"* company, which offers services such as Font Branding.  
you can also contact their "Font Licensing Specialists"

Sure designer [Steve
Matteson](http://www.ascendercorp.com/stevepage.html) knows exactly the
font system specs - and what he does: "*...in 1990 he began work at
Monotype to create the Windows core TrueType fonts: Arial, Times New
Roman and Courier New*". He designed a.o. Xbox 360 Branding Fonts and is
part of the Segoe Vista font team development.

By the way, there is no Comic-Sans-like font in the L-pack. Which
designer will have the honour of drawing it?
