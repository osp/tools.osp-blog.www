Title: Looking for F, Q, X and H
Date: 2008-05-05 08:15
Author: OSP
Tags: Type, LGM 2008
Slug: looking-for-f-q-and-h
Status: published

![]({filename}/images/uploads/slubice.jpg "slubice"){: .alignnone .size-full .wp-image-464 }  
On our way from Berlin to Wroclaw, OSP managed to photograph almost
every letter in the Polish roadsignage alphabet. We are preparing for
the [Local Universal](http://ospublish.constantvzw.org/?p=454) workshop
and still looking for **F**, **Q**, **X** and **H** (capital + lower
case). If you happen to come across one, please send us a picture?
