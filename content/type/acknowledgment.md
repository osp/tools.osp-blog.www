Title: Acknowledgment
Date: 2011-06-21 17:00
Author: Antoine
Tags: Type
Slug: acknowledgment
Status: published

*“I think it is impossible to copyright empty space*” (Pierre H quotes
Pierre M)

We received different responses to our open letter to Monotype that we
posted [here](news/sans-guilt-it-looks-like-the-story-continues) and on
[Typophile](http://typophile.com/node/82567).

Actually, we did get a reply from MonoType’s lawyer, acknowledging our
letter, and asking us to identify ourselves as a legal body. We
responded with a long list of our names and various locations; we
haven't heard since.

For metrics we used the existing Gill Sans metrics. There is some
antecedent to using existing metrics. Ascender Corporation made the
Liberation fonts for Red Hat match the metrics of Times New Roman, Arial
and Courier New. Bosch Office Sans, by Erik Spiekermann and Christian
Schwartz, matches Arial metrics. Yes, we could have been clearer about
this in our letter.

With regards to quality, we are aware that using Potrace doesn’t produce
the same kind of optimized Bézier curves that are produced in a
traditional type design process. This is not necessarily what we are
interested in. We are interested in the process, in ways of producing.
Iterative, collaborative. Release early, release often. Test out ideas
and processes. The difference between a traditional typeface and an open
source typeface is that an open source typeface can be redistributed and
modified.

Merci,

l’OSP
