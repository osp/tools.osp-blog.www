Title: Kaffeesatz + Tagesschrift
Date: 2006-07-15 15:22
Author: Femke
Tags: Type, Libre Fonts
Slug: kaffeesatz-tagesschrift
Status: published

[![kaffeesatz
font](http://ospublish.constantvzw.org/blog/wp-content/_kaffeesatz.gif "kaffeesatz font"){: }](http://ospublish.constantvzw.org/blog/wp-content/kaffeesatz.gif)  
**Kaffeeschrift** was developed by Jan Gerner from Dresden for use in
menus etc., hence the monospaced figures. It works well for titles and
headers, but was not designed for longer texts (though it holds out
surprisingly well)

Download and try out here: <http://www.yanone.de/typedesign/kaffeesatz/>

[![tagesschrift
font](http://ospublish.constantvzw.org/blog/wp-content/_tagesschrift.gif "tagesschrift font"){: }](http://ospublish.constantvzw.org/blog/wp-content/tagesschrift.gif)  
Tagesschrift is scanned and vectoralized handwriting; very nice
atmosphere and a more than useful addition to the often rather dull set
of open fonts available.

Download and try out here:
<http://www.yanone.de/typedesign/tagesschrift/>

Both fonts are available under a not too restrictive Creative Commons
License; you can copy, distribute, display, and perform (now that's a
nice idea! -&gt; I think this would mean you can embed them in
documents?) the fonts; make derivative versions and also make commercial
use of them. If you distribute (the same or altered versions), you are
obliged to attribute Jan Gerner / http://www.yanone.de
