Title: Appropriation and Type - before and today
Date: 2007-01-31 10:43
Author: Ricardo
Tags: Type, Libre Fonts, Reading list
Slug: appropriation-and-type-before-and-today-2
Status: draft

I will argue in this essay that appropriation has been a recurring and
accepted strategy in defining typography as activity and business. More
specifically, I will cite four cases where appropriation has definitely
been key in defining landmarks in the history of type, not only aiding
the breaking of technical and creative boundaries but also helping to
question legal and moral ones.

Afterwards, I'll present an analysis of the current situation in
typography, focusing on the approach to the subject by corporations,
users and designers. I'll argue that the current business model (digital
foundries, font files with copyrights) is a remnant of a time where a
typeface filled a whole drawer and fails to account for the necessary
changes that the information age demands, concluding with the definition
of an essentially contradictory business model that has very strong
stands against "font forging" and copyright issues, although it has
historically - and now, more than ever - thrived on constant, and often
uncredited, appropriation of ideas and designs.

<!--more-->

Index  
1. Appropriation in type through history  
i. The Gutenberg press  
ii. Stanley Morison and Monotype  
iii. Arial  
iv. Segoe  
2. The digital typography paradigm  
i. Corporate type  
ii. User type  
iii. Designer type  
3. Tweaking and reviving  
4. Technology on arcane standards  
5. What now

a\. Notes  
b. References  
c. Online references

1\. APPROPRIATION IN TYPE THROUGH HISTORY

We could certainly identify many more instances of inspiration or
downright copying of ideas in typography, but these four cases will
suffice to demonstrate the different uses of copy, inspiration and
appropriation in general. Our focus here will be on the issue of
creative appropriation (inspiration) on one hand, and corporate business
models and copyright issues (plagiarism) on the other.

i\. The Gutenberg Press

In 1450, Johannes Gutenberg produced the first commercially viable model
of his printing press, which was widely used for centuries until the
advent of the Linotype machine, the first way to automate, though
partially, the type setting and printing process.  
Gutenberg's press was the result of the combination of five key methods
and processes, three - possibly four - of which were not original:

\* The screw press, which was already used by the Greeks and Romans to
process olive oil and wine.  
\* Block printing, present in China since 594 AD. Gutenberg's innovation
was to use metal cast types (instead of the Chinese traditional
woodblock printing), although metal typecasting was already developed in
Korea around 1230 AD.  
\* Letter punches, which were a goldsmithing technique - Gutenberg was a
goldsmith - used to engrave letters in metal pieces.  
\* Letter replica casting, a method to quickly create new individual
characters, along with a particular metal alloy that made for durable
pieces. This method has been attributed to Gutenberg but recent studies
shed doubts on this fact.  
\* Metal-adherent ink, devised by Gutenberg.

This shows that originality is not a straightforward issue, in a time
before copyrights existed (it was not before 1700 that the first
copyright statute appeared in Britain), the protection of ideas could
have changed the fate of this invention. the combination of methods
made. What matters here is that they were combined in a way that made
typography as we know it possible, and there seems to be absolutely no
question to the legitimacy of this invention, which was made possible by
appropriating previous methods and processes. Gutenberg's model of
printing stood firm for centuries until the Linotype machine introduced
partial automation of the printing process.

ii\. Stanley Morison and Monotype

On 1886, the Linotype machine began to be produced by the Mergenthaler
Printing Co. in the United States. It wouldn't take long, though (a
year) for Lanston Monotype to begin production of their own
fully-automated typesetting machine, devised by Tolbert Lanston.

In 1922, Stanley Morison was appointed as typographic advisor of the
Monotype Corporation (the British branch of the Philadephia company), a
post he would keep until 1967. The Monotype Corporation built an
extensive catalog of cuts made by Morison from classic references, such
as Bodoni, Bembo, Baskerville, and several others. These revivals helped
to bring general interest to the old masters' works, besides consisting
of a general market strategy to try to push up the value of the Monotype
machine - the faces available would definitely determine the decision of
a buyer who fancies a particular style, and thus the Monotype
Corporation had no qualms about recruiting all the classics (which were
in the public domain).

It is tremendously unfair, though, to portray Morison as a hijacker - he
was one of the hallmarks of 20th century type, being responsible for the
creation of Times New Roman and hugely influencing the field of
typography to the present day by the efforts he dedicated to bringing
the classics to the general public - legitimately appropriating other
designs. Without Morison's endeavour, our legacy would certainly be
poorer today.

iii\. Arial, Monotype and Microsoft

1982 is the year in which the Arial typeface was released by Monotype
Typography (Monotype Corporation's type design division). Designed by
Robin Nicholas and Patricia Saunders, this typeface had a remarkable
issue. Not only does it have obvious similarities to other modern
sans-serifs (sharing features with Helvetica, Univers and Akzidenz
Grotesk), it exactly mirrors the glyph width tables from Helvetica,
which is the data included in a font file that describes each
character's dimensions. An exact match that gives little chance for
coincidence.

Microsoft licensed Arial from Monotype instead of the more expensive
Helvetica, and in 1990 it was bundled with Microsoft Windows 3.1. It has
been a staple of Windows systems until today. This is a specific case
where a typeface was chosen not by its genuine creative and/or practical
value but by external reasons, in this case backed by financial motives.
Type designers are almost unanimous in shunning Arial as a lesser
typeface: it is notably absent from Robert Bringhurst's typeface
selection in The Elements of Typographic Style (the current all-around
reference on type design from the designer's perspective), and is also
only mentioned as a passing remark on Robin Nicholas's entry on the
typographic encyclopedic survey by Friedl et al\[1\]. This is pretty
much a clear notion of the type designers' community on the Arial issue;
it's also worth noting that there has been, however, no attempt to
replace Arial as a standard font in operating systems\[2\].

In strict legal/copyright terms, it's appropriate to compare the Arial
case to a cheating student who argues that the fact that his exam has
exact passages from his nearest classmates' exams owes to coincidence.
It's reasonable to argue that borrowing from three sources rather than
just one does not make the situation more acceptable.

So Arial stands in mixed principles: the type community is unanimous in
calling shenanigans, but it still made its way to our current operating
systems despite that fact - it never met any legal actions.

iv\. Segoe

In early 2006, Microsoft announced a significant effort to dignify type
design in their upcoming Vista operating system: six type designers -
Lucas de Groot and Robin Nicholas figuring among them - were comissioned
to design appropriate typefaces for screen and print. The result was six
very attractive fonts that not only could appeal to general uses by less
savvy people, but also soothe the type designers' fancy.

Another font included in Vista is Segoe, a revival of Frutiger Next
(which in turn is a revival of Frutiger) that Microsoft licensed from
Monotype and altered. It's not the first case in which Adrian Frutiger's
work has been remade: Adobe's Myriad and Apple's Podium Sans also bear a
striking resemblance to Frutiger's structure. When Microsoft registered
Segoe in Europe in 2004, Linotype sued for copyright infringement since
European law, unlike the American one, recognises the rights to font
designs (although patent law is often used to circumvent this legal void
in the US).

The most significant fact is that Microsoft based their defense not on
the issue of originality - stating the differences between Segoe and
Frutiger Next, but on the fact that Linotype wasn't selling its typeface
in Europe when the request was filed. This situation could very well be
interpreted as an admission by Microsoft's part that the font in fact
owes credit to Frutiger's design.

This case becomes all more revealing in that it's a high-profile and
current example of an attempt to settle the authenticity of a type
design in courts. Unlike Arial, it didn't sneak past the critics and
found serious hurdles while Microsoft tried to implement it in its
Windows OS. A verdict on the Segoe case is expected in early 2007.

2\. THE DIGITAL TYPOGRAPHY PARADIGM

Typography, and type design in particular, is historically defined by a
constant recursion of past themes and trends, be it as inspiration -
revivals - or as a way to question them - as in post-modern type
examples, such as Emigre and Letterror's work). Nevertheless, modern
designs still owe heavily (with or without credit) to a tradition of
arts and crafts spanning five centuries.

Meanwhile, on the last 20 years, the type world hasn't ceased discussing
the issue of rights and plagiarism, a discussion that was sparked by the
digital revolution and the introduction of the personal computer as an
all-purpose design and production tool. This shift implied that the
tools used in typography and book production ceased to be the sole
domain of type makers, printers and book publishers - the only ones that
could afford the initial investment of a type foundry, workshop or
printing press and manage it effectively. Designing type soon became
cheaper and cheaper, as the physical footprint of the new tools
gradually became smaller and smaller. Nowadays, a computer and a printer
can do in minutes what a huge phototypesetting equipment would have
taken a lot of time, effort and money to produce 10 years ago.

The most important effect of the digital revolution in type design is
that typefaces became fonts - a radical change in that they were no more
lead blocks but data, files that describe how each glyph should be drawn
on screen or on a printer. FontForge, a free software solution to type
design, was released in 2004, doing away with any software costs
involved in font creation and editing, meaning the only overhead for a
type design business would be a PC, paper, drawing tools, an
image-capture device (scanner or camera) and eventually an Internet
connection. This change has massive repercussions in the whole
typography market: now type design wouldn't, in theory, require any kind
of intermediaries between the typographer/designer and its audience.
Reality developed otherwise, as we will see from three standpoints in
typography usage and creation.

i\. Corporate Type

The digital revolution made a deep re-definition of most areas of study
possible. We will show, though, that the field of typography has been
lagging behind when it comes to taking advantage of the digital medium.
Moreover, the corporate business model has failed to account for the
specific needs and features of information technology, sticking to an
artificial market sustained by an inflated value attributed to digital
files as if they still were physical objects that are owned.

Nowadays, there are three major players in the type business: Microsoft,
Adobe and Monotype Imaging.

Apple Computer hasn't been a key figure in the type market
(concentrating on developing font technology for its operating system),
but it had an essential role in developing the actual playing field.
Apple heralded the personal computer era in with their original
Macintosh and has intermittently collaborated and competed with
Microsoft and Adobe, being responsible for the development of the
TrueType font format along with Microsoft as a response to Adobe's
high-priced PostScript Type I font description format. The release of
TrueType in 1991 forced Adobe to gradually reduce prices and eventually
follow suit, releasing the PostScript specifications so that software
developers could implement it without limitations in their programs.

Adobe Systems Inc., besides being responsible for a highly successful
suite of imaging and DTP software, has a very strong position in the
type market: not only is it a type vendor (through its typography
division, Adobe Type) but also the most influential company in the sense
that it owns most digital design solutions - especially after acquiring
its main rival Macromedia in April 2005 and facing no significant
competition in its market.

Microsoft is responsible for creating the most widely used operating
system, as well as the most popular office suite. Along with Adobe,
Microsoft developed the currently dominant OpenType file format, which
is freely available to developers as long as they agree to the licensing
terms. Adobe converted its entire type collection to OpenType in a move
to spread the new standard.

Monotype Imaging is now a distant remnant of Tolbert Lanston's original
creation. It has adjusted technical breakthroughs in the 20th century
and claimed a staunch position in today's digital type market. It was
acquired by Agfa in 1999 forming Agfa Monotype, which in turn was
acquired by TA Associates (a North American investment firm), changing
its name to Monotype Imaging and developing a position in font software
and rendering engines, and also securing a strong standpoint in the font
vendor market after acquiring its rival Linotype (and the rights to
their entire type collection).

ii\. User Type

Most people get introduced to digital type by means of text editors. The
digital revolution would be the perfect reason to finally open
typography to everyone and make it a mainstream subject instead of a
limited-access craft. Things have happened otherwise, though, and the
inability to create a suitable interface for allowing basic
experimentation with type has severely crippled the possibilities of the
new medium.

The font selection paradigm has changed little during the years,
offering a whole collection of typefaces in a drop-down menu. Such is
the immediateness of digital type: It's just there, no need to open
drawers with thousands of lead characters. Users are encouraged, by
means of a simple GUI, to just pick their font and get to work on their
document. Even more: you don't even need to pick, just stick with the
default choice the software maker's made for you. Word processing
interfaces also assume the user doesn't want to be bothered with layout
choices such as margins, structure - and they also make the choice for
us (incidentally, they also made it quite awkward to change these
defaults). In short: the standard word-processing interface tells users
to not bother with type.

This paradigm helps to build the general perception that a font is a
finished, shrink-wrapped and untouchable product - pretty much like
prepackaged software. Although font files can be opened and edited given
we have a simple editor, most typeface editors are either crude or
catering exclusively to the type designer market. The user usually isn't
able to reach the underpinnings and intricacies of type, instead being
expected just to understand that the default template is more than
enough.

Such an approach to software designing effectively discourages any kind
of interest in typographic issues by the general public, and helps to
fuel the thought that fonts are "just there". It's worth noting that
there is still no easy and streamlined way to buy, install and use
fonts, unlike most other digital markets - iTunes would be a good
example of that kind of market strategy.

iii\. Designer Type

The type designer community is centered on the study of classical and
modern examples and making attempts to postulate theory and practical
guidelines for the craft of type design, sitting somewhere between the
methods of architecture and those of poetry.

Fred Smeijers's analysis of the type designer's duty, in his manifesto,
is quite straightforward. On the issue of responsibility of type
designers and commitment to specific guidelines, he states that "a type
designer cannot escape this responsibility of judgment (...). In the
end, people - the society - either accept it or they don't"\[3\].
Society, it seems, would be the ultimate judge of whether a typeface is
a hallmark of craft or doomed to failure.

On the other hand, we find a curious account on Smeijers's description
on Fontana, a typeface by Ruben Fontana inspired by Meta \[4\]: he
describes it as "uncomplicated", "tres sympathique", "sunny" and "open
minded". This certainly sounds more like a description of a person or a
song than that of an object, and indeed sheds some doubt on the touted
objectiveness of good type design in the sense that it seems unable to
find serious and objective terms to classify a typeface's features.
Historical categorisations of design tendencies vary from author to
author, and although there are some widely used terms to describe
historical periods and typeface features, such as "transitional type" or
"slab serifs", there's a tendency to borrow from poetry and music to
identify a type family's "soul" (which, though relevant from an artist
or a historian's point of view, is rather unscientific).

This is not a contradiction, though, since we can distinguish between
type as a creative activity (in which there would be no problem with
this kind of analogy) and type as an industry and commodity (where
profit, market tendency, shareholder demands and legal requirements
imply that things have a definite value and purpose). Naturally,
Smeijers's interest is on the craft and art of typography, and not the
market and the economic relationships that it spawns. On the other hand,
our interest is definitely that which Smeijers doesn't care for.

We need to account that defending the status of type as a functional
solution to practical problems requires an objective set of rules that
derive from the way we read and write. We cannot yet account for matters
of objective legibility while we don't possess all information on our
mental processes and the mechanisms in the brain involved in acquiring
and processing written information - this is the field of cognitive
psychology and neuroscience.

We know, from history, that a text with generous linespacing will
certainly read better than other with no linespacing at all. The German
blackletter used by Gutenberg in his Bible, however, is almost
unreadable to a contemporary westerner's eyes and definitely alien to
someone from a non-Western background. In the fifteenth century, though,
it was certainly the norm. History can help to avoid repeating mistakes,
but it also shows the relative importance of our current standards.

In short, we still cannot objectively define type, and won't be able to
before a major breakthrough in neural science. However, copyright issues
and legal matters impose formal specifications on what a font is and
what it is not. Whether a typeface is a tweak, a revival or a work of
art is left to the courts.

3\. TWEAKING AND REVIVING

In order to explain the type designer's first reluctance to embrace the
digital alternative, and also understand how design processes are not as
straightforward as they are presented to us, we'll concentrate on Fred
Smeijers's account on the current state of events in typography.
Specifically, we'll borrow his term "font tweaking"\[5\]. This process
consists of loading a font, "tweaking" it - altering small details - and
releasing them with different names. Smeijers is clear in pointing that
font tweakers have nothing to do with type design at all, reinforcing
the distinction between doing type as a labour of love and doing it for
a profit.

Font revivals, on the other hand, are re-interpretations of existing
designs, and our best example would be Morison's effort in bringing the
classical designs into the Monotype type library. Revivals matter to us
because they aren't original productions (as they draw inspiration from
existing designs) but aren't copies either (because no rights over them
could be warranted otherwise, since there would be no original idea).

Digital type foundries and vendors still maintain the tradition,
digitising and redoing the old masters' work. It's worth noting that
even if a certain typeface, such as those with expired copyrights,
resides in the public domain, anyone can make a digital version - a
revival - and claim the rights to it.

Digital type catalogues are rife with revivals: In Bringhurst's
inventory of digital foundries\[6\], we can find 14 that issue revivals,
and 4 that only release original designs. This interest in resuscitating
previous designs also has motives that stand apart from simple
typographic archaeology. Revivals are routinely issued by vendors and
foundries to protect the rights of the rightsholder when a typeface's
copyright is about to expire. Such is the case with Avenir LT, Adobe
Garamond and Frutiger Next - which is what allowed Linotype to retain
the rights to the original design and be able to sue Microsoft.

Revivals reside in a kind of legal in-between - some, like Arial (which
is more a tweak than a declared revival), manage to stick around; while
others, like Segoe, raise copyright lawyers' eyebrows.

Given these two aspects, one cannot but wonder that a type designer
wouldn't be thrilled with this perspective. One has also to question why
there is such a rift in reactions between font tweaking and font
revivals, which can be interpreted as no more than corporate font
tweaking. A practical example of this is MyFonts.com's description of
the Avenir LT font (LT stands for Linotype) - a "recut version of
Avenir", stating that "The 'LT' was added to the name as the metrics
differ from the original version". This definitely corresponds to
Smeijers' description of font tweaking, despite the fact that the name
change wasn't intended to avoid legal troubles, but to assert the brand
of the author of the revival. What is a revival, then, other than a
corporate-sanctioned font tweak?

4\. TECHNOLOGY ON ARCANE STANDARDS

The current terminology used in typography is also a clear signal of how
it still depends on former traditions instead of adapting to its new
medium.

Digital typography's rules and terminology have been determined by its
physical counterparts, and that still hasn't changed. For example, we
still talk about "leading" - a term for the spacing between lines that
takes its name from the lead strips used for that purpose - although the
term "line spacing" is gradually replacing it in user-oriented
applications such as Microsoft Word.

Another example: while type foundries got that name because of their
heavy use of metal, single-person studios with Macs are still referred
to as "foundries". And fonts are described as being "cut" or "cast",
more than "digitised". We talk about "digital versions" instead of
digital copies, perhaps to preserve their history and soul and not treat
them as just another file in a user's computer.

Although we can forgive this persistence in using traditional
typesetting terms (mayhap as a historic homage), it also is a symptom
that the type activity and business have failed to redefine themselves
for the digital medium. On the other hand, these examples can actually
be interpreted as quite an artificial and linguistic way to value the
work of the typographer, probably with the aim of distinguishing between
"true" type designers and mere font tweakers, and not let "true"
typography be contaminated by the creeping tweaker threat.

5\. WHAT NOW

Given that digital type is hanging around for thirty years, the progress
in improving on font technology and taking advantage of the digital
medium has been rather dim. On the other hand, type designers in general
(with the exception of rare cases such as Emigre) have not tried to get
to grips with font technology, rather limiting themselves to drawing and
tracing their designs in Fontographer and selling them on major font
vendors (MyFonts, Monotype) or independent ones (such as T26 and Veer).
Worse still, issues of originality and plagiarism have been discussed in
type design circles, but corporate entities break them routinely while
trying, at the same time, to assert their rights in courts.

The difference between major and minor vendors is not substantial:
though distributors like Veer try to create a community and improve on
the users' and designers' experience compared to major sellers through
research, designer spotlights and support, digital typefaces are still
regarded in an esoteric limbo between metal characters and abstract
data. And though the price tags have steadily declined (and recently
stabilised in the 20 dollar range in general), it is revealing that
business models like iTunes or Flickr, or collaborative methods in
producing typefaces (many typographers are still lone workers) haven't
shown up yet, and that file formats have changed so little in the face
of recent, sleeker solutions like XML and SVG. And there's little hope
for innovation: the Adobe-Macromedia and Monotype-Linotype mergers have
paved the ground for a corporate monoculture ruled by software and
typeface vendors and distributors, with very little margin for
competition.

We can also point a mutual apathy between developers and designers as a
possible reason - type designers try to adapt to outdated ways - file
formats and type tools - to create their works, while developers lag in
keeping up to date to new breakthroughs. Limiting the tools is limiting
the imagination.

On the other hand, font vendors have an incredibly contradictory stance
regarding font rights, using copyright law to protect their products
while violating it to borrow from others'. The different fate of Arial
and Segoe begs the question: are the vendors and distributors handling
this as it should be handled?

This model's obvious contradictions definitely invite serious
questioning as to the legitimacy and validity of the current type market
and business model, which cannot effectively release its standards and
technology because of the threat of competition. It's therefore left to
users, designers and independent developers to shape a new way of
defining type and creating effective communication channels between
providers and users, be it through online communities or real-world
discussion in type designer's circles and colleges.

As soon as this new model comes to fruition, type vendors will probably
have no more reason to stay in business; they could sustain an
artificial market for some years (like they have done in the last
decades), but as the designers and their target audiences gradually
learn how to directly contact with each other by means of personal
websites and type communities, the type design activity would be again
open to re-define its identity, freeing itself from the arcane
traditions of cuts, heavy presses, price tags and copyrights.

a\. Notes

\[1\] Friedl, Ott, Stein: Typography: An encyclopedic survey of type
design and techniques through history. (p. 409)  
\[2\] Arial is now a "standard" font of web typography, being part of a
very limited set of fonts that all browsers can read.  
\[3\] Smeijers, Fred: Type Now. (p.25)  
\[4\] id., p. 40  
\[5\] id., p. 32  
\[6\] Bringhurst, Robert: The Elements of Typographic Style. (p.309)

b\. References

Bringhurst, Robert: The Elements of Typographic Style. Vancouver,
Hartley & Marks, 2002.  
Smeijers, Fred: Type Now. London, Hyphen Press, 2003.  
Friedl, Ott, Stein: Typography: An encyclopedic survey of type design
and techniques through history. London, Black Dog & Leventhal, 1998.  
Steinberg, S.H., and Trevitt, John: Five Hundred Years of Printing (4th
Revised edition). London, Oak Knoll Press, 1996.

c: Online references

"The Scourge of Arial" by Mark Simonson (background and critical account
on Arial)  
http://www.ms-studio.com/articles.html

"Is Microsoft's Vista Font Just a Copy?" by Brian Livingston (news
article on the Segoe legal case)  
http://redir.internet.com/!search/itmanagement.earthweb.com/columns/executive\_tech/article.php/3599861

"Designer Says Vista Font Is Original" by Brian Livingston (followup on
the previous story)  
http://redir.internet.com/!search/itmanagement.earthweb.com/columns/executive\_tech/article.php/3599861

The Funny Font Forging Industry - A Report for Legal Authorities by
Ulrich Stiehl (aggressive report on font tweaking and appropriation)  
http://www.sanskritweb.net/forgers/\#FORGERS
