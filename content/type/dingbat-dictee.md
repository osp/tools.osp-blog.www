Title: Dingbat Dictée
Date: 2011-01-27 23:53
Author: OSP
Tags: Type, Recipe, Unicode
Slug: dingbat-dictee
Status: published

**Recipe for teaching Unicode poetry to [students of all
ages](http://www.notbored.org/avertissement.html)**: ((Previously tested
in [Brussels](http://zigzaganimal.be/?p=337) and
[Kiel](http://www.geuzen.org/gallery/The_Tailored_Alphabet/the_tailored_alphabet_workshop/page/3/).))

![]({filename}/images/uploads/dictee.jpg "dictee"){: .alignnone .size-medium .wp-image-5631 }

1.  Print out listings of the following Unicode blocks: **Dingbats**
    (2700–27BF), **Miscellaneous Symbols** (2600–26FF) and eventually
    **Miscellaneous Symbols and Arrows** (2B00–2BFF). It is important
    you include the descriptions in the language of your choice. Below
    are German, English and French sample files.
2.  Contextualize the Dictée by showing
    [this](http://www.mythic-beasts.com/~mark/random/unicode-poster/poster-complete-version-5.0-one-sheet-00-A4-300dpi.png)
    picture for example.
3.  One person selects a number of descriptions he or she finds
    interesting
4.  Read out the number, followed by it's description
5.  Allow participants a set time to come up with a proposal that fits
    the description. Depending on the medium of choice, rhythm can vary:
    pencil up to 30 seconds; cutting or digital drawing: up to
    10 minutes.
6.  Compare results of participants to each other and to official
    Unicode blocks

[unicode\_german]({filename}/images/uploads/unicode_german.pdf)  
[U2600\_FR.pdf](http://www.zigzaganimal.be/U2600_FR.pdf)  
[U2700\_FR.pdf](http://www.zigzaganimal.be/U2700_FR.pdf)
