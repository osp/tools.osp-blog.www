Title: Alpha font
Date: 2006-02-27 18:42
Author: Harrisson
Tags: Type, Libre Fonts
Slug: alpha-font
Status: published

![alphabetum.png]({filename}/images/uploads/2006/03/alphabetum.png){: #image62 }

Most amazing font so far, the Alphabetum font, for ancient languages. It
is now possible to compose text with languages that disappeared 5000
years ago. I have to say all my respect for such a nice and usefull
work! Thanks from university researchers that had to redraw all
archeologic inscriptions found on sites. Now those texts can circulates.

*Juan-José Marcos' [Alphabetum
font](http://guindo.cnice.mecd.es/%7Ejmag0042/alphaeng.html) is a large
Unicode font covering more than 4000 characters in the most recent
version. Although the full font is not free, costing €15 for individual
registration, a demo version of the font lacking about 500 glyphs
present in the full font can be downloaded for free. Coverage is
provided for classic and medieval Latin, ancient Greek, Old
Italic-Etruscan, Oscan, Umbrian, Faliscan, Messapic, Picene-Gothic,
Iberian, Celtiberian, old and middle English, Hebrew, Sanskrit, Runic,
Ogham, Ugaritic, Old Persian cuneiform, Phoenician, Linear B, Cypriot,
Aegean numbers, old and medieval Nordic.*

<http://guindo.cnice.mecd.es/~jmag0042/alphaeng.html>
