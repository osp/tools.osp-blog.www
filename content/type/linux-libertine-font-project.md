Title: Linux Libertine font project
Date: 2006-02-27 18:21
Author: Harrisson
Tags: Type, Libre Fonts
Slug: linux-libertine-font-project
Status: published

![libertine
pic](http://ospublish.constantvzw.org/blog/wp-content/libertine.png "libertine pic"){: width="346" height="47"}

The Libertine family font contains around 1500 "western" caracters among
which cyrillic, greek, turkish and so on. The font looks classical,
between Baskerville and Caslon style. The general impression is a
contemporary looking 19th century font. Hints are good looking above 8
points, and is comfortable on screen. Good typography seems to be
possible using it.

*Letters and fonts are two things in one: On the one hand they are basic
elements of communication and fundaments of our culture, on the other
hand they are cultural goods and artcraft.  
You are able to see just the first aspect, but when it comes to software
you'll see those copyrights and patents even on the most elementary
fonts. We want to give you an alternative: This is why we founded The
Libertine Open Fonts Project.*

<http://linuxlibertine.sourceforge.net/>
