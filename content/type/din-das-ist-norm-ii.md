Title: DIN - Das Ist Norm - II
Date: 2007-03-14 16:20
Author: admin
Tags: Type, Standards + Formats
Slug: din-das-ist-norm-ii
Status: published

[![plate\_new.jpg]({filename}/images/uploads/plate_new.jpg)]({filename}/images/uploads/plate_new.jpg "plate_new.jpg")

[![plate\_old.jpg]({filename}/images/uploads/plate_old.jpg)]({filename}/images/uploads/plate_old.jpg "plate_old.jpg")

"FE-Schrift or fälschungserschwerende Schrift (falsification-hindering
script) has been the only typeface used on new vehicle registration
plates in Germany since November, 2000. It was designed for the German
government in the late 1970s in the light of Red Army Fraction terrorist
activities, when it was discovered that with the then standard font for
vehicle registration plates (DIN 1451 road-sign font) it was
particularly easy to modify letters by applying a small amount of black
paint or black insulating tape. For example, it was easy to change a "P"
to an "R" or a "B", or an "L" or "F" to an "E". Modifications to FE-font
plates are somewhat more difficult as they also require the use of white
paint, which is easily distinguished at a distance from the peculiar
retroreflective white background of the plate, in particular at night."

From:  
<http://en.wikipedia.org/wiki/FE-Schrift>
