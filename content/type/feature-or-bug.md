Title: Feature or bug?
Date: 2009-02-04 13:04
Author: Femke
Tags: Type, Bugs, Design Samples, Printing + Publishing, Scribus, Type
Slug: feature-or-bug
Status: published

![]({filename}/images/uploads/cover_360_260_rvb1.png)

The FLOSS+Art book is finally rolling off the print-on-demand press and
in the spirit of the kinds of practices described in the book, GOTO10
distributes our 'source files' as a bittorrent: ((You can order a
printed copy of the FLOSS+Art book here:
<http://www.metamute.org/en/shop/floss_art> or download the pdf +
sourcefiles as a bittorent:
<http://thepiratebay.org/torrent/4671426/FLOSS_Art_v1.1>))

> Rather than just providing a “free” PDF, FLOSS+Art.v1.1.eBook-GOTO10
> is also available and contains all the Fonts, Images, PDF and Scribus
> source files that were used to make the book. Feel free to branch a
> translation or fork the chapters!

<!--more-->  
In that same spirit, an OSP-friend sent us a design-bug-report:

> `In the .pdf version, the Libertinage fonts are only appearing for the 14 first pages. From then on, it is something like Times New Roman. However, the fonts for the "footnotes" remain the same throughout the book. This thing happened with "Evince" and the "Acrobat Reader" on my linux, and I thought that maybe my computer was too slow or hadn't enough memory... I asked someone to check under Mac OSX, same thing. Finally, I got the .pdf printed at copy-shop (Windows) and it happened also.`

The font-issue he is experiencing, is luckily not a technical problem
((Even if we at OSP try to reserve the right to make mistakes, it would
have been sad to discover a technical mix up after having gone through
an already rather painful production process. We had misunderstood the
way RGB / CMYK conversion works in Scribus, and some texts in the first
version of the book had come out in 97% grey instead of full black.))
but might be a design version of the "feature-not-a-bug" phenomenon.

For the FLOSS+Art book, Harrisson and Ludivine created Libertinage ((The
font is included with the design source files, and also available from
the open font library:
<http://openfontlibrary.org/media/files/OSP/322>)), 27 different
variations on the free software font Linux Libertine. Linux Libertine
was designed to be used in place of staple-font Times New Roman, so it
is not surprising that it looks & feels more or less the same:
((<http://linuxlibertine.sourceforge.net/Libertine-EN.html>))

[![libertinagevstimes\_th]({filename}/images/uploads/libertinagevstimes_th.png "libertinagevstimes_th"){: .alignnone .size-full .wp-image-1747 width="400" height="53"}]({filename}/images/uploads/libertinagevstimes.png)  
<small>Times New Roman (top) vs. Linux Libertine</small> ((Instead of
the usual *The Quick Brown Fox jumped over the lazy dog*, this text on
silk pyjamas is used in Scribus Font Preview.
<http://ospublish.constantvzw.org/type/woven-silk-pyjamas-exchanged-for-blue-quartz>))

Each text in the FLOSS+Art book has been typeset in another version of
Libertinage. The A-Z versions are subtle derivations; in each version
only one letter of the alphabet has been altered. For the introduction
(which ends on page 14!) and footnotes, we used Libertinage Full, the
most extravagant of all 27 variations. I wished I had a nicer type
specimen to show you those transformations, but you get the point:

[![libertinage\_th]({filename}/images/uploads/libertinage_th.png "libertinage_th"){: .alignnone .size-full .wp-image-1747 width="400" height="53"}]({filename}/images/uploads/libertinage1.png)  
<small>Libertinage Full and Libertinage A-E. In Libertinage Full, each
letter of Linux Libertine has been transformed and functions as an index
to the A-Z variations.</small>

As The New Hackers Dictionary explains, "*a bug can be turned into a
feature simply by documenting it*".
((<http://www.jargon.net/jargonfile/b/bug.html>)) Now the question
remains whether it is a design bug or a design feature, that the
difference between a text typeset in Libertinage A and one typeset in
Libertinage B is easily overlooked?
