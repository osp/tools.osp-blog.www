Title: OSP @ Werkplaats Typografie
Date: 2007-10-09 09:57
Author: Femke
Tags: Type, Print Party
Slug: osp-werkplaats-typografie
Status: published

![werkplaats.JPG]({filename}/images/uploads/werkplaats.JPG)

OSP visits Werkplaats Typografie in Arnhem (The Netherlands) to discuss
a two-day workshop on Free Fonts with Anniek Brattinga and Karel
Martens. <http://www.werkplaatstypografie.org>
