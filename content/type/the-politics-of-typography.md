Title: The politics of typography
Date: 2006-03-01 19:56
Author: Femke
Tags: Type, Licenses
Slug: the-politics-of-typography
Status: published

The open source font **Gentium** and **The SIL Open Font License** are
both developed and distributed by [S.I.L.](http://www.sil.org), also
known as The Summer Institute for Linguistics, apparently a subsidiary
of the [Wicliffe Bible Translators](http://www.wicliffe.org). S.I.L. has
developed large-scale ethno-linguistic research projects such as
<http://www.ethnologue.org>, an attempt to map all indigenous languages
of the world. The S.I.L. site does not give much information about the
protestant character of it's mission, so we had to look for it somewhere
else. Marcio Ferreira da Silva (Universidade de Sao Paulo) about the
activities of S.I.L. in Brazil:

> "S.I.L.'s objectives are no different from those of any other
> traditional mission: the conversion of the indians and the saving of
> their souls. Their methods, however, are in some ways peculiar,
> incorporating a bilingual educational model which is an integral part
> of their evangelical strategy."

<!--more-->

> "The judicial and administrative references from the beginning of the
> seventies should therefore be interpreted as the coming together of
> the religiously dogmatic educational model idealized by S.I.L. and the
> indigenous framework put forward by the military regime. In the Indian
> Statute - a law passed in 1973 -, for example, there is explicit
> reference to teaching reading and writing "in the language of the
> group to which they belong", but nothing regarding the of official
> recognition of these languages as means of communication with these
> ethnically different minorities (...) Overall, the teaching of
> communication skills in native language, included in law in the
> seventies, is born of a purely instrumental missionary practice."

<http://www.clas.berkeley.edu:7001/Events/fall1999/10-28-99-ferreiradasilva/>
