Title: Bitstream Charter
Date: 2006-02-12 18:39
Author: Femke
Tags: Type, Libre Fonts, Licenses
Slug: bitstream-charter
Status: published

To my surprise **Bitstream Charter**, one of the few usable Open Source
fonts around I know of, is currently published on myfonts.com with the
following licence: <http://www.myfonts.com/viewlicense?id=315>

[![no
modifications]({filename}/images/uploads/2006/02/noMod.jpg)]({filename}/images/uploads/2006/02/noMod.jpg)

[Florian Cramer](http://cramer.plaintext.cc:70/) explains what is really
going on:

> "That's true, but the license change refers to a newer version of
> Bitstream Charter. A copyright owner of a work is free to change the
> licensing terms any time, either rendering a formely free work
> proprietary or vice versa. But a license change can never be
> retroactive, i.e. it can't affect the licensing terms of a previously
> released version of the same work. (Same happened to SSH for example:
> The original SSH continues to be developed as proprietary software,
> whereas OpenSSH - included among others in Mac OS X and Linux - was
> developed on the basis of an older, free version of the original SSH
> package.)
>
> An older version of Bitstream Charter was donated to the X Window
> system and continues to be available under the extremely liberal
> MIT/X11 license. In Debian and Ubuntu, the font is part of the package
> "xfonts-scalable" which is in the fully free standard distribution.
>
> (...)
>
> The
> "[Gentium](http://scripts.sil.org/cms/scripts/page.php?site_id=nrsi&item_id=Gentium&_sc=1)"
> font is another interesting typeface that just has switched to a free
> license. However, the license is still under review by Debian. It's a
> classicist font that looks more conservative than Charter, but it
> offers a greater number of international glyphs."
