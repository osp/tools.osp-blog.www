Title: Legal Soup
Date: 2009-08-18 18:08
Author: Femke
Tags: Type, Licenses, Type
Slug: legal-soup
Status: published

### 1. Unlimited Use License

Judging from the mysql errors flying around, the Open Records Generator
software developed by David Reinfurt, is not actively maintained at the
moment. Still, this
[GPL](http://www.o-r-g.com/STORE/OPEN-RECORDS-GENERATOR/License.txt)
licensed software presents an interesting mix of buyers and users: "*The
buyer receives full rights to modify and reuse the software for future
applications*" (found on:
[http://o-r-g.com/STORE/OPEN-RECORDS-GENERATOR](http://o-r-g.com/STORE/OPEN-RECORDS-GENERATOR/))

> Open Records Generator is available as Open Source Software. **By
> purchasing** this product from O R G inc., you will also receive the
> complete source code. Using this code, **the buyer** is free to alter,
> adapt and evolve this software to fit future projects.

### 2. Ecology

Dutch webdesign agency [Spranq](http://www.spranq.nl/) developed the
much publicised [Ecofont](http://www.ecofont.eu), based on the idea that
if you punch holes in Vera Sans, you decrease the amount of toner used
by 20%. The font can be downloaded for free, and on the project site
they refer to the fact that Vera Sans is 'open source', but
unfortunately none of this is carried over in to the derivative work. We
wrote:

> Hello,
>
> We saw an article on the Ecofonts you released in this month edition
> of Items. What a great idea to establish a connection between
> typography and ecology and also to create an inspiring example of how
> an 'open source' font can be used. We were therefore a bit
> disappointed to find that clear information on its legal status is
> missing. You indicate that you may download Ecofont for free, but the
> required original license is not included (See copyright Vera Sans:
> "The above copyright and trademark notices and this permission notice
> shall be included in all copies of one or more of the Font Software
> type faces"), and thus it is not clear what users can and can not do
> with your work. It creates unnecessary confusion about open source
> fonts, and wouldn't it be nice (and beneficial for the environment!)
> when other designers would feel invited to contribute a serif version
> or apply the same principle to another font?
>
> Kind regards,
>
> OSP

(Spranq responded within an hour and the downloadable font now includes
a correct copyright notice)

### 3. You may use this font only if you ...

@fontface is changing the rules of 'free' typography, which is confusing
fontdesigners everywhere. (Free Font License found on:
<http://www.myfonts.com/viewlicense?id=706>)

> You may use this font for Font-Face embedding, but only if you put a
> link to www.exljbris.nl on your page and/or put this notice /\* A font
> by Jos Buivenga (exljbris) -&gt; www.exljbris.nl \*/ in your CSS file
> as near as possible to the piece of code that declares the Font-Face
> embedding of this font.
