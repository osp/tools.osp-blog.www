Title: An update on the status of Utopia
Date: 2008-07-09 12:37
Author: Femke
Tags: Type, Libre Fonts, Licenses
Slug: an-update-on-the-status-of-utopia
Status: published

As you might have gathered from Thomas Phinney's [latest
comment](http://ospublish.constantvzw.org/?p=504#comment-40348) on our
post **[The Status of
Utopia](http://ospublish.constantvzw.org/?p=504)**, Adobe will not
re-release Utopia under an Open Font License. It doesn't mean though the
font cannot be studied, copied, modified and distributed: "*Although
changing the license would make it easier for folks who find licenses
confusing and don't want to read them*" writes Phinney in an e-mail,
"*my conclusion was that the existing license was close enough to open
source for most people's actual usage, if not for their brains :-)*"  
<!--more-->  
And indeed, when you read carefully, the existing license is peculiar,
but in fact open. It starts with allowing TeX users to make
modifications: "*Adobe also grants to the TeX Users Group a license to
modify the Software for any purpose and redistribute such modifications,
for any purpose and royalty-free, provided that the modified Software
shall not use the font name(s) or trademark(s)*", and thanks to Karl
Berry, TUG President, this invitation is extended to "*any and all
interested parties.*" ((License for the Utopia Typeface:
<http://archive.cs.uu.nl/mirror/CTAN/fonts/utopia/LICENSE-utopia.txt>))

On the one hand, the Utopia license is an interesting read, a narrative
account of changing technologies, people and ideas, contained in it's
particular phrasing and structure. But on the other hand, such a
labyrinth license hardly contributes to a typographic practice where
designers open up about their appropriations and affiliations. It is not
that we don't want to make an effort (hence this series of posts), but
if it was all somehow a bit more inviting and explicit, it would be
easier to make a difference to the culture of fear that surrounds the
author rights management of fonts.

Another argument for a more standardized licensing solution, is
discussed in the margins of the [Open Font
Library](http://openfontlibrary.org/) mailinglist. Sticking to
OFL-compatible licenses that are both machine and human readable would
facilitate the embedding of fonts in web pages. ((The so-called
@font-face solution is promoted by Håkon Wium Lie, W3C member and CTO of
the Opera browser: <http://www.alistapart.com/articles/cssatten>. Opera
is shipped with the Adobe Creative Suite by the way...)) The idea is,
that fonts uploaded to the Open Font Library in the future can be
'served' to web pages anywhere, which could motivate typographers to
release their fonts in the public domain or under an OFL. ((Scroll down
to the interesting discussion between Thomas Phinney and Dave Crossland
on intellectual property, DRM and embedding fonts:
<http://blogs.adobe.com/typblography/2007/11/web_fonts_1.html>))

Utopia was designed by typedesigner [Robert
Slimbach](http://www.identifont.com/show?17Z) in 1989 for Adobe and it
is this version that was donated to the X-consortium, now [X-org
foundation](http://www.x.org/wiki/), an open source implementation of
the X Window System. Like Bitstream Charter, donated to the X-org
foundation in 1991 ((See our earlier post on the status of Bitstream
Charter: <http://ospublish.constantvzw.org/?p=11>)), Utopia exists both
as an open and as a proprietary font, sold by Adobe under a conventional
license. The latter is a version that Slimbach heavily reworked as part
of it's conversion to the Open Type Format. ((David Lemon on the Typo-L
mailinglist:
<https://listserv.heanet.ie/cgi-bin/wa?A2=ind0305&L=TYPO-L&P=14185>)) So
if you plan to modify, use or redistribute Utopia, check whether you
start from the *open* version, which is downloadable from the TUG
website in .pfb format:  
<http://www.ctan.org/tex-archive/fonts/utopia>

In 2006, Adobe re-licensed the open version of Utopia to the TeX Users
Group, under similar terms as stated in the original license, but now
explicitly clarifying that modifying the fonts, and redistributing
modified versions, was allowed. Apparently, this was always the intent
but the original wording was ambiguous. ((See the Read Me included with
Utopia available from the Tex User Group website:
<http://www.ctan.org/tex-archive/fonts/utopia/>))

At this time, Adobe has no plans to release other fonts under open
license terms. Nor do they have any other fonts currently available
under anything resembling an open source license. But, "*Things change
quickly, sometimes*", Thomas Phinney writes.

Of course we're already thinking about many things we could do with
Utopia, and are even more curious about what you might make of it.

Keep us posted on your modifications!
