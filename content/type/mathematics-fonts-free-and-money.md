Title: Mathematics, fonts, free and money
Date: 2008-05-17 11:02
Author: Pierre
Tags: Type, LaTex, Libre Fonts, Reading list, Retrospective Reading
Slug: mathematics-fonts-free-and-money
Status: published

In a [déjà old interview (2000) by
Advogato](http://www.advogato.org/article/28.html), [Donald
Knuth](http://en.wikipedia.org/wiki/Donald_Knuth) (TeX and Metafont
author) answers in his sometimes-very-short sometimes-generous style. In
the middle of these all interesting things, a few exchanges about
relations between mathematics, fonts, free and money. Enough close to
some parts of talks at Wroclaw, like the Dave Crossland's one, and some
of our interviews, to serve as an intuitive background.

[![]({filename}/images/uploads/tex.jpg "Computer Modern"){: .alignnone .size-full .wp-image-497 }]({filename}/images/uploads/tex.jpg)  
<!--more-->  
**Advogato :** There was a quote that you had in the "Mathematical
Typography" essay reprinted in "Digital Typography" where you said,
"Mathematics belongs to God."

**Donald Knuth :** *Yes. When you have something expressed
mathematically, I don't see how you can claim... In the context, that
was about fonts. That was when I had defined the shape of the letter in
terms of numbers. And once I've done that, I don't know how you're going
to keep those numbers a secret...*

\[...\]

**Advogato :** Fonts seem like a really interesting edge case for that
argument, because a font is in some ways a mathematical formula,
especially a TeX font, much more so than what came before, but it's also
an artwork.

**Donald Knuth :** *Absolutely. It absolutely requires great artistry.
So the other part of this is that artists are traditionally not paid
like scientists. Scientists are supported by the National Science
Foundation to discover science, which benefits the human race. Artists,
or font designers, are not supported by the National Font Foundation to
develop fonts that are going to be beneficial to the human race. Fonts
are beneficial to the human race, they just don't traditionally get
supported that way. I don't know why. They're both important aspects of
our life. It's just that one part has traditionally gotten funded by a
royalty type mechanism and the other by public welfare grants for the
whole country.*

**Advogato :** Perhaps that has something to do with the absolute
necessity in science to have open access to the results of others, that
if you did science in a closed, proprietary framework that the
disadvantages would be so clear.

**Donald Knuth :** *With fonts, it was pretty clear to me.*

\[...\]

**Advogato :** You've gotten a number of free fonts contributed by
artists, in some cases very beautiful fonts, to TeX and to the Metafont
project. In general, this has been a real struggle for open source
development these days *(OSP : in 2000!)*, to get free fonts. Do have
any thoughts?

**Donald Knuth :** *I think it's still part of this idea of how are the
font designers going to get compensated for what they do. If they were
like a scientist, then they've got their salary for doing their science.
But as font designers, where do they get their salary? And musicians.
It's just a matter of tradition as to how these people are getting
paid.*

**Advogato :** But how did you address those problems with the fonts
that got contributed to TeX?

**Donald Knuth :** *In my case, I hired research associates and they put
their fonts out into the open. Or else, other people learned it and they
did it for the love of it. Some of the excellent fonts came about
because they were for Armenian and Ethiopian and so on, where there
wasn't that much money. It was either them taking time and making the
fonts or else their favorite language would be forever backwards, so I
made tools by which they could do this. But in every case, the people
who did it weren't relying on this for their income. If we had somebody
who would commission fonts and pay the font designer, the font designer
wouldn't be upset at all about having it open, as long as the font
designer gets some support.*

**Advogato :** And you did some of that.

**Donald Knuth :** *Yeah. In fact, I worked with some of the absolute
best type designers\*, and they were thrilled by the idea that they
could tell what they knew to students and have it published and
everything. They weren't interested in closed stuff. They're interested
in controlling the quality, that somebody isn't going to spoil it, but
we could assure them of that.*

**Advogato :** Right. Working with the creator of the software.

**Donald Knuth :** *Yeah, if they didn't like the software, I could fix
it for them.*

(\* Herman Zapf, Matthew Carter and lots of others known names has been
around Knuth for some times thirty years ago, but the complex and
difficult relations between designers and Metafont definitely need a
separate and future post, I'm still a nàíve garçöñ...)
