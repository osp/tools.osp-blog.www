---
Title: html2print as a practice of boilerplates
Date: 2023-06-23 16:00
Authors: Alex, Manetta
Tags: shoulder-to-shoulder, html2print, boilerplates
Slug: a-practice-of-boilerplates
---

In the middle of 10 years of web-to-print practices and on the crossing of different ways of working with HTML and CSS to make printed matter, including `html2print`, `OSPKit`, `CTRL+P` and `Paged.js`, there is a lot of material lingering around in the OSP git repositories. It's hard to find time for digestion when being in the middle of commissioned work, projects and other jobs. As satellite member and resident, we found some time for it. 

Where to start if you want to explore the web-to-print practices of OSP? 

We compiled an list of repositories below, annotated with notes and bits of context, to provide some handles for navigation.

As these repo's bundle a set of traces of a moment in time, you could say that these repositories are operating as *boilerplates*, crossing with multiple people, organisations, ideas, tools, aesthetics and timelines. The term "boilerplates" also came up during an online radio conversation hosted by Varia, in which OSP was invited to speak about their web-to-print practices. The snippets below are a selection of this conversation, focused around this term.

<br>

---------

<small>Alex Leray, Amélie Dumont, Gijs de Heij and Doriane Timmermans (OSP) in conversation with Simon Browne and Manetta Berends (Varia) at the OSP studio, in the late afternoon of Wednesday the 7th of September 2022. <br> Initially broadcasted in the context of the Publishing Partyline, a two day event in October 2022. The full conversation can be accessed and listened to here: <https://cc.vvvvvvaria.org/wiki/Standards_and_work_arounds><br></small>

**Gijs**: Html2print is a kind of a recipe or a boilerplate, which I think was initially created by Stéph [Stéphanie Vilayphiou], where you try to make a minimal version of what a book or printed publication is, using HTML and CSS and specifically CSS Regions. And possibly some Javascript to make things easier for you. For example to have not a fixed set of pages, but to use Javascript calculate how many pages you need. In the sense that this boilerplate, this HTML page, is really a document that’s filled with `<div>`’s, and these `<div>`’s have specific classes, like a page class or a crop-bar top-left, or a bleed-box. And a certain basic CSS that sets the dimensions of the page and also the dimensions of bleed-box. 

But we also realized today that there are like a plethora… there are so many versions of this recipe, they’re many plugins. And we also realized that often when we do a project with html2print, we kind of copy this boilerplate and start to modify it. So in a way we all have a different version of it, it’s kind of personal. And there is also intentionally… It’s quite similar to vim, the editor, which I personally don’t use… but this idea that it’s a personal tool that you make your own, that there are plugins available and that you extend it. 

What is also interesting about it is… We use git in our practice to share projects amongst each other, but also the html2print, this collection of HTML files, the actual structure of the pages with the content, is also part of the repository. So it’s like… It kind of is ingrained within the project.

**Manetta**: So you would say it’s difficult to see html2print without the practice, the content, the people? 

**Alex**: Yeah for me, I think, but that’s my personal opinion, I think we have 4 different opinions around the table, but for me it has never really been a tool, a fixed or solid tool. And I think that is maybe the reason why we never managed to make a proper package of it, like as a project. And for me it’s more like a collection of practices and the tools that support these practices. The tools and all this kind of knowledge we accumulated over the years are dispersed. It’s really difficult to separate this from the projects. 

**Gijs**: So for our own practice, I think that in a way Médor was… Or no sorry, first it was Balsamine, it started there. There was this intimate relationship with the theater or at least with the artistic directors of the theater, who gave us the space to do this experiment. 

**Manetta**: Balsamine is the theater in Brussels here right? 

**Gijs**: Yes and so in a way they’re part of the tool or at least they’re part of the history of the tool. And there is something about the role of the tool at Médor. Because I think all the journalists are very aware of this tool being used, and it being part of the making of the layout. There are people who use the boilerplate outside of OSP. But in a way, in our git repository, we see the projects that we have done with it. I don’t know and have personally no idea of the scope of the usage of it. 

**Doriane**: On this idea of html2print as a tool versus html2print as an approach, I think now in the Médor context, when we think about html2print, it comes with the CSS Regions. Which we will talk about later. It is dependent on one specific browser which we had to develop to be able to use CSS Regions. 

So sometimes, even though it’s more of an approach then a tool, you end up in a context we’re you’re like, okay, I want to do web-to-print, but I kind of have to choose between using this browser, which CSS Regions, or other web-to-print tools like Paged.js. Even if it’s an approach, sometimes this approach can materialize itself into a choice of tools, that are more specific and more material. 

**Manetta**: Can we call this a boilerplate practice?

**Alex**: Yes, there are many boilerplates around. Like almost every project is a boilerplate. In my case I often start with finding a project that fits the new project and try to take bits here and there. And recreating and gathering all the tools I need for the new project. 

I think the boilerplate that is the most complete, that combines all the features would be Médor, because in a way it’s like the most, not advanced that is not the right term… It has been going on for a long time and we had time to consolidate a lot of stuff. It involves almost all the requirements, like going to the printer, turning the PDF into CMYK. In my case it’s a bit THE boilerplate. 

But at the same time, it’s also split it into different bits. Like CMYK conversion is a separate git repository, that is dedicated to taking a bunch of RGB files and having some make scripts to turn a PDF into a CMYK PDF. But even there, you have to adapt it to your project, because you have to generate the right color profiles. And you have to change the code to make to right number of pages for the booklets and so on. So it’s not something you can just take as a tool. It’s really like a boilerplate. 

<br>

---------------

## List of OSP WebPrint-related repositories

<br>

### [work.spion](https://gitlab.constantvzw.org/osp/work.spion)

A repo from 2012. First attempt at making a print poster using HTML :)

### [Anna Kavan book](https://gitlab.constantvzw.org/osp/annak.print)

Derived from Médor first issues "boilerplate".  
Note: Using css regions polyfill for non-native support of css regions.

A script to fix common (French) microtypo rules: <https://gitlab.constantvzw.org/osp/annak.print/-/blob/master/bin/microtypo.py>

### [OSPKit](https://gitlab.constantvzw.org/osp/tools.browser) 

(initial repo? outdated)

### [Ether2HTML](https://gitlab.constantvzw.org/osp/tools.ether2html)

Skeleton to design a webpage (for screen or print) synchronously on an etherpad.

### [HTML2print](https://gitlab.constantvzw.org/osp/tools.html2print)

This little tool is a boilerplate, a minimal example to start a print project using HTML, less/CSS and Javascript/Jquery to design it.

Note: not really a boilerplate anymore since it has received modifications for specific projects  
There are many branches.  
The branch `devel` seems to be the most recently updated branch.  
A decent README, though probably not up-to-date.  
Also contains an attempt at documention through use cases/examples.

<https://gitlab.constantvzw.org/osp/tools.html2print/-/tree/merge-emile/examples>

### [tools.html2print.doc](https://gitlab.constantvzw.org/osp/tools.html2print.doc/-/tree/master/content)

Another attempt at documenting html2print.  
Great traces/glimpses of doc here by Quentin Astié!: 

### [tools.html2print-tiny](https://gitlab.constantvzw.org/osp/tools.html2print-tiny)

Empty repository! But should contain an extremly minimal "CTRL+P" only version of HTML2print

### [tools.html5lib_typogrify](https://gitlab.constantvzw.org/osp/tools.html5lib_typogrify)

A set of filters to correct common typographical mistakes (for French Typography).

### [tools.ospkit](https://gitlab.constantvzw.org/osp/tools.ospkit)

This browser is meant to be used with the project html2print available here: <http://osp.kitchen/tools/html2print/>. The aim is to lay out printed documents within a web browser. We built our own webkit browser in order to have a faster browser and good typography (weirdly, the bearings and kernings can be weird in certain webkit browsers). (and have a native implementation of CSS regions which allows us to layout multi-pages documents)

### [villa-arson.html2print](https://gitlab.constantvzw.org/osp/villa-arson.html2print)

A django project for running a generic service to generate pdf from any html page.

The idea was that you could enter any url, select part of the document, associate css (a bit like Jsfiddle) and it would flow that into a template rendered by a headless version of ospkit.
Seems down for a while now.

### [work.adva-zakai.overbooked](https://gitlab.constantvzw.org/osp/work.adva-zakai.overbooked)

Book design with HTML2print (2015-17 from the commit dates). Some crazy imposition plans with OSP it seems.

### [work.balsamine.2013-2014](https://gitlab.constantvzw.org/osp/work.balsamine.2013-2014)

First Balsa programme done with a browser.
Many tricks were found at that time.

The post-gutenberg issue.

> Making of. 
> 
> Mode lyrique — Répondre à et infuser la programmation spéculative de la Balsa par un déboitement un peu perché au-dessus des sillons tracés par Gutenberg il y a 550 ans. OSP tente une composition des textes et des images en utilisant les langages qui transforment le web mois après mois. Les propositions neuves et encore hésitantes sur leurs pattes de jeunes poulains de ces langues qui pensent le web à venir élargissent brutalement la manière dont des mots, des phrases et des visuels cohabitent comme des blocs de glaces sur une rivière à la fonte des neiges. La notion de page est soudain nettement plus flottante et intervient à la fin du processus comme une scansion temporaire, comme une résille au rectangle pointe d'autres potentiels. Un document du temps qui coule.
> 
> Mode technique — Depuis deux ans et demi, de multiples logiciels ont été utilisés pour la production graphique de la communication de la Balsa (fig 1). Cette saison (et peut-être bien à partir de cette saison), OSP a décidé de faire le mur et de sauter dans le verger ombragé de l'HTML récent et des CSS plus récents encore (fig 2). L'un et l'autre sont sortis de leur contexte naturel du web pour venir s'aventurer à produire les pages d'un petit livre. La liste des fonctionnalités nécessaires et des solutions qui peuvent y répondre se garni (même si elle reste un peu moto-cross - fig 4). Concrètement, en gros, les 48 pages de ce programme sont concentrés dans une longue et grande page web (html + css). Un javascript dessine les marques de repérages nécessaire aux imprimeurs page par page. Cette page est imprimée vers des pdf en fonction de leur couleur séparée. Le fichier de la mise en page de la Balsa peut-être visitée à l'aide de patience et d'un navigateur qui utilise le moteur libre Webkit le plus récent possible, comme Chromium, et en activant les Webkit Experimental Features dans la page `chrome://flags/`.

<http://osp.constantvzw.org/work/balsamine.2013-2014/tree/master/programme/programme.html>

### work.balsamine.20{14..21}-20{15..22}

Subsequent programmes of Balsamine Theater all made with variants of HTML2print.

<https://gitlab.constantvzw.org/osp/work.balsamine.2014-2015> proto HTML2print  
<https://gitlab.constantvzw.org/osp/work.balsamine.2015-2016> derived fron html2print boilerplate  
<https://gitlab.constantvzw.org/osp/work.balsamine.2016-2017> derived fron html2print boilerplate  
<https://gitlab.constantvzw.org/osp/work.balsamine.2017-2018> derived fron html2print boilerplate  
<https://gitlab.constantvzw.org/osp/work.balsamine.2018-2019> derived fron html2print boilerplate  
<https://gitlab.constantvzw.org/osp/work.balsamine.2019-2020> Main booklet in Scribus  
<https://gitlab.constantvzw.org/osp/work.balsamine.2020-2021> Main booklet with ether2html  
<https://gitlab.constantvzw.org/osp/work.balsamine.2021-2022> derived fron html2print boilerplate

### [work.iselp50](https://gitlab.constantvzw.org/osp/work.iselp50)

Ether2html-derived project.

### [work.medor.maquette](https://gitlab.constantvzw.org/osp/work.medor.maquette)

Some kind of stripped-down boilerplate for layouting one-shot articles?  
Old attemps, 7 years ago.  
Was used to layout a Medor article that was not part of an issue but distributed alone on the former website.  

### [work.medor.samples](https://gitlab.constantvzw.org/osp/work.medor.samples)

Half-baked standalone article attempt for Medor. Not really usable.

### [workshop.esadse.elif](https://gitlab.constantvzw.org/osp/workshop.esadse.elif)

HTML2print boilerpate derived project.  
A workshop at Esadse (Saint-Etienne School of Arts).

Documentation here: <https://gitlab.constantvzw.org/osp/workshop.esadse.elif/-/blob/archive/Documentation.md>

### [work.5blokken](https://gitlab.constantvzw.org/osp/work.5blokken)

A project made with Paged.js.

### [work.aesthetic-programming](https://gitlab.constantvzw.org/osp/work.aesthetic-programming)

Programming textbook made with Pelican, html2print, git and Paged.js, combined with Graphviz diagrams.

### [osp/workshop.saison-graphique](https://gitlab.constantvzw.org/osp/workshop.saison-graphique/)

In the journal directory, a text by Nicolas Malevé on OSP that was layed out using an old boilerplate of HTML2print.  
The content was collaborativelly written in a pad (like Ether2html) and the css too.  
There is a small javascript that "replay" the layout in the making, like the timeslider of Etherpads.

### [workshop.strasbourg-html2print](https://gitlab.constantvzw.org/osp/workshop.strasbourg-html2print)

Workshop at HEAR Strasbourg. The same files produce different ouputs using CSS media queries, aka responsive design for the print.

> «L'atelier propose d'approcher la mise en forme d'un texte de manière liquide, de débrancher les réflexes de blocs» (cf Gutenberg)

Based on HTML2print boilerplate.

### [work.the-riddle](https://gitlab.constantvzw.org/osp/work.the-riddle)

A thick book on architecture using html2print boilerplate and (from what I remember) content edited on gitlab by the writer/editor.
A epub version was also produced.

Some javascript in there to manage footnotes and colorize images. Big work on the appendix.

### [work.variable-publication](https://gitlab.constantvzw.org/osp/work.variable-publication)

Rue Gallaitstraat residency of Constant and co. report???

Plugged into etherpads for collaborative editing of the content and styles.

The publication and its editing interface are together in the same HTML. 

A commit message says: 

> «Copier/coller du boilerplate html2print.

From the README:
  
> Design of Constant/Variable publication.
> 
> Print party in 2 acts with HTML.
> This work relies on the experimental css regions feature of the webkit engine. 
> It is known to work on:
> * chromium 33 
> * epiphany 3.12.1-1 
> * safari 7.0.2

### [work.villa-arson.diplomes2015](https://gitlab.constantvzw.org/osp/work.villa-arson.diplomes2015)

Villa Arson graduation website module for web2print made in 2015.

From the README:

> Every year, the students of the art school of la Villa Arson, in Nice, France, show their work in a collective exhibition. A website comes along the exhibition to be used as a portfolio for these young artists who don't necessarily have a website yet. The website has been developed in Wordpress internally to the school with some graphical advices from OSP. OSP has then branched the Wordpress to the html2print boilerplate to design PDFs for each of the student and one gathering all the works.

<http://diplomes2015.villa-arson.org>

Every visitor can print the PDF by itself by using the Print dialog of their web browser (tested on Chrome, Firefox, Safari, Epiphany).

Note: using the css-regions polyfill to support non-css-regions browsers.

### [villa-arson.edition-experience](https://gitlab.constantvzw.org/osp/villa-arson.edition-experience)

Hybrid website/print booklet for a seminar on edition.

> L’édition comme expérience  Apprentissage, coopération et transmission par les projets éditoriaux. 

Webprint page for the event, using css grid and media queries.

### [work.villa-arson.ecart](https://gitlab.constantvzw.org/osp/work.villa-arson.ecart)

Website for the 2015 publication of Réseau ECART (European Ceramic Art & Research Team).

> «The website has been created with Ethertoff, a simple collaborative web platform, a wiki featuring realtime editing thanks to Etherpad. Its output is constructed with equal love for print and web.» :)

<http://incertains-genres.reseau-ecart.eu>

<br>

-----------------

## List of OSP WebPrint-related repositories hosted on <https://gitlab.constantvzw.org/medor/>

<br>

### [medor/boilerplate](https://gitlab.constantvzw.org/medor/boilerplate)

This is a boilerplate for the belgian quaterly Médor, based on HTML2print.

Note: 6 years old although it hasn't changed much since.

### [medor/boilerplate2019](https://gitlab.constantvzw.org/medor/boilerplate2019)

It's all in the name!

In the README, an attempt at documenting the install of OSPKIT and how to compile `qtwebkit`.

### [medor/medor0](https://gitlab.constantvzw.org/medor/medor0)

Yet another boilerplate for Médor. More up to date: changes/improvements/update made during medor issue are pushed back to this repo (but not consistently by everybody).

The README says: 

> «Maquette médor pour démarrer un nouveau numéro avec tout les layouts, une cheatsheet, et petites corrections. Cette maquette est un fork du numéro 21, cleanée et restructurée.»

A big cheatsheet to dive into with nice croner cases encountered with HTLM2print and Médor :) 

<https://gitlab.constantvzw.org/medor/medor0/-/blob/master/cheatsheet.md>

### Private repos (because we don't take time to make them public after the issue is launched :/)

https://gitlab.constantvzw.org/medor/medor{14..31}

https://gitlab.constantvzw.org/medor/numero{1..13}

### [medor/playground](https://gitlab.constantvzw.org/medor/playground)

> Répertoire de tests pour la mise en page du magazine : essais typographiques, css grid tetris games, spicy styles. Aller !

A few tests based on HTML2print boilerplate.

<br>

---------------

## PDF tools

<br>

### [tools.pdfutils](https://gitlab.constantvzw.org/osp/tools.pdfutils)

Initial (?) repo that was the basis for the rgb2cmyk repo (hosted on both Medor Gitlab and OSP)

The readme mentions the following features:

- Convert PDF RGB to only-Black 
- Convert PDF from RGB to CMYK
- Convert PDF from RGB to CMYK with (black) Overprint
- Combine PDFs having different color modes (including remapping black to PMS spot color)
- Check color separations (a set of ghoscript/html scripts generating a webpage to preview CMYK seperation)

Problem: some of those scripts convert PDF to PS to PDF back which leads to issues, among them images with transparency that get pixelated...
RGB2CMYK fork repo was an attempt at solving that by avoiding the PDF<>PS conversion but lack a lot of features.

### [tools.rgb2cmyk](https://gitlab.constantvzw.org/osp/tools.rgb2cmyk)

Stripped-down, modernized version of the tools.pdfutils repo above.

It was an attempt to moved the RGB2CMYK script out of HTML2print in a spirit of de-coupling rgb2cmyk script from the other utilities (like checking color seperation).
But in the end it is behind tools.pdfutils because it was updated.

<br>

---------------

## A few more repo's

<br>

There is overlap with the repo's above, but here are some more repo's made for Médor: 

<https://gitlab.com/medor/>



<br>

-----------------

