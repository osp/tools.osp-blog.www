---
Title: Installing OSPKit on Debian 11 and finding my html2print boilerplate
Date: 2023-06-23 16:00
Author: Manetta
Tags: shoulder-to-shoulder, OSPKit, html2print, README
Slug: ospkit-on-debian-11
---

How to install OSPKit on an *updated* operating system?

In the middle of many web-to-print projects that are made with Paged.js these days, exploring `OSPKit` and `html2print` feels like fresh breeze on a hot early summer day, even though OSPKit is stuck in 2016. But as I have never tried to install and work with `OSPKit` or tried to dive into `html2print` by myself, there are a lot of things to explore. 

I don't know why I never tried to work with these workflows actually. I might have always felt that I would need to be shoulder-to-shoulder with one of the OSP's to do so. Or to understand better how `OSPKit` and `html2print` are different from other ways of working with web-to-print. 

Below you find a short log written in a README style, that captures my installation process of `OSPKit` and my search for a `html2print` boilerplate to work with. :--)

<br>

------------------

## Installing OSPKit

I started from the README in the OSPKit repository: <https://gitlab.constantvzw.org/osp/tools.ospkit>

There are instructions there for install OSPKit on Manjaro and Ubuntu, so this post is adding instructions for Debian systems.

### Download OSPKit

`$ git clone git@gitlab.constantvzw.org:osp/tools.ospkit.git`

### Install QTwebkit 5.212

This is the part that makes OSPKit a timetravel machine! This version of Qt-Webkit and WebKit still supports the CSS Regions.

To install Qt-WebKit 5.212, it's easier to install the version shipped by the apt package manager, because building the OSP patch of Qt-WebKit 5.212 will take hours.

`$ sudo apt search qt5 | grep webkit`

> gambas3-gb-qt5-webkit/oldstable 3.15.2-1 amd64<br>
> libqt5webkit5/oldstable 5.212.0~alpha4-11 amd64<br>
> libqt5webkit5-dev/oldstable 5.212.0~alpha4-11 amd64<br>
> python3-pyqt5.qtwebkit/oldstable 5.15.2+dfsg-3 amd64<br>
> python3-pyqt5.qtwebkit-dbg/oldstable 5.15.2+dfsg-3 amd64<br>
> qutebrowser-qtwebkit/oldstable 2.0.2-2 all

`$ sudo apt install libqt5webkit5`

### Install OSPKit

`$ cd src`

`$ qmake`

> bash: qmake: command not found

Hmm, how to install qmake?

`$ sudo apt search qmake`

> qt5-qmake/oldstable 5.15.2+dfsg-9 amd64<br>
> Qt 5 qmake Makefile generator tool

`$ sudo apt install qt5-qmake`

`$ qmake`

> Info: creating stash file /home/mb/Documents/clusters/onderzoek/OSP/tools.ospkit/src/.qmake.stash<br>
> Project ERROR: Unknown module(s) in QT: core gui printsupport widgets

Hmm... 

Searching for this error brought me to Stack Overflow: <https://stackoverflow.com/questions/68261959/unknown-modules-in-qt-core-gui-quick>

`$ sudo apt-get install qtbase5-dev`

`$ qmake`

No output... I guess that is a good sign. 

`$ make`

> In file included from mainwindow.cpp:2:<br>
ui_mainwindow.h:13:10: fatal error: QtWebKitWidgets/QWebView: No such file or directory<br>
>    13 | #include <QtWebKitWidgets/QWebView>
>       |          ^~~~~~~~~~~~~~~~~~~~~~~~~~
> compilation terminated.
> make: *** [Makefile:390: mainwindow.o] Error 1

Hmm... it seems that the QtWebKitWidgets library is missing.

No idea how to continue, so I asked Alex ;).

We looked again at the qt5-webkit packages in apt, and saw the dev package. 

`sudo apt install libqt5webkit5-dev`

Oke installed...

Now let's go back to the Makefile.

`make`

It worked! :)

It generated an executable file called `OSPKit`.


`├── main.cpp`<br>
`├── main.o`<br>
`├── mainwindow.cpp`<br>
`├── mainwindow.h`<br>
`├── mainwindow.o`<br>
`├── mainwindow.ui`<br>
`├── Makefile`<br>
`├── moc_mainwindow.cpp`<br>
`├── moc_mainwindow.o`<br>
`├── moc_predefs.h`<br>
`├── OSPKit`<br>
`├── OSPKit.pro`<br>
`└── ui_mainwindow.h`

### Use OSPKit

I copied the executable to the bin folder that I have in my home folder, with is on my `$PATH`:
    
`$ cp ./OSPKit ~/bin/`

I also renamed it to all lowercase:

`$ mv OSPKit ospkit`

And now I can use it from anywhere on my system :).

`$ ospkit`

<br>

------------------

## Finding my html2print boilerplate

After install OSPKit, I would like to try to combine it with html2print. 

Some instructions that I received from Alex: 

<div style="color:blue;">

First clone your boilerplate of choice!<br>
<br>
<code>$ git clone https://gitlab.constantvzw.org/osp/tools.html2print.git</code><br>
<br>
<code>$ cd tools.html2print</code><br>
<br>
An article is edited in the <code>content</code> folder.
The <code>content</code> folder takes <code>.html</code> files.
So if you want to work in another way, for example in markdown, you need to add those steps yourself.<br>
<br>
There is a makefile that can be used to generate a <code>.json</code> file for each of the articles.<br>
<br>
<code>$ make</code><br>
<br>
Now it's time to open OSPKit... <br>
<br>
<code>$ ospkit</code><br>
<br>
and to run a local server in this folder...<br>
<br>
<code>$ python3 -m http.server</code><br>
<br>
and to navigate to: <code>localhost:8000</code><br>
</div>

Now, which repo to start from?

There are many different versions of html2print in the OSP repositories. Alex recommends me to first try the `devel` branch of the `tools.html2print` repo.

<https://gitlab.constantvzw.org/osp/tools.html2print/> (devel branch)

I tried it and realised that:

- there are so many branches in this repository!
- stylesheets are missing to make the spread/grid/preview buttons work
- print function does not take the content of the iframe, so a workaround is needed: print the file from the layout/filename.html directly.

Back to Alex, he recommends me to try the `Medor0` repo, which is the Médor flavoured version of `html2print`. The `Medor0` repo is the only one that is public, as the repo's of each issue includes API keys that should not be shared. The `Medor0` repo is made to collect changes in the `html2print` workflow made while working on the different issues of Médor. According to Alex, it's mainly Doriane who takes care of this repo these days.

<https://gitlab.constantvzw.org/medor/Medor0>

Hmm..., I tried to work with it and realised that:

- python2 is used in the Makefile and I don't have this version installed.

Back to Alex again :).

He recommends me to look at one of the latest repo's of Medor and gives me access to it on the OSP gitlab.

<https://gitlab.constantvzw.org/medor/Medor31>

The `Medor31` repo is the one that is used for the last issue.

When running it, I notice:

- python3 is used in the Makefile

That looks good, I will try this one!



