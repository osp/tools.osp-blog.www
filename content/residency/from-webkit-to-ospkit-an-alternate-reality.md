---
Title: From WebKit to OSPKit: an alternate reality to keep working with CSS Regions
Date: 2023-07-14 17:00
Author: Manetta
Tags: shoulder-to-shoulder, css-regions, html2print, web-to-print
Slug: from-webkit-to-ospkit
---

In 2013, in the middle of W3C discussion threads around Paged Media features of the web, browsers engine shifts and partially implemented CSS standards, OSP started to use HTML and CSS to make books and publications. 

While figuring out what impact it would have on their practice to make printed matter with web technologies, OSP listed their *issues to solve*. At the top of this list we find the issue of flowing text on a page. Which is marked as resolved with an *"ok"*, thanks to the presence of a specific CSS property: CSS Regions.

![](https://gitlab.constantvzw.org/osp/work.balsamine.2013-2014/-/raw/68417b61b23a73a9c6807d6265d4385964bad195/lgm-talk/55a.png)

<small class="caption">Slide made for a lightning talk during the 2013 edition of the LGM (Libre Graphics Meeting) in Madrid, found in the Balsamine 2013-2014 repository: <https://gitlab.constantvzw.org/osp/work.balsamine.2013-2014/-/blob/68417b61b23a73a9c6807d6265d4385964bad195/lgm-talk/></small>


The *Mmmmmm* at the right bottom of the slide might already indicate a gut feeling and awareness of the always-changing dynamics of the web. In the same year, in 2013, Chromium announced that they will switch browser engine, from WebKit to Blink, and that they will drop the support of CSS Regions. Since then, it has become increasingly hard to still use CSS Regions on Linux machines. But not impossible.

The story around OSP's work with CSS Regions introduces a particular example of a dependency relation that is entangled with a complex configuration of software timelines, web standards, layout traditions, commissioned work and excitement to explore book making with HTML and CSS.

Why did OSP choose to stay with a never fully implemented CSS standard?
Why are the CSS Regions important for OSP?
Which workarounds are needed to keep working with CSS Regions today?

![{ OSP CSS Regions W3C } -> { OSP -> CSS Regions -> W3C } [label=" ? "]](http://blog.osp.kitchen/images/uploads/css-regions/osp-css-regions-www.svg)

<small class="caption">`{ OSP CSS Regions W3C } -> { OSP -> CSS Regions -> W3C } [label=" ? "]`</small>

<br>

---------

<small>Alex Leray, Amélie Dumont, Gijs de Heij and Doriane Timmermans (OSP) in conversation with Simon Browne and Manetta Berends (Varia) at the OSP studio, in the late afternoon of Wednesday the 7th of September 2022. <br> Initially broadcasted in the context of the Publishing Partyline, a two day event in October 2022. The full conversation can be accessed and listened to here: <https://cc.vvvvvvaria.org/wiki/Standards_and_work_arounds><br></small>

**Manetta**: Can you maybe explain what CSS Regions are and how it works? 

**Doriane**: Yeah. \[laughter\] So CSS Regions is mainly a set of CSS
properties. And the way it works, is that's it separates a bit the
content from the layout, in the sense that you still have all your
`<div>`'s and paragraphs in one content `<div>`, but then you're able to
let all your content flow in another set of `<div>`'s. Which are
basically kind of empty `<div>`'s, because when you inspect them there
is not the real content in them, but the browser renders it as such that
the text is inside of it.

So what it allows you to do is that you have one big flow of content,
and to divide it into seperate content flows, and to place each of these
flows into a different `<div>`. So it's helpful to make magazine layouts
and printed media in general.

<br><br>

![](http://blog.osp.kitchen/images/uploads/css-regions/flow-from-into.svg)

<small>How you work with CSS Regions in HTML and CSS: you copy your content in the `<article>` element, which is flowed into the `<div class="region">`.</small>

<br><br>

**Manetta**: Why was it important to use CSS Regions in your work?

**Alex**: I think the first reason was to do a multi-paged document.
Because if you have a printed booklet, it might be as simple as you
want, like one column of text. But you might have a footer, or you might
have an image on the right page, and then you want to continue the text
on the page after. So at some point it was kind of the solution to that,
within this kind of broken environment of web-to-print at the time. So
it was not so much... because then it's funny to say where the CSS
Regions came in, but was not so much about... It was a little bit like
problem solving for this multi-paged output. That's the way that we
found to do more fragmented layouts, and also to go away a bit from the 
linearity from the web. But it also was at some costs in a way.

**Manetta**: We'll get to the costs in a bit. \[laughter\]

Because in 2013 the CSS Regions functionality was removed from the browser you are using in your practice, which is Chromium, a version of Chrome that is part of the Linux operating system that you are running on your computers.

It would be great to dive into this moment together and speak about what happened and why. 
This is going to be a bit of a technical part of the story, to which you are much more closer to, so please feel free to interrupt...

**Simon**: Yes please correct our research. \[laughter\]

**Manetta**: So in 2013 Google made a big change to Chrome and Chromium: they switched to a different browser engine. Google forked Apple's browser engine WebKit and started Blink, a new browser engine. And as part of this change, they also decided to remove the support for CSS Regions from Blink. 

Maybe we should start with explaining what a browser engine is, before we continue? Because that is quite important.

**Gijs**: So a browser engine is a piece of software that translates the
code of a web page into pixels, or the code into an image. So it
combines the content together with the rules for the layout and the
constraints that are there, for example the width of your browser
window, and then calculates or determines what the layout should be.

Maybe a clear example is to think about an image with a width of 50% and a text flowing next to it.
If your screen is very wide, the image will become bigger, but also more text fits next to it. 
So that needs to be calculated. And if your screen is smaller, then the image is
smaller as well and the text has to flow differently. 

So that's what this engine does. It takes the instructions or the limitations set in CSS and
combines it with the content that it finds in the HTML, and it
determines what is looks like on your screen.

<br><br>

![diagram to demonstrate what a browser engine does](http://blog.osp.kitchen/images/uploads/css-regions/browser-engine-diagram.svg)

<small>A browser engine renders the HTML + CSS into a web page, taking the size and resolution of your screen into account.</small>

<br><br>

**Manetta**: And you could work with CSS Regions because they were implemented in the WebKit browser engine, right? Can you say a bit more about WebKit? What made you aware that you were reyling on this particular browser engine?

**Gijs**: Well WebKit is a fork of KHTML. Apple introduced its own
browser as a competition with I think Firefox. And at that moment also
Internet Explorer was also still working on Mac. So Apple took an existing
open source project, KHTML, and brought other engineers into the project and turned it into WebKit eventually. 
So they took over the project in a way. And because WebKit was an open source project, 
it was also implemented in other browsers that you could use on Linux.

**Manetta**: What happened when Chrome switched from using WebKit to Blink?

**Gijs**: I don't know exactly, but...

**Alex**: First Chrome was running on Blink...

**Gijs**: No WebKit.

**Alex**: On WebKit, sorry. And they were sharing the same web rendering...

**Gijs**: Engine.

**Alex**: ...engine –thank you– with Safari basically. And Chrome took
some shares on the market. At some point they decided that they wanted
to continue the development of the browser, they probably disagreed with
something, I don't know the story, but I think there was some kind of
disagreement.

**Gijs**: I think, in my mind, CSS Regions was the reason for the split.
In the sense that there were blog posts about the enormity of... Let's
say, there were a lot of lines of code that were there specifically to
support CSS Regions. And the developers wanted to decrease the size of Blink.

And also, which is something else, CSS Regions has been proposed as a standard by Adobe. 
It very closely imitates the idea that Adobe has about layout, where you draw boxes on a page and there's content
flowing into the boxes. Very much like how their tool InDesign works. 
And there's also kind of a clear relationship between Adobe and Apple. As
in, I think at that moment, the most important reason for people to use
Apple computers was because Adobe software was running on it. So I also think 
that that heavily influenced Adobe's proposal and their interest in the 
WebKit project. 

And Google wanted to remove CSS Regions, or at least that is my understanding of the story. 
They wanted to remove the CSS Regions functionality, because it would make the browser faster.

**Manetta**: Yes that is what we also read. That CSS Regions occupied
10.000 lines of code, which could be removed from the browser engine
basically, which was written in 350.0000 lines of C++ code in total.

**Manetta**: Did you heavily rely on Chrome in your practice actually?

**Alex**: I think when we discovered CSS Regions, I think we used
Chromium. Which is an open source... it is a version of the Chrome browser on Linux. 
But we used it only for a very brief time, if I remember it correctly, because right after Chrome and thus also Chromium
decided to remove the CSS Regions functionality.

**Gijs**: Safari does not run on Linux. So at that moment Chromium was the
biggest browser on the Linux platform that used the WebKit rendering engine.

**Manetta**: Just to clarify, you all the using Linux in your practice? That is an important detail.

**Together**: Yes. 

**Manetta**: So the browser you were using to produce your work in, stopped supporting the CSS Regions.

**Alex**: Exactly.

**Manetta**: Which meant that the way in which you were producing layouts with HTML and CSS was not working anymore, thanks to switch of Chrome from WebKit to Blink in 2013. That must have been quite scary. How did you respond to it?

**Alex**: I think we, we tried..., I mean... we started a bit panicking
I think. Not because we liked so much this CSS Regions functionality,
because like I said, it was our only way at the time, or the only way
how you could think about multi-page layout in the web browser. And we
were not so much enthusiastic to come back to the former tools, such as Scribus. 
We liked working with the web so much that we wanted to continue like that, even though we had some
reservations about CSS Regions itself.

<br><br>

![Chrome switching its browser engine in 2013 caused panick.](http://blog.osp.kitchen/images/uploads/css-regions/browser-engines-simple.svg)

<small>Chrome switching its browser engine in 2013 caused a bit of panick, as other WebKit-based browsers did not fulfill the needs of OSP, or could not be used on a Linux machine which was the case for Safari.</small>

<br><br>

**Alex**: What we tried was to use a polyfill, that was developed by a student at
Adobe, actually, to circumvent or to re-implement in a way this idea of
CSS Regions. 

What we found was that it was very nice to have this code
around, but it was also very difficult to work with the Javascript
implementations of it. Because first of all, it was written in Javascript which is not a low level programming
language and it made it very very slow when working on large documents. And second, it
was breaking some nice CSS features, like selectors, which you use for
instance if you want to select the first paragraph of your document.
And when using the polyfill, it will suddenly select the first paragraph of every
page, because the content is literally broken into chunks.

**Manetta**: Can you say maybe more about this notion of a "polyfill"?

**Alex**: I think the name comes from polyfilla. The thing you put in
the wall, no?

**Simon**: Oh like when you get a crack in the wall? Polyfill, yes, it's
a brand. Yes it's a brand for fixing cracks in the wall.

**Alex**: So it's exactly that, this idea to fix cracks in the wall.

**Simon**: Never thought about that.

**Alex**: Yes the idea is that, correct me if I'm wrong but, so
like... you write your code as if you were using natively the
functionality, but in the background there is some Javascript or a set
of assets, that kind of turn it into a compatible mode for you.

<br><br>

![Chrome stopped supporting CSS Regions, but with the use of a polyfill made by a student at Adobe, the CSS Regions could be used again.](http://blog.osp.kitchen/images/uploads/css-regions/polyfill.svg)

<small>Chrome stopped supporting CSS Regions, but with the use of a polyfill made by a student at Adobe, the CSS Regions could be used again.</small>

<br><br>

**Manetta**: And this brought the CSS Regions back?

**Alex**: Briefly, but then, like I said, there was this speed issue. It
was really a mess to layout the magazine we were working on, Médor, with this polyfill. 
It was really really slow. It was kind of breaking this interactivity we had with the
browser.

**Doriane**: And also, there is an important difference with the
polyfill. It tries to replace the way how CSS Regions work, but in doing
so it totally changes the way that CSS Regions are working. Because CSS
Regions is this kind of illusion, that is rendering content like it was
inside the document. And the polyfill is actually breaking up the
content and actually putting it into the `<div>`. So there is this
confusion where you can see how CSS Regions was removed, because it was
confusing how you could target a specific element. Because for example,
if you wanted to target the first element of the column, there is no
first element of this column, because it is just rendered as an illusion
through the property of the CSS Regions.

But also, if you use the polyfill, then you can actually do this,
because the paragraph becomes the first element of the column. But you
cannot do the old approach, which is the native approach of CSS Regions,
which is for example able to select the 5th paragraph of my text. 

I think this is an interesting approach. This is also one of the expressed
arguments why CSS Regions was removed. But at the same time, in Médor
when we started to use the polyfill, the polyfill was not right,
because we were used to the reason why it was removed. 

\[laughter\]

**Manetta**: Did you work with the polyfill for a while, or what happened?

**Alex**: In my case for a couple of weeks. And then I gave up and we
tried to look for other WebKit engines, because actually there were
some. I remember using another browser for a while: Epiphany.

**Manetta**: Which also uses WebKit?

**Alex**: Yes at least at that time it was using WebKit.
And there were some others. 
But the problem was that the projects were not so active.
And sometimes they lack very much on the trunk of the WebKit engine.

**Gijs**: Yes so there's the difference between the browser and the
engine, the browser being the interface and the engine translating the
instructions. Just to explain what you said about the trunk and lagging
behind. 

So what it means to lag behind, is that you work with an old version of the
engine. Meanwhile time goes on and new exciting CSS properties emerge, that you cannot
use, because the engine is too old, in the sense that it is not updated. 
So when an engine is lagging behind for a year, you can bump into unexpected surprises, 
which force you to think why some specific CSS properties are suddenly not working.

<br><br>

![From WebKit to OSPKit.](http://blog.osp.kitchen/images/uploads/css-regions/from-webkit-to-ospkit.svg)

<small>From WebKit to OSPKit!</small>

<br><br>

**Manetta**: In the end you forked a browser engine yourself, right?

**Alex**: Not a browser engine, but... So actually when we did this
review of all the browsers using the WebKit engine, at some
point we found one, but it was not a browser. It was a wrapper
around the WebKit engine, that allowed you to insert a kind of widget
into your program, with a web view. 

The project we found is called [Qt-WebKit](https://wiki.qt.io/Qt_WebKit). And at
some point we got enthusiastic about it and started to make a "web browser" –I'm
using quotes because it's a very very minimal one. It is basically a software 
that has a big web view and a small URL bar. And you click OK and then you can
see the page inside the web view. And that is what we called [OSPKit](http://osp.kitchen/tools/ospkit/), which is part of our html2print workflows.

**Manetta**: And because OSPKit is based on WebKit, it brought the CSS Regions back? 

**Alex**: Yes. And the developer of Qt-WebKit was still trying to keep the thing
updated. And it also was someone who we could reach on IRC and discuss
with. I remember once I asked him if there was a specific property
available in the browser, and he said no. And 3 minutes later he
implemented it for me. So it was a very nice experience, to be so close
to the developer and his project.

**Manetta**: And why was it important to keep working with CSS Regions? 

**Gijs**: So we had developed more and more projects around using CSS Regions, 
or that were depending on CSS Regions.

**Manetta**: One of the recurrent projects in which you worked with CSS Regions was Médor, right? 

**Amélie**: Yes so Médor is a Belgian magazine, that is about... I'm
not sure how to say it in English. It's journalism and a news magazine,
doing deep investigation. There is an issue every three months and it
has been laid out with html2print since the beginning.

**Manetta**: So it was an important project for which you needed OSPKit?

**Alex**: Yes. I think the first issue was in 2015, so it was really at
the time when we were very active about building our toolset. 
The Médor project both benefited from our research and also was a drive to conduct
more research. And because it was ambitious, not in the sense of
aesthetics or whatever –it was that as well I hope– but it was
ambitious in the sense that the magazine was broadly distributed and reaching a lot of people. 
So there was a lot of pressure to make sure that we have a correct PDF at the printer in
time. Because in journalism the release is a very important milestone
that you cannot really miss.

**Manetta**: Do you want to say more about that question why it was then
important to develop OSPKit?

**Gijs**: If we hadn't done that it wouldn't have been possible to
continue working with our workflow. It would have fallen apart and we would
have had to rethink completely how we would make the layout for Médor. 
The layout of Médor is very much based on a grid, using all the boxes and all the space that
is available on the page. And without CSS Regions it would not have been possible to produce such layout at that
moment. We would have only been able to work with a single flow. You can maybe float elements to the
left and right, but that is it. State of the art at that moment were multi-column layouts, and this was often not
supported in html2print. Which means that you're left with a very impoverished experience.

And there's also something about... it being possible. Like you're also
maybe clinging on to the possibilities of the moment. In the sense that... I think
it's important to mention that there is this promise of open source,
that you are able to influence or control your tool. But here it became
very clear that a browser engine is such a complex piece of software, and so
many people are working on it, and if those people decide to take a
different direction, that they don't care about the things that you care
about, for whatever reason. This might feel very foreign or might
also feel wrong. But it sort of leaves you in the dark. You're there, and
the browser caravan is carrying on, following their own path. And you try everything you can to keep
on working with it, as long as you can. Also from the hope that, you know... 
that in WebKit, the CSS Regions remain supported.

**Manetta**: So did the maintainance work of OSPKit become part of your practice? Next to producing the layout for the magazine, or other projects that you were working on, you also needed to maintain a browser.

I'm curious to understand the impact of such workarounds on a design practice like yours. 
Because in the end OSPKit is a workaround, no? A work around the main direction of the development of the web. 
A work around the decisions that the makers of browsers make. 

What happens when you introduce such workarounds into a design practice? Because it is quite something. Can we unpack that?

**Doriane**: Yes, maybe. One of the things is that it creates a bit of
an alternate reality. Because you're suddenly living in your own browser.
The path is split in two. And the current status of web-to-print goes
further and new things are happening there. But in the world of this
OSPKit browser, things are a bit stuck in time. And okey you have
this work around that allows you to use a magic property that you want
to keep close to yourself. But then you have to live in your own
reality, that is a bit outside of the evolution and the tendency of the
rest of the designer practice in web-to-print specifically.

**Alex**: Yes exactly... Because now OSPKit is kind of fixed in time, and
it's already static since 2016 or something. It's getting very old, especially
in this world. 

\[laughter\] 

<br><br>

![The versions of HTML, CSS and Javascript that can be used in OSPKit are stuck in 2016.](http://blog.osp.kitchen/images/uploads/css-regions/stuck-in-2016.svg)

<small>The versions of HTML, CSS and Javascript that can be used in OSPKit are stuck in 2016.</small>

<br><br>

**Alex**: It was a way to keep a feature feature alive, a very nice feature, 
or at least a work around that allowed us to stay with our practice. 
But at the same time it's also, like you said, it is cutting us
from new practices that could arise, with new web CSS properties and
developments of the web. So yes, it's a bit, I don't know how to say it,
but it's doing good and bad at the same time.

**Amélie**: Just a few hours before the interview we were chatting
and Gijs used the word *technological archeology*, and I think it fits
to the way I feel as I'm coming back on Médor and I didn't especially
follow the development of html2print. Yes that's it. I'm using that
tool, that's using old technologies, and we cannot use more recent CSS
properties. And so yes, we have to work in other ways and find other
ways of doing.

Sometimes I'm trying to do something, and then I realise, oh I cannot use the
recent CSS, so let's try to find another way to do it otherwise. It's
another mindset.

**Doriane**: Yeah and it's a weird feeling. Like when you're used to
moments when you think, oh I don't know how to do this thing, then
you're looking at the docs online, and then you're doing it. And of
course it's working, because you copy paste the example from the doc. But
then you cannot just look at the doc, you need to test everything and if
something is not working you're not sure what exactly is working and what not.

I remember that especially when working with Javascript, realising that
yes, we're stuck with using a version of Javascript of 2016, which
has evolved a lot since. And it's also different to work with HTML and
CSS from 2016. 

For example, when you want to make a border around a
font, and the border does not show, you know that this CSS property was 
not supported in 2016. But if you're writing Javascript it becomes super hard to
debug, because you have no idea which line is supported and which one not.




