---
Title: 8 years of Médor, CSS Regions and OSPKit
Date: 2023-07-20 11:00
Author: Manetta
Tags: shoulder-to-shoulder, css-regions
Slug: 8-years-of-medor-css-regions-and-ospkit
Status: draft
---

Since 2015, OSP has been working on [Médor](https://medor.coop/), a quarterly magazine for Belgian investigative journalism. Some of the OSP members are actually co-founders and current board members of the magazine.

A couple of weeks ago I visited the Médor office during one of the production weeks of the magazine, to get an impression of how the magazine is made. 

![](/images/uploads/medor-visit-2.jpg)

<small class="caption">A picture of the design team working on Médor n°31, May 2023.</small>

A few words about the magazine itself first. The magazine started as a printed magazine, but later also became available in an online form. Médor works with a membership model, the layout is made with free software, illustrations are often released under open licenses and there is a rotating editorial team. Médor currently has 2872 members and 1766 associates, and if you are curious to know more you can read [this nice page on their website with a description of how Médor started](https://medor.coop/medor-cest-quoi-cest-qui/notre-histoire/).

It has been 8 years now since Médor started and 4 issues are made every year. The layout of each issue is made by a group of 3 or 4 designers in the timeframe of 3 weeks, in which they closely work together with the editors, journalists and illustrators. The group of designers who works on Médor rotates, every issue is made by a different constallation of people, of which some are members of OSP and others not. 

![](https://gitlab.constantvzw.org/osp/work.balsamine.2013-2014/-/raw/archive/lgm-talk/30a.png)

<small class="caption">A list of tools that OSP was working with around 2013 *and other magicks*.</small>

At the time that Médor started, OSP worked with a range of tools: Scribus, Inkscape, ConTeXt, Gimp, Graphviz, FontForge, Libre Office, amongst other scripts and *magicks*. Alongside, they also started to work with html2print. The ongoing work for the local Balsamine theater was one of the first commissioned projects in which html2print was explored, which continued during the making of Médor: the magazine is made with html2print from the very beginning. But! Not only. Some of the pages in the magazine are made in Scribus and Inkscape, which was an interesting discovery for me.

It turned out that the final PDF of a Médor issue is not rendered from one document. Instead, a workflow is set up that makes it possible to produce the final PDF in chunks, and each chunk is produced by a different person and also with different tools. Most of the article pages are made with html2print for example, but a special page like the announcement page (see above) explaining how someone can become a member of Médor, is made with Scribus.

The html2print setup that OSP uses to make Médor heavily relies on CSS Regions for text fragmentation and predictable image placement. However, CSS Regions were at the time not available anymore in any of the *updated* browsers. After trying multiple polyfills and browser alternatives, it eventually triggered the production of OSPKit: a light weight very minimal browser based on Qt-WebKit 5.212 with CSS Regions support.

I am curious to dive into the editorial workflow of this project and to hear more about the role of the CSS regions and how they are used in relation to the workflow as a whole.

How does the Médor workflow break away from linear editorial workflows? How is the html2print workflow connected to the Scribus and Inkscape workflows? What made it possible to work with multiple tools on one magazine?

---------

<small>Alex Leray & Manetta Berends in conversation at the OSP studio, in the morning of Friday the 26th of May 2023.<br></small>

There is this schematic that I just showed you, that Ludi and Louis made for this Toulouse presentation. 

![](/images/uploads/medor-editorial-workflow/500-schema-outils.png)

<small class="caption">A diagram of the Médor workflow, made by Ludi and Louis.</small>

There is the cloud first, it's basically nextcloud, but I remember that there were a lot of questions at the beginning. Like wheter we should make our own writing and ask everyone to work in that tool, whether it's a pad or a CMS or whatever. And there were already issues with that, because Médor is not only the team of Médor but also external journalists that they work with, or the proof reader. If we take for instance the proof reader, he has its own tools and methodologies and for instance a plugin for Microsoft Word, to check spelling errors and micro typography and stuff. And tracking changes is used a lot too.

And also because they often collaborate with journalists, some are recurring and come back, but some just work with them for one article. So it was really too much to ask everyone to work with the same set of tools. And also it is very intimate to work with specific tools. 

So in the end that was also why we work with a cloud for file sharing. The cloud is a nextcloud installation that we have. We have this repository of files online, where everyone can drop all the kind of documents that they want, wheter it is odt, docx, jpg, tiff, whatever is good for the articles. 

Basically the journalists and proof reader all work with the odt or docx documents. And once it has gone through the proof reading and the text content is validated, only then we switch to the CMS where we encode the articles. 

**What do you mean with "encoding"?**

Encoding is copy-pasting the content into the CMS. The docx or odt's are mainly text and a bit of structure, but we have some additional structures in the CMS that can be used. For example, if you want to encode a portfolio, you can use a gallery block. And then it also can be translated into the web version of the article, so it is displayed as a gallery.

And once we went through the whole process of writing the articles and proof reading, we create an article page in the CMS, inside of an issue page. 

**Do you do this, or do the journalists make a new page in the CMS?**

We do it. 

**So the journalists only upload their odt or docx documents to the nextcloud?**

Yes, and then they come back for minor edits and stuff.

But yes when we do this, the article is supposed to be finished, but that's not always the case of course.

**Are there other tools used to organise the collaboration between the designers, the editors and the journalists?**

We always needed to make a flatplan of the magazine, to see if there was no conflict between let's say two articles. Or just to see how we want to spread the content. To, for example, not have too much depressing investigations around corruption at the same place in the magazine. The journalists came up with a simple solution that we still use actually, which is a simple spreadsheet with 128 cells to dispatch the pages of the magazine. 

![The spreadsheet that is used for the flatplan.](/images/uploads/medor-editorial-workflow/medor-editorial-workflow-24.png)

<small class="caption">The spreadsheet that is used for the Médor flatplan.</small>

The problem with this spreadsheet based flatplan is that it is not so practical, because let's say you have an eight paged article that is spread between booklet one and two... It's not really practical to move things from one place to the other. So you have to use extra cells on the side of the spreadsheet as extra space to park an article, and so one. 

At some point we came up with this idea to make a tool to fetch from the website the list of articles in order, to generate some kind of automatic flatplan in HTML, to be able to move things around and push the new order back to the website. And this was working for a while, but then we switched CMS and it did not work anymore. 

**And now you're back to the spreadsheet?**

Yes back to the spreadsheet.

**Is such a flatplan an important tool for the overall workflow? Is it important for you for example to know on which page an article starts?**

It's important in the sense that we specify the pagination by hand. And we have to know if an article starts on the left of right page.

And potentially, to have an overview to see how the articles are spread, in terms of rhythm and stuff.

And also, it can be nice sometimes to know where the center folds are, for example when you have images across two pages.

We used to have as well a different paper on the second booklet. But in the end it was too complicated to manage this.

**Coming back to the CMS, which software is used?**
 
The first website was plain Django. For which we made a few modules. So for instance an article module, or an person module. So you can put the article in the article module and connect it to persons, and so one. We remade the website a few years later, I think it was 4 years or something. We switched to Wagtail CMS. 

![The Wagtail CMS used to collect all the articles and other materials, which feeds both <https://medor.coop> and the printed magazine.](/images/uploads/medor-editorial-workflow/medor-editorial-workflow-03.png)

<small class="caption">The Wagtail CMS used to collect all the articles and other materials, which feeds both <https://medor.coop> and the printed magazine.</small>

The old website needed to be ugraded, and we started to look for Django developers. And in the end it's always the same story, when you ask someone else to work on the website, usually their answer is not to take what is there, but to restart from scratch. And so yes, this person advised to use Wagtail. The CMS was pretty much following the structure that we were already using on the old website. The main structure is the article. 

I think the main feature is the text editor of Wagtail, which allows you to construct an article with blocks. And each block can have a different kind of type. So if you have a piece of text you can just use the rich text block. If you want an image, an image block and so on. But you can also make your own blocks, like the gallery block, for instance. And also, at some point there was the need for an article to make a kind of simulation of a messenger conversation so we made a special block for that. 

![Wagtail's block feature.](/images/uploads/medor-editorial-workflow/medor-editorial-workflow-06.png)

<small class="caption">Wagtail's block feature.</small>

**How do you connect the articles with OSPKit?**

There is an API. In the former website it used to be a simple HTML output. We were just taking the article from the CMS and it almost had not structure, just a few classes. 

And now it's a json API. You can see it raw there. 

![The Wagtail CMS can be accessed through its API and content can be rendered in JSON.](/images/uploads/medor-editorial-workflow/medor-editorial-workflow-07.png)

<small class="caption">The Wagtail CMS can be accessed through its API and content can be rendered in JSON.</small>

**Did you prefer the HTML output?**

Yes the HTML output was very nice, it was much more simpler. 

**It was one step less?**

Yes. I know that Doriane reminded me that the HTML output is way simpler, and she suggested to go back to that.

It was a choice to switch to JSON, because usually all the articles follow the same structure, but sometimes we have a special need for the layout. A simple example is the use of subhead. We have a specific CSS class for that. But sometimes you want this subhead to be... eh... wait, what is the contrary of subhead? When something is below a header, but you want it to be above? Above-head?

When you want to change the order, it is an issue with HTML output, because we had to go through Javascript to reorder things. We could also have made two divs and two different CSS flows, but it's extra work, when you just want something to be in a different order.

**So it is about the seperation between content and form?**

Exactly.

**Because in the HTML output you could not change the content.**

Yes you would have to manipulate it with Javascript basically.

But in the case of the JSON API, since we get the "raw" data, I thought that we could have the freedom to change the template.

**So to have access to both content and form again?**

Yes.

The set of tools that we made to connect to this API, retrieve the JSON content and turns it into a HTML chunk through templating. This is done in Javascript with Vue.js. 

So we have a setting.js file. And it contains a token to connect to the API. And if we look at one of the articles that is in the layout directory, let's say the *Edito*. So what happens is that at the end of the file you have a kind of HTML tag, it's a custom one: \<art\>.

![The <art> component that is used by Vue.js to insert the HTML of this specific article.](/images/uploads/medor-editorial-workflow/medor-editorial-workflow-09.png)

<small class="caption">The `<art>` component that is used by Vue.js to insert the HTML of this specific article.</small>

It's a kind of tag, it's more a directive actually. Vue.js is looking for this directive and then looks at the src attribute that points to the API. And then with this directive there is some Javascript that fetches the URL of this article, retreive the JSON and turns the JSON content into an HTML fragment with some templating, to just put the title as a h2 and so on. And then it replaces the <art> directive with the HTML. I think Vue.js calls it a component. So it replaces the component with the HTML fragment.

So in the end it's the same as just writing our content in the HTML document, except that we don't have to and we can retreive it from somewhere else.

**So you can for each article generate different layouts?**

Yes. 

**So the content is structured in one way but it does not mean that the layout is always following the same structure? Which creates more possibilities?**

Yes, so at the moment the content is usually quite simple. It's made out of a title, paragraph, a couple of images and quotes. It's almost the same for all the articles. And we change the appearance with CSS classes.

But we can potentially do different kind of HTML formatting according to the kind of content. 

**Do you make a custom template for each article?**

It's very much a division between content and form actually. The HTML is usually pretty much the same. And then we apply different kind of styling depending of the type of article. If it is an editorial, the body gets this style. If it is an investigation it gets this style. 

And we have a base stylesheet and another one. Basically we have two styles: the global styles who are applied by default and the article styles, which allows us to override the global styles.

**So in the end, the switch to the JSON API, did not bring a different way of working. It is only used for exceptions?**

Yes. 

**So structure is kept the same as much as possible, unless you really need to change something?**

Yes. 

**Is it more difficult for you to work with html2print when there is a strict seperation between content and form?**

Yes but since we have the CSS Regions we can still move things around. 

Let's say we have a sidenote in an article, this sidenote does not have to be encoded at this specific place in the CMS. 

**Do CSS Regions bring back, in a way, the ability to work with both content and form at the same time?**

Yes it allows to move things around and change the structure that is hardcoded and very linear in HTML. Sometimes it is more practical that a text flows in order, but this order does not be the final order in the layout. 

**Do you have an example in which we can see the CSS Regions at work?**

For this article [on the screenshot below] there was a mix of images that should appear inside the body text, in the columns, and others were placed on specific positions. 

This image for example is placed at a specific position, at the bottom of the page. 

![Image positioned at a predefined place: at the bottom of the page.](/images/uploads/medor-editorial-workflow/medor-editorial-workflow-12.png)

<small class="caption">Image positioned at a predefined place: at the bottom of the page.</small>

And if you go to "computed" you have a feature to see from which region it is flowing. So if I click there, it will bring me to the div element that contains the flow. Not from where it originates but to where it flows. 

![Text flowing into different div elements, using CSS Regions.](/images/uploads/medor-editorial-workflow/medor-editorial-workflow-13.png)

<small class="caption">Text flowing into different `<div>` elements, using CSS Regions.</small>

**Ah nice you see the text flowing from one page to the other here, nice!**

Maybe in another article. 

![Within the html2print interface you can switch between different articles.](/images/uploads/medor-editorial-workflow/medor-editorial-workflow-14.png)

<small class="caption">Within the html2print interface you can switch between different articles.</small>

**Ah you can see the different articles in html2print? That is really useful.**

Yes in the script... "make json" probably, yes, you see all the pages inside the layout document, and it will generate all these pages.

![Output of "make json" in the terminal.](/images/uploads/medor-editorial-workflow/medor-editorial-workflow-15.png)

<small class="caption">Output of "make json" in the terminal.</small>

The list of article names is made manually. We actually don't use the CMS API to retreive information about an issue. This was done in the old website, but not anymore. And you see that this list is just in alphabetical order, not in the order of the magazine.

**So later you reshuffle the articles in the right order?**

Yes. 

And the version of html2print that we use for Médor is actually a page with a big iframe in which we load the different layouts. And at the bottom there is a small toolbar that allows to change zoom, change articles and a couple of other things. 

![The Médor flavoured version of html2print.](/images/uploads/medor-editorial-workflow/medor-editorial-workflow-10.png)

<small class="caption">The Médor flavoured version of html2print.</small>

**And this is an html2print version made for Médor?**

Yes, adjusted to Médor. But for me there is no official version of html2print, so yes. It's one of the many versions that is around. 

I can't remember who made this version. Probably there was already the preview option. This was probably a CSS class that we used to write by hand in our HTML, and then we thought oke, we can turn this into a button. 

(...)

Something else, and you're going to like it...

![The table of contents is the only place where CSS grid is used.](/images/uploads/medor-editorial-workflow/medor-editorial-workflow-23.png)

<small class="caption">The table of contents is the only place where CSS grid is used.</small>

The table of content is the only place where we use CSS grid. I think CSS grid is really nice. But we had a nasty little issue. For some reason it cut the descendant of the text in italics on the left side of the text box. Is it clear? Let me draw you the situation...

<!--

![Scan of the drawing that Alex made to illustrate how letters are cut off in CSS grid when they fall outside of the text box.](/images/uploads/medor-editorial-workflow/medor-editorial-workflow-XXX.png)

<small class="caption">Scan of the drawing that Alex made to illustrate how letters are cut off in CSS grid when they fall outside of the text box.</small>

-->

Let's say we have a big piece of text, like a big J, I don't know how to draw a J, let's say it looks like this. And the J is in italic. And for some reason with CSS grid it would just cut the J on the left. It was quite disturbing, it was not overflowing. 

We didn't manage to solve this thing, so we kept our own CSS grid in which the elements are actually absolute positioned, and we have different classes to position them in different steps in the grid, in our grid.

**The grid was too rigid.**

Yes. 

And because we're stuck with an old version of WebKit and CSS, it is this kind of bug that is not really solvable.

**What is the difference between CSS Regions and CSS grid? Both of them can be used to flow content into a template, right?**

CSS Grid positions already fragmented content. When something overflows, it's hidden, it does not flow into another div. It does not fragment content, it only positions it.

**What are the limits of CSS Regions? When do you switch to another tool, like Scribus?**

Usually when we switch it is about color. What happens sometimes is that we have illustrations with spot colors, or not even spot colors, but with precise CMYK values.

Here we wanted to have pure cyan, but i think we had issues with this one.
There are some other examples when there were drawings that need to be printed in pure cyan. 
And with the color conversion we cannot be precise enough.
Because the color in html2print is described in RGB. 

Pure black is oke, it stays the same after the conversion.

But for the other colors, it's hard to be sure to know what is going to happen. 

On this page it is silly, we did the layout in ospkit. And then we imported each page of the pdf in Scribus, select the element that needed to be printed in silver and replace the spot color. This is possible because these are vectors. And you cannot set a spot color in html2print. 

There was also a spot color in the bitmaps. And scribus support the possibility to replace colors in bitmaps. When you describe the colorname in Photoshop and Scribus the same, the connection will be made by the printer. You can make an extra channel in Photoshop I think, and as long as the names are the same, it will work.

**It's interesting that the workflow enables you to work with different tools. Was it a concious choice to do it in this way?**

At first there was this disappointment with... or let's turn it positively... 
there was this excitement with using HTML. 

But i think the mix of software also came back because we could see that each software has certain weak points and strenghts, or features. 
I think for me something that is very important in OSP is that we embrace the diversity of tools. We have never been so excited about replacing X with Y. Replacing illustrator with Inkscape for example. There was the excitement to use different tools for different kind of things. Graphviz is for example a good example.

And for layout, i still like the idea that you can use different tools.







