import shlex
import subprocess

from pelican import signals, contents
from pelican.utils import get_date

from bs4 import BeautifulSoup

import random



def process_css(pelican):
    path = str(pelican.settings['OUTPUT_PATH'] + '/theme/css/' + pelican.settings['CSS_FILE'])
    cmd = "./node_modules/.bin/postcss --map --use postcss-cssnext --output {} {}".format(path, path)

    call_params = shlex.split(cmd)
    subprocess.call(call_params)

def register():
    signals.finalized.connect(process_css)
